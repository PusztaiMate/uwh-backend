#!/usr/bin/env sh

echo "Starting server"
gunicorn --bind 0.0.0.0:5000 manage:app --daemon

echo "PORT is $PORT"

sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/conf.d/default.conf && \
nginx -g 'daemon off;'
