-i https://pypi.org/simple
black==23.1a1
boto3==1.29.6
casbin==1.33.0
casbin-sqlalchemy-adapter==0.5.2
Flask==2.2.2
Flask-Cors==3.0.10
Flask-Migrate==4.0.3
Flask-SQLAlchemy==3.0.3
gunicorn==20.1.0
pillow==10.3.0
psycopg2-binary==2.9.5
pyjwt==2.6.0
python-dateutil==2.8.2
pytest==7.2.1
pywebpush==1.14.0
Werkzeug==2.3.7
