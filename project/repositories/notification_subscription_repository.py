from abc import ABC, abstractmethod

from requests import delete

from project.api.models.notification import NotificationSubscription


class NotificationSubscriptionRepository(ABC):
    @abstractmethod
    def create(self, sub: NotificationSubscription) -> NotificationSubscription:
        pass

    @abstractmethod
    def get(self, id: int) -> NotificationSubscription:
        pass

    @abstractmethod
    def get_all(self) -> list[NotificationSubscription]:
        pass

    @abstractmethod
    def delete(self, id: int) -> None:
        pass

    @abstractmethod
    def delete_by_endpoint(self, endpoint: str) -> None:
        pass


class NotificationSubscriptionDb(NotificationSubscriptionRepository):
    def __init__(self, db):
        self.db = db

    def create(self, sub: NotificationSubscription) -> NotificationSubscription:
        self.db.session.add(sub)
        self.db.session.commit()
        return self.get(sub.id)

    def get(self, id: int) -> NotificationSubscription:
        return NotificationSubscription.query.get(id)

    def get_all(self) -> list[NotificationSubscription]:
        return NotificationSubscription.query.all()

    def delete(self, id: int) -> None:
        sub = self.get(id)
        self.db.session.delete(sub)
        self.db.session.commit()

    def delete_by_endpoint(self, endpoint: str) -> None:
        sub = NotificationSubscription.query.filter_by(endpoint=endpoint).first()
        self.db.session.delete(sub)
        self.db.session.commit()
