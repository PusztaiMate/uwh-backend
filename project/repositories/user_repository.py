from abc import ABC, abstractmethod
from flask_sqlalchemy import SQLAlchemy

from project.api.models.user import User


class UserRepository(ABC):
    @abstractmethod
    def create_user(self, username: str, password: str, email: str = "") -> User:
        pass

    @abstractmethod
    def get_user_by_id(self, id: int) -> User:
        pass


class UserDB(UserRepository):
    def __init__(self, db: SQLAlchemy):
        self.db = db

    def get_all(self) -> list[User]:
        return User.query.all()

    def create_user(self, username: str, password: str, email: str = "") -> User:
        user = User(username=username, password=password, email=email)
        self.db.session.add(user)
        self.db.session.commit()
        return self.get_user_by_id(user.id)

    def get_user_by_id(self, id: int) -> User:
        return User.query.get(id)
