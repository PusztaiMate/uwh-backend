class ResourceNotFound(RuntimeError):
    pass


class ResourceAlreadyExists(RuntimeError):
    pass
