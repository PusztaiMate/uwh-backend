from flask_sqlalchemy import SQLAlchemy
from project.api.models.image import Image


class ImageRepository:
    def __init__(self, db: SQLAlchemy):
        self.db = db

    def find_by_uuid(self, uuid: str) -> Image:
        return Image.query.filter_by(uuid=uuid).first()

    def create(self, image: Image) -> Image:
        self.db.session.add(image)
        self.db.session.commit()
        return self.find_by_uuid(image.uuid)
