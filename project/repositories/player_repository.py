from abc import ABC, abstractmethod
from typing import List, Optional
from flask_sqlalchemy import SQLAlchemy

from project.api.models.player import Player
from project.repositories.exceptions import ResourceNotFound


class PlayerRepository(ABC):
    @abstractmethod
    def get_all(self) -> List[Player]:
        pass

    @abstractmethod
    def save(self, player: Player) -> Player:
        pass

    @abstractmethod
    def delete(self, id: int) -> None:
        pass

    @abstractmethod
    def find_by_id(self, id: int) -> Optional[Player]:
        pass


class PlayerDB(PlayerRepository):
    def __init__(self, db: SQLAlchemy):
        self.db = db

    def get_all(self) -> List[Player]:
        return Player.query.all()

    def save(self, player: Player) -> Player:
        self.db.session.add(player)
        self.db.session.commit()
        return self.find_by_id(player.id)

    def find_by_id(self, id: int) -> Optional[Player]:
        return Player.query.get(id)

    def delete(self, id: int) -> None:
        player = self.find_by_id(id)
        if not player:
            raise ResourceNotFound(f"could not find player with id '{id}'")
        self.db.session.delete(player)
        self.db.session.commit()
