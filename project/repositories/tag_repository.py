from abc import ABC, abstractmethod
from typing import List, Optional

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import IntegrityError

from project.api.models.clip_tags import Tag
from project.repositories.exceptions import ResourceAlreadyExists, ResourceNotFound


class TagRepository(ABC):
    @abstractmethod
    def save(self, tag: Tag) -> Tag:
        raise NotImplementedError()

    @abstractmethod
    def find_by_id(self, id: int) -> Optional[Tag]:
        raise NotImplementedError()

    @abstractmethod
    def get_all(self) -> List[Tag]:
        raise NotImplementedError()

    @abstractmethod
    def delete(self, id: int) -> None:
        raise NotImplementedError()

    @abstractmethod
    def find_by_name(self, name: str) -> Optional[Tag]:
        raise NotImplementedError()


class TagDB(TagRepository):
    def __init__(self, db: SQLAlchemy):
        self.db = db

    def save(self, tag: Tag) -> Tag:
        try:
            self.db.session.add(tag)
            self.db.session.commit()
            return tag
        except IntegrityError:
            raise ResourceAlreadyExists(f"tag with name '{tag.name}' already exists")

    def get_all(self) -> List[Tag]:
        return Tag.query.all()

    def find_by_id(self, id: int) -> Optional[Tag]:
        return Tag.query.get(id)

    def delete(self, id: int) -> None:
        tag = self.find_by_id(id)
        if tag is None:
            raise ResourceNotFound(f"could not find tag with id: '{id}'")
        self.db.session.delete(tag)
        self.db.session.commit()

    def find_by_name(self, name: str) -> Optional[Tag]:
        return Tag.query.filter(Tag.name == name).first() # type: ignore
