from datetime import datetime
from typing import List, Optional
from project.api.db_utils import ResourceAlreadyPresent, ResourceNotFound

from project.api.models.strength_value import StrengthValue
from project import db


class StrengthValuesDb:
    def __init__(self, db):
        self.db = db

    def get_all(self) -> List[StrengthValue]:
        return StrengthValue.query.all()

    def get_by_id(self, id: int) -> StrengthValue:
        return StrengthValue.query.get(id)

    def get_all_for_player(self, player_id: int) -> List[StrengthValue]:
        return (
            StrengthValue.query.filter(StrengthValue.player_id == player_id)
            .order_by(StrengthValue.date)
            .all()
        )

    def get_all_for_player_between_dates(
        self, player_id: int, start_date: datetime, end_date: datetime
    ) -> List[StrengthValue]:
        return (
            StrengthValue.query.filter(StrengthValue.player_id == player_id)
            .filter(StrengthValue.date >= start_date)
            .filter(StrengthValue.date <= end_date)
            .order_by(StrengthValue.date)
            .all()
        )

    def get_value_for_player_on_date(
        self, player_id: int, date: datetime
    ) -> StrengthValue:
        return (
            StrengthValue.query.filter(StrengthValue.player_id == player_id)
            .filter(StrengthValue.date < date)
            .order_by(StrengthValue.date.desc())  # type: ignore
            .first()
        )

    def get_latest_for_player(
        self, player_id: int, date: Optional[datetime] = None
    ) -> StrengthValue:
        if date is None:
            date = datetime.now()

        return (
            StrengthValue.query.filter(StrengthValue.date <= date)
            .order_by(self.db.desc(StrengthValue.date))
            .filter(StrengthValue.player_id == player_id)
            .first()
        )

    def delete_by_id(self, sv_id: int):
        sv_from_db = self.get_by_id(sv_id)
        if sv_from_db is None:
            raise ResourceNotFound(f"strength value with id '{sv_id}' not found")

        self.db.session.delete(sv_from_db)
        self.db.session.commit()

    def delete_by_date(self, date: datetime) -> int:
        svs = StrengthValue.query.filter(StrengthValue.date == date).all()

        for sv in svs:
            self.db.session.delete(sv)

        self.db.session.commit()
        return len(svs)

    def add_multiple_strength_updates(
        self, date: datetime, player_strength_values: dict, forced: bool = False
    ):
        for p_id, s in player_strength_values.items():
            try:
                sv = self._create_sv_update_for_player(date, p_id, s, forced)
                self.db.session.add(sv)
            except ResourceAlreadyPresent as e:
                raise e
        self.db.session.commit()

    def update_strength_values_for_given_date(self, date, values: dict):
        self.delete_by_date(date)
        self.add_multiple_strength_updates(date, values)

    def _create_sv_update_for_player(self, date, p_id, strength, forced=False):
        is_duplicate = StrengthValue.query.filter(
            StrengthValue.date == date, StrengthValue.player_id == p_id
        ).first()
        if is_duplicate is not None and not forced:
            raise ResourceAlreadyPresent(
                f"entry with the same player id ({p_id})"
                f" and date ({date.isoformat()}) already exists"
            )
        return StrengthValue(strength, p_id, date)
