from abc import ABC, abstractmethod
from typing import Optional

from ..repositories.exceptions import ResourceNotFound
from ..api.models.hockey_event import EVENT_TYPE_VALUES, HockeyEvent


class HockeyEventsRepository(ABC):
    @abstractmethod
    def save(self, hockey_event: HockeyEvent) -> HockeyEvent:
        raise NotImplementedError()

    @abstractmethod
    def find_by_id(self, id: int) -> HockeyEvent:
        raise NotImplementedError()

    @abstractmethod
    def get_all(self, type_: Optional[str]) -> list[HockeyEvent]:
        raise NotImplementedError()

    @abstractmethod
    def delete(self, id: int) -> None:
        raise NotImplementedError()

    @abstractmethod
    def partial_update(self, id: int, hockey_event: HockeyEvent) -> HockeyEvent:
        raise NotImplementedError()

    @abstractmethod
    def update(self, id: int, hockey_event: HockeyEvent) -> HockeyEvent:
        raise NotImplementedError()


class HockeyEventsDB(HockeyEventsRepository):
    def __init__(self, db):
        self.db = db

    def save(self, hockey_event: HockeyEvent) -> HockeyEvent:
        self.db.session.add(hockey_event)
        self.db.session.commit()
        return hockey_event

    def find_by_id(self, id: int) -> HockeyEvent | None:
        return HockeyEvent.query.get(id)

    def get_all(self, type_: Optional[str]) -> list[HockeyEvent]:
        if type_ is not None:
            if type_ not in EVENT_TYPE_VALUES:
                raise ValueError(f"Invalid type {type_}")
            return (
                HockeyEvent.query.filter_by(type=type_).order_by(HockeyEvent.date).all()
            )
        return HockeyEvent.query.order_by(HockeyEvent.date).all()

    def delete(self, id: int) -> None:
        hockey_event = self.find_by_id(id)
        self.db.session.delete(hockey_event)
        self.db.session.commit()

    def update(self, id: int, hockey_event: HockeyEvent) -> HockeyEvent:
        original = self.find_by_id(id)
        if original is None:
            raise ResourceNotFound(f"Could not find hockey event with id {id}")
        original.date = hockey_event.date
        original.title = hockey_event.title
        original.type = hockey_event.type
        original.description = hockey_event.description
        self.db.session.commit()
        return hockey_event

    def partial_update(self, id: int, hockey_event: HockeyEvent) -> HockeyEvent:
        original = self.find_by_id(id)
        if original is None:
            raise ResourceNotFound(f"Could not find hockey event with id {id}")
        if hockey_event.date is not None:
            original.date = hockey_event.date
        if hockey_event.title is not None:
            original.title = hockey_event.title
        if hockey_event.type is not None:
            original.type = hockey_event.type
        if hockey_event.description is not None:
            original.description = hockey_event.description
        self.db.session.commit()
        return hockey_event
