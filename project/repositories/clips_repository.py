from abc import ABC, abstractmethod
from typing import List, Optional
from flask_sqlalchemy import SQLAlchemy

from project.api.models.clip import Clip
from project.repositories.exceptions import ResourceNotFound


class ClipRepository(ABC):
    @abstractmethod
    def get_all(self) -> List[Clip]:
        pass

    @abstractmethod
    def save(self, clip: Clip) -> Clip:
        pass

    @abstractmethod
    def delete(self, id: int) -> None:
        pass

    @abstractmethod
    def find_by_id(self, id: int) -> Optional[Clip]:
        pass

    @abstractmethod
    def find_by_url(self, url: str) -> List[Clip]:
        pass


class ClipDB(ClipRepository):
    def __init__(self, db: SQLAlchemy):
        self.db = db

    def get_all(self) -> List[Clip]:
        return Clip.query.all()

    def save(self, clip: Clip) -> Clip:
        self.db.session.add(clip)
        self.db.session.commit()
        return self.find_by_id(clip.id)

    def find_by_id(self, id: int) -> Optional[Clip]:
        return Clip.query.get(id)

    def delete(self, id: int) -> None:
        clip = self.find_by_id(id)
        if not clip:
            raise ResourceNotFound(f"could not find clip with id '{id}'")
        self.db.session.delete(clip)
        self.db.session.commit()

    def find_by_url(self, url: str) -> List[Clip]:
        clips = self.get_all()
        return list(filter(lambda x: x.video_url == url, clips))
