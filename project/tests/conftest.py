from itertools import product
import os

import pytest
from flask_sqlalchemy import SQLAlchemy
from flask import Flask
from flask.testing import FlaskClient
from werkzeug.test import TestResponse

from project.api.models.positions import Position

from .api.db_utils import (
    add_club_if_not_present,
    add_player_if_not_present,
    add_training,
    add_new_position,
)
from .. import create_app, db
from ..api.models import User
from ..api.auth_utils import create_access_token
from ..repositories.image_repository import ImageRepository

os.environ["SECRET_KEY"] = "my_precious"
os.environ["APP_SETTINGS"] = "project.config.TestingConfig"


@pytest.fixture(scope="module")
def test_app():
    app = create_app()
    app.config.from_object("project.config.TestingConfig")
    with app.app_context():
        yield app


def create_positions(db: SQLAlchemy):
    p = [
        Position(id=1, name="Leghátsó"),
        Position(id=2, name="Középső"),
        Position(id=3, name="Első"),
        Position(id=4, name="Szélső"),
    ]
    for position in p:
        db.session.add(position)
    db.session.commit()


@pytest.fixture(scope="module")
def test_database():
    db.create_all()
    create_positions(db)
    yield db
    db.session.remove()
    db.drop_all()


@pytest.fixture(scope="module")
def image_repository(test_database: SQLAlchemy) -> ImageRepository:
    return ImageRepository(test_database)


@pytest.fixture(scope="function")
def test_db_for_clubs():
    db.create_all()
    p1, p2, p3 = (
        add_player_if_not_present("Jakab Gipsz"),
        add_player_if_not_present("József Gipsz"),
        add_player_if_not_present("János Gipsz"),
    )
    add_training(black_team=[p1, p2, p3])
    add_training(black_team=[p2, p3])
    yield db
    db.session.remove()
    db.drop_all()


@pytest.fixture(scope="function")
def test_db_for_trainings():
    db.create_all()
    p1, p2, p3 = (
        add_player_if_not_present("Jakab Gipsz"),
        add_player_if_not_present("József Gipsz"),
        add_player_if_not_present("János Gipsz"),
    )
    add_club_if_not_present("Egyszusz VSE", players=[p1, p2, p3])
    yield db
    db.session.remove()
    db.drop_all()


@pytest.fixture(scope="function")
def full_team(positions):
    fnames = ["Jakab", "János", "Jenő", "József"]
    lnames = ["Kovács", "Szabó", "Gipsz"]

    names = product(fnames, lnames)
    goalie, center, wing, forward = positions

    players = []
    for i, name in enumerate(names):
        if i < 2:
            players.append(add_player_if_not_present(" ".join(name), 8, goalie, center))
            continue
        if i < 4:
            players.append(add_player_if_not_present(" ".join(name), 9, center, wing))
            continue
        if i < 8:
            players.append(add_player_if_not_present(" ".join(name), 7, wing, forward))
            continue

        if i < 12:
            players.append(add_player_if_not_present(" ".join(name), 6, forward, wing))
            continue
    return players


@pytest.fixture(scope="module")
def positions(test_database):
    """returns goalie, center, wing, forward in this order"""
    goalie = add_new_position("leghátsó")
    center = add_new_position("középső")
    wing = add_new_position("szélső")
    forward = add_new_position("első")
    return goalie, center, wing, forward


@pytest.fixture(scope="module")
def admin_user(test_database) -> User:
    user = User.query.filter(User.username == "Admin").first() # type: ignore
    if user is None:
        user = User("Admin", "admin", "admin@admin.admin")
        test_database.session.add(user)
        test_database.session.commit()
    return user


class AuthClient:
    def __init__(self, client: FlaskClient):
        self.client = client

    def login(self, username: str, password: str) -> str:
        response = self.client.post(
            "/api/v1/users/login/", json={"username": username, "password": password}
        )
        return response.json["token"]  # type: ignore


@pytest.fixture(scope="module")
def auth_client(test_app: Flask) -> AuthClient:
    test_client = test_app.test_client()
    return AuthClient(test_client)


@pytest.fixture(scope="module")
def admin_token(admin_user):
    return create_access_token(username=admin_user.username)


class AdminTestClient:
    def __init__(self, admin_token: str, test_client: FlaskClient):
        self._test_client = test_client
        self.admin_token = admin_token

    def post(self, *args, **kw) -> TestResponse:
        return self._test_client.post(
            *args, **kw, headers={"Authorization": f"Bearer {self.admin_token}"}
        )

    def get(self, *args, **kw) -> TestResponse:
        return self._test_client.get(
            *args, **kw, headers={"Authorization": f"Bearer {self.admin_token}"}
        )

    def put(self, *args, **kw) -> TestResponse:
        return self._test_client.put(
            *args, **kw, headers={"Authorization": f"Bearer {self.admin_token}"}
        )

    def delete(self, *args, **kw) -> TestResponse:
        return self._test_client.delete(
            *args, **kw, headers={"Authorization": f"Bearer {self.admin_token}"}
        )


@pytest.fixture(scope="module")
def admin_test_client(test_app: Flask, admin_token: User) -> AdminTestClient:
    test_client = test_app.test_client()
    admin_test_client = AdminTestClient(
        admin_token=admin_token, test_client=test_client
    )
    return admin_test_client
