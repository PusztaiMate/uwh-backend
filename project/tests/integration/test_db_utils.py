from datetime import datetime, timedelta

import pytest

from project import db
from project.api.models import StrengthValue
from project.api.db_utils import (
    ResourceAlreadyPresent,
    ResourceNotFound,
    add_multiple_strength_updates,
    add_player_to_db,
    add_strength_update_to_player,
    delete_strength_value_from_db,
    get_all_strength_values,
    get_latest_strength_value_for_player,
    get_latest_strength_values_for_multiple_players,
    get_strenght_value_by_id,
    update_strength_value,
)


def test_sv_retrieves_correct_object_from_db(test_app, test_database):
    p_id = add_player_to_db("Jakab", "Gipsz")
    new_strength = 7
    date = datetime(2020, 1, 1)

    new_sv = StrengthValue(new_strength, p_id, date)
    db.session.add(new_sv)
    db.session.commit()

    sv_from_db = get_strenght_value_by_id(new_sv.id)

    assert sv_from_db.date == date
    assert sv_from_db.value == new_strength
    assert sv_from_db.player_id == p_id


def test_sv_retrieves_all_instances_in_db(test_app, test_database):
    p_id = add_player_to_db("Jakab", "Gipsz")
    new_strength = 7
    date_1 = datetime(2020, 1, 1)
    date_2 = datetime(2020, 1, 2)

    sv1_id = add_strength_update_to_player(p_id, new_strength, date_1)
    sv2_id = add_strength_update_to_player(p_id, new_strength, date_2)

    all_svs = get_all_strength_values()

    sv1_from_db = get_strenght_value_by_id(sv1_id)
    sv2_from_db = get_strenght_value_by_id(sv2_id)

    assert sv1_from_db in all_svs
    assert sv2_from_db in all_svs


def test_sv_addition_works(test_app, test_database):
    p_id = add_player_to_db("Jakab", "Gipsz")
    new_strength = 7
    date = datetime(2020, 1, 1)

    sv_id = add_strength_update_to_player(p_id, new_strength, date)

    sv_from_db = get_strenght_value_by_id(sv_id)

    assert sv_from_db.value == new_strength
    assert sv_from_db.date == date


def test_sv_can_not_add_to_new_entries_with_same_player_id_and_date_unless_forced(
    test_app, test_database
):
    p_id = add_player_to_db("Jakab", "Gipsz")
    date = datetime(2020, 1, 1)

    add_strength_update_to_player(p_id, 5, date)

    with pytest.raises(ResourceAlreadyPresent) as e:
        add_strength_update_to_player(p_id, 6, date)

    assert (
        f"entry with the same player id ({p_id})"
        f" and date ({date.isoformat()}) already exists" in str(e)
    )

    sv_forced_id = add_strength_update_to_player(p_id, 7, date, forced=True)
    sv_forced_from_db = get_strenght_value_by_id(sv_forced_id)

    assert sv_forced_from_db.value != 6
    assert sv_forced_from_db.value == 7
    assert sv_forced_from_db.date == date


def test_sv_can_update_values(test_app, test_database):
    p_id = add_player_to_db("Jakab", "Gipsz")
    date = datetime(2020, 1, 1)

    sv_id = add_strength_update_to_player(p_id, 5, date)

    sv_from_db = get_strenght_value_by_id(sv_id)
    assert sv_from_db.value == 5
    assert sv_from_db.date == date

    update_strength_value(sv_id, new_value=7)
    sv_from_db = get_strenght_value_by_id(sv_id)
    assert sv_from_db.value == 7
    assert sv_from_db.date == date


def test_sv_update_fails_if_original_is_not_present(test_app, test_database):
    # hopefully this is not found
    sv_id = 99999
    with pytest.raises(ResourceNotFound) as e:
        update_strength_value(sv_id, new_value=10)

    assert f"strength value with id '{sv_id}' not found" in str(e)


def test_sv_can_delete_by_id(test_app, test_database):
    p_id = add_player_to_db("Jakab", "Gipsz")
    date = datetime(2020, 1, 1)
    sv_id = add_strength_update_to_player(p_id, 5, date)

    delete_strength_value_from_db(sv_id)

    sv_from_db = get_strenght_value_by_id(sv_id)
    assert sv_from_db is None


@pytest.mark.skip(reason="not implemented yet")
def test_sv_can_delete_multiple_using_dates(test_app, test_database):
    p1_id = add_player_to_db("Jakab", "Gipsz")
    date = datetime(2020, 2, 1, 19, 30)
    p2_id = add_player_to_db("Jenő", "Gipsz")
    date = datetime(2020, 2, 1, 20, 30)

    sv1_id = add_strength_update_to_player(p1_id, 5, date)
    sv2_id = add_strength_update_to_player(p2_id, 5, date)

    assert get_strenght_value_by_id(sv1_id) is not None
    assert get_strenght_value_by_id(sv2_id) is not None

    assert get_strenght_value_by_id(sv1_id) is None
    assert get_strenght_value_by_id(sv2_id) is None


def test_multiple_svs_can_be_added_at_once(test_app, test_database):
    p1_id = add_player_to_db("Jakab", "Gipsz")
    s1 = 2
    p2_id = add_player_to_db("Jenő", "Gipsz")
    s2 = 5
    date = datetime(2020, 2, 8, 19, 30)

    num_of_svs_before = len(get_all_strength_values())

    add_multiple_strength_updates(date, {p1_id: s1, p2_id: s2})

    num_of_svs_after = len(get_all_strength_values())

    assert num_of_svs_after == num_of_svs_before + 2


def test_get_latest_strength_value_for_player(test_app, test_database):
    p_id = add_player_to_db("Jakab", "Gipsz")
    s1 = 2
    s2 = 2.1
    s3 = 2.3
    s4 = 2.2
    date1 = datetime(2020, 2, 8, 19, 30)  # earliest
    date2 = datetime(2021, 2, 8, 19, 30)  # latest
    date3 = datetime(2020, 2, 9, 19, 30)
    date4 = datetime(2020, 2, 10, 19, 30)

    add_strength_update_to_player(p_id, s1, date1)
    add_strength_update_to_player(p_id, s2, date2)
    add_strength_update_to_player(p_id, s3, date3)
    add_strength_update_to_player(p_id, s4, date4)

    sv_latest = get_latest_strength_value_for_player(p_id)

    assert sv_latest.date == date2
    assert sv_latest.player_id == p_id
    assert sv_latest.value == s2


def test_get_latest_strength_value_for_player_until_given_date(test_app, test_database):
    p_id = add_player_to_db("Jakab", "Gipsz")
    s1 = 2
    s3 = 2.3
    s2 = 2.2
    s4 = 2.1
    date1 = datetime(2020, 2, 8, 19, 30)  # earliest
    date3 = datetime(2020, 2, 9, 19, 30)
    date2 = datetime(2020, 2, 11, 19, 30)
    date4 = datetime(2021, 2, 8, 19, 30)  # latest

    add_strength_update_to_player(p_id, s1, date1)
    add_strength_update_to_player(p_id, s2, date2)
    add_strength_update_to_player(p_id, s3, date3)
    add_strength_update_to_player(p_id, s4, date4)

    sv_latest = get_latest_strength_value_for_player(p_id, date3 + timedelta(days=1))

    assert sv_latest.date == date3
    assert sv_latest.player_id == p_id
    assert sv_latest.value == s3


def test_can_get_latest_values_for_multiple_players(test_app, test_database):
    p1_id = add_player_to_db("Jakab", "Gipsz")
    p2_id = add_player_to_db("Jenő", "Gipsz")
    p3_id = add_player_to_db("János", "Gipsz")
    p1_sv1 = 6.0
    p1_sv2 = 6.1
    p2_sv1 = 6.5
    date1 = datetime(2020, 2, 8, 19, 30)
    date2 = datetime(2021, 2, 8, 19, 30)

    sv1 = add_strength_update_to_player(p1_id, p1_sv1, date1)
    sv2 = add_strength_update_to_player(p1_id, p1_sv2, date2)
    sv3 = add_strength_update_to_player(p2_id, p2_sv1, date2)

    latest_vals = get_latest_strength_values_for_multiple_players([p1_id, p2_id, p3_id])

    assert len(latest_vals) == 3
    assert get_strenght_value_by_id(sv1) not in latest_vals
    assert get_strenght_value_by_id(sv2) in latest_vals
    assert get_strenght_value_by_id(sv3) in latest_vals
