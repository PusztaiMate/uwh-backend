import json
from typing import Any

import pytest

from project.tests.api.db_utils import (
    add_player_if_not_present,
    get_all_players_in_db,
    generate_random_players,
    add_training,
)
from project.api.db_utils import add_new_position, get_player_by_id
from project.tests.conftest import AdminTestClient


def assert_in_message(json_: Any | None, expected: str):
    assert expected in json_["message"]  # type: ignore


def assert_in_status(json_: Any | None, expected: str):
    assert expected in json_["status"]  # type: ignore


@pytest.mark.skip("authorization fucked up on pythons side")
@pytest.mark.parametrize(
    ["fname", "lname", "status_code", "message"],
    [
        ["Jacob", "Gipsz", 201, "player Jacob Gipsz successfully added"],
        ["János", None, 400, "input payload validation failed"],
        [None, "Gipsz", 400, "input payload validation failed"],
    ],
)
def test_add_player(admin_test_client, fname, lname, status_code, message):
    resp = admin_test_client.post(
        "/api/v1/players/",
        data=json.dumps({"fname": fname, "lname": lname}),
        content_type="application/json",
    )

    data = resp.json

    assert resp.status_code == status_code
    assert message in data["message"]


@pytest.mark.skip("authorization fucked up on pythons side")
def test_post_req_with_bad_content_type_rejected(admin_test_client: AdminTestClient):
    resp = admin_test_client.post(
        "/api/v1/players/",
        data=json.dumps({"fname": "Jakab", "lname": "Gipsz"}),
        content_type="text/html",
    )

    data = resp.json

    assert resp.status_code == 400
    assert_in_message(data, "wrong content type")
    assert_in_status(data, "failed")


def test_get_single_player(test_app, test_database):
    client = test_app.test_client()
    player = add_player_if_not_present("Jakab Gipsz")

    resp = client.get(f"/api/v1/players/{player.id}/")
    data = resp.json

    assert resp.status_code == 200
    assert data["fname"] == "Jakab"
    assert data["lname"] == "Gipsz"


def test_when_player_not_present_single_player_query_returns_404(
    test_app, test_database
):
    client = test_app.test_client()
    resp = client.get("/api/v1/players/0/")

    data = resp.json

    assert "player with id 0 not found" in data["message"]
    assert "fail" in data["status"]
    assert resp.status_code == 404


def test_when_invalid_id_is_requested_then_not_found_is_returned(
    test_app, test_database
):
    test_client = test_app.test_client()
    resp = test_client.get("/api/v1/players/this_is_not_an_id/")

    data = resp.json

    assert resp.status_code == 404
    assert "requested page not found" in data["message"]


def test_get_all_players(test_app, test_database):
    client = test_app.test_client()
    resp = client.get("/api/v1/players/")
    before = len(resp.json)

    p1 = add_player_if_not_present("Jakab Gipsz")
    p2 = add_player_if_not_present("János Gipsz")

    resp = client.get("/api/v1/players/")
    after = resp.json

    assert resp.status_code == 200
    assert before <= len(after)


def test_put_request_can_be_used_to_modify_resource(test_app, test_database):
    client = test_app.test_client()
    add_player_if_not_present("Jakab Gipsz")

    resp = client.put(
        "/api/v1/players/1/",
        content_type="application/json",
        data=json.dumps({"fname": "János", "lname": "Gipsz"}),
    )

    assert resp.status_code == 200

    resp = client.get("/api/v1/players/1/")
    data = resp.json

    assert data["fname"] == "János"
    assert data["lname"] == "Gipsz"
    assert data["id"] == 1

@pytest.mark.skip("authorization fucked up on pythons side")
def test_put_adds_new_player_if_none_found_with_given_id(
    admin_test_client: AdminTestClient,
):
    players = get_all_players_in_db()

    player_count_before_put = len(players)

    # get an id that is not present in the database
    new_id = 0
    for i in range(1, 100000):
        if i not in [p.id for p in players]:
            new_id = i
            break

    resp = admin_test_client.put(
        f"/api/v1/players/{new_id}/",
        content_type="application/json",
        data=json.dumps({"fname": "János", "lname": "Gipsz"}),
    )

    assert resp.status_code == 201
    assert_in_message(resp.json, "successfully added")

    players_after_put = get_all_players_in_db()

    assert len(players_after_put) == player_count_before_put + 1

    # find our new player
    for p in players_after_put:
        if p.id == new_id:
            assert p.fname == "János"
            assert p.lname == "Gipsz"
            break


def test_player_pairing_stats_can_be_queried(test_app, test_database):
    client = test_app.test_client()
    p1, p2, p3, p4 = generate_random_players(4)
    add_training([p1, p2], [p3, p4])
    add_training([], [])

    resp = client.get("/api/v1/players/pairings/")

    assert resp.status_code == 200

    print(resp.json)
    data = resp.json

    assert data[str(p1.id)]["most_paired"]["player_ids"] == [p2.id]
    assert data[str(p1.id)]["most_paired"]["value"] == 1
    assert data[str(p2.id)]["most_paired"]["player_ids"] == [p1.id]
    assert data[str(p3.id)]["most_paired"]["value"] == 1


def test_id_name_pairing_can_be_queried_with_get(test_app, test_database):
    client = test_app.test_client()

    p1, p2 = generate_random_players(2)

    resp = client.get("/api/v1/players/id-name-pairs/")

    assert resp.status_code == 200
    json_data = resp.json
    assert str(p1.id) in json_data
    assert str(p2.id) in json_data
    assert {"fname": p1.fname, "lname": p1.lname} in json_data.values()
