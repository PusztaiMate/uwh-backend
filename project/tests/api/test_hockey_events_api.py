from datetime import datetime
import pytest

from ...api.models.hockey_event import HockeyEvent
from ...repositories.hockey_events_repository import (
    HockeyEventsDB,
    HockeyEventsRepository,
)


@pytest.fixture
def hockey_event_repository(test_database) -> HockeyEventsRepository:
    return HockeyEventsDB(test_database)


def _clean_up_events(hockey_event_repository: HockeyEventsRepository):
    all_event_ids = [e.id for e in hockey_event_repository.get_all(type_=None)]
    for event_id in all_event_ids:
        hockey_event_repository.delete(event_id)  # type: ignore


def _event_is_in_response_json(event, data):
    for e in data["hockey_events"]:
        if (
            e["date"] == event.date.isoformat()
            and e["title"] == event.title
            and e["type"] == event.type
            and e["description"] == event.description
        ):
            return True
    return False


def _assert_event_in_get_all_hockey_response(event, data):
    assert _event_is_in_response_json(
        event, data
    ), f"Event with id {event.id} not in response"


def _assert_event_in_get_hockey_by_id_response(event, data):
    assert (
        data["data"]["date"] == event.date.isoformat()
        and data["data"]["title"] == event.title
        and data["data"]["type"] == event.type
        and data["data"]["description"] == event.description
    ), f"Event with id {event.id} not in response"


def test_get_all_hockey_events(test_app, hockey_event_repository):
    event = HockeyEvent(
        date=datetime(2021, 1, 1),
        title="Test Event",
        type="competition",
        description="This is a test event",
    )
    hockey_event_repository.save(event)

    client = test_app.test_client()

    resp = client.get("/api/v1/hockey-events/")
    data = resp.get_json()

    assert resp.status_code == 200
    assert len(data["hockey_events"]) != 0
    _assert_event_in_get_all_hockey_response(event, data)


def test_get_hockey_event_by_id(test_app, hockey_event_repository):
    event = HockeyEvent(
        date=datetime(2021, 1, 1),
        title="Test Event",
        type="competition",
        description="This is a test event",
    )
    hockey_event_repository.save(event)

    client = test_app.test_client()

    resp = client.get(f"/api/v1/hockey-events/{event.id}")
    data = resp.get_json()

    assert resp.status_code == 200
    _assert_event_in_get_hockey_by_id_response(event, data)


def test_get_hockey_event_by_id_invalid_id(test_app, hockey_event_repository):
    client = test_app.test_client()

    resp = client.get("/api/v1/hockey-events/invalid_id")
    data = resp.get_json()

    assert resp.status_code == 404


def test_get_hockey_events_by_type(test_app, hockey_event_repository):
    _clean_up_events(hockey_event_repository)

    event1 = HockeyEvent(
        date=datetime(2021, 1, 1),
        title="Test Event 1",
        type="competition",
        description="This is a test event 1",
    )
    event2 = HockeyEvent(
        date=datetime(2021, 1, 2),
        title="Test Event 2",
        type="practice",
        description="This is a test event 2",
    )
    hockey_event_repository.save(event1)
    hockey_event_repository.save(event2)

    client = test_app.test_client()

    resp = client.get("/api/v1/hockey-events?type=competition")
    data = resp.get_json()

    assert resp.status_code == 200
    assert len(data["hockey_events"]) == 1
    _assert_event_in_get_all_hockey_response(event1, data)


def test_get_hockey_events_by_type_invalid_type(test_app, hockey_event_repository):
    client = test_app.test_client()

    resp = client.get("/api/v1/hockey-events?type=invalid_type")
    data = resp.get_json()

    assert resp.status_code == 400


def test_save_hockey_event(test_app, hockey_event_repository):
    event = {
        "date": datetime(2021, 1, 1).isoformat(),
        "title": "Test Event",
        "type": "competition",
        "description": "This is a test event",
    }

    client = test_app.test_client()

    resp = client.post("/api/v1/hockey-events/", json=event)
    data = resp.get_json()

    assert resp.status_code == 201
    assert data["message"] == "Hockey event created successfully"


def test_update_hockey_event(test_app, hockey_event_repository):
    event = HockeyEvent(
        date=datetime(2021, 1, 1),
        title="Test Event",
        type="competition",
        description="This is a test event",
    )
    event = hockey_event_repository.save(event)

    updated_event = {
        "date": datetime(2021, 1, 2).isoformat(),
        "title": "Updated Test Event",
        "type": "practice",
        "description": "This is an updated test event",
    }

    client = test_app.test_client()

    resp = client.put(f"/api/v1/hockey-events/{event.id}", json=updated_event)
    data = resp.get_json()

    assert resp.status_code == 200
    assert data["message"] == "Hockey event updated successfully"


def test_delete_hockey_event(test_app, hockey_event_repository):
    event = HockeyEvent(
        date=datetime(2021, 1, 1),
        title="Test Event",
        type="competition",
        description="This is a test event",
    )
    hockey_event_repository.save(event)

    client = test_app.test_client()

    resp = client.delete(f"/api/v1/hockey-events/{event.id}")
    data = resp.get_json()

    assert resp.status_code == 200
    assert data["message"] == "Hockey event deleted successfully"


def test_get_all_hockey_events_empty(test_app, hockey_event_repository):
    _clean_up_events(hockey_event_repository)

    client = test_app.test_client()

    resp = client.get("/api/v1/hockey-events/")
    data = resp.get_json()

    assert resp.status_code == 200
    assert len(data["hockey_events"]) == 0
