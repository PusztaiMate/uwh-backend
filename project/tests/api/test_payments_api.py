import json
from datetime import date

from .db_utils import (
    add_payment_obl,
    add_player_if_not_present,
    get_number_of_obligations_in_db,
    obligation_is_in_db,
    num_of_obligations_in_month_in_db,
)


def test_empty_list_returned_if_no_payment_obligations_present(test_app, test_database):
    client = test_app.test_client()
    resp = client.get("/api/v1/payments/obligations/2020/1/")

    data = resp.json

    assert resp.status_code == 200
    assert "success" in data["message"]
    assert data["playerPayObligations"] == []


def test_single_payment_obligation_is_correctly_returned(test_app, test_database):
    player = add_player_if_not_present("Jakab Gipsz")
    add_payment_obl(year=2020, month=1, player_id=player.id, is_active=True)

    client = test_app.test_client()

    resp = client.get("/api/v1/payments/obligations/2020/1/")
    data = resp.json

    assert resp.status_code == 200
    assert "success" in data["message"]

    assert "playerPayObligations" in data

    obligations_list = data["playerPayObligations"]
    assert len(obligations_list) == 1

    p1_obligations = obligations_list[0]
    assert p1_obligations["playerId"] == player.id
    assert p1_obligations["isActive"]
    assert p1_obligations["month"] == date(2020, 1, 1).isoformat()


def test_multiple_payments_are_returned_correctly(test_app, test_database):
    player_1 = add_player_if_not_present("Jakab Gipsz")
    player_2 = add_player_if_not_present("Jenő Gipsz")

    num_of_obligations_before_add = get_number_of_obligations_in_db()
    obl_1 = add_payment_obl(year=2020, month=1, player_id=player_1.id, is_active=True)
    obl_2 = add_payment_obl(year=2020, month=1, player_id=player_2.id, is_active=True)

    client = test_app.test_client()

    resp = client.get("/api/v1/payments/obligations/2020/1/")
    data = resp.json

    assert resp.status_code == 200
    assert "success" in data["message"]

    assert "playerPayObligations" in data

    obligations_list = data["playerPayObligations"]
    assert len(obligations_list) == num_of_obligations_before_add + 2
    assert obligation_is_in_db(obl_1)
    assert obligation_is_in_db(obl_2)


def test_put_can_be_used_to_update_values_for_the_whole_month(test_app, test_database):
    player_1 = add_player_if_not_present("Jakab Gipsz")
    player_2 = add_player_if_not_present("János Gipsz")
    obl_1 = add_payment_obl(year=2020, month=1, player_id=player_1.id, is_active=True)
    obl_2 = add_payment_obl(year=2020, month=1, player_id=player_2.id, is_active=True)

    assert obligation_is_in_db(obl_1)
    assert obligation_is_in_db(obl_2)

    player_3 = add_player_if_not_present("Jenő Gipsz")
    player_4 = add_player_if_not_present("Júlia Gipsz")

    client = test_app.test_client()
    # TODO: have year/month outside the list!
    resp = client.put(
        "/api/v1/payments/obligations/2020/1/",
        content_type="application/json",
        data=json.dumps(
            {
                "obligations": [
                    {
                        "playerId": player_3.id,
                        "isActive": True,
                    },
                    {
                        "playerId": player_4.id,
                        "isActive": True,
                    },
                ]
            }
        ),
    )

    assert resp.status_code == 200
    data = resp.get_json()
    assert "success" in data["message"]
    assert num_of_obligations_in_month_in_db(year=2020, month=1) == 2


def test_put_creates_data_if_nothing_present_before(test_app, test_database):
    client = test_app.test_client()
    player = add_player_if_not_present("Jakab Gipsz")
    resp = client.put(
        "/api/v1/payments/obligations/1234/12/",
        content_type="application/json",
        data=json.dumps(
            {
                "obligations": [
                    {
                        "playerId": player.id,
                        "isActive": True,
                    },
                ]
            }
        ),
    )

    assert resp.status_code == 200
    assert "success" in resp.json["message"]
    assert num_of_obligations_in_month_in_db(1234, 12) == 1
