from datetime import datetime
from typing import Any, Dict, List
from random import choice
from string import ascii_letters
from project import db
from project.api.models.club import Club
from project.api.models.player import Player
from project.tests.api.db_utils import add_player, add_training


def _generate_random_string(length: int) -> str:
    return "".join(choice(ascii_letters) for _ in range(length))


def _generate_random_name() -> str:
    return _generate_random_string(8) + " " + _generate_random_string(8)


def _generate_random_training_strength() -> int:
    return choice(range(1, 11))


def _generate_random_players(number_of_players: int) -> List[Player]:
    players = [
        add_player(_generate_random_name(), _generate_random_training_strength())
        for _ in range(number_of_players)
    ]
    return players


def _add_training_with_players_to_month(year: int, month: int, players: List[Player]):
    # add players if we have less than 12
    if len(players) < 12:
        players += _generate_random_players(12 - len(players))

    black_team = [p for i, p in enumerate(players) if i % 2 == 0]
    white_team = [p for i, p in enumerate(players) if i % 2 == 1]

    add_training(
        black_team=black_team,
        white_team=white_team,
        date_=datetime(year=year, month=month, day=1),
    )


def assert_player_has_N_trainings_in_month_M(
    player_id: int, n: int, m: int, per_player: List[Dict[str, Any]]
):
    for data in per_player:
        if data["player_id"] == player_id:
            assert data["trainings_per_month"][m] == n
            return
    assert False, "player not found in per_player"


def test_yearly_csv_stats_returned(test_app, test_database):
    client = test_app.test_client()

    p1, p2 = _generate_random_players(2)

    # p1 had 2 trainings: 1 in january and 1 in february
    # p2 had 1 training in january
    _add_training_with_players_to_month(2020, 1, [p1, p2])
    _add_training_with_players_to_month(2020, 2, [p1])

    resp = client.get("/api/v1/attendance/yearly/2020/")

    assert resp.status_code == 200
    assert resp.content_type == "text/csv"
