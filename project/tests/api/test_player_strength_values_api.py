import json
from datetime import datetime, timedelta
from typing import List

import pytest
from flask.wrappers import Response
from project.api.db_utils import (
    add_player_to_db,
    add_strength_update_to_player,
    get_strenght_value_by_id,
)

API_PATH = "/api/v1/strength-values/"


def test_can_retrieve_all_saved_values(test_app, test_database):
    client = test_app.test_client()
    p1_id = add_player_to_db("Jakab", "Gipsz")
    p2_id = add_player_to_db("Jenő", "Gipsz")
    p1_sv = 3
    p2_sv = 7
    date_1 = datetime(2020, 1, 1)
    sv_id_1 = add_strength_update_to_player(p1_id, p1_sv, date_1)
    sv_id_2 = add_strength_update_to_player(p2_id, p2_sv, date_1)

    resp = _send_get(client)
    resp_json = resp.json
    assert resp_json is not None

    expected_sv_1 = {
        "id": sv_id_1,
        "player_id": p1_id,
        "date": date_1.isoformat(),
        "value": p1_sv,
    }
    expected_sv_2 = {
        "id": sv_id_2,
        "player_id": p2_id,
        "date": date_1.isoformat(),
        "value": p2_sv,
    }

    assert resp.status_code == 200
    assert expected_sv_1 in resp_json
    assert expected_sv_2 in resp_json


def test_can_query_for_players_latest_value(test_app, test_database):
    client = test_app.test_client()
    p1_id = add_player_to_db("Jakab", "Gipsz")
    p2_id = add_player_to_db("Jenő", "Gipsz")
    p1_sv_1 = 3
    p1_sv_2 = 3.1
    p1_sv_3 = 3.3
    p2_sv_1 = 7.0
    p2_sv_2 = 6.9
    p2_sv_3 = 6.6
    date_1 = datetime(2020, 1, 1)
    date_2 = datetime(2020, 1, 2)
    date_3 = datetime(2020, 2, 1)

    add_strength_update_to_player(p1_id, p1_sv_1, date_1)
    add_strength_update_to_player(p1_id, p1_sv_2, date_2)
    add_strength_update_to_player(p1_id, p1_sv_3, date_3)
    add_strength_update_to_player(p2_id, p2_sv_3, date_3)
    add_strength_update_to_player(p2_id, p2_sv_2, date_2)
    add_strength_update_to_player(p2_id, p2_sv_1, date_1)

    resp = _send_get_players_latest(client, [p1_id, p2_id])
    resp_json = resp.json
    assert resp_json is not None

    assert resp.status_code == 200
    assert resp_json[str(p1_id)] == p1_sv_3
    assert resp_json[str(p2_id)] == p2_sv_3


def test_can_query_for_latest_sv_until_given_date(test_app, test_database):
    client = test_app.test_client()
    p1_id = add_player_to_db("Jakab", "Gipsz")
    p1_sv_1 = 2.9
    p1_sv_2 = 3.1
    p1_sv_3 = 3.3
    p1_sv_4 = 3.2
    date_1 = datetime(2020, 1, 1)
    date_2 = datetime(2020, 1, 2)
    date_3 = datetime(2020, 2, 1)
    date_4 = datetime(2020, 2, 3)
    add_strength_update_to_player(p1_id, p1_sv_1, date_1)
    add_strength_update_to_player(p1_id, p1_sv_2, date_2)
    add_strength_update_to_player(p1_id, p1_sv_3, date_3)
    add_strength_update_to_player(p1_id, p1_sv_4, date_4)

    resp = _send_get_player_sv_until_date(
        client, [p1_id], date=date_2 + timedelta(days=1)
    )

    assert resp.status_code == 200
    resp_json = resp.json
    assert resp_json is not None

    assert resp_json[str(p1_id)] == p1_sv_2


def test_can_query_for_multiple_players_latest_value(test_app, test_database):
    client = test_app.test_client()
    p1_id = add_player_to_db("Jakab", "Gipsz")
    p2_id = add_player_to_db("Jenő", "Gipsz")
    p3_id = add_player_to_db("János", "Gipsz", training_strength=6.3)
    p1_sv_1 = 5.1
    p1_sv_2 = 5.3
    p2_sv_1 = 7.0
    date_1 = datetime(2020, 1, 1)
    date_2 = datetime(2020, 1, 3)

    add_strength_update_to_player(p1_id, p1_sv_1, date_1)
    add_strength_update_to_player(p1_id, p1_sv_2, date_2)
    add_strength_update_to_player(p2_id, p2_sv_1, date_1)

    resp = _send_get_players_latest(client, [p1_id, p2_id, p3_id])
    resp_json = resp.json
    assert resp_json is not None

    assert resp.status_code == 200
    assert resp_json[str(p1_id)] == p1_sv_2


def test_default_sv_is_used_if_no_sv_row_is_present_for_player(test_app, test_database):
    client = test_app.test_client()
    sv = 4
    p = add_player_to_db("Jakab", "Gipsz", training_strength=sv)

    resp = _send_get_player_latest(client, p)
    resp_json = resp.json
    assert resp_json is not None

    assert resp.status_code == 200
    assert resp_json[str(p)] == sv


def test_404_if_player_and_strength_value_not_found(test_app, test_database):
    client = test_app.test_client()

    # hopefully id 9999 for player is not taken yet
    resp = _send_get_player_latest(client, 9999)
    resp_json = resp.json
    assert resp_json is not None

    assert resp.status_code == 404
    assert "player with id '9999' not found" in resp_json["message"]


def test_can_create_multiple_new_entries_at_once(test_app, test_database):
    client = test_app.test_client()
    p1_id = add_player_to_db("Jakab", "Gipsz")
    p2_id = add_player_to_db("Jenő", "Gipsz")

    data = {
        "date": "2020-01-02T19:30:00",
        "new_values": [
            {"player_id": p1_id, "strength_value": 5},
            {"player_id": p2_id, "strength_value": 3},
        ],
    }

    resp = _send_post(client, data)

    assert resp.status_code == 201
    assert "successfully added (2) entries to db"


def test_multiple_create_fail_if_one_new_entry_is_colliding_with_existing_data(
    test_app, test_database
):
    client = test_app.test_client()
    p1_id = add_player_to_db("Jakab", "Gipsz")
    p2_id = add_player_to_db("Jenő", "Gipsz")

    data = {
        "date": "2020-01-02T19:30:00",
        "new_values": [
            {"player_id": p1_id, "strength_value": 5},
            {"player_id": p2_id, "strength_value": 3},
        ],
    }

    resp_ok = _send_post(client, data)
    assert resp_ok.status_code == 201

    # sending the same post again
    resp_nok = _send_post(client, data)
    assert resp_nok.status_code == 400
    assert resp_nok.json is not None
    assert (
        "value(s) already exist(s) in the db, use PUT to update it"
        in resp_nok.json["message"]
    )


def test_adding_new_data_fails_if_json_data_missing(test_app, test_database):
    client = test_app.test_client()

    # empty
    data = {}

    resp = _send_post(client, data)

    assert resp.status_code == 400
    resp_json = resp.json
    assert resp_json is not None
    assert "invalid payload, data is not present" in resp_json["message"]


@pytest.mark.parametrize(
    "payload,missing_key",
    [
        ({"new_values": [{"player_id": 1, "strength_value": 3}]}, "date"),
        (
            {
                "date": "2020-01-02T19:30:00",
            },
            "new_values",
        ),
        (
            {
                "date": "2020-01-02T19:30:00",
                "new_values": [{"strength_value": 3}],
            },
            "player_id",
        ),
        (
            {
                "date": "2020-01-02T19:30:00",
                "new_values": [{"player_id": 1}],
            },
            "strength_value",
        ),
    ],
)
def test_request_to_add_fails_if_data_missing(
    test_app, test_database, payload, missing_key
):
    client = test_app.test_client()
    add_player_to_db("Jakab", "Gipsz")

    resp = _send_post(client, payload)

    assert resp.status_code == 400
    resp_json = resp.json
    assert resp_json is not None
    assert f"data missing from request json ('{missing_key}')" in resp_json["message"]


def test_can_delete_multiple_entries_for_given_date(test_app, test_database):
    client = test_app.test_client()
    p1_id = add_player_to_db("James", "Gipsz")  # relative from foreign land
    p2_id = add_player_to_db("Jane", "Gipsz")  # his sister
    date = datetime(2021, 12, 2)
    sv_1 = add_strength_update_to_player(p1_id, 1, date)
    sv_2 = add_strength_update_to_player(p2_id, 1, date)

    data = {"date": date.isoformat()}
    resp = _send_delete(client, data)

    assert resp.status_code == 200
    assert get_strenght_value_by_id(sv_1) is None
    assert get_strenght_value_by_id(sv_2) is None


def test_delete_successful_even_when_there_is_nothing_to_delete(
    test_app, test_database
):
    client = test_app.test_client()
    data = {"date": "1980-12-12"}  # hockey was not here yet

    resp = _send_delete(client, data)

    assert resp.status_code == 200
    resp_json = resp.json
    assert resp_json is not None
    assert "nothing to delete" in resp_json["message"]


def test_delete_by_date_fails_if_date_is_missing_from_payload(test_app, test_database):
    client = test_app.test_client()
    # to make sure our db is not empty
    p1_id = add_player_to_db("Jakab", "Gipsz")
    date = datetime(2021, 12, 2)
    add_strength_update_to_player(p1_id, 1, date)

    data = {"something": "else, but not date"}
    resp = _send_delete(client, data)

    assert resp.status_code == 400
    resp_json = resp.json
    assert resp_json is not None
    assert "'date' not found in payload" in resp_json["message"]


def _send_get(client) -> Response:
    return client.get(API_PATH)


def _send_get_player_latest(client, player_id: int) -> Response:
    return _send_get_players_latest(client, [player_id])


def _send_get_players_latest(client, player_ids: List[int]) -> Response:
    ids = ",".join(map(str, player_ids))
    return client.get(API_PATH + "latest-for-players/", query_string={"ids": ids})


def _send_get_player_sv_until_date(
    client, player_ids: List[int], date: datetime
) -> Response:
    ids = ",".join(map(str, player_ids))
    d = date.isoformat()
    return client.get(
        API_PATH + "latest-for-players/", query_string={"ids": ids, "date": d}
    )


def _send_post(client, data: dict | None = None) -> Response:
    return client.post(
        API_PATH,
        content_type="application/json",
        data=json.dumps(data),
    )


def _send_delete(client, data: dict | None = None) -> Response:
    resp = client.delete(
        API_PATH,
        content_type="application/json",
        data=json.dumps(data),
    )
    return resp
