from io import BytesIO

from PIL import Image


# create a fake image file
def create_fake_image_file():
    image = Image.new("RGB", (100, 100))
    image_file = BytesIO()
    image.save(image_file, "JPEG")
    image_file.seek(0)
    return image_file
