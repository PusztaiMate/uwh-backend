import json
import pytest
from project.api.models.clip_tags import Tag

from project.repositories.clips_repository import ClipDB, ClipRepository
from project.repositories.tag_repository import TagDB
from project.services.clips import convert_clip_to_clip_dto
from project.tests.api.utils import make_random_clip, random_string


@pytest.fixture
def clip_repository(test_database):
    return ClipDB(test_database)


def test_get_all_clips(test_app, clip_repository: ClipRepository):
    c1 = make_random_clip()
    c2 = clip_repository.save(c1)
    client = test_app.test_client()

    resp = client.get("/api/v1/clips/")
    data = resp.get_json()

    assert resp.status_code == 200
    clips = data["clips"]
    assert len(clips) == 1
    assert clips[0] == convert_clip_to_clip_dto(c2).to_dict()


def test_add_clip(test_app, clip_repository: ClipRepository):
    client = test_app.test_client()

    clipnum_before = len(clip_repository.get_all())

    url = random_string(16)
    payload = {"video_url": url, "start_ts": "00:32", "end_ts": "00:51", "tags": []}
    resp = client.post(
        "/api/v1/clips/", content_type="application/json", data=json.dumps(payload)
    )

    clipnum_after = len(clip_repository.get_all())

    assert resp.status_code == 201
    assert clipnum_before + 1 == clipnum_after
    assert len(clip_repository.find_by_url(url)) == 1


def test_delete_clip(test_app, clip_repository: ClipRepository):
    client = test_app.test_client()

    # add clip
    url = random_string(16)
    payload = {"video_url": url, "start_ts": "00:32", "end_ts": "00:51", "tags": []}
    resp = client.post(
        "/api/v1/clips/", content_type="application/json", data=json.dumps(payload)
    )
    assert resp.status_code == 201

    # delete it
    c_id = clip_repository.find_by_url(url)[0].id
    assert clip_repository.find_by_id(c_id) is not None

    before_delete = len(clip_repository.get_all())
    resp = client.delete(f"/api/v1/clips/{c_id}")
    after_delete = len(clip_repository.get_all())

    assert resp.status_code == 200
    assert clip_repository.find_by_id(c_id) is None
    assert after_delete == before_delete - 1


def test_delete_clip_not_exists(test_app, test_database):
    client = test_app.test_client()
    resp = client.delete("/api/v1/clips/99999")
    assert resp.status_code == 404
    assert "could not find clip with id '99999'" in resp.json["message"]
    assert "failed" in resp.json["status"]


def test_get_single_clip(test_app, clip_repository: ClipRepository):
    client = test_app.test_client()
    c = clip_repository.save(make_random_clip())

    resp = client.get(f"/api/v1/clips/{c.id}")

    assert resp.status_code == 200
    assert resp.json["video_url"] == c.video_url


def test_get_clip_not_exists(test_app, test_database):
    client = test_app.test_client()
    resp = client.get("/api/v1/clips/99999")
    assert resp.status_code == 404
    assert "clip with id '99999' not found" in resp.json["message"]
    assert "failed" in resp.json["status"]


def test_create_multiple_clip(test_app, clip_repository: ClipRepository):
    client = test_app.test_client()

    url = random_string(16)
    p1 = {"video_url": url, "start_ts": "00:32", "end_ts": "00:41", "tags": []}
    p2 = {"video_url": url, "start_ts": "00:50", "end_ts": "00:56", "tags": []}
    p3 = {"video_url": url, "start_ts": "00:59", "end_ts": "01:51", "tags": []}

    payload = {"clips": [p1, p2, p3]}
    resp = client.post(
        "/api/v1/clips/multi/",
        content_type="application/json",
        data=json.dumps(payload),
    )
    assert resp.status_code == 201

    assert len(clip_repository.find_by_url(url)) == 3


def test_add_single_clip_with_tag(test_app, test_database):
    client = test_app.test_client()
    tag_repo = TagDB(test_database)
    clip_repo = ClipDB(test_database)

    url = random_string(32)
    tag1 = Tag(name=random_string(10))
    tag2 = Tag(name=random_string(10))

    tag_repo.save(tag1)
    tag_repo.save(tag2)

    payload = {
        "video_url": url,
        "start_ts": "00:32",
        "end_ts": "00:41",
        "tags": [tag1.name, tag2.name],
    }

    resp = client.post(
        "/api/v1/clips/", content_type="application/json", data=json.dumps(payload)
    )

    assert resp.status_code == 201
    clips = clip_repo.find_by_url(url)
    assert len(clips) == 1
    assert clips[0].start_timestamp.as_seconds() == 32
    assert clips[0].end_timestamp.as_seconds() == 41
    assert tag1 in clips[0].tags
    assert tag2 in clips[0].tags


def test_add_duplicate_clip_with_multi_adds_the_first_one(test_app, test_database):
    client = test_app.test_client()
    tag_repo = TagDB(test_database)
    clip_repo = ClipDB(test_database)

    url = random_string(32)
    tag1 = Tag(name=random_string(10))
    tag2 = Tag(name=random_string(10))

    tag_repo.save(tag1)
    tag_repo.save(tag2)

    # the two clips differ only in tags
    clip_1 = {
        "video_url": url,
        "start_ts": "01:23",
        "end_ts": "02:34",
        "tags": [tag1.name],
    }
    clip_2 = {
        "video_url": url,
        "start_ts": "01:23",
        "end_ts": "02:34",
        "tags": [tag2.name],
    }

    payload = {"clips": [clip_1, clip_2]}
    resp = client.post(
        "/api/v1/clips/multi/",
        content_type="application/json",
        data=json.dumps(payload),
    )

    assert resp.status_code == 400

    clips = clip_repo.find_by_url(url)
    assert len(clips) == 1

    clip = clips[0]
    assert len(clip.tags) == 1
    assert tag1 in clip.tags
    assert clip.start_timestamp.as_seconds() == 83
    assert clip.end_timestamp.as_seconds() == 154
