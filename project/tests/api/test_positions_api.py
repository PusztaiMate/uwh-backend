import json
from project.api.db_utils import add_new_position


def test_query_all_positions(test_app, test_database):
    client = test_app.test_client()

    resp = client.get("/api/v1/positions/")

    assert resp.status_code == 200
    assert len(resp.json) == 4
    assert {"name": "leghátsó", "id": 1} in resp.json
    assert {"name": "középső", "id": 2} in resp.json
    assert {"name": "első", "id": 3} in resp.json
    assert {"name": "szélső", "id": 4} in resp.json


def test_404_when_position_name_is_not_in_request(test_app, test_database):
    client = test_app.test_client()
    payload = json.dumps({})

    resp = client.post(
        "/api/v1/positions/", content_type="application/json", data=payload
    )

    assert resp.status_code == 400
    assert "position name is missing" in resp.json["message"]
