from typing import Any, Dict, List
from random import choice
from datetime import date, datetime

from sqlalchemy.exc import IntegrityError

from project import db
from project.api.models import (
    Club,
    Player,
    Training,
    MonthlyPayObligation,
    User,
    Position,
)
from project.services.training_service import TrainingRepository, TrainingService


FNAMES = "Jakab, Judit, József, Janka, János, Júlia".split(", ")
LNAMES = "Gipsz, Kovács, Szabó, Német, Magyar".split(", ")

training_service = TrainingService(TrainingRepository(db))


def add_player(name: str, training_strenght: int = 1) -> Player:
    fname, lname = name.split(" ")
    p = Player(fname=fname, lname=lname)
    p.training_strength = training_strenght
    db.session.add(p)
    db.session.commit()
    return p


def generate_random_players(number_of_players: int) -> List[Player]:
    players = []
    for _ in range(number_of_players):
        name = choice(FNAMES) + " " + choice(LNAMES)
        players.append(add_player(name))
    return players


def add_player_if_not_present(
    name: str,
    training_strength: int = 3,
    primary_pos: Position | None = None,
    seconondary_pos: Position | None = None,
) -> Player:
    fname, lname = name.split(" ")
    p = None
    for player in Player.query.all():
        if player.fname == fname and player.lname == lname:
            p = player
            break
    if not p:
        p = Player(fname=fname, lname=lname)
        p.training_strength = training_strength
        if primary_pos:
            p.primary_position = primary_pos.id
        if seconondary_pos:
            p.secondary_position = seconondary_pos.id
        db.session.add(p)
        db.session.commit()
    return p


def add_training_with_only_players_field(date: datetime, players: List[Player]):
    t = Training()
    t.date = date
    t.players = players
    db.session.add(t)
    db.session.commit()


def add_training(
    black_team: list[Player] | None = None,
    white_team: list[Player] | None = None,
    swimming: list[Player] | None = None,
    club_id: int | None = None,
    date_: datetime | None = None,
) -> Training:
    black_team = black_team or []
    white_team = white_team or []
    swimming = swimming or []

    black_team_ids = [p.id for p in black_team]
    white_team_ids = [p.id for p in white_team]
    swimming_ids = [p.id for p in swimming]
    t = Training(
        black_team=black_team_ids,
        white_team=white_team_ids,
        swimming_players=swimming_ids,
        club_id=club_id,
        date=date_,
    )
    db.session.add(t)
    db.session.commit()
    return t


def add_club(name: str, players: list[Player] | None = None) -> Club:
    players = players or []
    c = Club(name=name, player_ids=[p.id for p in players], training_ids=None)
    db.session.add(c)
    db.session.commit()
    return c


def add_club_if_not_present(name: str, players: List[Player] | None = None) -> Club:
    players = players or []
    c = Club.query.filter_by(name=name).first()
    if not c:
        c = Club(name=name, player_ids=[p.id for p in players], training_ids=None)
        db.session.add(c)
        db.session.commit()
    return c


def add_payment_obl(year: int, month: int, player_id: int, is_active: bool = True):
    new_obligation = MonthlyPayObligation(player_id, date(year, month, 1), is_active)
    add_and_commit_to_db(new_obligation)
    return new_obligation


def add_and_commit_to_db(obj: Any):
    db.session.add(obj)
    db.session.commit()


def get_number_of_obligations_in_db():
    return len(MonthlyPayObligation.query.all())


def get_all_players_in_db() -> List[Player]:
    return Player.query.all()


def is_obligation_in_db(obligation: MonthlyPayObligation) -> bool:
    return MonthlyPayObligation.query.get(obligation.id) is not None


def obligation_is_in_db(obligation: MonthlyPayObligation) -> bool:
    return is_obligation_in_db(obligation)


def num_of_obligations_in_month_in_db(year: int, month: int) -> int:
    _date = date(year, month, 1)
    return len(MonthlyPayObligation.query.filter_by(month=_date).all())


def add_user_if_not_present(username: str, password: str) -> User:
    user = User.query.filter_by(username=username).first()
    if not user:
        user = User(username=username, password=password)
        db.session.add(user)
        db.session.commit()
    return user


def add_goal_scorer_to_db(training_id: int, scorers: Dict[int, int]):
    training_service.add_scorer_data_to_training(training_id, scorers)


def query_all_goal_scorers() -> List[Dict[int, int]]:
    all_trainings = training_service.get_all_trainings()
    all_scorers = []
    for training in all_trainings:
        all_scorers.extend(training.get_scorers())
    return all_scorers


def num_of_goal_scorer_records() -> int:
    return len(query_all_goal_scorers())


def get_scorers_for_training(training_id: int) -> Dict[int, int]:
    training = training_service.get_training(training_id)
    if not training:
        return {}
    return training.get_scorers()


def add_new_position(name: str) -> Position:
    new_position = Position(name)

    try:
        db.session.add(new_position)
        db.session.commit()
    except IntegrityError:
        print("position already exists")

    return new_position
