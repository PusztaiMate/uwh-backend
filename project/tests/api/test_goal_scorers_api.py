import json

import pytest

from .db_utils import (
    add_goal_scorer_to_db,
    add_training,
    generate_random_players,
    get_scorers_for_training,
    num_of_goal_scorer_records,
)


def test_get_request_returns_all_the_saved_goal_scorers(test_app, test_database):
    client = test_app.test_client()

    scorer_number_before = num_of_goal_scorer_records()

    p1, p2, p3, p4 = generate_random_players(4)
    t1 = add_training([p1, p2], [p3, p4])
    t2 = add_training([p1, p3], [p2, p4])

    add_goal_scorer_to_db(
        t1.id,
        {
            p1.id: 1,
            p2.id: 2,
            p3.id: 3,
            p4.id: 4,
        },
    )

    resp = client.get("/api/v1/goal-scorers/")

    assert resp.status_code == 200

    json_data = resp.json
    assert len(json_data) == scorer_number_before + 4
    assert {"player_id": t2.id, "training_id": t1.id, "scored": 2} in json_data


def test_can_add_new_goal_scorer_with_put_request(test_app, test_database):
    client = test_app.test_client()

    p1, p2 = generate_random_players(2)
    t1 = add_training([p1], [p2])

    resp = client.put(
        "/api/v1/goal-scorers/",
        content_type="application/json",
        data=json.dumps({"training_id": t1.id, "scorers": {p1.id: 2, p2.id: 3}}),
    )

    assert resp.status_code == 200

    assert len(get_scorers_for_training(t1.id)) == 2


def test_put_can_be_used_to_update_scorers_for_training(test_app, test_database):
    client = test_app.test_client()

    p1, p2, p3, p4 = generate_random_players(4)
    t1 = add_training([p1, p2], [p3, p4])
    add_goal_scorer_to_db(
        t1.id,
        {
            p1.id: 1,
            p2.id: 2,
            p4.id: 2,
        },
    )

    assert len(get_scorers_for_training(t1.id)) == 3

    resp = client.put(
        "/api/v1/goal-scorers/",
        content_type="application/json",
        data=json.dumps(
            {"training_id": t1.id, "scorers": {p1.id: 1, p2.id: 3, p3.id: 2, p4.id: 2}}
        ),
    )

    assert resp.status_code == 200
    assert len(get_scorers_for_training(t1.id)) == 4
    assert {"player_id": p1.id, "scored": 1} in [
        {"player_id": id_, "scored": scored}
        for id_, scored in get_scorers_for_training(t1.id).items()
    ]


def test_get_can_be_used_to_query_for_selected_trainings(test_app, test_database):
    client = test_app.test_client()

    # two matches, with 2 and 3 goal scorers in each
    pb1, pb2, pw1, pw2 = generate_random_players(4)
    t1 = add_training(black_team=[pb1, pb2], white_team=[pw1, pw2])
    t2 = add_training(black_team=[pb1, pb2], white_team=[pw1, pw2])

    add_goal_scorer_to_db(
        t1.id,
        {
            pb1.id: 1,
            pw1.id: 3,
        },
    )
    add_goal_scorer_to_db(
        t2.id,
        {
            pb2.id: 2,
            pw1.id: 1,
            pw2.id: 2,
        },
    )

    resp_for_t1 = client.get(f"/api/v1/goal-scorers/training/{t1.id}/")

    assert resp_for_t1.status_code == 200
    assert "black" in resp_for_t1.json
    assert "white" in resp_for_t1.json
    assert len(resp_for_t1.json) == 2
    assert resp_for_t1.json["white"][str(pw1.id)] == 3

    resp_for_t2 = client.get(f"/api/v1/goal-scorers/training/{t2.id}/")

    assert resp_for_t2.status_code == 200
    assert len(resp_for_t2.json["black"]) == 2
    assert len(resp_for_t2.json["white"]) == 2
    assert resp_for_t2.json["black"][str(pb2.id)] == 2


def test_get_to_not_existing_training_score_data(test_app, test_database):
    client = test_app.test_client()

    resp = client.get("/api/v1/goal-scorers/training/12345/")

    assert resp.status_code == 404
    assert "training not found" in resp.json["message"]


def test_400_is_returned_for_invalid_goal_data(test_app, test_database):
    client = test_app.test_client()

    p1, p2 = generate_random_players(2)
    t1 = add_training([p1], [p2])

    resp = client.put(
        "/api/v1/goal-scorers/",
        content_type="application/json",
        data=json.dumps({"training_id": t1.id, "scorers": {p1.id: 1, p2.id: "b"}}),
    )

    assert resp.status_code == 400
    assert "invalid input data" in resp.json["message"]


@pytest.mark.skip("with the API change I'm not quite sure how to handle this")
def test_404_is_returned_if_player_or_training_not_found(test_app, test_database):
    client = test_app.test_client()

    p1, p2 = generate_random_players(2)
    t1 = add_training([p1], [p2])

    resp = client.put(
        "/api/v1/goal-scorers/",
        content_type="application/json",
        data=json.dumps({"training_id": t1.id, "scorers": {12345: 1}}),
    )

    assert resp.status_code == 404
    assert "training or player not found in db" in resp.json["message"]

    resp = client.put(
        "/api/v1/goal-scorers/",
        content_type="application/json",
        data=json.dumps({"training_id": 999, "scorers": {p1.id: 1}}),
    )

    assert resp.status_code == 404
    assert "training or player not found in db" in resp.json["message"]
