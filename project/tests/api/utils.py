import random
import string

from project.api.models.clip import Clip
from project.services.models.timestamp import TimeStamp


def random_string(length: int) -> str:
    return "".join(random.choices(string.ascii_letters, k=length))


def make_random_clip():
    url = random_string(8)
    start = random.randint(0, 9999)
    end = random.randint(10000, 19999)
    return Clip(
        url,
        [],
        TimeStamp.from_seconds(start).to_db_string(),
        TimeStamp.from_seconds(end).to_db_string(),
    )
