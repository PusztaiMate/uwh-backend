import json
from datetime import datetime
from typing import Any

import pytest
from flask import Flask

from project.tests.api.db_utils import (
    add_goal_scorer_to_db,
    add_training,
    generate_random_players,
)
from project.api.models import Training
from ..conftest import AdminTestClient


def assert_in_message(json_: Any | None, expected: str):
    assert expected in json_["message"]  # type: ignore


def assert_in_status(json_: Any | None, expected: str):
    assert expected in json_["status"]  # type: ignore


@pytest.mark.skip("authorization fucked up on pythons side")
def test_request_with_badly_formatted_json_is_rejected(
    admin_test_client: AdminTestClient,
):
    resp = admin_test_client.post(
        "/api/v1/trainings/",
        content_type="application/json",
        data="this is not a json",
    )

    assert resp.status_code == 400
    assert_in_message(resp.json, "couldn't parse json")
    assert_in_status(resp.json, "failed")


@pytest.mark.skip("authorization fucked up on pythons side")
def test_adding_trainings_with_wrong_content_type_fails(
    admin_test_client: AdminTestClient,
):
    resp = admin_test_client.post(
        "/api/v1/trainings/",
        data=json.dumps({"club_id": 0, "player_ids": [], "date": "2020-01-01T19:30"}),
        content_type="text/html",
    )
    assert resp.status_code == 400
    assert_in_message(resp.json, "wrong content type")
    assert_in_status(resp.json, "failed")


def test_all_trainings(admin_test_client: AdminTestClient):
    p1, p2, p3, p4, p5 = generate_random_players(number_of_players=5)
    add_training(black_team=[p1, p2], white_team=[p3, p5], swimming=[p4])
    add_training(black_team=[p1, p2], white_team=[p3, p4], swimming=[p5])
    resp = admin_test_client.get("/api/v1/trainings/")
    data = resp.json

    assert resp.status_code == 200
    assert data is not None
    assert len(data) == 2
    assert len(data[0]["players"]) == 5
    assert len(data[1]["players"]) == 5


def test_get_last_3_trainings(admin_test_client: AdminTestClient):
    p1, p2, p3, p4, p5, p6 = generate_random_players(number_of_players=6)
    # note: date are latest to earliest, so we don't accidentally rely on the order
    # which we added them to the db
    t1 = add_training([p1, p2], [p3, p4], [p5, p6], date_=datetime(2020, 5, 1))
    t2 = add_training([p2, p3], [p4, p5], [p6, p1], date_=datetime(2020, 4, 1))
    t3 = add_training([p3, p4], [p5, p6], [], date_=datetime(2020, 3, 1))
    t4 = add_training([p4, p5], [p1, p2], [], date_=datetime(2020, 2, 1))
    t5 = add_training([p5, p6], [p2, p3], [], date_=datetime(2020, 1, 1))

    resp = admin_test_client.get("/api/v1/trainings/?limit=3")
    data = resp.json

    assert data is not None
    assert len(data) == 3
    ids_in_resp = [t["id"] for t in data]
    assert t1.id not in ids_in_resp
    assert t2.id not in ids_in_resp
    assert t3.id in ids_in_resp
    assert t4.id in ids_in_resp
    assert t5.id in ids_in_resp


def test_get_single_training(admin_test_client: AdminTestClient):
    date = datetime(2020, 1, 1)
    p1, p2 = generate_random_players(2)
    t1 = add_training(black_team=[p1], white_team=[p2], date_=date)

    resp = admin_test_client.get(f"/api/v1/trainings/{t1.id}/")
    data = resp.json

    assert resp.status_code == 200
    assert data is not None
    assert t1.id == data["id"]
    assert t1.date.isoformat() == data["date"]
    assert p1.id in t1.black_team
    assert p2.id in t1.white_team


@pytest.mark.parametrize(
    ["id", "expected_sc", "expected_msg"],
    [
        ("invalid", 405, None),
        ("999", 404, "training with id 999 not found"),
    ],
)
def test_single_training_query_failing_cases(
    admin_test_client: AdminTestClient, id: str, expected_sc: int, expected_msg: str
):
    resp = admin_test_client.get(f"/api/v1/trainings/{id}/")

    assert resp.status_code == expected_sc
    if expected_msg is not None:
        assert_in_message(resp.json, expected_msg)
        assert_in_status(resp.json, "failed")


@pytest.mark.skip("authorization fucked up on pythons side")
def test_players_can_be_separated_into_teams(admin_test_client: AdminTestClient):
    generate_random_players(4)
    resp = admin_test_client.post(
        "/api/v1/trainings/",
        content_type="application/json",
        data=json.dumps(
            {"date": "2020.01.02T19:30:00", "black_team": [1, 2], "white_team": [3, 4]}
        ),
    )

    assert resp.status_code == 201
    assert_in_message(resp.json, "training successfully added")
    assert_in_status(resp.json, "success")
    assert resp.json and "id" in resp.json

    training_in_db = Training.query.get(resp.json["id"])

    assert len(training_in_db.players) == 4


@pytest.mark.skip(reason="club id related sqlalchemy error")
@pytest.mark.parametrize(
    ["black_ids", "white_ids", "swimming_ids", "exp_player_num"],
    [([1, 2, 3], [4, 5, 6], [], 6), ([1, 2, 3], [], [4, 5, 6, 7], 7), ([], [], [], 0)],
)
def test_different_successfull_add_scenarios(
    black_ids: list[int],
    white_ids: list[int],
    swimming_ids: list[int],
    exp_player_num: int,
    admin_test_client: AdminTestClient,
):
    generate_random_players(10)

    resp = admin_test_client.post(
        "/api/v1/trainings/",
        content_type="application/json",
        data=json.dumps(
            {
                "black_team": black_ids,
                "white_team": white_ids,
                "swimming_players": swimming_ids,
            }
        ),
    )

    assert resp.status_code == 201
    assert resp.json
    training_in_db = Training.query.get(resp.json["id"])
    assert len(training_in_db.players) == exp_player_num
    assert len(training_in_db.black_team) == len(black_ids)
    assert len(training_in_db.white_team) == len(white_ids)


@pytest.mark.skip("authorization fucked up on pythons side")
def test_put_can_be_used_to_update_training(admin_test_client: AdminTestClient):
    p1, p2, p3 = generate_random_players(3)
    t = add_training(black_team=[p1], white_team=[p2])

    training_in_db = Training.query.get(t.id)

    assert len(training_in_db.black_team) == 1
    assert len(training_in_db.white_team) == 1
    assert len(training_in_db.swimming_players) == 0

    resp = admin_test_client.put(
        f"/api/v1/trainings/{t.id}/",
        content_type="application/json",
        data=json.dumps(
            {
                "black_team": [],
                "white_team": [p1.id, p2.id, p3.id],
                "swimming_players": [],
            }
        ),
    )

    assert resp.json
    assert t.id == resp.json["id"]
    assert_in_message(resp.json, "successfully updated")
    assert_in_status(resp.json, "success")

    # training in the db is updated
    training_in_db = Training.query.get(t.id)

    assert len(training_in_db.black_team) == 0
    assert len(training_in_db.white_team) == 3
    assert len(training_in_db.swimming_players) == 0


def test_training_data_is_enriched_with_scorer_info(admin_test_client: AdminTestClient):
    pb1, pb2, pw1, pw2 = generate_random_players(4)
    t = add_training(black_team=[pb1, pb2], white_team=[pw1, pw2])
    add_goal_scorer_to_db(
        t.id,
        {
            pb1.id: 2,
            pw1.id: 1,
            pw2.id: 3,
        },
    )

    resp = admin_test_client.get(
        f"/api/v1/trainings/{t.id}/",
        content_type="application/json",
    )

    assert resp.status_code == 200
    assert resp.json
    json_data = resp.json
    assert len(json_data["black_scorers"]) == 1
    assert json_data["black_scorers"][str(pb1.id)] == 2
    assert len(json_data["white_scorers"]) == 2
    assert json_data["white_scorers"][str(pw1.id)] == 1
    assert json_data["white_scorers"][str(pw2.id)] == 3
    assert json_data["white_score"] == 1 + 3
    assert json_data["black_score"] == 2


def test_get_returns_with_empty_score_related_fields_even_when_score_data_missing(
    admin_test_client: AdminTestClient,
):
    pb1, pb2, pw1, pw2 = generate_random_players(4)
    t = add_training(black_team=[pb1, pb2], white_team=[pw1, pw2])

    resp = admin_test_client.get(
        f"/api/v1/trainings/{t.id}/",
        content_type="application/json",
    )

    assert resp.status_code == 200
    assert resp.json
    json_data = resp.json
    assert len(json_data["black_scorers"]) == 0
    assert len(json_data["white_scorers"]) == 0
    assert json_data["white_score"] == 0
    assert json_data["black_score"] == 0


def test_training_api_works_even_if_no_color_or_score_data_present(
    admin_test_client: AdminTestClient, test_database
):
    p1, p2, p3, p4 = generate_random_players(4)
    t = Training()
    t.players = [p1, p2, p3, p4]
    test_database.session.add(t)
    test_database.session.commit()

    resp = admin_test_client.get(
        f"/api/v1/trainings/{t.id}/",
        content_type="application/json",
    )

    assert resp.status_code == 200
    assert resp.json
    json_data = resp.json
    assert "black_scorers" in json_data
    assert "white_scorers" in json_data
    assert "black_score" in json_data
    assert "white_score" in json_data


# TODO: create test_training function scoped fixture
@pytest.mark.skip("only duplicated training is allowed to be deleted for now")
def test_delete_training_works(admin_test_client: AdminTestClient, test_database):
    p1, p2, p3, p4 = generate_random_players(4)
    t = Training()
    t.players = [p1, p2, p3, p4]
    test_database.session.add(t)
    test_database.session.commit()
    resp = admin_test_client.get(
        f"/api/v1/trainings/{t.id}/",
    )
    assert resp.status_code == 200

    resp = admin_test_client.delete(
        f"/api/v1/trainings/{t.id}/",
    )
    assert resp.status_code == 200

    resp = admin_test_client.get(
        f"/api/v1/trainings/{t.id}/",
    )
    assert resp.status_code == 404
