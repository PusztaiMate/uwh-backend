import json
from datetime import datetime, timedelta, timezone

import jwt

from .db_utils import add_user_if_not_present


def test_existing_user_can_be_authenticated_successfully(test_app, test_database):
    test_app.config["SECRET_KEY"] = "secret!"
    client = test_app.test_client()
    add_user_if_not_present("TestUser", "1234")

    resp = client.post(
        "/api/v1/users/login/",
        content_type="application/json",
        data=json.dumps({"username": "TestUser", "password": "1234"}),
    )
    data = resp.get_json()

    assert resp.status_code == 200
    assert "success" in data["status"]
    assert "Successfully logged in" in data["message"]

    assert "token" in data
    decoded_token = jwt.decode(data["token"], "secret!", algorithms=["HS256"])
    assert "TestUser" in decoded_token["sub"]
    assert "iat" in decoded_token
    assert "exp" in decoded_token


def test_not_existing_username_results_authentication_failure(test_app, test_database):
    test_app.config["SECRET_KEY"] = "secret!"
    client = test_app.test_client()
    add_user_if_not_present("TestUser", "1234")

    resp = client.post(
        "/api/v1/users/login/",
        content_type="application/json",
        data=json.dumps({"username": "NotTheRightUsername", "password": "1234"}),
    )
    data = resp.get_json()

    assert resp.status_code == 401
    assert "fail" in data["status"]
    assert "Invalid credentials" in data["message"]


def test_missing_username_result_authentication_failure(test_app, test_database):
    test_app.config["SECRET_KEY"] = "secret!"
    client = test_app.test_client()
    add_user_if_not_present("TestUser", "1234")

    resp = client.post(
        "/api/v1/users/login/",
        content_type="application/json",
        data=json.dumps({"password": "1234"}),
    )
    data = resp.get_json()

    assert resp.status_code == 401
    assert "fail" in data["status"]
    assert "Invalid credentials" in data["message"]


def test_missing_password_results_authentication_failure(test_app, test_database):
    test_app.config["SECRET_KEY"] = "secret!"
    client = test_app.test_client()
    add_user_if_not_present("TestUser", "1234")

    resp = client.post(
        "/api/v1/users/login/",
        content_type="application/json",
        data=json.dumps({"username": "TestUser"}),
    )
    data = resp.get_json()

    assert resp.status_code == 401
    assert "fail" in data["status"]
    assert "Invalid credentials" in data["message"]


def test_wrong_password_results_authentication_failure(test_app, test_database):
    test_app.config["SECRET_KEY"] = "secret!"
    client = test_app.test_client()
    add_user_if_not_present("TestUser", "1234")

    resp = client.post(
        "/api/v1/users/login/",
        content_type="application/json",
        data=json.dumps({"username": "TestUser", "password": "not_the_right_password"}),
    )
    data = resp.get_json()

    assert resp.status_code == 401
    assert "fail" in data["status"]
    assert "Invalid credentials" in data["message"]


def test_issued_time_is_present_in_token(test_app, test_database):
    test_app.config["SECRET_KEY"] = "secret!"
    client = test_app.test_client()
    add_user_if_not_present("TestUser", "1234")

    resp = client.post(
        "/api/v1/users/login/",
        content_type="application/json",
        data=json.dumps({"username": "TestUser", "password": "1234"}),
    )
    data = resp.get_json()

    assert resp.status_code == 200
    decoded_token = jwt.decode(data["token"], "secret!", algorithms=["HS256"])
    iat = datetime.fromtimestamp(decoded_token["iat"], tz=timezone.utc)
    assert (datetime.now(tz=timezone.utc) - iat) < timedelta(seconds=1)


def test_52_weeks_expiration_time_is_issued(test_app, test_database):
    test_app.config["SECRET_KEY"] = "secret!"
    client = test_app.test_client()
    add_user_if_not_present("TestUser", "1234")

    resp = client.post(
        "/api/v1/users/login/",
        content_type="application/json",
        data=json.dumps({"username": "TestUser", "password": "1234"}),
    )
    data = resp.get_json()

    assert resp.status_code == 200
    decoded_token = jwt.decode(data["token"], "secret!", algorithms=["HS256"])
    exp = datetime.fromtimestamp(decoded_token["exp"], tz=timezone.utc)
    assert (exp - datetime.now(timezone.utc)) > timedelta(
        weeks=51, days=6, hours=23, minutes=59, seconds=30
    )
    assert (exp - datetime.now(timezone.utc)) < timedelta(weeks=52)
