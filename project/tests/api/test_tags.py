import json
from project.api.models.clip_tags import Tag
from project.repositories.tag_repository import TagDB
from project.tests.api.utils import random_string


def test_create_new_tag(test_app, test_database):
    client = test_app.test_client()
    repository = TagDB(test_database)
    tag_name = random_string(8)

    tags_num_before = len(repository.get_all())

    resp = client.post(
        "/api/v1/tags/",
        content_type="application/json",
        data=json.dumps({"name": tag_name}),
    )

    assert resp.status_code == 201
    tags_num_after = len(repository.get_all())
    assert tags_num_before + 1 == tags_num_after
    json_data = resp.json
    assert "tag successfully added" in json_data["message"]
    assert "success" in json_data["status"]


def test_get_one_tag(test_app, test_database):
    client = test_app.test_client()
    repository = TagDB(test_database)
    tag_name = random_string(8)
    tag = repository.save(Tag(tag_name))

    resp = client.get(f"/api/v1/tags/{tag.id}/")

    assert resp.status_code == 200
    assert resp.json["id"] == tag.id
    assert resp.json["name"] == tag_name


def test_get_one_tag_not_found(test_app, test_database):
    client = test_app.test_client()

    resp = client.get("/api/v1/tags/12345/")

    assert resp.status_code == 404
    assert "tag with id '12345' not found" in resp.json["message"]
    assert "failed" in resp.json["status"]


def test_get_one_tag_invalid_id(test_app, test_database):
    client = test_app.test_client()

    resp = client.get("/api/v1/tags/abc/")

    assert resp.status_code == 404
    assert "requested page not found" in resp.json["message"]
    assert "failed" in resp.json["status"]


def test_get_all_tags(test_app, test_database):
    client = test_app.test_client()
    repository = TagDB(test_database)
    tag_name_1 = random_string(8)
    tag_1 = repository.save(Tag(tag_name_1))
    tag_name_2 = random_string(8)
    tag_2 = repository.save(Tag(tag_name_2))

    resp = client.get("/api/v1/tags/")

    assert resp.status_code == 200
    assert tag_1.to_dict() in resp.json["tags"]
    assert tag_2.to_dict() in resp.json["tags"]


def test_delete_one_clip(test_app, test_database):
    client = test_app.test_client()
    repository = TagDB(test_database)
    tag_name_1 = random_string(8)
    tag = repository.save(Tag(tag_name_1))

    resp = client.delete(f"/api/v1/tags/{tag.id}/")

    assert resp.status_code == 200
    assert "successfully deleted tag" in resp.json["message"]
    assert "success" in resp.json["status"]
