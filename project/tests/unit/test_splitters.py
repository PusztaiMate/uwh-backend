from typing import Iterable
import pytest

from project.api.lib.team_selector import BalancedSplitter, UwhPlayer


@pytest.fixture
def perfectly_balanced():
    return [
        UwhPlayer(["forward"], 1, 2),
        UwhPlayer(["forward"], 2, 4),
        UwhPlayer(["forward"], 3, 7),
        UwhPlayer(["forward"], 4, 9),
    ]


@pytest.fixture
def slight_offbalance():
    return [
        UwhPlayer(["forward"], 1, 3),
        UwhPlayer(["forward"], 2, 4),
        UwhPlayer(["forward"], 3, 7),
        UwhPlayer(["forward"], 4, 9),
    ]


@pytest.fixture
def big_offbalance():
    return [
        UwhPlayer(["forward"], 1, 4),
        UwhPlayer(["forward"], 2, 5),
        UwhPlayer(["forward"], 3, 5),
        UwhPlayer(["forward"], 4, 9),
    ]


@pytest.fixture
def well_balanced_6():
    return [
        UwhPlayer(["forward"], 5, 3),
        UwhPlayer(["forward"], 1, 4),
        UwhPlayer(["forward"], 2, 5),
        UwhPlayer(["forward"], 3, 6),
        UwhPlayer(["forward"], 6, 8),
        UwhPlayer(["forward"], 4, 10),
    ]


@pytest.fixture
def well_balanced_5():
    return [
        UwhPlayer(["forward"], 1, 3),
        UwhPlayer(["forward"], 2, 4),
        UwhPlayer(["forward"], 3, 5),
        UwhPlayer(["forward"], 4, 6),
        UwhPlayer(["forward"], 5, 7),
    ]


@pytest.fixture
def hard_balanced_5():
    return [
        UwhPlayer(["forward"], 1, 3),
        UwhPlayer(["forward"], 3, 5),
        UwhPlayer(["forward"], 4, 8),
        UwhPlayer(["forward"], 5, 7),
        UwhPlayer(["forward"], 2, 9),
    ]


def test_perfectly_balanced(perfectly_balanced):
    splitter = BalancedSplitter()
    mate, kata, ada, andris = perfectly_balanced

    group_1, group_2 = splitter.get(perfectly_balanced)

    assert_groups_are([mate, andris], [ada, kata], group_1, group_2)


def test_slightly_offbalance(slight_offbalance):
    splitter = BalancedSplitter()
    mate, kata, ada, andris = slight_offbalance

    group_1, group_2 = splitter.get(slight_offbalance)

    assert_groups_are([mate, andris], [ada, kata], group_1, group_2)


def test_big_offbalance(big_offbalance):
    splitter = BalancedSplitter()
    mate, kata, ada, andris = big_offbalance

    group_1, group_2 = splitter.get(big_offbalance)

    assert_groups_are([mate, andris], [ada, kata], group_1, group_2)


def test_6_players(well_balanced_6):
    splitter = BalancedSplitter()
    edi, mate, kata, ada, bence, andris = well_balanced_6

    group_1, group_2 = splitter.get(well_balanced_6)

    assert_groups_are([edi, kata, andris], [ada, bence, mate], group_1, group_2)


def test_5_players(well_balanced_5):
    splitter = BalancedSplitter()
    edi, mate, kata, ada, bence = well_balanced_5

    group_1, group_2 = splitter.get(well_balanced_5)

    assert_groups_are([edi, bence], [ada, kata, mate], group_1, group_2)


def test_hard_balance_5_players(hard_balanced_5):
    splitter = BalancedSplitter()
    edi, kata, ada, bence, mate = hard_balanced_5

    group_1, group_2 = splitter.get(hard_balanced_5)

    assert_groups_are([edi, mate, bence], [kata, ada], group_1, group_2)


def is_player_in_group(player: UwhPlayer, group: Iterable[UwhPlayer]) -> bool:
    for p in group:
        if p.id == player.id:
            return True
    return False


def assert_groups_are(exp_group_1, exp_group_2, real_group_1, real_group_2):
    if len(set(exp_group_1) - set(real_group_1)) == 0:
        assert (
            len(set(exp_group_2) - set(real_group_2)) == 0
        ), f"{exp_group_2} != {real_group_2}"
    else:
        assert (
            len(set(exp_group_1) - set(real_group_2)) == 0
        ), f"{exp_group_1} != {real_group_2}"
        assert (
            len(set(exp_group_2) - set(real_group_1)) == 0
        ), f"{exp_group_2} != {real_group_1}"


def find_player_by_name(player_name, group):
    for p in group:
        if p.name == player_name:
            return p
    return None
