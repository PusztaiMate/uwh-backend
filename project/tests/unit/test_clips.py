from threading import Lock
from typing import List, Optional

import pytest
from project.api.models.clip import Clip
from project.api.models.clip_tags import Tag
from project.repositories.clips_repository import ClipRepository
from project.repositories.tag_repository import TagRepository
from project.services.clips import (
    ClipService,
    convert_clip_to_clip_dto,
    is_almost_same_clip,
    ClipAlreadyExists,
)
from project.services.models.clip_dto import ClipDTO
from project.services.models.timestamp import (
    TimeStamp,
    InvalidTimeStamp,
    TimeStampParsingError,
)
from project.services.tag import TagDTO, TagService
from project.tests.api.utils import random_string
from project.tests.unit.test_tag import InMemoryTagDB


def make_clip_dto(url: str, tags: List[TagDTO], ts_start: str, ts_end: str) -> ClipDTO:
    return ClipDTO(
        url,
        tags,
        TimeStamp.from_min_sec_string(ts_start),
        TimeStamp.from_min_sec_string(ts_end),
    )


def make_clip(url: str, tags: List[Tag], ts_start: str, ts_end: str) -> Clip:
    return Clip(url, tags, ts_start, ts_end)


class InMemoryClipDB(ClipRepository):
    def __init__(self) -> None:
        self.current_id = 1
        self.id_lock = Lock()
        self._clips = []

    def get_all(self) -> List[Clip]:
        return self._clips

    def save(self, clip: Clip) -> Clip | None:
        clip.id = self._get_id_and_increment()
        self._clips.append(clip)
        return clip

    def find_by_id(self, id: int) -> Optional[Clip]:
        for clip in self._clips:
            if clip.id == id:
                return clip
        return None

    def delete(self, id: int) -> None:
        c = self.find_by_id(id)
        if c is None:
            return
        self._clips.remove(c)

    def find_by_url(self, url: str) -> List[Clip]:
        return [c for c in self._clips if c.video_url == url]

    def _get_id_and_increment(self):
        with self.id_lock:
            curr = self.current_id
            self.current_id += 1
            return curr


class TestClipService:
    @pytest.fixture(scope="function")
    def clip_db(self) -> ClipRepository:
        return InMemoryClipDB()

    @pytest.fixture(scope="function")
    def tag_db(self) -> TagRepository:
        return InMemoryTagDB()

    @pytest.fixture(scope="function")
    def tag_service(self, tag_db):
        return TagService(tag_db)

    @pytest.fixture(scope="function")
    def service(self, tag_service, clip_db) -> ClipService:
        return ClipService(tag_service, clip_db)

    def test_get_all(self, service: ClipService, clip_db: ClipRepository):
        # the db is empty
        clips = service.get_all()
        assert len(clips) == 0

        # add a single clip
        clip_1 = make_clip("url", [], "00:10", "00:20")
        clip_db.save(clip_1)
        clips = service.get_all()
        assert len(clips) == 1
        assert convert_clip_to_clip_dto(clip_1) in clips

        # add another clip
        clip_2 = make_clip("url2", [], "00:10", "00:20")
        clip_db.save(clip_2)
        clips = service.get_all()
        assert len(clips) == 2
        assert convert_clip_to_clip_dto(clip_1) in clips
        assert convert_clip_to_clip_dto(clip_2) in clips

    def test_get_by_id(self, service: ClipService, clip_db: ClipRepository):
        clip_1 = clip_db.save(make_clip("url", [], "00:10", "00:20"))

        clip_2 = service.get(clip_1.id)

        assert clip_2 is not None
        assert clip_1.id == clip_2.id
        assert clip_1.video_url == clip_2.video_url
        assert clip_1.start_timestamp == clip_2.start_ts
        assert clip_1.end_timestamp == clip_2.end_ts

        clip_not_exists = service.get(id=999)
        assert clip_not_exists is None

    def test_delete(self, service: ClipService, clip_db: ClipRepository):
        clip_1 = clip_db.save(make_clip("url", [], "00:10", "00:20"))

        clip = clip_db.find_by_id(clip_1.id)
        assert clip is not None

        service.delete(clip_1.id)
        assert clip_db.find_by_id(1) is None

    @pytest.mark.skip("don't really know how to make this a unittest right now")
    def test_filter_by_tags(
        self, service: ClipService, tag_db: TagRepository, clip_db: ClipRepository
    ):
        tag_1 = tag_db.save(Tag(random_string(6)))
        tag_2 = tag_db.save(Tag(random_string(6)))
        tag_3 = tag_db.save(Tag(random_string(6)))

        clip_db.save(make_clip("url", [tag_1], "00:10", "00:20"))
        clip_db.save(make_clip("url", [tag_1, tag_2], "01:10", "01:20"))
        clip_db.save(make_clip("url", [tag_2, tag_3], "02:10", "02:20"))

        clips_with_tag1 = service.get_clips_with_tags(tag_names=[tag_1.name])
        clips_with_tag2 = service.get_clips_with_tags(tag_names=[tag_2.name])
        clips_with_tag2_tag3 = service.get_clips_with_tags(
            tag_names=[tag_2.name, tag_3.name]
        )

        assert len(clips_with_tag1) == 2
        assert len(clips_with_tag2) == 2
        assert len(clips_with_tag2_tag3) == 1

    def test_can_save_clip(self, service: ClipService, clip_db: ClipRepository):
        assert len(clip_db.get_all()) == 0

        clip_1 = make_clip_dto("url", [], "00:10", "00:20")
        clip_2 = service.save(clip_1)

        assert len(clip_db.get_all()) == 1
        assert clip_2.id == 1
        assert clip_1 == clip_2

    @pytest.mark.parametrize(
        "c1,c2",
        [
            (
                make_clip_dto("url", [], "00:10", "00:20"),
                make_clip_dto("url", [], "00:10", "00:20"),
            ),
            (
                make_clip_dto("url", [], "00:10", "00:21"),
                make_clip_dto("url", [], "00:10", "00:20"),
            ),
            (
                make_clip_dto("url", [], "00:11", "00:20"),
                make_clip_dto("url", [], "00:10", "00:20"),
            ),
            (
                make_clip_dto("url", [], "00:10", "00:20"),
                make_clip_dto("url", [], "00:11", "00:20"),
            ),
            (
                make_clip_dto("url", [], "00:10", "00:20"),
                make_clip_dto("url", [], "00:10", "00:21"),
            ),
        ],
    )
    def test_duplicate_clips_are_rejected(
        self,
        service: ClipService,
        clip_db: ClipRepository,
        c1: ClipDTO,
        c2: ClipDTO,
    ):
        assert len(clip_db.get_all()) == 0

        service.save(c1)
        assert len(clip_db.get_all()) == 1

        with pytest.raises(ClipAlreadyExists):
            service.save(c2)

        assert len(clip_db.get_all()) == 1


class TestTimeStamp:
    def test_can_create_from_string(self):
        s = "01:12"
        ts = TimeStamp.from_min_sec_string(s)

        assert ts.min == 1
        assert ts.sec == 12

    @pytest.mark.parametrize(
        "input_string", [("12"), (":12"), ("12:"), ("12.34"), ("12:34:56")]
    )
    def test_throws_TimeStampParsingError_if_format_is_wrong(self, input_string: str):
        with pytest.raises(TimeStampParsingError) as e:
            TimeStamp.from_min_sec_string(input_string)

        assert "could not parse input" in str(e)

    @pytest.mark.parametrize("m,sec", [(10, 60), (100, -1), (-1, 10)])
    def test_throw_InvalidTimeStamp_when_min_or_sec_invalid(self, m: int, sec: int):
        with pytest.raises(InvalidTimeStamp) as e:
            TimeStamp(m, sec)
        assert "invalid value" in str(e)

    @pytest.mark.parametrize(
        "inp_str_1,inp_str_2,is_equal",
        [
            ("00:00", "00:00", True),
            ("00:01", "00:01", True),
            ("00:01", "00:00", False),
            ("01:00", "01:00", True),
            ("01:00", "00:00", False),
            ("01:00", "01:00", True),
            ("01:01", "01:00", False),
        ],
    )
    def test_equals_from_string(self, inp_str_1, inp_str_2, is_equal):
        ts1 = TimeStamp.from_min_sec_string(inp_str_1)
        ts2 = TimeStamp.from_min_sec_string(inp_str_2)

        assert (ts1 == ts2) is is_equal

    @pytest.mark.parametrize(
        "inp_str_1,inp_str_2,is_less",
        [
            ("00:00", "00:00", False),
            ("00:00", "00:01", True),
            ("00:01", "00:00", False),
            ("00:01", "00:01", False),
            ("00:01", "00:02", True),
            ("01:00", "00:10", False),
            ("00:10", "01:00", True),
            ("01:10", "01:00", False),
            ("01:00", "01:00", False),
        ],
    )
    def test_less_from_string(self, inp_str_1, inp_str_2, is_less):
        ts1 = TimeStamp.from_min_sec_string(inp_str_1)
        ts2 = TimeStamp.from_min_sec_string(inp_str_2)

        assert (ts1 < ts2) is is_less

    @pytest.mark.parametrize(
        "ts_str,expected", [("00:00", 0), ("00:12", 12), ("12:00", 720), ("12:34", 754)]
    )
    def test_can_convert_to_seconds(self, ts_str, expected):
        ts = TimeStamp.from_min_sec_string(ts_str)
        actual = ts.as_seconds()
        assert actual == expected

    @pytest.mark.parametrize(
        "seconds,expected_ts",
        [
            (0, TimeStamp(0, 0)),
            (1, TimeStamp(0, 1)),
            (60, TimeStamp(1, 0)),
            (83, TimeStamp(1, 23)),
            (754, TimeStamp(12, 34)),
        ],
    )
    def test_from_seconds_work(self, seconds, expected_ts):
        actual_ts = TimeStamp.from_seconds(seconds)

        assert actual_ts == expected_ts

    @pytest.mark.parametrize(
        "ts_str_1,ts_str_2,delta",
        [
            ("01:10", "01:20", 10),
            ("00:00", "00:00", 0),
            ("00:10", "01:20", 70),
            ("01:10", "00:20", 50),
            ("12:34", "00:00", 754),
        ],
    )
    def test_substraction_calculates_timedelta(
        self, ts_str_1: str, ts_str_2: str, delta: int
    ):
        ts1 = TimeStamp.from_min_sec_string(ts_str_1)
        ts2 = TimeStamp.from_min_sec_string(ts_str_2)

        assert (ts1 - ts2) == (ts2 - ts1)
        assert (ts1 - ts2) == delta


@pytest.mark.parametrize(
    "c1,c2,delta,expected",
    [
        (
            make_clip_dto("url", [], "01:10", "01:20"),
            make_clip_dto("url", [], "01:10", "01:20"),
            0,
            True,
        ),
        (
            make_clip_dto("url", [], "01:10", "01:21"),
            make_clip_dto("url", [], "01:10", "01:20"),
            0,
            False,
        ),
        (
            make_clip_dto("url", [], "01:11", "01:20"),
            make_clip_dto("url", [], "01:10", "01:20"),
            0,
            False,
        ),
        (
            make_clip_dto("url", [], "01:10", "01:21"),
            make_clip_dto("url", [], "01:10", "01:20"),
            1,
            True,
        ),
        (
            make_clip_dto("url", [], "01:11", "01:20"),
            make_clip_dto("url", [], "01:10", "01:20"),
            1,
            True,
        ),
        (
            make_clip_dto("url", [], "01:11", "01:21"),
            make_clip_dto("url", [], "01:10", "01:20"),
            1,
            True,
        ),
        (
            make_clip_dto("url", [], "01:12", "01:20"),
            make_clip_dto("url", [], "01:10", "01:20"),
            1,
            False,
        ),
        (
            make_clip_dto("url", [], "01:10", "01:22"),
            make_clip_dto("url", [], "01:10", "01:20"),
            1,
            False,
        ),
    ],
)
def test_is_almost_same_clip(c1, c2, delta, expected):
    actual = is_almost_same_clip(c1, c2, delta)
    assert actual == expected
