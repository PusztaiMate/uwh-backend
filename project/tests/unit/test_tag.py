from typing import List, Optional

import pytest
from project.api.models.clip_tags import Tag
from project.repositories.tag_repository import TagRepository
from project.services.tag import TagDTO, TagService
from project.tests.api.utils import random_string


class InMemoryTagDB(TagRepository):
    id = 0

    def __init__(self):
        self._tags = []

    def get_all(self) -> List[Tag]:
        return self._tags

    def find_by_id(self, id: int) -> Optional[Tag]:
        for t in self._tags:
            if t.id == id:
                return t
        return None

    def save(self, tag: Tag) -> Tag:
        tag.id = InMemoryTagDB._get_id_and_increment()
        self._tags.append(tag)
        return tag

    def delete(self, id: int) -> None:
        t = self.find_by_id(id)
        if t is None:
            return
        self._tags.remove(t)

    def find_by_name(self, name: str) -> Optional[Tag]:
        for tag in self._tags:
            if tag.name == name:
                return tag
        return None

    @staticmethod
    def _get_id_and_increment():
        _id = InMemoryTagDB.id
        InMemoryTagDB.id += 1
        return _id


class TestTagService:
    @pytest.fixture(scope="function")
    def in_memory_db(self) -> TagRepository:
        return InMemoryTagDB()

    @pytest.fixture(scope="function")
    def service(self, in_memory_db) -> TagService:
        return TagService(in_memory_db)

    def test_get_all(self, service: TagService):
        tags = service.get_all()
        assert len(tags) == 0

        tag_dto = TagDTO(random_string(6), [], 0)
        service.save(tag_dto=tag_dto)

        tags = service.get_all()
        assert len(tags) == 1

    def test_get_one_tag(self, service: TagService, in_memory_db: TagRepository):
        tag_1 = in_memory_db.save(Tag(random_string(6)))

        tag_2 = service.get(tag_1.id)
        assert tag_2 is not None

        assert tag_2.id == tag_1.id
        assert tag_2.name == tag_1.name

    def test_delete_one_tag(self, service: TagService, in_memory_db: TagRepository):
        tag_1 = in_memory_db.save(Tag(random_string(6)))

        assert in_memory_db.find_by_id(tag_1.id) is not None

        service.delete(tag_1.id)

        assert in_memory_db.find_by_id(tag_1.id) is None

    def test_save_tag(self, service: TagService, in_memory_db: TagRepository):
        tag_num_before = len(in_memory_db.get_all())

        tag_name = random_string(6)
        service.save(TagDTO(tag_name, []))

        tag_num_after = len(in_memory_db.get_all())
        assert tag_num_after == tag_num_before + 1
        assert service.find_by_name(tag_name) is not None

    def test_find_tag_by_name(self, service: TagService, in_memory_db: TagRepository):
        tag_name = random_string(6)
        tag_1 = service.find_by_name(tag_name)
        assert tag_1 is None

        in_memory_db.save(Tag(tag_name))
        tag_1 = service.find_by_name(tag_name)

        assert tag_1 is not None
        assert tag_1.name == tag_name
