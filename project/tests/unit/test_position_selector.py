from project.api.lib.team_selector.position_selector import StrongestFillsSelector
from typing import List

import pytest

from project.api.lib.team_selector.models import Team, UwhPlayer
from project.api.lib.team_selector import (
    GoaliesCentersFirst,
    Position,
    PositionSelector,
    find_player_by_id,
)
from project.tests.unit.position_test_data import (
    get_12_players_not_enough_wings,
    get_12_players_perfect,
    get_12_players_perfect_with_secondaries,
)


def test_perfect_12_with_no_secondaries():
    players = get_12_players_perfect()
    strategy = GoaliesCentersFirst()
    selector = PositionSelector(strategy=strategy)

    selection = selector.split_players(players)

    assert len(selection[Position.FORWARD]) == 4
    assert len(selection[Position.WING]) == 4
    assert len(selection[Position.GOALIE]) == 2
    assert len(selection[Position.CENTER]) == 2


def test_perfect_with_secondaries():
    players = get_12_players_perfect_with_secondaries()
    strategy = GoaliesCentersFirst()
    selector = PositionSelector(strategy=strategy)

    selection = selector.split_players(players)

    assert len(selection[Position.FORWARD]) == 4
    assert len(selection[Position.WING]) == 4
    assert len(selection[Position.GOALIE]) == 2
    assert len(selection[Position.CENTER]) == 2

    assert find_player_by_id(1, selection[Position.WING])
    assert find_player_by_id(2, selection[Position.CENTER])
    assert find_player_by_id(4, selection[Position.GOALIE])
    assert find_player_by_id(3, selection[Position.FORWARD])


def test_12_players_but_not_enough_wings():
    players = get_12_players_not_enough_wings()
    strategy = GoaliesCentersFirst()
    selector = PositionSelector(strategy=strategy)

    selection = selector.split_players(players)

    # players, whose secondary is wing
    potential_wings = [3, 5, 7, 8, 10]

    selected_as_wing = []
    for name in potential_wings:
        player = find_player_by_id(name, selection[Position.WING])
        if player:
            selected_as_wing.append(player)

    assert len(selected_as_wing) == 2


def dataset_perfect():
    players = [
        UwhPlayer([Position.GOALIE, Position.WING], 1, 4),
        UwhPlayer([Position.CENTER, Position.WING], 2, 5),
        UwhPlayer([Position.WING, Position.FORWARD], 3, 6),
        UwhPlayer([Position.FORWARD, Position.WING], 4, 4),
        UwhPlayer([Position.GOALIE, Position.WING], 5, 5),
        UwhPlayer([Position.CENTER, Position.FORWARD], 6, 6),
        UwhPlayer([Position.WING, Position.FORWARD], 7, 4),
        UwhPlayer([Position.FORWARD, Position.CENTER], 8, 5),
        UwhPlayer([Position.WING, Position.FORWARD], 9, 6),
        UwhPlayer([Position.WING, Position.GOALIE], 10, 4),
        UwhPlayer([Position.FORWARD, Position.CENTER], 11, 5),
        UwhPlayer([Position.FORWARD, Position.GOALIE], 12, 6),
    ]

    pg1, pc1, pw1, pf1, pg2, pc2, pw2, pf2, pw3, pw4, pf3, pf4 = players
    expected = Team([pg1, pg2], [pc1, pc2], [pw1, pw2, pw3, pw4], [pf1, pf2, pf3, pf4])

    return players, expected


def dataset_no_primary_goalie(id_offset: int = 0):
    players = [
        UwhPlayer([Position.FORWARD, Position.GOALIE], 1 + id_offset, 4),
        UwhPlayer([Position.CENTER, Position.WING], 2 + id_offset, 5),
        UwhPlayer([Position.WING, Position.FORWARD], 3 + id_offset, 6),
        UwhPlayer([Position.FORWARD, Position.WING], 4 + id_offset, 4),
        UwhPlayer([Position.FORWARD, Position.GOALIE], 5 + id_offset, 5),
        UwhPlayer([Position.CENTER, Position.FORWARD], 6 + id_offset, 6),
        UwhPlayer([Position.WING, Position.FORWARD], 7 + id_offset, 4),
        UwhPlayer([Position.FORWARD, Position.CENTER], 8 + id_offset, 5),
        UwhPlayer([Position.WING, Position.FORWARD], 9 + id_offset, 6),
        UwhPlayer([Position.WING, Position.GOALIE], 10 + id_offset, 4),
        UwhPlayer([Position.FORWARD, Position.CENTER], 11 + id_offset, 5),
        UwhPlayer([Position.FORWARD, Position.GOALIE], 12 + id_offset, 6),
    ]

    pf5, pc1, pw1, pf1, pf6, pc2, pw2, pf2, pw3, pw4, pf3, pf4 = players
    expected = Team([pf6, pf4], [pc1, pc2], [pw1, pw2, pw3, pw4], [pf1, pf2, pf3, pf5])

    return players, expected


def dataset_not_enough_wings():
    players = [
        UwhPlayer([Position.GOALIE, Position.WING], 1, 4),
        UwhPlayer([Position.CENTER, Position.WING], 2, 5),
        UwhPlayer([Position.WING, Position.FORWARD], 3, 6),
        UwhPlayer([Position.FORWARD, Position.WING], 4, 4),
        UwhPlayer([Position.GOALIE, Position.WING], 5, 6),
        UwhPlayer([Position.CENTER, Position.FORWARD], 6, 6),
        UwhPlayer([Position.WING, Position.FORWARD], 7, 4),
        UwhPlayer([Position.FORWARD, Position.CENTER], 8, 5),
        UwhPlayer([Position.WING, Position.FORWARD], 9, 6),
        UwhPlayer([Position.FORWARD, Position.WING], 10, 4),
        UwhPlayer([Position.FORWARD, Position.CENTER], 11, 6),
        UwhPlayer([Position.FORWARD, Position.GOALIE], 12, 6),
    ]

    p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12 = players
    expected = Team([p1, p5], [p6, p2], [p3, p7, p9, p4], [p10, p8, p11, p12])

    return players, expected


@pytest.mark.parametrize(
    "players, expected",
    [dataset_perfect(), dataset_no_primary_goalie(), dataset_not_enough_wings()],
)
def test_strongest_fill_selector(players: List[UwhPlayer], expected: Team):
    selector = StrongestFillsSelector()

    actual = selector.split_players(players)

    assert actual == expected
