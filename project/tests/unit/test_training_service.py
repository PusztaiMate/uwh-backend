from datetime import datetime
from project.api.models.training import Training
from project.services.training_service import (
    InMemoryTrainingsRepository,
    TrainingService,
)


def test_can_retrieve_trainings():
    repository = InMemoryTrainingsRepository()
    service = TrainingService(repository)

    repository.save_training(Training(date=datetime(2020, 1, 1)))
    repository.save_training(Training(date=datetime(2020, 2, 1)))

    trainings = service.get_trainings_for_year(2020)

    assert len(trainings) == 2
