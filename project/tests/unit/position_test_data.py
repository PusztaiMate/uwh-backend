from copy import deepcopy
from typing import Dict, List

from project.api.lib.team_selector.models import Position, UwhPlayer

perfect = [
    UwhPlayer([Position.WING], 1, 6),
    UwhPlayer([Position.CENTER], 2, 8),
    UwhPlayer([Position.FORWARD], 3, 7),
    UwhPlayer([Position.GOALIE], 4, 9),
    UwhPlayer([Position.CENTER], 5, 7),
    UwhPlayer([Position.WING], 6, 7),
    UwhPlayer([Position.GOALIE], 7, 9),
    UwhPlayer([Position.FORWARD], 8, 6),
    UwhPlayer([Position.FORWARD], 9, 4),
    UwhPlayer([Position.FORWARD], 10, 4),
    UwhPlayer([Position.WING], 11, 6),
    UwhPlayer([Position.WING], 12, 7),
]

everybody_has_secondary = [
    UwhPlayer([Position.WING, Position.FORWARD], 1, 6),
    UwhPlayer([Position.CENTER, Position.GOALIE], 2, 8),
    UwhPlayer([Position.FORWARD, Position.WING], 3, 7),
    UwhPlayer([Position.GOALIE, Position.CENTER], 4, 9),
    UwhPlayer([Position.CENTER, Position.WING], 5, 7),
    UwhPlayer([Position.WING, Position.GOALIE], 6, 7),
    UwhPlayer([Position.GOALIE, Position.WING], 7, 9),
    UwhPlayer([Position.FORWARD, Position.WING], 8, 6),
    UwhPlayer([Position.FORWARD, Position.CENTER], 9, 4),
    UwhPlayer([Position.FORWARD, Position.WING], 10, 4),
    UwhPlayer([Position.WING, Position.FORWARD], 11, 6),
    UwhPlayer([Position.WING, Position.FORWARD], 12, 7),
]

not_enough_wings = [
    UwhPlayer([Position.CENTER, Position.FORWARD], 1, 6),
    UwhPlayer([Position.CENTER, Position.GOALIE], 2, 8),
    UwhPlayer([Position.FORWARD, Position.WING], 3, 7),
    UwhPlayer([Position.GOALIE, Position.CENTER], 4, 9),
    UwhPlayer([Position.CENTER, Position.WING], 5, 7),
    UwhPlayer([Position.FORWARD, Position.GOALIE], 6, 7),
    UwhPlayer([Position.GOALIE, Position.WING], 7, 9),
    UwhPlayer([Position.FORWARD, Position.WING], 8, 6),
    UwhPlayer([Position.FORWARD, Position.CENTER], 9, 4),
    UwhPlayer([Position.FORWARD, Position.WING], 10, 4),
    UwhPlayer([Position.WING, Position.FORWARD], 11, 6),
    UwhPlayer([Position.WING, Position.FORWARD], 12, 7),
]


def get_12_players_perfect() -> List[UwhPlayer]:
    return deepcopy(perfect)  # type: ignore


def get_12_players_perfect_with_secondaries() -> List[UwhPlayer]:
    return deepcopy(everybody_has_secondary)  # type: ignore


def get_12_players_not_enough_wings() -> List[UwhPlayer]:
    return deepcopy(not_enough_wings)  # type: ignore
