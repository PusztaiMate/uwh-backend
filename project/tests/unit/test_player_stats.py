from unittest.mock import MagicMock

from project.api.lib.player_stats import (
    calculate_all_player_pairings,
    calculate_most_and_least_common_teammate,
    create_pairing_stats,
    find_least_paired,
    find_most_paired,
    update_pairing_stats,
)


def test_two_trainings_with_the_same_teams():
    training_1 = MagicMock()
    training_1.white_team.player_ids = [1, 2]
    training_1.black_team.player_ids = [3, 4]

    training_2 = MagicMock()
    training_2.white_team.player_ids = [1, 2]
    training_2.black_team.player_ids = [3, 4]

    trainings = [training_1, training_2]

    stats = calculate_most_and_least_common_teammate(trainings)

    assert stats[1]["most_paired"]["player_ids"] == [2]
    assert stats[1]["most_paired"]["value"] == 2
    assert stats[1]["least_paired"]["player_ids"] == [2]


def test_two_teams_with_one_player_switched():
    training_1 = MagicMock()
    training_1.white_team.player_ids = [1, 2]
    training_1.black_team.player_ids = [3, 4]

    training_2 = MagicMock()
    training_2.white_team.player_ids = [1, 3]
    training_2.black_team.player_ids = [2, 4]

    trainings = [training_1, training_2]

    stats = calculate_most_and_least_common_teammate(trainings)

    assert stats[1]["most_paired"]["player_ids"] == [2, 3]
    assert stats[1]["most_paired"]["value"] == 1
    assert stats[1]["least_paired"]["player_ids"] == [2, 3]
    assert stats[1]["least_paired"]["value"] == 1


def test_player_pairing_with_three_teams_works_fine():
    training_1 = MagicMock()
    training_1.white_team.player_ids = [1, 2]
    training_1.black_team.player_ids = [3, 4]

    training_2 = MagicMock()
    training_2.white_team.player_ids = [1, 3]
    training_2.black_team.player_ids = [2, 4]

    training_3 = MagicMock()
    training_3.white_team.player_ids = [1, 3]
    training_3.black_team.player_ids = [2, 4]

    trainings = [training_1, training_3, training_2]

    stats = calculate_most_and_least_common_teammate(trainings)

    assert stats[1]["most_paired"]["player_ids"] == [3]
    assert stats[1]["most_paired"]["value"] == 2
    assert stats[1]["least_paired"]["player_ids"] == [2]
    assert stats[1]["least_paired"]["value"] == 1
    assert stats[2]["least_paired"]["player_ids"] == [1]
    assert stats[3]["least_paired"]["value"] == 1


def test_calculation_work_even_if_black_and_white_team_is_empty():
    training_1 = MagicMock()
    training_1.white_team.player_ids = []
    training_1.black_team.player_ids = []

    training_2 = MagicMock()
    training_2.white_team.player_ids = []
    training_2.black_team.player_ids = []

    trainings = [training_1, training_2]

    stats = calculate_most_and_least_common_teammate(trainings)

    assert len(stats) == 0


def test_most_and_least_common_calc_works_with_empty_and_full_teams():
    training_1 = MagicMock()
    training_1.white_team = None
    training_1.black_team = None

    training_2 = MagicMock()
    training_2.white_team.player_ids = [1, 2]
    training_2.black_team.player_ids = [3, 4]

    trainings = [training_1, training_2]

    stats = calculate_most_and_least_common_teammate(trainings)

    assert stats[1]["most_paired"]["player_ids"] == [2]
    assert stats[1]["most_paired"]["value"] == 1
    assert stats[2]["least_paired"]["player_ids"] == [1]
    assert stats[3]["least_paired"]["value"] == 1


def test_can_update_stats_based_on_team():
    pairing_stats = create_pairing_stats()
    pairing_stats[1][2] = 1
    pairing_stats[1][3] = 0

    update_pairing_stats(pairing_stats, [1, 2, 3])

    assert pairing_stats[1][1] == 0
    assert pairing_stats[1][2] == 2
    assert pairing_stats[1][3] == 1


def test_create_pairing_stats_returns_correct_object():
    stats = create_pairing_stats()

    assert stats[1][1] == 0
    assert stats[1][2] == 0
    assert stats[2][1] == 0
    assert stats[2][2] == 0


def test_can_calculate_full_pairing_table():
    training_1 = MagicMock()
    training_1.white_team.player_ids = [1, 2]
    training_1.black_team.player_ids = [3, 4]

    training_2 = MagicMock()
    training_2.white_team.player_ids = [1, 3]
    training_2.black_team.player_ids = [2, 4]

    training_3 = MagicMock()
    training_3.white_team.player_ids = [1, 4]
    training_3.black_team.player_ids = [2, 3]

    training_4 = MagicMock()
    training_4.white_team.player_ids = [1, 2]
    training_4.black_team.player_ids = [3, 4]

    trainings = [training_1, training_2, training_3, training_4]

    all_player_pairings = calculate_all_player_pairings(trainings)

    assert all_player_pairings[1][1] == 0
    assert all_player_pairings[1][2] == 2
    assert all_player_pairings[1][3] == 1
    assert all_player_pairings[1][4] == 1

    for i in range(1, 4):
        for j in range(i, 5):
            assert all_player_pairings[i][j] == all_player_pairings[j][i]


def test_can_find_most_paired_players_in_raw_data():
    all_pairings = {
        1: {2: 1, 3: 2},
        2: {1: 1, 3: 1},
        3: {1: 2, 2: 1},
    }

    stats_for_1 = find_most_paired(pairings=all_pairings[1])
    stats_for_2 = find_most_paired(pairings=all_pairings[2])

    assert stats_for_1["player_ids"] == [3]
    assert stats_for_1["value"] == 2
    assert stats_for_2["player_ids"] == [1, 3]
    assert stats_for_2["value"] == 1


def test_can_find_least_paired_players_in_raw_data():
    all_pairings = {
        1: {2: 2, 3: 1},
        2: {1: 2, 3: 1},
        3: {1: 1, 2: 1},
    }

    stats_for_1 = find_least_paired(pairings=all_pairings[1])
    stats_for_2 = find_least_paired(pairings=all_pairings[2])

    assert stats_for_1["player_ids"] == [3]
    assert stats_for_1["value"] == 1
    assert stats_for_2["player_ids"] == [3]
    assert stats_for_2["value"] == 1
