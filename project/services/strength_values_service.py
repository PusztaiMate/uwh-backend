from typing import List, Optional
from datetime import datetime
from project.api.models.strength_value import StrengthValue
from project.repositories.strength_values_repository import StrengthValuesDb


class StrengthValuesService:
    def __init__(self, repository: StrengthValuesDb):
        self.repository = repository

    def get_all(self) -> List[StrengthValue]:
        return self.repository.get_all()

    def get_by_id(self, id: int) -> StrengthValue:
        return self.repository.get_by_id(id)

    def get_all_for_player(self, player_id: int) -> List[StrengthValue]:
        return self.repository.get_all_for_player(player_id)

    def get_latest_for_player(
        self, player_id: int, date: Optional[datetime] = None
    ) -> StrengthValue:
        return self.repository.get_latest_for_player(player_id, date)

    def delete_by_id(self, sv_id: int):
        self.repository.delete_by_id(sv_id)

    def add_multiple_strength_updates(
        self, date: datetime, player_strength_values: dict, forced: bool = False
    ):
        self.repository.add_multiple_strength_updates(
            date, player_strength_values, forced
        )

    def update_strength_values_for_given_date(self, date, values: dict):
        self.repository.update_strength_values_for_given_date(date, values)

    def delete_by_date(self, date: datetime) -> int:
        return self.repository.delete_by_date(date)

    def get_all_for_player_between_dates(
        self, player_id: int, start_date: datetime, end_date: datetime
    ) -> List[StrengthValue]:
        return self.repository.get_all_for_player_between_dates(
            player_id, start_date, end_date
        )

    def get_value_for_player_on_date(
        self, player_id: int, date: datetime
    ) -> StrengthValue:
        return self.repository.get_value_for_player_on_date(player_id, date)
