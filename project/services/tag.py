from typing import List, Optional

from project.api.models.clip_tags import Tag
from project.repositories.exceptions import ResourceAlreadyExists, ResourceNotFound
from project.repositories.tag_repository import TagRepository


class TagNotFound(RuntimeError):
    pass


class TagAlreadyExists(RuntimeError):
    pass


class TagDTO:
    def __init__(self, name: str, clips: List[int], id: int = 0):
        self.name = name
        self.clips = clips
        self.id = id

    def to_dict(self) -> dict:
        return {"name": self.name, "id": self.id}

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, TagDTO):
            return False
        return self.id == other.id and self.name == other.name

    def __hash__(self) -> int:
            return hash((self.id, self.name))


def convert_tag_to_tag_dto(tag: Tag) -> TagDTO:
    return TagDTO(tag.name, [c.id for c in tag.clips], tag.id)


def convert_tag_dto_to_tag(tag_dto: TagDTO) -> Tag:
    return Tag(tag_dto.name)


class TagService:
    def __init__(self, repository: TagRepository):
        self.repository = repository

    def save(self, tag_dto: TagDTO) -> TagDTO:
        tag = convert_tag_dto_to_tag(tag_dto)
        try:
            return convert_tag_to_tag_dto(self.repository.save(tag))
        except ResourceAlreadyExists as e:
            raise TagAlreadyExists(str(e))

    def get_all(self) -> List[TagDTO]:
        return [convert_tag_to_tag_dto(t) for t in self.repository.get_all()]

    def delete(self, tag_id: int) -> None:
        try:
            self.repository.delete(tag_id)
        except ResourceNotFound as e:
            raise TagNotFound(str(e))

    def get(self, tag_id: int) -> Optional[TagDTO]:
        tag = self.repository.find_by_id(tag_id)
        if tag is None:
            return None
        return convert_tag_to_tag_dto(tag)

    def find_by_name(self, name: str) -> Optional[TagDTO]:
        tag = self.repository.find_by_name(name)
        if tag is None:
            return None
        return convert_tag_to_tag_dto(tag)
