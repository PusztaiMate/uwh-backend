from typing import List, Optional
from project.api.models.clip import Clip
from project.repositories.clips_repository import ClipRepository
from project.repositories.exceptions import ResourceNotFound
from project.services.models.clip_dto import ClipDTO
from project.services.tag import (
    TagService,
    convert_tag_dto_to_tag,
    convert_tag_to_tag_dto,
)


class ClipAlreadyExists(RuntimeError):
    pass


class ClipNotFound(RuntimeError):
    pass


class ClipService:
    def __init__(self, tag_service: TagService, repository: ClipRepository):
        self.repository = repository
        self.tag_service = tag_service

    def get_all(self) -> List[ClipDTO]:
        return [convert_clip_to_clip_dto(c) for c in self.repository.get_all()]

    def get(self, id: int) -> Optional[ClipDTO]:
        clip = self.repository.find_by_id(id)
        if clip is None:
            return None
        return convert_clip_to_clip_dto(clip)

    def delete(self, id: int) -> None:
        try:
            self.repository.delete(id)
        except ResourceNotFound as e:
            raise ClipNotFound(str(e))

    def get_clips_with_tags(self, tag_names: List[str]) -> List[ClipDTO]:
        clips = self.get_all()
        tags = []
        for tag_name in tag_names:
            tag = self.tag_service.find_by_name(tag_name)
            if tag is None:
                continue
            tags.append(tag)

        for tag in tags:
            clips = filter(lambda x: tag in x.tags, clips)

        return list(clips)

    def save(self, c1: ClipDTO) -> ClipDTO:
        for clip_from_db in self.repository.find_by_url(c1.video_url):
            c2 = convert_clip_to_clip_dto(clip_from_db)
            if is_almost_same_clip(c1, c2, 1):
                raise ClipAlreadyExists(
                    f"clip already exists in db with id: ({clip_from_db.id})"
                )
        new_clip = convert_clip_dto_to_clip(c1)
        return convert_clip_to_clip_dto(self.repository.save(new_clip))


def convert_clip_dto_to_clip(new_clip_dto: ClipDTO) -> Clip:
    tags = [convert_tag_dto_to_tag(t) for t in new_clip_dto.tags]
    return Clip(
        new_clip_dto.video_url,
        tags,
        new_clip_dto.start_ts.to_db_string(),
        new_clip_dto.end_ts.to_db_string(),
    )


def convert_clip_to_clip_dto(clip: Clip) -> ClipDTO:
    tags = [convert_tag_to_tag_dto(t) for t in clip.tags]
    return ClipDTO(
        clip.video_url, tags, clip.start_timestamp, clip.end_timestamp, clip.id
    )


def is_almost_same_clip(c1: ClipDTO, c2: ClipDTO, d_sec: int = 0) -> bool:
    if (
        c1.video_url == c2.video_url
        and (c1.start_ts - c2.start_ts <= d_sec)
        and (c1.end_ts - c2.end_ts <= d_sec)
    ):
        return True
    return False
