from project.api.lib.team_selector.models import Position


class PlayerDTO:
    def __init__(
        self,
        fname: str,
        lname: str,
        training_strength: float,
        prim_pos: Position,
        sec_pos: Position,
    ):
        self.fname = fname
        self.lname = lname
        self.training_strength = training_strength
        self.primary_position = prim_pos
        self.secondary_position = sec_pos
        self.id = -1
