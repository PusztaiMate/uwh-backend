import functools


class TimeStampParsingError(RuntimeError):
    pass


class InvalidTimeStamp(RuntimeError):
    pass


@functools.total_ordering
class TimeStamp:
    def __init__(self, min: int, sec: int) -> None:
        self.min = min
        self.sec = sec

    @property
    def min(self):
        return self._min

    @min.setter
    def min(self, new_value):
        if not isinstance(new_value, int):
            raise InvalidTimeStamp(f"invalid value: ({new_value}) is not an integer")
        if new_value < 0:
            raise InvalidTimeStamp(f"invalid value: ({new_value}) is less than 0")
        self._min = new_value

    @property
    def sec(self):
        return self._sec

    @sec.setter
    def sec(self, new_value):
        if not isinstance(new_value, int):
            raise InvalidTimeStamp(f"invalid value: ({new_value}) is not an integer")
        if new_value < 0 or new_value > 59:
            raise InvalidTimeStamp(f"invalid value: ({new_value}) is not in [0, 59]")
        self._sec = new_value

    @classmethod
    def from_min_sec_string(cls, s: str) -> "TimeStamp":
        try:
            m, sec = map(int, s.split(":"))
        except ValueError:
            raise TimeStampParsingError(
                f"could not parse input: expected format 'min:sec', got '{s}'"
            )
        return cls(m, sec)

    @classmethod
    def from_seconds(cls, secs: int) -> "TimeStamp":
        mins, ss = secs // 60, secs % 60
        return TimeStamp(mins, ss)

    def as_seconds(self):
        return self.min * 60 + self.sec

    def to_db_string(self) -> str:
        return f"{self.min}:{self.sec}"

    def __sub__(self, o: object) -> int:
        if isinstance(o, int):
            ts = TimeStamp.from_seconds(o)
            return abs(self.as_seconds() - ts.as_seconds())
        elif isinstance(o, TimeStamp):
            return abs(self.as_seconds() - o.as_seconds())
        else:
            raise ValueError(
                f"{o} is not a TimeStamp or int, can't calculate time distance"
            )

    def __eq__(self, o: object) -> bool:
        if not isinstance(o, TimeStamp):
            return False
        return self.min == o.min and self.sec == o.sec

    def __lt__(self, o: object) -> bool:
        if not isinstance(o, TimeStamp):
            raise ValueError(f"'{o}' is not a TimeStamp, can not compare")
        if self.min < o.min:
            return True
        if self.min == o.min and self.sec < o.sec:
            return True
        return False
