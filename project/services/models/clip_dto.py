from typing import List

from project.services.models.timestamp import TimeStamp
from project.services.tag import TagDTO


class ClipDTO:
    def __init__(
        self,
        video_url: str,
        tags: List[TagDTO],
        start_ts: TimeStamp,
        end_ts: TimeStamp,
        id: int = 0,
    ):
        self.video_url = video_url
        self.tags = tags
        self.start_ts = start_ts
        self.end_ts = end_ts
        self.id = id

    def __eq__(self, o: object) -> bool:
        if not isinstance(o, ClipDTO):
            return False
        return (
            self.video_url == o.video_url
            and self.start_ts == o.start_ts
            and self.end_ts == o.end_ts
        )

    def to_dict(self) -> dict:
        return {
            "id": self.id,
            "video_url": self.video_url,
            "tags": [t.to_dict() for t in self.tags],
            "start_ts": self.start_ts.to_db_string(),
            "end_ts": self.end_ts.to_db_string(),
        }
