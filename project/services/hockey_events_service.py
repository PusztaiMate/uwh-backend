from datetime import datetime
from typing import Optional

from project.repositories.exceptions import ResourceNotFound

from ..repositories.hockey_events_repository import HockeyEventsRepository
from ..api.models.hockey_event import EVENT_TYPES
from ..api.models.hockey_event import HockeyEvent as DBHockeyEvent


class HockeyEvent:
    def __init__(
        self,
        date: datetime,
        title: str,
        type: EVENT_TYPES,
        description: Optional[str],
        id: Optional[int] = None,
    ):
        self.id = id
        self.date = date
        self.title = title
        self.type = type
        self.description = description

    def as_dict(self):
        return {
            "id": self.id,
            "date": self.date.isoformat(),
            "title": self.title,
            "type": self.type,
            "description": self.description,
        }

    @classmethod
    def from_dict(cls, data):
        if data.get("date") is not None and isinstance(data.get("date"), str):
            date = datetime.fromisoformat(data.get("date").split(".")[0])
        elif data.get("date") is not None and isinstance(data.get("date"), datetime):
            date = data.get("date")
        else:
            raise ValueError("Invalid date")
        return cls(
            id=data.get("id"),
            date=date,
            title=data.get("title"),
            type=data.get("type"),
            description=data.get("description"),
        )


def convert_hockey_event_to_db_model(hockey_event: HockeyEvent) -> DBHockeyEvent:
    return DBHockeyEvent(
        id=hockey_event.id,
        date=hockey_event.date,
        title=hockey_event.title,
        type=hockey_event.type,  # type: ignore
        description=hockey_event.description,
    )


def convert_db_model_to_hockey_event(hockey_event: DBHockeyEvent) -> HockeyEvent:
    return HockeyEvent(
        id=hockey_event.id,  # type: ignore
        date=hockey_event.date,  # type: ignore
        title=hockey_event.title,  # type: ignore
        type=hockey_event.type,  # type: ignore
        description=hockey_event.description,  # type: ignore
    )


class HockeyEventsService:
    def __init__(self, hockey_events_repository: HockeyEventsRepository):
        self.hockey_events_repository = hockey_events_repository

    def get_all(self, type_: Optional[str]) -> list[HockeyEvent]:
        return [
            convert_db_model_to_hockey_event(hockey_event)
            for hockey_event in self.hockey_events_repository.get_all(type_=type_)
        ]

    def get_by_id(self, id: int) -> HockeyEvent:
        found = self.hockey_events_repository.find_by_id(id)
        if found is None:
            raise ResourceNotFound(f"Could not find hockey event with id {id}")
        return convert_db_model_to_hockey_event(found)

    def save(self, hockey_event: HockeyEvent) -> HockeyEvent:
        return convert_db_model_to_hockey_event(
            self.hockey_events_repository.save(
                convert_hockey_event_to_db_model(hockey_event)
            )
        )

    def create(
        self, date: datetime, title: str, type: EVENT_TYPES, description: str
    ) -> HockeyEvent:
        hockey_event = HockeyEvent(
            date=date, title=title, type=type, description=description
        )
        return self.save(hockey_event)

    def update_from_dict(self, id: int, **kwargs) -> HockeyEvent:
        hockey_event = self.get_by_id(id)
        for key, value in kwargs.items():
            if hasattr(hockey_event, key):
                setattr(hockey_event, key, value)
        return self.save(hockey_event)

    def update(self, id: int, hockey_event: HockeyEvent) -> HockeyEvent:
        hockey_event.id = id
        updated = self.hockey_events_repository.update(
            id, convert_hockey_event_to_db_model(hockey_event)
        )
        return convert_db_model_to_hockey_event(updated)

    def delete(self, id: int) -> None:
        self.hockey_events_repository.delete(id)
