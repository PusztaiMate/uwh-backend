from abc import ABC, abstractmethod
from datetime import datetime
from typing import List, Optional

from flask_sqlalchemy import SQLAlchemy

from project.api.models.training import Training


class TrainingStore(ABC):
    @abstractmethod
    def fetch_trainings_for_year(self, year: int) -> List[Training]:
        pass

    @abstractmethod
    def save_training(self, training: Training) -> None:
        pass

    @abstractmethod
    def get_training_by_id(self, training_id: int) -> Optional[Training]:
        pass

    @abstractmethod
    def get_all_trainings(self) -> List[Training]:
        pass

    @abstractmethod
    def get_trainings_between_dates(
        self, start_date: datetime, end_date: datetime
    ) -> List[Training]:
        pass


class InMemoryTrainingsRepository(TrainingStore):
    def __init__(self):
        self._trainings: List[Training] = []

    def fetch_trainings_for_year(self, year: int) -> List[Training]:
        return [t for t in self._trainings if t.date.year == year]

    def save_training(self, training: Training) -> None:
        self._trainings.append(training)

    def get_training_by_id(self, training_id: int) -> Training | None:
        for training in self._trainings:
            if training.id == training_id:
                return training
        return None

    def get_all_trainings(self) -> List[Training]:
        return self._trainings

    def get_trainings_between_dates(
        self, start_date: datetime, end_date: datetime
    ) -> List[Training]:
        trainings: List[Training] = []
        for training in self._trainings:
            if training.date >= start_date and training.date <= end_date:
                trainings.append(training)
        return trainings


class TrainingRepository(TrainingStore):
    def __init__(self, db: SQLAlchemy):
        self._db = db

    def fetch_trainings_for_year(self, year: int) -> List[Training]:
        trainings = Training.query.all()
        # TODO: do this in the database query
        trainings_that_year = [t for t in trainings if t.date.year == year]
        return trainings_that_year

    def save_training(self, training: Training) -> None:
        self._db.session.add(training)
        self._db.session.commit()

    def get_training_by_id(self, training_id: int) -> Training | None:
        return Training.query.get(training_id)

    def get_all_trainings(self) -> List[Training]:
        return Training.query.all()

    def get_trainings_between_dates(
        self, start_date: datetime, end_date: datetime
    ) -> List[Training]:
        return Training.query.filter(Training.date.between(start_date, end_date)).all() # type: ignore


class TrainingService:
    def __init__(self, repository: TrainingStore):
        self._repository = repository

    def get_trainings_for_year(self, year: int) -> List[Training]:
        return self._repository.fetch_trainings_for_year(year)

    def add_scorer_data_to_training(self, training_id: int, scorer_data: dict[int, int]) -> None:
        training = self.get_training(training_id)
        if not training:
            raise ValueError(f"Training with id {training_id} not found")
        training.scorers = scorer_data
        self._repository.save_training(training)

    def get_training(self, training_id: int) -> Training | None:
        return self._repository.get_training_by_id(training_id)

    def get_all_trainings(self) -> List[Training]:
        return self._repository.get_all_trainings()

    def get_trainings_between_dates(
        self, start_date: datetime, end_date: datetime
    ) -> List[Training]:
        return self._repository.get_trainings_between_dates(start_date, end_date)
