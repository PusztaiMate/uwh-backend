from typing import List
from project.api.models.player import Player
from project.repositories.player_repository import PlayerRepository
from project.services.models.player_dto import PlayerDTO


class PlayerService:
    def __init__(self, repository: PlayerRepository):
        self.repository = repository

    def save(self, player_dto: PlayerDTO) -> PlayerDTO:
        player_dto = convert_player_dto_to_player(player_dto)
        return player_dto

    def get_all_players(self) -> List[Player]:
        return self.repository.get_all()

    def get_by_id(self, id: int) -> Player:
        return self.repository.find_by_id(id)


def convert_player_dto_to_player(player_dto: PlayerDTO) -> Player:
    player = Player(fname=player_dto.fname, lname=player_dto.lname)
    if player_dto.primary_position:
        player.primary_position = player_dto.primary_position.value
    if player_dto.secondary_position:
        player.secondary_position = player_dto.secondary_position.value
    if player_dto.training_strength:
        player.training_strength = player_dto.training_strength
    return player


def convert_player_to_player_dto(player: Player) -> PlayerDTO:
    return PlayerDTO(
       fname=player.fname,
       lname=player.lname,
       prim_pos=player.primary_position, # type: ignore
       sec_pos=player.secondary_position, # type: ignore
       training_strength=player.training_strength
    )
