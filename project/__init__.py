import os
import logging

from flask import Flask
from flask_migrate import Migrate
from flask_cors import CORS

from .database import db
from .error_handlers import register_error_handlers
from .middlewares import username_exporter

migrate = Migrate()


def create_app() -> Flask:
    app = Flask(__name__)

    # uncomment to see SQL queries
    # app.config["SQLALCHEMY_ECHO"] = True

    app.logger.setLevel(logging.INFO)
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter("[%(asctime)s][%(name)s][%(levelname)s]: %(message)s")
    ch.setFormatter(formatter)
    app.logger.addHandler(ch)

    app_settings = os.getenv("APP_SETTINGS", "project.config.DevelopmentConfig")
    app.logger.info(f"Using settings: {app_settings}")
    app.config.from_object(app_settings)

    app.logger.info("Starting up application")

    CORS(app, resources={r"/api/*": {"origins": "*"}})
    db.init_app(app)
    migrate.init_app(app, db)

    from project.api.players import players_api
    from project.api.trainings import trainings_api
    from project.api.payments import payments_api
    from project.api.users import users_api
    from project.api.goal_scorers import goal_scorers_api
    from project.api.notifications import notifications_api
    from project.api.hockey_events_api import hockey_events_api

    # from project.api.image_api import images_api
    from project.api.positions import positions_api
    from project.api.team_picker import team_picker_api
    from project.api.player_strength_values import player_strength_api
    from project.api.clips_db import clips_db_api
    from project.api.tags import tags_api
    from project.api.attendance_api import attendance_api

    app.logger.info("Setting up blueprints")
    app.register_blueprint(players_api)
    app.register_blueprint(trainings_api)
    app.register_blueprint(payments_api)
    app.register_blueprint(users_api)
    app.register_blueprint(goal_scorers_api)
    app.register_blueprint(notifications_api)
    app.register_blueprint(hockey_events_api)
    # app.register_blueprint(images_api)
    app.register_blueprint(positions_api)
    app.register_blueprint(team_picker_api)
    app.register_blueprint(player_strength_api)
    app.register_blueprint(clips_db_api)
    app.register_blueprint(tags_api)
    app.register_blueprint(attendance_api)

    # temporary
    from project.api.stats_for_2023 import stats_for_2023

    app.register_blueprint(stats_for_2023)

    from project.api.models import (
        Player,
        Training,
        Club,
        User,
        MonthlyPayObligation,
        Position,
        Image,
        StrengthValue,
        Clip,
        Tag,
        HockeyEvent,
    )

    app.logger.info("Setting up app shell")

    @app.shell_context_processor
    def ctx():
        return {
            "app": app,
            "db": db,
            "Player": Player,
            "Training": Training,
            "Club": Club,
            "User": User,
            "MonthlyPayObligation": MonthlyPayObligation,
            "Image": Image,
            "Position": Position,
            "StrengthValue": StrengthValue,
            "Clip": Clip,
            "Tag": Tag,
            "HockeyEvent": HockeyEvent,
        }

    app.logger.info("Provisioning admin user roles")

    app.logger.info("Registering error handlers")
    register_error_handlers(app)

    app.logger.info("Registering middlewares")
    app.wsgi_app = username_exporter(app.wsgi_app)

    app.logger.info("Setup successful")
    return app
