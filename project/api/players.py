from collections import defaultdict
from functools import lru_cache

from flask import Blueprint, request, jsonify
from werkzeug.exceptions import BadRequest
from project.api.lib.team_selector.models import Position

from project.repositories.player_repository import PlayerDB
from project.services.player_service import PlayerService

from .models import Player, Training
from project import db
from .db_utils import (
    get_active_players,
    query_all_players,
    query_all_scorers,
    query_single_player,
    add_player_to_db,
    update_player_in_db,
    query_all_trainings,
)
from .lib.player_stats import calculate_most_and_least_common_teammate
from .lib.authorizer import authorizer

players_api = Blueprint("players_api", __name__, url_prefix="/api/v1/players")

player_db = PlayerDB(db)
player_service = PlayerService(player_db)


@players_api.route("/", methods=["GET"])
def all_players():
    if request.args.get("status", "") == "active":
        players = [player.to_json() for player in get_active_players()]
    else:
        players = get_all_players()
    return jsonify(players), 200


@players_api.route("/", methods=["POST"])
def add_player():
    authorizer.create_player()
    if not request.is_json:
        return {"message": "wrong content type", "status": "failed"}, 400

    try:
        json_data = request.get_json()
    except BadRequest:
        return {"message": "couldn't parse json", "status": "failed"}, 400

    fname, lname = json_data.get("fname"), json_data.get("lname")
    primary_position = json_data.get("primary_position", Position.FORWARD.value)
    secondary_position = json_data.get("secondary_position", Position.CENTER.value)
    training_strength = json_data.get("training_strength", 3.0)

    if not fname or not lname:
        return {"message": "input payload validation failed", "status": "failed"}, 400

    prim_pos = Position(primary_position)
    sec_pos = Position(secondary_position)

    new_player_id = add_player_to_db(fname, lname, training_strength, prim_pos, sec_pos)

    return (
        {
            "message": f"player {fname} {lname} successfully added",
            "status": "success",
            "player_id": new_player_id,
        },
        201,
    )


@players_api.route("/pairings/")
def player_pairings():
    trainings = query_all_trainings()
    stats = calculate_most_and_least_common_teammate(trainings)
    return stats, 200


@players_api.get("/pairings/<int:player_id>/")
def player_pairings_for_player(player_id: int):
    trainings = query_all_trainings()
    stats = calculate_most_and_least_common_teammate(trainings)
    return stats[(player_id)], 200


@players_api.route("/<int:player_id>/", methods=["GET", "PUT"])
def single_player(player_id):
    try:
        player_id = int(player_id)
    except ValueError:
        return {"message": f"invalid id given ({player_id})", "status": "failed"}, 400

    player = query_single_player(Player, player_id)

    if request.method == "GET":
        if player:
            return jsonify(player.to_json()), 200
        return (
            {"message": f"player with id {player_id} not found", "status": "failed"},
            404,
        )

    if request.method == "PUT":
        if not player:
            return add_player()
        return update_player(player, request)


@players_api.route("/id-name-pairs/")
def get_id_name_pairs():
    players = Player.query.all()
    return {p.id: {"fname": p.fname, "lname": p.lname} for p in players}


def get_all_players():
    players = query_all_players()
    return [p.to_json() for p in players]


def get_single_player_as_json(player_id: int):
    player = query_single_player(Player, player_id)
    if not player:
        return None
    return player.to_json()


def update_player(player, req):
    if not req.is_json:
        return {"message": "wrong content type", "status": "failed"}, 400

    try:
        json_data = req.get_json()
    except BadRequest:
        return {"message": "couldn't parse json", "status": "failed"}, 400

    fname = json_data.get("fname") or player["fname"]
    lname = json_data.get("lname") or player["lname"]
    primary_position = json_data.get("primary_position")
    secondary_position = json_data.get("secondary_position")
    training_strength = json_data.get("training_strength")
    status = json_data.get("status")

    update_player_in_db(
        player,
        fname,
        lname,
        primary_position,
        secondary_position,
        training_strength,
        status,
    )

    return (
        {
            "message": f"player {fname} {lname} successfully updated",
            "status": "success",
        },
        200,
    )


@players_api.route("/win-stats/")
def winning_percents_for_players():
    scores = query_all_scorers()
    all_trainigs = query_all_trainings()

    training_with_scorer_data = defaultdict(
        lambda: {
            "blackScore": 0,
            "whiteScore": 0,
            "blackPlayers": [],
            "whitePlayers": [],
        }
    )

    @lru_cache
    def find_training(training_id) -> Training | None:
        for t in all_trainigs:
            if t.id == training_id:
                return t
        return None

    for score in scores:
        t = find_training(score["training_id"])
        if score["player_id"] in t.black_team:
            training_with_scorer_data[score["training_id"]]["blackScore"] += score[
                "scored"
            ]
        else:
            training_with_scorer_data[score["training_id"]]["whiteScore"] += score[
                "scored"
            ]

    player_stats = defaultdict(lambda: {"won": 0, "all": 0, "draw": 0})

    for id, training in training_with_scorer_data.items():
        t = find_training(id)
        if t is None:
            continue

        if training["whiteScore"] > training["blackScore"]:
            for player_id in t.white_team:
                player_stats[player_id]["won"] += 1
                player_stats[player_id]["all"] += 1
            for player_id in t.black_team:
                player_stats[player_id]["all"] += 1

        elif training["whiteScore"] < training["blackScore"]:
            for player_id in t.black_team:
                player_stats[player_id]["won"] += 1
                player_stats[player_id]["all"] += 1
            for player_id in t.white_team:
                player_stats[player_id]["all"] += 1

        else:  # draw
            if training["whiteScore"] == 0 and training["blackScore"] == 0:
                # we had some early trainings where score input was not possible
                continue
            for player_id in t.white_team.player_ids + t.black_team.player_ids:
                player_stats[player_id]["all"] += 1
                player_stats[player_id]["draw"] += 1

    return player_stats
