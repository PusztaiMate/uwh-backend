import os
from uuid import uuid4
from flask import Blueprint, jsonify, request, current_app, send_file
from io import BytesIO
import boto3
import tempfile

from project import db
from project.api.db_utils import (
    query_all_images,
)
from project.api.models.image import Image
from project.repositories.image_repository import ImageRepository


images_api = Blueprint("images", __name__, url_prefix="/api/v1/images")


class S3PhotoRepository:
    def __init__(self, access_id: str, access_key: str, endpoint_url: str):
        self.bucket = "photos"
        self.client = boto3.client(
            "s3",
            aws_access_key_id=access_id,
            aws_secret_access_key=access_key,
            endpoint_url=endpoint_url,
        )
        self._create_bucket()

    def _create_bucket(self):
        self.client.create_bucket(Bucket=self.bucket)

    def upload_photo(self, filename: str, file_content: BytesIO):
        self.client.upload_fileobj(
            file_content,
            self.bucket,
            filename,
        )

    def download_photo(self, filename: str) -> BytesIO:
        file_content = BytesIO()
        self.client.download_fileobj(self.bucket, filename, file_content)
        file_content.seek(0)
        return file_content

    def get_filepath(self, file_name: str) -> str:
        return f"https://{self.bucket}.s3.amazonaws.com/{file_name}"


access_id = os.environ.get("AWS_ACCESS_KEY_ID", "testing_access_id")
access_key = os.environ.get("AWS_SECRET_ACCESS_KEY", "testing_secret_key")
aws_s3_endpoint_url = os.environ.get("AWS_S3_ENDPOINT_URL", "http://localhost:4566")

s3_repo = S3PhotoRepository(
    access_id=access_id,
    access_key=access_key,
    endpoint_url=aws_s3_endpoint_url,
)
image_repository = ImageRepository(db)


@images_api.route("/", methods=["GET"])
def get_all_images():
    return jsonify([img.to_dict() for img in query_all_images()]), 200


@images_api.route("/", methods=["POST"])
def upload_image():
    if "file" not in request.files:
        return {"message": "file is missing"}, 400

    file = request.files["file"]
    if not file.filename:
        return {"message": "file is not selected"}, 400

    file_ext = os.path.splitext(file.filename)[1]
    image_uuid = str(uuid4())
    file_content = BytesIO(file.read())
    s3_repo.upload_photo(f"{image_uuid}{file_ext}", file_content)
    description = request.form.get("description", "")
    try:
        tags = request.form.get("tags", [])
        image_repository.create(
            Image(
                description=description,
                tags=tags,
                uuid=image_uuid,
                orig_extension=file_ext,
            )
        )
        return {"message": "image successfully uploaded"}, 201
    except RuntimeError as e:
        current_app.logger.error(str(e))
        return {"message": "couldn't upload image"}, 400


@images_api.route("/<string:uuid>/", methods=["GET"])
def download_image(uuid: str):
    image_metadata = image_repository.find_by_uuid(uuid)
    content = s3_repo.download_photo(f"{uuid}{image_metadata.orig_extension}")
    return send_file(content, mimetype="image/jpeg")
