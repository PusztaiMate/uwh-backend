from flask import Blueprint, jsonify, request


from .notifications import send_push_to_all_subscribers
from ..repositories.exceptions import ResourceNotFound
from ..repositories.hockey_events_repository import HockeyEventsDB
from ..services.hockey_events_service import HockeyEvent, HockeyEventsService
from ..database import db

url_template = "https://www.vizalattihoki.hu/events/{id}"
# url_template = "http://localhost:3000/events/{id}"

hockey_events_api = Blueprint(
    "hockey_events_api", __name__, url_prefix="/api/v1/hockey-events"
)


hockey_events_db = HockeyEventsDB(db)
hockey_events_service = HockeyEventsService(hockey_events_db)


@hockey_events_api.get("", strict_slashes=False)
def list_all():
    type = request.args.get("type")
    try:
        all_events = hockey_events_service.get_all(type_=type)
    except ValueError as e:
        return jsonify({"error": str(e)}), 400

    events = [e.as_dict() for e in all_events]
    return jsonify({"hockey_events": events})


@hockey_events_api.get("/<int:id>")
def get_by_id(id: int):
    try:
        event = hockey_events_service.get_by_id(id)
        return jsonify({"data": event.as_dict()})
    except ResourceNotFound:
        return jsonify({"error": f"Could not find event with id {id}"}), 404


@hockey_events_api.post("", strict_slashes=False)
def create():
    data = request.get_json()
    try:
        event = hockey_events_service.create(**data)
        send_push_to_all_subscribers(
            f"""Új esemény!
{event.date.isoformat().split('T')[0]}: {event.title}
További infortmáció a weboldalon: {url_template.format(id=event.id)}"""
        )
        return (
            jsonify(
                {
                    "status": "success",
                    "message": "Hockey event created successfully",
                    "data": event.as_dict(),
                }
            ),
            201,
        )
    except Exception as e:
        return jsonify({"error": str(e)}), 400


@hockey_events_api.patch("/<int:id>")
def patch(id: int):
    data = request.get_json()
    try:
        event = hockey_events_service.update_from_dict(id, **data)
        send_push_to_all_subscribers(
            f"""Egy esemény módosult!
{event.date.isoformat().split('T')[0]}: {event.title}
További infortmáció a weboldalon: {url_template.format(id=event.id)}"""
        )
        return (
            jsonify(
                {
                    "status": "success",
                    "message": "Hockey event updated successfully",
                    "data": event.as_dict(),
                }
            ),
            200,
        )
    except Exception as e:
        return jsonify({"error": str(e)}), 400


@hockey_events_api.put("/<int:id>")
def put(id: int):
    data = request.get_json()
    try:
        event = HockeyEvent.from_dict(data)
        event = hockey_events_service.update(id, event)
        send_push_to_all_subscribers(
            f"""Egy esemény módosult!
{event.date.isoformat().split('T')[0]}: {event.title}
További infortmáció a weboldalon: {url_template.format(id=event.id)}"""
        )
        return (
            jsonify(
                {
                    "status": "success",
                    "message": "Hockey event updated successfully",
                    "data": event.as_dict(),
                }
            ),
            200,
        )
    except Exception as e:
        return jsonify({"error": str(e)}), 400


@hockey_events_api.delete("/<int:id>")
def delete(id: int):
    try:
        event = hockey_events_service.get_by_id(id)
        hockey_events_service.delete(id)
        send_push_to_all_subscribers(
            f"""Egy esemény törlésre került!
{event.date.isoformat().split('T')[0]}: {event.title}"""
        )
        return jsonify(
            {"status": "success", "message": "Hockey event deleted successfully"}
        )
    except Exception as e:
        return jsonify({"error": str(e)}), 400
