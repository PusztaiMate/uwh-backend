from datetime import datetime, timedelta
from typing import Dict, List, Optional, Tuple, Union

from flask import current_app
from sqlalchemy import desc, select
from sqlalchemy.exc import IntegrityError

from project import db
from project.api.lib.utils import parse_date
from project.api.models.strength_value import StrengthValue
from project.services.training_service import TrainingRepository, TrainingService

from .lib import team_selector
from .models import Image, Player, Position, Training

training_repository = TrainingRepository(db)
training_service = TrainingService(training_repository)


class ResourceAlreadyPresent(RuntimeError):
    pass


class ResourceNotFound(RuntimeError):
    pass


class DBCommitError(Exception):
    pass


def query_all_players() -> List[Player]:
    return db.session.execute(select(Player)).scalars().all()


def query_single_player(player_model, player_id: int) -> Player:
    current_training_strength = (
        db.session.query(StrengthValue)
        .filter(StrengthValue.player_id == player_id)
        .order_by(desc(StrengthValue.date))  # type: ignore
        .first()
    )
    player = player_model.query.get(player_id)
    if current_training_strength:
        player.training_strength = current_training_strength.value
    return player


def get_active_players() -> List[Player]:
    all_players: List[Player] = Player.query.all()
    return [p for p in all_players if p.status.lower() in ("active", "aktiv", "aktív")]


def add_player_to_db(
    fname: str,
    lname: str,
    training_strength: float = 0,
    primary_position: team_selector.Position = team_selector.Position.FORWARD,
    secondary_position: team_selector.Position = team_selector.Position.WING,
) -> int:
    new_player = Player(
        fname=fname,
        lname=lname,
        training_strenght=training_strength,
        primary_position=primary_position,
        secondary_position=secondary_position,
    )
    commit_to_db(new_player)
    return new_player.id


def update_player_in_db(
    original_player: Player,
    new_fname: str,
    new_lname: str,
    primary_pos: int | None = None,
    secondary_pos: int | None = None,
    training_strength: float | None = None,
    status: str | None = None,
):
    original_player.fname = new_fname
    original_player.lname = new_lname

    if primary_pos:
        original_player.primary_position = primary_pos

    if secondary_pos:
        original_player.secondary_position = secondary_pos

    if training_strength:
        new_sv = StrengthValue(
            player_id=original_player.id,
            value=training_strength,
            date=datetime.now(),
        )
        db.session.add(new_sv)

    if status:
        original_player.status = status

    db.session.add(original_player)
    db.session.commit()


def update_training_id_db(
    original_training, white_team, black_team, swimming, new_date
):
    original_training.white_team = white_team
    original_training.black_team = black_team
    original_training.swimming_players = swimming
    original_training.date = new_date or original_training.date

    db.session.add(original_training)
    db.session.commit()


def query_all_trainings() -> List[Training]:
    return db.session.execute(select(Training)).scalars().all()


def query_single_training(training_id: int) -> Union[Training, None]:
    return Training.query.get(training_id)


def delete_trainings(training_ids: List[int]) -> None:
    for training_id in training_ids:
        training = query_single_training(training_id)
        if not training:
            return
        db.session.delete(training)
        db.session.commit()


def query_trainings_on_date(year: int, month: int, day: int) -> List[Training]:
    day_start = datetime(year, month, day)
    next_day = day_start + timedelta(days=1)
    return Training.query.filter(
        Training.date > day_start, Training.date < next_day
    ).all()


def add_training_to_db(json_data) -> int:
    new_training = Training(**json_data)
    commit_to_db(new_training)
    return new_training.id


def commit_to_db(obj):
    db.session.add(obj)
    db.session.commit()


def query_all_scorers() -> List[Dict[int, int]]:
    scorers = []
    for training in training_repository.get_all_trainings():
        for scorer_id, number_scored in training.get_scorers().items():
            scorers.append(
                {
                    "training_id": training.id,
                    "player_id": scorer_id,
                    "scored": number_scored,
                }
            )
    return scorers


def find_goal_scorers_by_training_id(training_id) -> dict[int, int] | None:
    training = training_repository.get_training_by_id(training_id)
    if not training:
        return None
    return training.get_scorers()


def is_training_exists(training_id) -> bool:
    return bool(Training.query.get(training_id))


def update_goal_scorers_for_trainig(training_id, scorers):
    training_service.add_scorer_data_to_training(training_id, scorers)


def rollback_db_session():
    db.session.rollback()


def query_all_images() -> List[Image]:
    return Image.query.all()


def add_multiple_images_to_db(data: List[dict]):
    for entry in data:
        src = entry.get("src", "")
        if is_image_source_already_in_db(src):
            raise ResourceAlreadyPresent(
                f"image with source '{src}' is already in the database"
            )
        if not src:
            current_app.logger.error("source is missing from request json data")
            return None
        desc = entry.get("desc", "")
        tags = entry.get("tags", [])

        image = Image(src, desc, tags, "")
        db.session.add(image)
    try:
        db.session.commit()
    except IntegrityError:
        current_app.logger.error("couldn't save data to db, rolling back")
        rollback_db_session()


def get_image_by_id(img_id: int) -> Image:
    return Image.query.get(img_id)


def delete_images(image_ids: List[int]):
    for img_id in image_ids:
        img = get_image_by_id(img_id)
        if img:
            db.session.delete(img)
    try:
        db.session.commit()
    except IntegrityError:
        rollback_db_session()
        raise DBCommitError(f"couldn't delete images: {image_ids}")


def update_img(img_id: int, json_data: dict):
    img = get_image_by_id(img_id)
    if not img:
        return {"message": "image not found"}, 404

    src, desc, tags = extract_image_data_from_json(json_data)

    img.src = src
    img.description = desc
    img.tags = ",".join(tags)

    db.session.add(img)
    db.session.commit()

    return {"message": "image successfully updated"}, 200


def extract_image_data_from_json(json_data: dict) -> Tuple[str, str, list]:
    src = json_data.get("src", "")
    desc = json_data.get("desc", "")
    tags = json_data.get("tags", [])

    return src, desc, tags


def is_image_source_already_in_db(source: str) -> bool:
    return Image.query.filter(Image.source == source).first() is not None


def query_all_positions():
    return Position.query.all()


def add_new_position(name: str) -> Position:
    new_position = Position(name)

    try:
        db.session.add(new_position)
        db.session.commit()
    except IntegrityError:
        raise ResourceAlreadyPresent(f"Position with name '{name}' already present")

    return new_position


def add_new_position_if_not_present(name: str):
    position = Position.query.filter_by(name=name)
    if position:
        return position

    return add_new_position(name)


def get_player_by_id(id: int) -> Optional[Player]:
    return Player.query.get(id)


def add_strength_update_to_player(
    p_id: int, strength: float, date: datetime, forced: bool = False
) -> int:
    sv = _create_sv_update_for_player(date, p_id, strength, forced)
    commit_to_db(sv)
    return sv.id


def add_multiple_strength_updates(
    date: datetime, player_strength_values: Dict[int, float], forced: bool = False
):
    for p_id, s in player_strength_values.items():
        try:
            sv = _create_sv_update_for_player(date, p_id, s, forced)
            db.session.add(sv)
        except ResourceAlreadyPresent as e:
            raise e
    db.session.commit()


def _create_sv_update_for_player(date: datetime, p_id: int, strength: float, forced: bool = False):
    is_duplicate = StrengthValue.query.filter(
        StrengthValue.date == date, StrengthValue.player_id == p_id
    ).first()
    if is_duplicate is not None and not forced:
        raise ResourceAlreadyPresent(
            f"entry with the same player id ({p_id})"
            f" and date ({date.isoformat()}) already exists"
        )
    return StrengthValue(value=strength, player_id=p_id, date=date)


def get_strenght_value_by_id(id: int) -> StrengthValue:
    return StrengthValue.query.get(id)


def update_strength_value(sv_id: int, new_value: int):
    sv_from_db = get_strenght_value_by_id(sv_id)
    if sv_from_db is None:
        raise ResourceNotFound(f"strength value with id '{sv_id}' not found")

    sv_from_db.value = new_value
    commit_to_db(sv_from_db)


def delete_strength_value_from_db(sv_id: int):
    sv_from_db = get_strenght_value_by_id(sv_id)
    if sv_from_db is None:
        raise ResourceNotFound(f"strength value with id '{sv_id}' not found")

    db.session.delete(sv_from_db)
    db.session.commit()


def get_all_strength_values() -> List[StrengthValue]:
    return StrengthValue.query.all()


def get_all_strength_values_for_player(player_id: int) -> List[StrengthValue]:
    return (
        StrengthValue.query.filter(StrengthValue.player_id == player_id)
        .order_by(StrengthValue.date)
        .all()
    )


def get_latest_strength_value_for_player(
    player_id: int, date: datetime = None # type: ignore
) -> StrengthValue:
    if date is None:
        date = datetime.now()

    if isinstance(date, str):
        date = parse_date(date)

    sv = (
        StrengthValue.query.filter(StrengthValue.date <= date)
        .order_by(desc(StrengthValue.date)) # type: ignore
        .filter(StrengthValue.player_id == player_id)
        .first()
    )
    return sv


def get_latest_strength_values_for_multiple_players(
    player_ids: List[int], date: datetime = None # type: ignore
) -> List[StrengthValue]:
    return [get_latest_strength_value_for_player(p_id, date) for p_id in player_ids]
