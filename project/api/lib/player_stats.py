from typing import List
from collections import defaultdict


def create_pairing_stats() -> dict:
    return defaultdict(lambda: defaultdict(lambda: 0))


def update_pairing_stats(stats: dict, players_on_team: List[int]) -> None:
    for i, player_a in enumerate(players_on_team):
        for player_b in players_on_team[(i + 1) :]:
            stats[player_a][player_b] += 1
            stats[player_b][player_a] += 1


def calculate_most_and_least_common_teammate(trainings: list) -> dict:
    raw_stats = calculate_all_player_pairings(trainings)
    filtered_stats = create_pairing_stats()

    for player in raw_stats:
        filtered_stats[player]["most_paired"] = find_most_paired(raw_stats[player])
        filtered_stats[player]["least_paired"] = find_least_paired(raw_stats[player])

    return filtered_stats


def calculate_all_player_pairings(trainings: list) -> dict:
    """Returns an object, that contains all the players and the number
    of times they were teammates with the other players. The returned dict is
    similar to this:
    {
        1: {
            2: 3,  # played 3 times with player 2
            3: 1,  # played once with player 3
            4: 2,  # ...
        },
        2: {
            1: 3,  # same value as returned_dict[1][2] of course
            # ...
        }
    }
    """
    stats = create_pairing_stats()

    for t in trainings:
        if t.white_team and t.black_team:
            update_pairing_stats(stats, t.white_team.player_ids)
            update_pairing_stats(stats, t.black_team.player_ids)

    return stats


def find_most_paired(pairings: dict) -> dict:
    """
    Finds the highest pairing number and the corresponding players. For
    example if player no. 1 played twice with player no. 2, twice times with
    player no. 3 and once with player no. 4, we receive the following dict:
    {
        "player_ids": [2, 3],  # id of player 2 and player 3
        "value": 2  # they played 2 times with player 1
    }
    """

    most_pairings = 0
    paired_with = []

    for player, number_of_pairings in pairings.items():
        if number_of_pairings == most_pairings:
            paired_with.append(player)
        elif number_of_pairings > most_pairings:
            paired_with = [player]
            most_pairings = number_of_pairings
    return {"player_ids": paired_with, "value": most_pairings}


# TODO: it would be nice to have a "never_played_with" field also
def find_least_paired(pairings: dict) -> dict:
    """
        {
            2: 2,
            3: 12,
            4: 1
        }
    Similar to find_most_paired, but for the least amount of games played together
    is the goal (but at least 1!).
    """
    least_pairings = 9999  # as we have at most 100 games a year this should do it
    pairs = []

    for player, number_of_pairings in pairings.items():
        if number_of_pairings == least_pairings:
            pairs.append(player)
        if number_of_pairings < least_pairings:
            pairs = [player]
            least_pairings = number_of_pairings

    return {
        "player_ids": pairs,
        "value": least_pairings,
    }
