import os
import casbin_sqlalchemy_adapter
import casbin
from flask import current_app


def convert_database_url_to_sqlalchemy_url(database_url: str) -> str:
    return database_url.replace("postgres://", "postgresql://")


MODEL_CONFIG_PATH = "project/rbac_model.conf"
database_url = convert_database_url_to_sqlalchemy_url(
    os.environ.get(
        "DATABASE_URL", "postgresql://postgres:postgres@localhost:5432/players_dev"
    )
)


class CasbinClient:
    def __init__(self, database_url: str, model_config_path: str):
        adapter = casbin_sqlalchemy_adapter.Adapter(database_url)
        self.enforcer = casbin.Enforcer(model_config_path, adapter)

    def enforce(self, sub: str, obj: str, act: str) -> bool:
        return self.enforcer.enforce(sub, obj, act)

    def add_policy(self, sub: str, obj: str, act: str) -> bool:
        return self.enforcer.add_policy(sub, obj, act)

    def add_grouping_policy(self, sub: str, obj: str) -> bool:
        return self.enforcer.add_grouping_policy(sub, obj)


casbin_client = CasbinClient(database_url, MODEL_CONFIG_PATH)
