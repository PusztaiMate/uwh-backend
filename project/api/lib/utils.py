from datetime import datetime
from dateutil.parser import parse


def parse_date(date_string: str | None) -> datetime:
    if not date_string:
        raise ValueError("Date is required, cannot be None")
    return parse(date_string)
