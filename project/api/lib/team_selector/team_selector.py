import json
from subprocess import SubprocessError
from subprocess import check_output as co
from typing import Iterable, List, Tuple

from .models import Position, Team, UwhPlayer
from .position_selector import PositionSelector, SelectorABC
from .splitter import SplitStrategy

BINARY_NAME = "calculate_teams"


class TeamCalculationError(Exception):
    pass


def pick_team(
    players: List[UwhPlayer],
    positon_selector: SelectorABC,
    splitter: SplitStrategy,
) -> List[Tuple[Team, Team]]:
    all_players = positon_selector.split_players(players)

    teams = _call_to_binary_team_calculator(
        all_players, "input_for_calc.json", "calc_output.json"
    )

    return teams


def _call_to_binary_team_calculator(
    all_players: Team, input_filename: str, output_filename: str
) -> List[Tuple[Team, Team]]:
    with open(input_filename, "w") as f:
        json.dump(all_players.to_dict(), f)

    try:
        co(f"/binaries/{BINARY_NAME} {input_filename} {output_filename}", shell=True)
    except SubprocessError:
        raise TeamCalculationError("could not calculate teams")

    with open(output_filename, "r") as f:
        data = json.load(f)

    teams = []
    for t in data:
        team_a_data = t.get("team_a")
        team_b_data = t.get("team_b")
        teams.append(
            (
                _make_team_from_json_data(team_a_data, all_players),
                _make_team_from_json_data(team_b_data, all_players),
            )
        )

    return teams


def _make_team_from_json_data(team_data: dict, all_players: Team) -> Team:
    goalies = [
        p
        for p in all_players.goalies
        if p.id in [pp["id"] for pp in team_data["goalies"]]
    ]
    centers = [
        p
        for p in all_players.centers
        if p.id in [pp["id"] for pp in team_data["centers"]]
    ]
    wings = [
        p for p in all_players.wings if p.id in [pp["id"] for pp in team_data["wings"]]
    ]
    forwards = [
        p
        for p in all_players.forwards
        if p.id in [pp["id"] for pp in team_data["forwards"]]
    ]

    return Team(goalies, centers, wings, forwards)


class TeamSelector:
    def __init__(
        self,
        players: List[UwhPlayer],
        selector_strategy: PositionSelector,
        splitting_strategy: SplitStrategy,
    ):
        self.players = players
        self.selector_strategy = selector_strategy
        self.splitting_strategy = splitting_strategy

    def calc_teams(self) -> Tuple[Iterable[UwhPlayer], Iterable[UwhPlayer]]:
        selection = self.selector_strategy.get(self.players) # type: ignore

        team_1 = []
        team_2 = []

        for pos in Position:
            g_1, g_2 = self.splitting_strategy.get(selection[pos])
            team_1 += g_1
            team_2 += g_2

        return team_1, team_2
