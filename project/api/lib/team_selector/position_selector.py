from abc import abstractmethod, ABC
from collections import defaultdict
from copy import deepcopy
from typing import List

from .position_selecting_strategies import PickStrategy
from .models import Team, UwhPlayer, Position


class SelectorABC(ABC):
    @abstractmethod
    def split_players(self, players: List[UwhPlayer]) -> Team:
        pass

    @abstractmethod
    def default_order(self) -> List[Position]:
        pass


class StrongestFillsSelector(SelectorABC):
    def __init__(self, order: List[Position] | None = None) -> None:
        self.order = order or self.default_order()

    def split_players(self, players: List[UwhPlayer]) -> Team:
        d = defaultdict(lambda: [])

        for position in self.order:
            for _ in range(2):
                player = self._find_strongest(
                    position, players
                ) or self._find_strongest_remaining(players)

                if not player:
                    break

                d[position].append(player)
                players.remove(player)

        return Team.from_dict(d)

    def _find_strongest(
        self, position: Position, players: List[UwhPlayer]
    ) -> UwhPlayer | None:
        potential_primaries = []
        potential_secondaries = []
        for player in players:
            if player.preferred_position == position:
                potential_primaries.append(player)
            elif player.secondary_position == position:
                potential_secondaries.append(player)

        return (
            self._find_strongest_remaining(potential_primaries)
            or self._find_strongest_remaining(potential_secondaries)
            or None
        )

    def _find_strongest_remaining(self, players: List[UwhPlayer]) -> UwhPlayer | None:
        if not players:
            return None
        return sorted(players, key=lambda player: player.strength, reverse=True)[0]

    def default_order(self) -> List[Position]:
        return [
            Position.GOALIE,
            Position.WING,
            Position.FORWARD,
            Position.CENTER,
            Position.WING,
            Position.FORWARD,
            Position.FORWARD,
            Position.WING,
            Position.FORWARD,
            Position.WING,
            Position.CENTER,
            Position.GOALIE,
        ]


class PositionSelector:
    def __init__(self, strategy: PickStrategy):
        self.strategy = strategy

    def split_players(self, players_: List[UwhPlayer]):
        players = deepcopy(players_)
        splitted = defaultdict(lambda: [])

        for position in self.strategy:  # type: ignore
            for _ in range(2):
                player = self.pop_player(position, players)
                if player:
                    splitted[position].append(player)
                else:
                    break

        return splitted

    def pop_player(self, position: Position, players: List[UwhPlayer]):
        secondaries = []
        for player in players:
            if player.preferred_position == position:
                players.remove(player)
                return player
            if player.secondary_position == position:
                secondaries.append(player)

        if secondaries:
            picked = secondaries[0]
            players.remove(picked)
            return picked

        return None
