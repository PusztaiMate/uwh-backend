from abc import ABC, abstractmethod

from .models import Position


class PickStrategy(ABC):
    @abstractmethod
    def __iter__(self) -> "PickStrategy":
        pass

    @abstractmethod
    def __next__(self) -> Position:
        pass


class GoaliesCentersFirst(PickStrategy):
    def __init__(self) -> None:
        self._order = [
            Position.GOALIE,
            Position.CENTER,
            Position.WING,
            Position.FORWARD,
            Position.WING,
            Position.FORWARD,
            Position.FORWARD,
            Position.WING,
            Position.FORWARD,
            Position.WING,
            Position.GOALIE,
            Position.CENTER,
        ]
        self._curr = 0

    def __iter__(self):
        return self

    def __next__(self) -> Position:
        pos = self._order[self._curr]
        self._curr += 1
        if self._curr == len(self._order):
            raise StopIteration()
        return pos
