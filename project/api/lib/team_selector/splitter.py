from abc import ABC, abstractmethod
from itertools import combinations
from typing import Tuple

from .models import Team


class SplitStrategy(ABC):
    @abstractmethod
    def get(self, players: Team) -> Tuple[Team, Team]:
        pass

    @staticmethod
    def calc_strength(group: Team):
        return sum(p.strength for p in group)


class ExhaustiveSplitter(SplitStrategy):
    MAX_STRENGTH_DIFF = 3

    def get(self, players: Team) -> Tuple[Team, Team]:
        goalies_per_team = len(players.goalies) // 2
        centers_per_team = len(players.centers) // 2
        wings_per_team = len(players.wings) // 2
        forwards_per_team = len(players.forwards) // 2

        candidates = []
        min_diff = 999

        for goalies in combinations(players.goalies, goalies_per_team):
            for centers in combinations(players.centers, centers_per_team):
                for wings in combinations(players.wings, wings_per_team):
                    for forwards in combinations(players.forwards, forwards_per_team):
                        team_a = Team(goalies, centers, wings, forwards)
                        team_b = players - team_a

                        if (
                            abs(team_a.strength - team_b.strength)
                            > self.MAX_STRENGTH_DIFF
                        ):
                            continue
                        str_diff = abs(team_a.strength - team_b.strength)

                        if abs(team_a.strength - team_b.strength) > min_diff:
                            continue
                        elif str_diff < min_diff + 0.5 and str_diff >= min_diff:
                            candidates.append((team_a, team_b))

                        elif str_diff < min_diff:
                            candidates.clear()
                            candidates.append((team_a, team_b))

        return candidates[0]


class BalancedSplitter(SplitStrategy):
    def get(self, players: Team) -> Tuple[Team, Team]:
        min_diff = 9999
        num_of_group_1 = len(players) // 2
        num_of_group_2 = len(players) - num_of_group_1
        best_group_1 = None

        for group_1 in combinations(players, num_of_group_1): # type: ignore
            sum_strength_of_group_1 = self.calc_strength(group_1) # type: ignore
            sum_strength_of_group_2 = (
                self.calc_strength(players) - sum_strength_of_group_1
            )

            diff = abs(
                (sum_strength_of_group_1 / num_of_group_1)
                - (sum_strength_of_group_2 / num_of_group_2)
            )

            if diff < min_diff:
                min_diff = diff
                best_group_1 = group_1

        return best_group_1, set(players) - set(best_group_1) # type: ignore
