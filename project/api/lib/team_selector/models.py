from typing import Iterable, List, Dict
from enum import Enum
from copy import deepcopy


class Position(Enum):
    GOALIE = 1
    CENTER = 2
    WING = 3
    FORWARD = 4


class UwhPlayer:
    def __init__(self, positions, id_, strength, **kw):
        self.positions = positions
        self.strength = strength
        self.id = id_

    @property
    def preferred_position(self):
        return self.positions[0]

    @property
    def secondary_position(self) -> Position | None:
        if len(self.positions) == 1:
            return None
        return self.positions[1]

    @staticmethod
    def _pos_as_str(pos: Position | None) -> str:
        if pos is None:
            return "?"
        if pos == Position.CENTER:
            return "C"
        elif pos == Position.GOALIE:
            return "G"
        elif pos == Position.WING:
            return "W"
        elif pos == Position.FORWARD:
            return "F"
        else:
            return "?"

    def __str__(self) -> str:
        pos_1 = self._pos_as_str(self.preferred_position)
        pos_2 = self._pos_as_str(self.secondary_position)
        return f"Player({self.id}, [{pos_1}, {pos_2}], {self.strength})"

    def __repr__(self) -> str:
        return str(self)

    def __eq__(self, other):
        if not isinstance(other, UwhPlayer):
            return False
        return self.id == other.id

    def __hash__(self) -> int:
        return hash(self.id)

    def to_dict(self):
        return {"id": self.id, "strength": self.strength}


class Team(Iterable):
    def __init__(
        self,
        goalies: Iterable[UwhPlayer],
        centers: Iterable[UwhPlayer],
        wings: Iterable[UwhPlayer],
        forwards: Iterable[UwhPlayer],
    ):
        self._players_by_position = {
            Position.GOALIE: goalies,
            Position.CENTER: centers,
            Position.WING: wings,
            Position.FORWARD: forwards,
        }

    def __iter__(self) -> Iterable[UwhPlayer]:
        return iter(self.members)


    def __getitem__(self, position: Position) -> List[UwhPlayer]:
            return list(self._players_by_position.get(position, []))

    def __contains__(self, player: UwhPlayer) -> bool:
        return player in self.members

    @classmethod
    def from_dict(cls, d: Dict[Position, List[UwhPlayer]]) -> "Team":
        goalies = d.get(Position.GOALIE, [])
        centers = d.get(Position.CENTER, [])
        wings = d.get(Position.WING, [])
        forwards = d.get(Position.FORWARD, [])

        return cls(goalies, centers, wings, forwards)

    @property
    def goalies(self) -> List[UwhPlayer]:
        return self[Position.GOALIE]

    @property
    def forwards(self) -> List[UwhPlayer]:
        return self[Position.FORWARD]

    @property
    def wings(self) -> List[UwhPlayer]:
        return self[Position.WING]

    @property
    def centers(self) -> List[UwhPlayer]:
        return self[Position.CENTER]

    def __eq__(self, other) -> bool:
        if not isinstance(other, Team):
            return False
        return (
            self._same_members(self.centers, other.centers)
            and self._same_members(self.goalies, other.goalies)
            and self._same_members(self.wings, other.wings)
            and self._same_members(self.forwards, other.forwards)
        )

    @property
    def members(self) -> List[UwhPlayer]:
        return self.centers + self.goalies + self.wings + self.forwards

    def _same_members(self, group_a: List[UwhPlayer], group_b: List[UwhPlayer]) -> bool:
        if len(group_b) != len(group_a):
            return False
        for m in group_a:
            if m not in group_b:
                return False
        return True

    def __str__(self) -> str:
        return (
            f"Team(Goalies: {', '.join([str(p) for p in self.goalies])},"
            f" Centers: {', '.join([str(p) for p in self.centers])},"
            f" Wings: {', '.join([str(p) for p in self.wings])},"
            f" Forwards: {', '.join([str(p) for p in self.forwards])},"
        )

    def __len__(self) -> int:
        return len(self.members)

    @staticmethod
    def _sub(
        from_: List[UwhPlayer], members_to_remove: Iterable[UwhPlayer]
    ) -> List[UwhPlayer]:
        from_ = deepcopy(from_)
        for member in members_to_remove:
            try:
                from_.remove(member)
            except ValueError:
                pass

        return from_

    def __sub__(self, other: "Team") -> "Team":
        return Team(
            goalies=self._sub(self.goalies, other.goalies),
            centers=self._sub(self.centers, other.centers),
            wings=self._sub(self.wings, other.wings),
            forwards=self._sub(self.forwards, other.forwards),
        )

    @staticmethod
    def _avg_strength(group: List[UwhPlayer]) -> float:
        if not group:
            return 0.0
        if len(group) == 1:
            return group[0].strength
        return sum([p.strength for p in group]) / len(group)

    @property
    def strength(self) -> float:
        return (
            self._avg_strength(self.goalies)
            + self._avg_strength(self.centers)
            + 2 * self._avg_strength(self.wings)
            + 2 * self._avg_strength(self.forwards)
        )

    def to_dict(self) -> Dict[str, List[Dict[str, int]]]:
        return {
            "goalies": [p.to_dict() for p in self.goalies],
            "centers": [p.to_dict() for p in self.centers],
            "wings": [p.to_dict() for p in self.wings],
            "forwards": [p.to_dict() for p in self.forwards],
        }

    def to_json(self) -> Dict[str, List[int] | float]:
        return {"members": [p.id for p in self.members], "strength": self.strength}


def position_from_string(pos_name: str):
    return {
        "forward": Position.FORWARD,
        "goalie": Position.GOALIE,
        "center": Position.CENTER,
        "wing": Position.WING,
    }[pos_name.lower()]


def find_player_by_id(id_: int, players):
    for player in players:
        if player.id == id_:
            return player
    return None
