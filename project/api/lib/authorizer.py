
from .casbin_client import casbin_client, CasbinClient


class AuthorizerService:
    def __init__(self, casbin_client: CasbinClient):
        self.casbin_client = casbin_client

    def create_training(self, sub: str = ""):
        # current_user = sub or request.environ.get("x-user", "")
        # is_allowed = casbin_client.enforce(current_user, "trainings", "create")
        # if not is_allowed:
        #     raise UnauthorizedError("User is not allowed to create trainings")
        return True

    def read_training(self, sub: str = ""):
        return True

    def create_user(self, sub: str = ""):
        # current_user = sub or request.environ.get("x-user", "")
        # is_allowed = casbin_client.enforce(current_user, "users", "create")
        # if not is_allowed:
        #     raise UnauthorizedError("User is not allowed to create users")
        return True

    def read_user(self, sub: str = ""):
        return True

    def create_player(self, sub: str = ""):
        # current_user = sub or request.environ.get("x-user", "")
        # is_allowed = casbin_client.enforce(current_user, "players", "create")
        # if not is_allowed:
        #     raise UnauthorizedError("User is not allowed to create players")
        return True

    def read_player(self, sub: str = ""):
        return True


authorizer = AuthorizerService(casbin_client)
