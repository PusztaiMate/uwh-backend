from datetime import datetime
from typing import Optional
from flask import Blueprint
from project.api.models.training import Training

from project.services.training_service import TrainingRepository, TrainingService


from ..database import db
from ..repositories.strength_values_repository import StrengthValuesDb
from ..repositories.player_repository import PlayerDB
from ..services.strength_values_service import StrengthValuesService
from ..services.player_service import PlayerService

stats_for_2023 = Blueprint(
    "stats_for_2023", __name__, url_prefix="/api/v1/stats_for_2023"
)

sv_db = StrengthValuesDb(db)
sv_service = StrengthValuesService(sv_db)
player_db = PlayerDB(db)
player_service = PlayerService(player_db)
training_db = TrainingRepository(db)
training_service = TrainingService(training_db)


@stats_for_2023.route("/", methods=["GET"])
def get_stats_for_2023():
    for player_id in [1, 3, 6]:
        player = player_service.get_by_id(player_id)
        print(f"player {player.id} {player.fname} {player.lname} missed trainings:")
        missed_trainings = get_trainings_where_player_was_missing_in_2023(player_id)
        for date in missed_trainings:
            print(date.isoformat())
    return {
        "1. Melyik játékosok szerezték a legtöbb pontot idén?": get_top_n_most_improved_players(
            5
        ),
        "2. Kik lőtték a legtöbb gólt?": {
            _get_name_for_player(k): v for k, v in get_top_scorers_for_2023(5)
        },
        "3. Kik lőtték átlagosan a legtöbb gólt?": get_avg_goal_for_top_n_player_in_2023(
            5
        ),
        "4. Kik voltak a legtöbb edzésen idén?": get_top_n_training_number_by_player_in_2023(
            5
        ),
        "5. Legsikeresebb párosok idén?": {
            k: v
            for k, v in get_pairings_with_names_instead_of_ids(1).items()
            if v != {}
        },
        "6. Hány (és milyen) versenyeken vettek részt idén Egyszusz játékosok (eddig)?": {
            "1": "Ausztráliai 4v4",
            "2": "Kranj",
            "3": "Ceske Budejovice",
            "4": "Csapat EB",
            "5": "Francia bajnokság tavaszi forduló",
            "6": "Prága",
            "7": "Francia bajnokság harmad osztály",
            "8": "Breda",
            "9": "Francia bajnokság őszi forduló",
            "10": "Belga 4v4",
        },
        "7. Hol volt a legutóbbi világbajnokság?": "2023, Gold Coast, Ausztrália",
        "8. Kik voltak a döntők szereplői és mik voltak az eredmények?": {
            "Férfi elit": "Új-Zéland - Franciaország 4-2",
            "Női elit": "Ausztália - Franciaország 2-1",
            "Férfi master": "USA - Franciaország 4-0",
            "Női master": "Franciaország - Ausztrália 2-0",
        },
        "9. Hány világversenyen (EB/VB) vett részt a magyar válogatott?": {
            "Belgrád 2001": "EB",
            "San Marino 2003": "EB",
            "Marseille 2005": "EB",
            "Sheffield 2006": "VB",
            "Kranj 2009": "VB",
            "Eger 2013": "VB",
            "Eger 2017": "EB",
        },
        "10. Hova szoktuk lerakni a kapuk szélét jelző tappancsokat?": "A faltól számított harmadik lefolyótól visszamérve 1 csempényit kb.",
        "11. Ki jár a legmesszebbről edzésre?": "Kovács Zoli?",
        "12. Ki a legidősebb játékos?": "Kovács Zoli",
        "13. Ki a legfiatalabb játékos?": "Donát",
        "14. Úsztunk 2 200m-es felmérőt idén. Ki gyorsult a legtöbbet a kettő között?": "Anti és Dávid, 9-9 másodperc",
        "15. Hány FB posztot raktunk ki idén": "14",
        "16. Melyik volt a legnépszerűbb és hány like/szív/stb. érkezett rá összesen?": "80",
    }


def get_pairings_with_names_instead_of_ids(n: int = 3) -> dict:
    pairings = get_n_best_pairing_for_every_player_in_2023(n)
    from pprint import pprint

    pprint(pairings)
    return {
        _get_name_for_player(player_id): {
            _get_name_for_player(k): v for k, v in pairing_list.items()
        }
        for player_id, pairing_list in pairings.items()
    }


def get_top_scorers_for_2023(n: int) -> list[tuple]:
    goals_scored = get_number_of_goals_scored_by_players_in_2023()
    return sorted(goals_scored.items(), key=lambda x: x[1], reverse=True)[:n]


def get_number_of_goals_scored_by_players_in_2023() -> dict:
    goals_scored = {}
    trainings_in_2023 = training_service.get_trainings_for_year(2023)
    for training in trainings_in_2023:
        for id, goals in training.scorers.items():
            if id not in goals_scored:
                goals_scored[id] = 0
            goals_scored[id] += goals
    return goals_scored


def get_top_n_most_improved_players(n: int) -> dict:
    changes = get_players_sv_changes_from_jan_1st_to_current()
    return {
        k: v for k, v in sorted(changes.items(), key=lambda x: x[1], reverse=True)[:n]
    }


def get_top_n_training_number_by_player_in_2023(n: int) -> dict:
    trainings_per_player = get_number_of_trainings_per_player_in_2023()
    return {
        _get_name_for_player(k): v
        for k, v in sorted(
            trainings_per_player.items(), key=lambda x: x[1], reverse=True
        )[:n]
    }


def get_trainings_where_player_was_missing_in_2023(player_id: int) -> list:
    trainings = training_service.get_trainings_for_year(2023)
    return [
        t.date
        for t in trainings
        if player_id not in t.white_team
        and player_id not in t.black_team
        and player_id not in t.swimming_players
        and player_id not in t.players
    ]


def get_number_of_trainings_per_player_in_2023() -> dict:
    trainings_per_player = {}
    trainings_in_2023 = training_service.get_trainings_for_year(2023)
    for training in trainings_in_2023:
        for player in training.players:
            id_ = player.id
            if id_ not in trainings_per_player:
                trainings_per_player[id_] = 0
            trainings_per_player[id_] += 1
    return trainings_per_player


def get_avg_goal_for_top_n_player_in_2023(n: int) -> dict:
    goals_scored = get_number_of_goals_scored_by_players_in_2023()
    number_of_trainings_per_player = get_number_of_trainings_per_player_in_2023()
    avg_goals = {}
    for player_id, goals in sorted(
        goals_scored.items(), key=lambda x: x[1], reverse=True
    )[:n]:
        avg_goals[_get_name_for_player(player_id)] = (
            goals / number_of_trainings_per_player[int(player_id)]
        )
    return avg_goals


def get_2023_strength_value_change_for_player(player_id) -> Optional[float]:
    player = player_service.get_by_id(player_id)
    if player is None:
        return None

    sv_on_january_1st_2023 = sv_service.get_value_for_player_on_date(
        player_id, datetime(2023, 1, 1)
    )
    current_sv = sv_service.get_latest_for_player(player_id)

    if sv_on_january_1st_2023 is None or current_sv is None:
        return None

    diff = current_sv.value - sv_on_january_1st_2023.value
    if diff == 0:
        return None

    return current_sv.value - sv_on_january_1st_2023.value


def get_players_sv_changes_from_jan_1st_to_current() -> dict:
    players = player_service.get_all_players()
    sv_changes = {}
    for player in players:
        change = get_2023_strength_value_change_for_player(player.id)
        if change is None:
            continue
        sv_changes[_get_name_for_player(player.id)] = change
    return sv_changes


def get_pairings_for_players_in_2023() -> dict:
    trainings = training_service.get_trainings_for_year(2023)
    pairings = {}
    for training in trainings:
        score = _get_score_for_training(training)
        print(f"score: {score}")

        for player_id_1 in training.white_team:  # type: ignore
            if player_id_1 not in pairings:
                pairings[player_id_1] = {}
            for player_id_2 in training.white_team:  # type: ignore
                if player_id_2 == player_id_1:
                    continue
                if player_id_2 not in pairings[player_id_1]:
                    pairings[player_id_1][player_id_2] = {
                        "egy csapatban": 0,
                        "nyertek közösen": 0,
                    }
                pairings[player_id_1][player_id_2]["egy csapatban"] += 1
                if score["white"] > score["black"]:
                    pairings[player_id_1][player_id_2]["nyertek közösen"] += 1

        for player_id_1 in training.black_team:  # type: ignore
            if player_id_1 not in pairings:
                pairings[player_id_1] = {}
            for player_id_2 in training.black_team:  # type: ignore
                if player_id_2 == player_id_1:
                    continue
                if player_id_2 not in pairings[player_id_1]:
                    pairings[player_id_1][player_id_2] = {
                        "egy csapatban": 0,
                        "nyertek közösen": 0,
                    }
                pairings[player_id_1][player_id_2]["egy csapatban"] += 1
                if score["white"] < score["black"]:
                    pairings[player_id_1][player_id_2]["nyertek közösen"] += 1

    return pairings


def get_n_best_pairing_for_every_player_in_2023(n: int = 3) -> dict:
    pairings = get_pairings_for_players_in_2023()
    best_pairings = {}
    for player_id, pairings_with_other_players in pairings.items():
        # filter out pairings with less than 5 games
        pairings_with_other_players = {
            k: v
            for k, v in pairings_with_other_players.items()
            if v["egy csapatban"] >= 5
        }

        best_pairings[player_id] = dict(
            sorted(
                pairings_with_other_players.items(),
                key=lambda x: x[1]["nyertek közösen"] / x[1]["egy csapatban"],
                reverse=True,
            )[:n]
        )

    return best_pairings


def _get_score_for_training(training: Training) -> dict:
    white_score, black_score = 0, 0
    for id, goals in training.scorers.items():
        if int(id) in training.white_team:  # type: ignore
            white_score += goals
        elif int(id) in training.black_team:  # type: ignore
            black_score += goals
    return {
        "white": white_score,
        "black": black_score,
    }


def get_final_score_for_training(training: Training) -> dict:
    return {
        "white": sum(training.scorers.get(p_id, 0) for p_id in training.white_team),
        "black": sum(training.scorers.get(p_id, 0) for p_id in training.black_team),
    }


def _get_name_for_player(player_id: int) -> str:
    player = player_service.get_by_id(player_id)
    return f"{player.fname} {player.lname}"
