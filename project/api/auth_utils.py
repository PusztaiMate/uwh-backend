from datetime import datetime, timedelta, timezone
from functools import wraps

from flask import request, current_app
import jwt

from project.api.models import User


def token_required(f, username=None):
    @wraps(f)
    def _verify(*args, **kwargs):
        auth_headers = request.headers.get("Authorization", "").split()
        current_app.logger.debug(auth_headers)

        invalid_msg = {
            "message": "Invalid token. Registeration and / or authentication required",
            "authenticated": False,
            "status": "fail",
        }
        expired_msg = {
            "message": "Expired token. Reauthentication required.",
            "authenticated": False,
            "status": "success",
        }

        if len(auth_headers) != 2:
            return invalid_msg, 401

        try:
            token = auth_headers[1]
            data = jwt.decode(
                token, current_app.config["SECRET_KEY"], algorithms=["HS256"]
            )
            user = User.query.filter_by(username=data["sub"]).first()
            current_app.logger.debug(f"subject: {data['sub']}")
            if not user:
                raise RuntimeError("User not found")
            return f(user, *args, **kwargs)
        except jwt.ExpiredSignatureError:
            return expired_msg, 401
        except (jwt.InvalidTokenError, RuntimeError) as e:
            current_app.logger.error(e)
            return invalid_msg, 401

    return _verify


def admin_required(f):
    @wraps(f)
    def _inner(user, *args, **kwargs):
        if user.username != "Admin":
            return {
                "message": "Admin privileges are needed for this API call",
                "status": "fail",
            }, 401
        return f(*args, **kwargs)

    return _inner


def user_required(username: str):
    def decor(f):
        @wraps(f)
        def _inner(user, *args, **kwargs):
            if user.username != username:
                return {
                    "message": "Special user privileges are needed for this API call",
                    "status": "fail",
                }, 401
            return f(*args, **kwargs)

        return _inner

    return decor


def create_access_token(username: str):
    return jwt.encode(
        {
            "sub": username,
            "iat": datetime.now(tz=timezone.utc),
            "exp": datetime.now(tz=timezone.utc) + timedelta(weeks=52),
        },
        current_app.config["SECRET_KEY"],
    )
