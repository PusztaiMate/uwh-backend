class BadRequest(RuntimeError):
    pass


class UnauthorizedError(RuntimeError):
    pass
