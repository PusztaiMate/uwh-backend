from flask import Blueprint, jsonify, request

from project.api.db_utils import (
    ResourceAlreadyPresent,
    query_all_positions,
    add_new_position,
)


positions_api = Blueprint("positions_api", __name__, url_prefix="/api/v1/positions")


@positions_api.route("/", methods=["GET"])
def list_all_positions():
    return jsonify([pos.to_dict() for pos in query_all_positions()]), 200


@positions_api.route("/", methods=["POST"])
def new_position():
    name = request.json.get("name", None)

    if not name:
        return {"message": "position name is missing"}, 400

    try:
        add_new_position(name)
    except ResourceAlreadyPresent as e:
        return {"message": str(e)}, 400

    return {"message": f"position '{name}' successfully added"}, 200
