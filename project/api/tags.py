from flask import Blueprint, request

from project import db
from project.api.exceptions import BadRequest
from project.repositories.tag_repository import TagDB
from project.services.tag import (
    TagAlreadyExists,
    TagDTO,
    TagNotFound,
    TagService,
)


tags_api = Blueprint("tags_api", __name__, url_prefix="/api/v1/tags")


tag_db = TagDB(db)
service = TagService(tag_db)


@tags_api.route("/", methods=["GET"])
def get_all():
    return {"tags": [t.to_dict() for t in service.get_all()]}, 200


@tags_api.route("/", methods=["POST"])
def add_one():
    try:
        tag = _get_tag_data_from_request(request.get_json())
        service.save(tag)
        return {"message": "tag successfully added", "status": "success"}, 201
    except BadRequest as e:
        return {"message": str(e), "status": "failed"}, 400
    except TagAlreadyExists as e:
        return {"message": str(e), "status": "failed"}, 400
    except Exception as e:
        return {"message": str(e), "status": "failed"}, 500


@tags_api.route("/<int:tag_id>/", methods=["DELETE"])
def delete(tag_id: int):
    try:
        service.delete(tag_id)
        return {"message": "successfully deleted tag", "status": "success"}, 200
    except TagNotFound as e:
        return {"message": str(e), "status": "failed"}, 404


@tags_api.route("/<int:tag_id>/", methods=["GET"])
def get_one(tag_id: int):
    tag = service.get(tag_id)
    if tag is None:
        return {"message": f"tag with id '{tag_id}' not found", "status": "failed"}, 404
    return tag.to_dict()


@tags_api.app_errorhandler(404)
def not_found(exc):
    return {"message": "requested page not found", "status": "failed"}, 404


def _get_tag_data_from_request(json_data: dict) -> TagDTO:
    try:
        name = json_data["name"]
        clips = []
        tag = TagDTO(name, clips)
        return tag
    except KeyError as e:
        raise BadRequest(f"data is missing from request: '{e.args[0]}'")
