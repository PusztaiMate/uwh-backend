"""This api is only present to test ongoing changes"""

from flask import Blueprint

from project.api.auth_utils import token_required, user_required


testing_api = Blueprint("testing_api", __name__, url_prefix="/api/v1/test")


@testing_api.route("/")
@token_required
def login_test(user):
    return {"username": user.username}, 200


@testing_api.route("/testuser/")
@token_required
@user_required("TestUser1")
def required_to_be_testuser():
    return "<h1>Congrats, you are the TestUser</h1>", 200
