import os
from random import randint
from typing import TypedDict
from flask import Blueprint, jsonify, request, current_app
from pywebpush import webpush, WebPushException
from sqlalchemy.exc import IntegrityError
from project.api.models.notification import NotificationSubscription

from project.repositories.notification_subscription_repository import (
    NotificationSubscriptionDb,
)
from ..database import db


notifications_api = Blueprint("webpush", __name__, url_prefix="/api/v1/notifications")


class SubscriptionKeys(TypedDict):
    auth: str
    p256dh: str


class SubscriptionInfo(TypedDict):
    endpoint: str
    keys: SubscriptionKeys


subscription_db = NotificationSubscriptionDb(db)


@notifications_api.post("/register")
def register():
    subscription_info: SubscriptionInfo = request.get_json()
    sub = NotificationSubscription(
        endpoint=subscription_info["endpoint"],
        keys={
            "auth": subscription_info["keys"]["auth"],
            "p256dh": subscription_info["keys"]["p256dh"],
        },
    )
    try:
        subscription_db.create(sub)
    except IntegrityError as ex:
        current_app.logger.info(
            f"Subscription already exists: {subscription_info['endpoint']}"
        )
        return {"success": False, "message": "Already subscribed"}
    return {"success": True}


@notifications_api.get("/")
def list_subscriptions():
    current_app.logger.info(f"list_subscriptions {subscription_db.get_all()}")
    return jsonify([s.to_dict() for s in subscription_db.get_all()])


@notifications_api.post("/test")
def test():
    for subscription_info in subscription_db.get_all():
        send_push(
            subscription_info.to_subscription_info(),
            f"Ez egy teszt üzenet, amit a webpush-ban kaptál (plusz egy random szám: {randint(0, 9999)}).",
        )
    return {"success": True}


def send_push_to_all_subscribers(message: str):
    for subscription_info in subscription_db.get_all():
        send_push(subscription_info.to_subscription_info(), message)


def send_push(subscription_info, message):
    vapid_info = {
        "subject": os.getenv("VAPID_SUBJECT"),
        "publicKey": os.getenv("VAPID_PUBLIC_KEY"),
        "privateKey": os.getenv("VAPID_PRIVATE_KEY"),
    }
    VAPID_PRIVATE_KEY = vapid_info["privateKey"]
    VAPID_CLAIMS = {
        "sub": vapid_info["subject"],
    }

    try:
        webpush(
            subscription_info=subscription_info,
            data=message,
            vapid_private_key=VAPID_PRIVATE_KEY,
            vapid_claims=VAPID_CLAIMS,
        )
    except WebPushException as ex:
        current_app.logger.error(f"Web Push failed: {ex}")
        if _is_gone_error(ex):
            current_app.logger.info(
                f"Subscription gone. Deleting from database: {subscription_info['endpoint']}",
            )
            subscription_db.delete_by_endpoint(subscription_info["endpoint"])


def _is_gone_error(ex: WebPushException) -> bool:
    if ex.response is None:
        current_app.logger.info("No response from Web Push service exception")
        return False

    is_gone_error = "410 Gone" in ex.message

    return is_gone_error
