from typing import List
from flask import Blueprint, request

from project import db
from project.api.exceptions import BadRequest
from project.repositories.clips_repository import (
    ClipDB,
)
from project.repositories.tag_repository import TagDB
from project.services.clips import ClipAlreadyExists, ClipNotFound, ClipService
from project.services.models.clip_dto import ClipDTO
from project.services.models.timestamp import TimeStamp, TimeStampParsingError
from project.services.tag import TagDTO, TagService


clips_db_api = Blueprint("clips_db_api", __name__, url_prefix="/api/v1/clips")


clips_db = ClipDB(db)
tags_db = TagDB(db)
tag_service = TagService(tags_db)
service = ClipService(tag_service, clips_db)


@clips_db_api.route("/", methods=["GET"])
def get_all():
    return {"clips": [c.to_dict() for c in service.get_all()]}, 200


@clips_db_api.route("/", methods=["POST"])
def add_one():
    try:
        clip = _get_clip_data_from_post_request(request.get_json())
    except BadRequest as e:
        return {"message": str(e), "status": "failed"}, 400
    except Exception as e:
        return {"message": str(e), "status": "failed"}, 500

    try:
        service.save(clip)
    except ClipAlreadyExists as e:
        return {"message": str(e), "status": "failed"}, 400
    except Exception as e:
        return {"message": str(e), "status": "failed"}, 500

    return {"message": "clip successfully added", "status": "success"}, 201


@clips_db_api.route("/multi/", methods=["POST"])
def create_multiple_clips():
    errors = []
    clip_list = request.get_json().get("clips", [])

    if not clip_list:
        return {
            "message": "no clip list passed in request, doing nothing",
            "status": "success",
        }, 200

    for clip_data in clip_list:
        try:
            clip_dto = _get_clip_data_from_post_request(clip_data)
            service.save(clip_dto)
        except BadRequest as e:
            errors.append(str(e))
        except ClipAlreadyExists as e:
            errors.append(str(e))
        except Exception as e:
            errors.append(str(e))

    if errors:
        return {"message": ", ".join(errors), "status": "fail"}, 400
    return {
        "message": f"successfully created {len(clip_list)} clips!",
        "status": "success",
    }, 201


@clips_db_api.route("/<int:clip_id>", methods=["DELETE"])
def delete(clip_id: int):
    try:
        service.delete(clip_id)
    except ClipNotFound as e:
        return {"message": str(e), "status": "failed"}, 404

    return {"message": "clip successfully deleted", "status": "success"}, 200


@clips_db_api.route("/<int:clip_id>", methods=["GET"])
def get_one(clip_id: int):
    clip = service.get(clip_id)
    if clip is None:
        return {
            "message": f"clip with id '{clip_id}' not found",
            "status": "failed",
        }, 404
    return clip.to_dict(), 200


def _get_clip_data_from_post_request(request_data: dict) -> ClipDTO:
    try:
        video_url = request_data["video_url"]
        start_ts = request_data["start_ts"]
        end_ts = request_data["end_ts"]
        tags = collect_available_tags(request_data.get("tags", []))
        clip = ClipDTO(
            video_url,
            tags,
            TimeStamp.from_min_sec_string(start_ts),
            TimeStamp.from_min_sec_string(end_ts),
        )
        return clip
    except KeyError as e:
        raise BadRequest(f"data is missing from request: '{e.args[0]}'")
    except TimeStampParsingError:
        raise BadRequest("bad time format")


def collect_available_tags(tag_names: List[str]) -> List[TagDTO]:
    tags = []
    for tag in tag_names:
        tag_or_none = tag_service.find_by_name(tag)
        if tag_or_none is None:
            continue
        tags.append(tag_or_none)
    return tags
