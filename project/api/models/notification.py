from ...database import db


class NotificationSubscription(db.Model):
    __tablename__ = "notification_subscriptions"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    endpoint = db.Column(db.String(2048), nullable=False, unique=True)
    keys = db.Column(db.JSON, nullable=False)

    def __init__(self, endpoint: str, keys: dict):
        self.endpoint = endpoint
        self.keys = keys

    def to_subscription_info(self) -> dict:
        return {
            "endpoint": self.endpoint,
            "keys": self.keys,
        }

    def to_dict(self) -> dict:
        return {
            "id": self.id,
            "endpoint": self.endpoint,
            "keys": self.keys,
        }

    def __repr__(self):
        return f"<NotificationSubscription {self.id} {self.endpoint} {self.keys}>"
