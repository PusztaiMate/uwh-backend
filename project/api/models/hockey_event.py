from typing import Optional, Literal
from datetime import datetime

from sqlalchemy import Column, Integer, DateTime, String, Text

from ...database import db


# no idea how to do this nicely
EVENT_TYPE_VALUES = ["competition", "practice", "other"]
EVENT_TYPES = Literal["competition", "practice", "other"]


class HockeyEvent(db.Model):
    __tablename__ = "hockey_events"

    id = Column(Integer(), primary_key=True, autoincrement=True)
    date = Column(DateTime(), nullable=False)
    title = Column(String(128), nullable=False)
    type = Column(String(64), nullable=False)
    description = Column(Text())

    def __init__(
        self,
        date: datetime,
        title: str,
        type: EVENT_TYPES,
        description: Optional[str] = "",
        id: Optional[int] = None,
    ):
        self.date = date
        self.title = title
        self.type = type
        self.description = description
        self.id = id
