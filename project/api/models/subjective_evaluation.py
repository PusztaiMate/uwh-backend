from flask import current_app

from ...database import db


class Image(db.Model):
    __tablename__ = "images"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    source = db.Column(db.String(256), nullable=False)
    description = db.Column(db.String(256), default="")
    tags = db.Column(db.String(256), default="")

    def __init__(self, source: str, description: str, tags: list):
        self.source = source
        self.description = description
        if isinstance(tags, str):
            self.tags = tags
        self.tags = ",".join([tag.strip() for tag in tags])

    def __repr__(self):
        return f"Image(src={self.source}, desc={self.description}, tags={self.tags!r})"

    def to_dict(self):
        return {"src": self.source, "desc": self.description, "tags": self.tags_split}

    @property
    def tags_split(self):
        return self.tags.split(",")

    @classmethod
    def from_json_data(cls, data: dict):
        src = data.get("src", "")
        if not src:
            current_app.logger.error("source is missing from request json data")
            return None
        desc = data.get("desc", "")
        tags = data.get("tags", [])
        return cls(src, desc, tags)
