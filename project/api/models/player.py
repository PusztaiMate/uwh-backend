from project.api.lib.team_selector.models import Position
from .association_tables import players_to_trainings
from ...database import db


class Player(db.Model):
    __tablename__ = "players"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    fname = db.Column(db.String(64), nullable=False)
    lname = db.Column(db.String(64), nullable=False)
    description = db.Column(db.String(2048), nullable=True, default="")
    club_id = db.Column(db.Integer, db.ForeignKey("clubs.id"))
    trainings = db.relationship(
        "Training", secondary=players_to_trainings, back_populates="players"
    )
    user = db.relationship("User", uselist=False, back_populates="player")
    training_strength = db.Column(db.Float(), default=1)
    primary_position = db.Column(db.Integer, db.ForeignKey("positions.id"))
    secondary_position = db.Column(db.Integer, db.ForeignKey("positions.id"))
    status: str = db.Column(db.String(32), nullable=False, default="active")

    def __init__(
        self,
        fname: str,
        lname: str,
        training_strenght: float = 3.0,
        primary_position: Position = Position.FORWARD,
        secondary_position: Position = Position.WING,
    ):
        self.fname = fname
        self.lname = lname
        self.training_strength = training_strenght
        self.primary_position = primary_position.value
        self.secondary_position = secondary_position.value

    def __repr__(self):
        return f"Player(fname={self.fname!r}, lname={self.lname!r})"

    def __str__(self):
        return f"{self.fname} {self.lname}"

    def to_json(self):
        return {
            "id": self.id,
            "fname": self.fname,
            "lname": self.lname,
            "description": self.description,
            "club_id": self.club_id,
            "training_strength": self.training_strength,
            "primary_position": self.primary_position,
            "secondary_position": self.secondary_position,
            "active": self.is_active,
            "status": self.status,
        }

    @property
    def is_active(self):
        return self.status.lower() in ["aktiv", "active", "aktív"]
