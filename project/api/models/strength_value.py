from datetime import datetime

from ...database import db


class StrengthValue(db.Model):
    __tablename__ = "strenght_values"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    value = db.Column(db.Float, nullable=False)
    player_id = db.Column(db.Integer, db.ForeignKey("players.id"))
    date = db.Column(db.DateTime())

    def __init__(self, value: int | float, player_id: int, date: datetime):
        self.value = value
        self.player_id = player_id
        self.date = date if date else datetime.now()

    def __repr__(self) -> str:
        return f"StrenghtValue({self.value}, {self.player_id}, {self.date!r}"

    def __eq__(self, o: object) -> bool:
        if not isinstance(o, StrengthValue):
            return False
        return o.value == self.value

    def to_dict(self):
        return {
            "id": self.id,
            "value": self.value,
            "player_id": self.player_id,
            "date": self.date.isoformat(),
        }
