from datetime import datetime

from dateutil import parser
import sqlalchemy

from .association_tables import players_to_trainings
from .training_team import TrainingTeam
from .player import Player
from ...database import db


class Training(db.Model):  # type: ignore
    TEAM_WHITE = 0
    TEAM_BLACK = 1
    SWIMMING_ONLY = 2

    __tablename__ = "trainings"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    date = db.Column(db.DateTime())
    club_id = db.Column(db.Integer, db.ForeignKey("clubs.id"))
    scorers = db.Column(sqlalchemy.JSON)
    players = db.relationship(
        "Player", secondary=players_to_trainings, back_populates="trainings"
    )
    teams = db.relationship("TrainingTeam")

    def __init__(
        self,
        club_id=None,
        date=None,
        swimming_players=None,
        black_team=None,
        white_team=None,
        scorers: dict[str, int] = None,
    ):
        self.players = self._save_teams(swimming_players, black_team, white_team)
        self.club_id = club_id
        self.date = self._determine_date(date)
        self.scorers = scorers or {}

    def _save_teams(self, swimming, black, white):
        swimming = swimming or []
        black = black or []
        white = white or []

        tw = TrainingTeam(self.id, color="white", player_ids=white)
        tb = TrainingTeam(self.id, color="black", player_ids=black)
        ts = TrainingTeam(self.id, color="none", player_ids=swimming)

        self.teams = [tw, tb, ts]

        return [Player.query.get(p_id) for p_id in set(swimming + black + white)]

    def get_scorers(self):
        return {int(player_id): int(score) for player_id, score in self.scorers.items()}

    @property
    def white_team(self):
        try:
            return self.teams[Training.TEAM_WHITE]
        except (TypeError, ValueError, IndexError):
            return None

    @white_team.setter
    def white_team(self, new_team):
        self.teams[Training.TEAM_WHITE] = TrainingTeam(
            self.id, color="white", player_ids=new_team
        )

    @property
    def black_team(self):
        try:
            return self.teams[Training.TEAM_BLACK]
        except (TypeError, ValueError, IndexError):
            return None

    @black_team.setter
    def black_team(self, new_team):
        self.teams[Training.TEAM_BLACK] = TrainingTeam(
            self.id,
            color="black",
            player_ids=new_team,
        )

    @property
    def swimming_players(self):
        try:
            return self.teams[Training.SWIMMING_ONLY]
        except (TypeError, ValueError, IndexError):
            return None

    @swimming_players.setter
    def swimming_players(self, new_team):
        self.teams[Training.SWIMMING_ONLY] = TrainingTeam(
            self.id, color="none", player_ids=new_team
        )

    def __repr__(self):
        return (
            f"Training(club_id={self.club_id!r},"
            f"date={self.date!r}, players={self.players!r})"
        )

    def __str__(self):
        return f"Training({self.date.isoformat()}, {len(self.players)} players)"

    def to_dict(self):
        return {
            "id": self.id,
            "date": self.date.isoformat(),
            "players": [p.id for p in self.players],
            "black_team": [] if not self.black_team else self.black_team.player_ids,
            "white_team": [] if not self.white_team else self.white_team.player_ids,
            "swimming_players": []
            if not self.swimming_players
            else self.swimming_players.player_ids,
            "scorers": self.get_scorers(),
        }

    def _determine_date(self, date):
        if isinstance(date, str):
            return parser.parse(date)
        elif isinstance(date, datetime):
            return date
        else:
            return datetime.now()
