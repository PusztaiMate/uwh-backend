from .player import Player
from .training import Training
from ...database import db


class Club(db.Model):
    __tablename__ = "clubs"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(128), nullable=False, unique=True)
    players = db.relationship("Player", backref="club")
    trainings = db.relationship("Training", backref="club")

    def __init__(self, name, player_ids=None, training_ids=None):
        self.name = name
        self.players = (
            [Player.query.get(int(p_id)) for p_id in player_ids] if player_ids else []
        )
        self.trainings = (
            [Training.query.get(int(t_id)) for t_id in training_ids]
            if training_ids
            else []
        )

    def __repr__(self) -> str:
        return (
            f"Club(name={self.name!r}, players={self.players!r}, trainings={self.trainings!r})"
        )
