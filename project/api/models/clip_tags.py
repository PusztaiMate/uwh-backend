from ...database import db
from project.api.models.association_tables import clips_to_tags


class Tag(db.Model):
    __tablename__ = "clip_tags"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(128), nullable=False, unique=True)
    clips = db.relationship("Clip", secondary=clips_to_tags, back_populates="tags")

    def __init__(self, name, clips=None):
        self.name = name
        self.clips = clips or []

    def to_dict(self):
        return {"id": self.id, "name": self.name}
