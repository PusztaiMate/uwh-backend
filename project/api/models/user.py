from werkzeug.security import check_password_hash, generate_password_hash

from ...database import db


class User(db.Model):  # type: ignore
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    password_hash = db.Column(db.String(128))
    player = db.relationship("Player", back_populates="user")
    player_id = db.Column(db.Integer, db.ForeignKey("players.id"))
    username = db.Column(db.String(64), unique=True, nullable=False)

    def __repr__(self):
        return f"<User {self.username}>"

    def __init__(self, username, password, email=None):
        self.username = username
        self.set_password(password)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    @classmethod
    def authenticate(cls, username, password):
        if not username or not password:
            return None

        user = cls.query.filter_by(username=username).first()
        if not user or not user.check_password(password):
            return None

        return user
