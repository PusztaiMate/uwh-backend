from ...database import db


class MonthlyPayObligation(db.Model):
    __tablename__ = "monthly_pay_obligations"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    player_id = db.Column(db.Integer, db.ForeignKey("players.id"))
    month = db.Column(db.Date())
    is_active = db.Column(db.Boolean)

    def __init__(self, player_id, month, is_active):
        self.player_id = player_id
        self.month = month
        self.is_active = is_active

    def to_dict(self):
        return {
            "id": self.id,
            "playerId": self.player_id,
            "month": self.month.isoformat(),
            "isActive": self.is_active,
        }

    def __repr__(self):
        return (
            f"{self.__class__.__name__}(id={self.id}, player_id={self.player_id},"
            f"month={self.month}, is_active={self.is_active})"
        )
