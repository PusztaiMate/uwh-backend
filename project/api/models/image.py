from typing import List
from uuid import uuid4

from flask import current_app
from sqlalchemy.dialects.postgresql import UUID

from ...database import db


class Image(db.Model):
    __tablename__ = "images"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    uuid = db.Column(UUID(as_uuid=True), default=uuid4, unique=True, nullable=False)
    description = db.Column(db.String(256), default="")
    tags = db.Column(db.String(256), default="")
    orig_extension = db.Column(db.String(10), default="")

    def __init__(
        self, description: str, tags: str | List[str], uuid: str, orig_extension: str
    ):
        self.description = description
        if isinstance(tags, str):
            self.tags = tags
        self.tags = ",".join([tag.strip() for tag in tags])
        self.uuid = uuid
        self.orig_extension = orig_extension

    def __repr__(self):
        return f"Image(desc={self.description}, tags={self.tags!r})"

    def to_dict(self):
        return {"uuid": self.uuid, "desc": self.description, "tags": self.tags_split}

    @property
    def tags_split(self):
        return self.tags.split(",")

    @classmethod
    def from_json_data(cls, data: dict):
        uuid = data.get("uuid", "")
        desc = data.get("desc", "")
        tags = data.get("tags", [])
        # add a dot to the extension if it's missing
        orig_extension = data.get("orig_extension", "")
        if not orig_extension.startswith(".") and orig_extension:
            orig_extension = f".{orig_extension}"
        return cls(desc, tags, uuid, orig_extension)
