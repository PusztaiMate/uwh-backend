from ...database import db


class Position(db.Model):
    __tablename__ = "positions"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(24), nullable=False, unique=True)

    def __init__(self, name: str, id: int | None = None) -> None:
        if id is not None:
            self.id = id
        self.name = name.lower()

    def to_dict(self) -> dict:
        return {"id": self.id, "name": self.name}
