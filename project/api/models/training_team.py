from ...database import db


class TrainingTeam(db.Model):
    __tablename__ = "training_teams"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    color = db.Column(db.String(20), nullable=False, default="none")
    _player_ids = db.Column(db.String(60), nullable=False, default="")
    training_id = db.Column(db.Integer, db.ForeignKey("trainings.id"))

    def __init__(self, training_id, color="none", player_ids=None):
        self.training_id = training_id
        self.color = color
        if not player_ids:
            self._player_ids = []
        else:
            self._player_ids = ";".join(map(str, player_ids))

    @property
    def player_ids(self):
        if not self._player_ids or self._player_ids == "{}":
            return []
        return list(map(int, self._player_ids.split(";")))

    @player_ids.setter
    def player_ids(self, player_ids):
        self._player_ids = ";".join(map(str, player_ids))

    def __getitem__(self, index):
        return self.player_ids[index]

    def __len__(self):
        return len(self.player_ids)
