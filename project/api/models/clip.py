from typing import List
from ...database import db
from project.api.models.association_tables import clips_to_tags
from project.api.models.clip_tags import Tag
from project.services.models.timestamp import TimeStamp


class Clip(db.Model):
    __tablename__ = "clips"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    video_url = db.Column(db.String(256), nullable=False)
    tags = db.relationship("Tag", back_populates="clips", secondary=clips_to_tags)
    start_ts = db.Column(db.String(32), nullable=False)
    end_ts = db.Column(db.String(32), nullable=False)

    def __init__(self, video_url: str, tags: List[Tag], start_ts: str, end_ts: str):
        ts = []
        for t in tags:
            tag_in_db = Tag.query.filter(Tag.name == t.name).first() # type: ignore
            if tag_in_db is None:
                continue
            ts.append(tag_in_db)

        self.video_url = video_url
        self.tags = ts
        self.start_ts = start_ts
        self.end_ts = end_ts

    @property
    def start_timestamp(self) -> TimeStamp:
        return TimeStamp.from_min_sec_string(self.start_ts)

    @start_timestamp.setter
    def start_timestamp(self, new_value):
        if not isinstance(new_value, TimeStamp):
            raise ValueError(
                f"'{new_value}' is not a TimeStamp object"
                " (maybe you meant to set Clip.start_ts?)"
            )
        self.start_ts = new_value.to_db_string()

    @property
    def end_timestamp(self) -> TimeStamp:
        return TimeStamp.from_min_sec_string(self.end_ts)

    @end_timestamp.setter
    def end_timestamp(self, new_value):
        if not isinstance(new_value, TimeStamp):
            raise ValueError(
                f"'{new_value}' is not a TimeStamp object"
                " (maybe you meant to set Clip.end_ts?)"
            )
        self.end_ts = new_value.to_db_string()
