from ...database import db


players_to_trainings = db.Table(
    "players_to_trainings",
    db.Column("player_id", db.Integer, db.ForeignKey("players.id")),
    db.Column("training_id", db.Integer, db.ForeignKey("trainings.id")),
)

clips_to_tags = db.Table(
    "clips_to_tags",
    db.Column("clip_id", db.Integer, db.ForeignKey("clips.id")),
    db.Column("tag_id", db.Integer, db.ForeignKey("clip_tags.id")),
)
