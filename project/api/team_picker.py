from project.api.models.player import Player

from flask import current_app, jsonify
from project.api.lib.team_selector.team_selector import TeamCalculationError, pick_team
from project.api.lib.team_selector.position_selector import StrongestFillsSelector
from project.api.lib.team_selector.splitter import ExhaustiveSplitter
from flask import Blueprint, request

from project.api.lib.team_selector import (
    Position,
    UwhPlayer,
)
from project.api.db_utils import get_latest_strength_value_for_player, query_all_players

team_picker_api = Blueprint(
    "team_picker_api", __name__, url_prefix="/api/v1/team-picker"
)


@team_picker_api.route("/", methods=["POST"])
def generate_team():
    try:
        player_ids = [int(id_) for id_ in request.json]
    except ValueError:
        return {
            "status": "failure",
            "message": "provided ids contain non integers",
        }, 400

    players_in_db = query_all_players()

    players = [create_player_from_db_data(players_in_db, p_id) for p_id in player_ids]

    splitter = ExhaustiveSplitter()
    position_selector = StrongestFillsSelector()

    try:
        teams = pick_team(players, position_selector, splitter)
    except TeamCalculationError as e:
        return {"status": "fail", "message": str(e)}, 500

    return (
        jsonify(
            {
                "status": "success",
                "message": "successfully generated teams",
                "teams": [
                    {"team_a": t[0].to_json(), "team_b": t[1].to_json()} for t in teams
                ],
            }
        ),
        200,
    )


def id_to_pos_enum(pos_id: int) -> Position:
    return {
        1: Position.GOALIE,
        2: Position.CENTER,
        3: Position.FORWARD,
        4: Position.WING,
    }.get(pos_id, Position.FORWARD)


def pos_enum_to_id(enum_val: Position) -> int:
    return {
        Position.GOALIE: 1,
        Position.CENTER: 2,
        Position.FORWARD: 3,
        Position.WING: 4,
    }.get(enum_val, 4)


def find_player_in_list_by_id(player_list, id_) -> Player:
    for player in player_list:
        if player.id == id_:
            return player
    return None


def create_player_from_db_data(players_in_db, player_id) -> UwhPlayer:
    player_from_db = find_player_in_list_by_id(players_in_db, player_id)
    if not player_from_db:
        current_app.logger.error(f"couldn't find player in list ({player_from_db})")
        raise RuntimeError("server error, for further info check the logs")
    primary = id_to_pos_enum(player_from_db.primary_position)
    secondary = id_to_pos_enum(player_from_db.secondary_position)
    sv = get_latest_strength_value_for_player(player_id)
    if sv is None:
        strength = player_from_db.training_strength
    else:
        strength = sv.value

    return UwhPlayer([primary, secondary], player_id, strength)
