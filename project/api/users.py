from flask import Blueprint, request, current_app
import jwt

from .models import User
from .auth_utils import create_access_token


users_api = Blueprint("users_api", __name__, url_prefix="/api/v1/users")
invalid_msg = {
    "message": "Invalid token. Registeration and / or authentication required",
    "authenticated": False,
    "status": "fail",
}
expired_msg = {
    "message": "Expired token. Reauthentication required.",
    "authenticated": False,
    "status": "success",
}


@users_api.route("/login/", methods=["POST"])
def login():
    data = request.get_json()
    username, password = data.get("username"), data.get("password")

    user = User.authenticate(username, password)

    if not user:
        return {"message": "Invalid credentials", "status": "fail"}, 401

    token = create_access_token(username)

    current_app.logger.info(f"New token generated for user {user.username}")

    return {
        "message": "Successfully logged in",
        "status": "success",
        "token": token,
    }, 200
