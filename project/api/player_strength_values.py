from datetime import datetime
from flask import Blueprint, jsonify, request, current_app

from project.repositories.player_repository import PlayerDB
from project.services.player_service import PlayerService

from ..api.models.strength_value import StrengthValue
from ..api.db_utils import (
    ResourceAlreadyPresent,
    get_player_by_id,
)
from ..api.lib.utils import parse_date
from ..database import db
from ..repositories.strength_values_repository import StrengthValuesDb
from ..services.strength_values_service import StrengthValuesService


player_strength_api = Blueprint(
    "player-strenght-api", __name__, url_prefix="/api/v1/strength-values"
)

sv_repository = StrengthValuesDb(db)
sv_service = StrengthValuesService(sv_repository)
player_repository = PlayerDB(db)
player_service = PlayerService(player_repository)


@player_strength_api.route("/", methods=["GET"])
def all_strength_values():
    s_values = sv_service.get_all()
    return jsonify([sv.to_dict() for sv in s_values]), 200


@player_strength_api.route("/", methods=["POST"])
def add_new_strength_values():
    data = request.json

    if not data:
        return {"message": "invalid payload, data is not present"}, 400

    try:
        # this is the happy path
        p_strength_vals = {}
        date = parse_date(data["date"])
        for info in data["new_values"]:
            p_id = info["player_id"]
            sv = info["strength_value"]
            p_strength_vals[p_id] = sv
        sv_service.add_multiple_strength_updates(date, p_strength_vals)
        return {
            "message": f"successfully added ({len(p_strength_vals)}) entries to db"
        }, 201
    except KeyError as e:
        return {"message": f"data missing from request json ({str(e)})"}, 400
    except ResourceAlreadyPresent:
        return {
            "message": "value(s) already exist(s) in the db, use PUT to update it"
        }, 400


@player_strength_api.route("/", methods=["DELETE"])
def delete_strength_values():
    if not request.is_json or not request.json:
        return {"message": "invalid payload, data is not present"}, 400
    date = request.json.get("date", None)
    if not date:
        return {"message": "'date' not found in payload"}, 400
    date = parse_date(date)
    num_of_deleted_objects = sv_service.delete_by_date(date)
    if num_of_deleted_objects == 0:
        return {"message": "nothing to delete"}, 200
    return {"message": f"successfully deleted ({num_of_deleted_objects}) objects"}, 200


@player_strength_api.route("/", methods=["PUT"])
def update_strength_values_for_date():
    data = request.json
    if not data:
        return {"message": "invalid payload, data is not present"}, 400

    try:
        # this is the happy path
        p_strength_vals = {}
        date = parse_date(data["date"])
        for info in data["new_values"]:
            p_id = info["player_id"]
            sv = info["strength_value"]
            p_strength_vals[p_id] = sv

        sv_service.update_strength_values_for_given_date(date, p_strength_vals)
        return {
            "message": f"successfully added ({len(p_strength_vals)}) entries to db"
        }, 201

    except KeyError as e:
        return {"message": f"data missing from request json ({str(e)})"}, 400
    except ResourceAlreadyPresent:
        return {
            "message": "value(s) already exist(s) in the db, use PUT to update it"
        }, 400


@player_strength_api.route("/<int:player_id>/", methods=["GET"])
def get_all_values_for_single_player(player_id: int):
    values = sv_service.get_all_for_player(player_id)
    return jsonify([val.to_dict() for val in values]), 200


@player_strength_api.get("/latest-for-players/")
def get_latest_value_for_multiple_players():
    player_ids_raw = request.args.get("ids", None)
    player_ids = player_ids_raw.split(",") if player_ids_raw else []
    date = request.args.get("date", datetime.now())

    if isinstance(date, str):
        date = parse_date(date)
        current_app.logger.info(f"parsed received date into {date}")

    latest_values = []
    for p_id in player_ids:
        p_id = int(p_id)
        sv_latest = sv_service.get_latest_for_player(p_id, date)

        if sv_latest is None:
            player = player_service.get_by_id(p_id)
            if player is None:
                return {"message": f"player with id '{p_id}' not found"}, 404

            sv_latest = StrengthValue(player.training_strength, p_id, datetime.now())
            latest_values.append(sv_latest)

        latest_values.append(sv_latest)

    resp = {int(sv.player_id): sv.value for sv in latest_values}
    return jsonify(resp), 200
