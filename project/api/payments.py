from typing import List
from datetime import date

from flask import Blueprint, request

from project import db
from project.api.models import MonthlyPayObligation

payments_api = Blueprint("payments_api", __name__, url_prefix="/api/v1/payments")


@payments_api.route("/obligations/<int:year>/<int:month>/", methods=["GET", "PUT"])
def all_payments_for_month(year: int, month: int):
    if request.method == "GET":
        return _handle_get_req_obligations_for_given_month(year, month)

    # this is a PUT.get_json()}
    new_obls = [
        _get_obligations_from_resp(year, month, entry)
        for entry in request.get_json()["obligations"]
    ]
    _add_new_and_remove_old_obligations(year, month, new_obls)
    return {"message": "success"}, 200


def _add_new_and_remove_old_obligations(year, month, new_obligations) -> bool:
    had_old_value = False
    picked_date = date(year, month, 1)
    curr_obl_for_month = _get_all_obligations_for_month_from_db(picked_date)
    if curr_obl_for_month:
        had_old_value = True
    for old_obl in curr_obl_for_month:
        db.session.delete(old_obl)
    for new_obl in new_obligations:
        db.session.add(new_obl)
    db.session.commit()
    return had_old_value


def _get_obligations_from_resp(year: int, month: int, entry: dict):
    return MonthlyPayObligation(
        player_id=entry["playerId"],
        month=date(year, month, 1),
        is_active=entry["isActive"],
    )


def _handle_get_req_obligations_for_given_month(year, month):
    picked_date = date(year, month, 1)
    obligations = [
        obligation.to_dict()
        for obligation in _get_all_obligations_for_month_from_db(picked_date)
    ]
    return {"message": "success", "playerPayObligations": obligations}


def _get_all_obligations_for_month_from_db(date) -> List[MonthlyPayObligation]:
    return MonthlyPayObligation.query.filter_by(month=date).all()
