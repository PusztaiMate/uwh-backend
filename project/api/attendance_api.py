import csv
import io
from typing import Any, Dict, List
from flask import Blueprint, Response, make_response
from project import db
from project.repositories.player_repository import PlayerDB
from project.services.player_service import PlayerService

from project.services.training_service import TrainingRepository, TrainingService


attendance_api = Blueprint("attendance", __name__, url_prefix="/api/v1/attendance")


class AttendanceService:
    def __init__(
        self, training_service: TrainingService, player_service: PlayerService
    ):
        self._training_service = training_service
        self._player_service = player_service

    def get_attendance_for_year(self, year) -> List[Dict[str, Any]]:
        trainings = self._training_service.get_trainings_for_year(year)
        players = self._player_service.get_all_players()
        per_player = []
        for player in players:
            trainings_per_month = []
            for month in range(1, 13):
                trainings_per_month.append(
                    len(
                        [
                            training
                            for training in trainings
                            if training.date.month == month
                            and player.id in [p.id for p in training.players]
                        ]
                    )
                )
            per_player.append(
                {
                    "player_full_name": f"{player.lname} {player.fname}",
                    "player_id": player.id,
                    "trainings_per_month": trainings_per_month,
                }
            )
        return per_player


player_repository = PlayerDB(db)
training_repository = TrainingRepository(db)
player_service = PlayerService(player_repository)
training_service = TrainingService(training_repository)
attendance_service = AttendanceService(training_service, player_service)


@attendance_api.get("/yearly/<int:year>/")
def get_attendance_for_year(year):
    per_player = attendance_service.get_attendance_for_year(year)

    csv_data = []
    headers = [
        "Név",
        "Január",
        "Február",
        "Március",
        "Április",
        "Május",
        "Június",
        "Július",
        "Augusztus",
        "Szeptember",
        "Október",
        "November",
        "December",
    ]
    csv_data.append(headers)

    for data in per_player:
        row = [data["player_full_name"]] + data["trainings_per_month"]
        csv_data.append(row)

    si = io.StringIO()
    writer = csv.writer(si)
    writer.writerows(csv_data)
    response = make_response(si.getvalue())
    response.headers["Content-Disposition"] = "attachment; filename=attendance.csv"
    response.headers["Content-type"] = "text/csv"
    return response
