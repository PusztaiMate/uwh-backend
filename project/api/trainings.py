from typing import Dict, List, Tuple
from flask import Blueprint, request, jsonify
from werkzeug.exceptions import BadRequest
from project.api.lib.utils import parse_date

from project.api.models import Training
from project.services.training_service import TrainingRepository, TrainingService
from .db_utils import (
    delete_trainings,
    find_goal_scorers_by_training_id,
    query_all_trainings,
    query_single_training,
    add_training_to_db,
    update_training_id_db,
)
from project.database import db
from .lib.authorizer import authorizer

trainings_api = Blueprint("trainings_api", __name__, url_prefix="/api/v1/trainings")

training_repository = TrainingRepository(db)
training_service = TrainingService(training_repository)

PARAM_LIMIT = "limit"


@trainings_api.post("/")
def add_new_trainings():
    authorizer.create_training()

    return add_training(request)


@trainings_api.get("/")
def all_trainings():
    authorizer.read_training()

    all_trainings = query_all_trainings()
    limit_param = _get_limit_param(request)
    resp = []
    for training in sorted(all_trainings[-1 * limit_param :], key=lambda t: t.date):
        training_json = training.to_dict()
        white_scorers, black_scorers = get_white_and_black_scorers(training)
        enrich_training_json_with_scorer_data(
            training_json, white_scorers, black_scorers
        )
        resp.append(training_json)

    return jsonify(resp), 200


@trainings_api.get("/<int:training_id>/")
def get_single_training(training_id: int):
    authorizer.read_training()

    training = training_service.get_training(training_id)

    if not training:
        return (
            {
                "message": f"training with id {training_id} not found",
                "status": "failed",
            },
            404,
        )

    training_json = training.to_dict()
    white_scorers, black_scorers = get_white_and_black_scorers(training)
    enrich_training_json_with_scorer_data(training_json, white_scorers, black_scorers)

    return jsonify(training_json), 200


# this is just a temporary endpoint to compact the database a bit
@trainings_api.put("/<training_id>/scorers")
def update_scorers(training_id):
    authorizer.create_training()

    training = training_service.get_training(training_id)
    if not training:
        return (
            {
                "message": f"training with id {training_id} not found",
                "status": "failed",
            },
            404,
        )

    scorers = {
        scorer.player_id: scorer.scored
        for scorer in find_goal_scorers_by_training_id(training.id)
    }

    training_service.add_scorer_data_to_training(training_id, scorers)

    return (
        {
            "message": f"Training {training_id} successfully updated",
            "status": "success",
            "id": training_id,
        },
        200,
    )


@trainings_api.put("/<training_id>/")
def update_training(training_id):
    authorizer.create_training()

    try:
        training_id = int(training_id)
    except ValueError:
        return (
            {"message": f"invalid id provided ({training_id})", "status": "failed"},
            400,
        )

    training = query_single_training(training_id)
    if not training:
        return (
            {
                "message": f"training with id {training_id} not found",
                "status": "failed",
            },
            404,
        )

    _update_training(training, request.get_json())
    return (
        {
            "message": f"Training {training_id} successfully updated",
            "status": "success",
            "id": training_id,
        },
        200,
    )


@trainings_api.delete("/<training_id>/")
def delete_training(training_id: int):
    authorizer.create_training()

    to_be_delete = query_single_training(training_id)
    if to_be_delete is None:
        return {
            "status": "success",
            "message": f"training ({training_id}) was not present to start with",
        }, 200

    delete_trainings([training_id])
    return {
        "status": "success",
        "message": f"training '{training_id}' successfully deleted",
    }, 200


@trainings_api.get("/participant-count/")
def get_participant_count():
    authorizer.read_training()

    starting_date = request.args.get("starting_date")
    ending_date = request.args.get("ending_date")

    try:
        starting_date, ending_date = parse_date(starting_date), parse_date(ending_date)
        trainings = training_service.get_trainings_between_dates(
            starting_date, ending_date
        )
        participant_count = sum(
            [
                len(t.white_team) + len(t.black_team) + len(t.swimming_players)
                for t in trainings
            ]
        )

        return (
            {
                "message": "participant count successfully retrieved",
                "status": "success",
                "count": participant_count,
            },
            200,
        )

    except ValueError:
        return (
            {
                "message": "invalid date provided",
                "status": "failed",
            },
            400,
        )


def _update_training(original_training, new_data):
    update_training_id_db(
        original_training=original_training,
        white_team=new_data.get("white_team"),
        black_team=new_data.get("black_team"),
        swimming=new_data.get("swimming_players"),
        new_date=new_data.get("new_date"),
    )


def add_training(req):
    if not req.is_json:
        return {"message": "wrong content type", "status": "failed"}, 400

    try:
        json_data = req.get_json()
    except BadRequest:
        return {"message": "couldn't parse json", "status": "failed"}, 400

    id_ = add_training_to_db(json_data)

    return (
        {"message": "training successfully added", "status": "success", "id": id_},
        201,
    )


def get_single_training_as_json(training_id):
    training = query_single_training(training_id)
    if not training:
        return None
    return training.to_dict()


def _get_limit_param(request) -> int:
    try:
        return int(request.args.get(PARAM_LIMIT, 0))
    except ValueError:
        return 0


def enrich_training_json_with_scorer_data(
    training_json: dict,
    white_scorers: List[Dict[int, int]],
    black_scorers: List[Dict[int, int]],
):
    training_json["white_scorers"] = {}
    training_json["black_scorers"] = {}
    training_json["white_score"] = 0
    training_json["black_score"] = 0
    for scorer in white_scorers:
        training_json["white_scorers"][scorer["player_id"]] = scorer["scored"]
        training_json["white_score"] += scorer["scored"]
    for scorer in black_scorers:
        training_json["black_scorers"][scorer["player_id"]] = scorer["scored"]
        training_json["black_score"] += scorer["scored"]


def get_white_and_black_scorers(
    training: Training,
) -> Tuple[List[Dict[int, int]], List[Dict[int, int]]]:
    black_scorers, white_scorers = [], []
    for scorer_id, number_scored in training.get_scorers().items():
        if scorer_id in training.black_team.player_ids:
            black_scorers.append({"player_id": scorer_id, "scored": number_scored})
        if scorer_id in training.white_team.player_ids:
            white_scorers.append({"player_id": scorer_id, "scored": number_scored})
    return white_scorers, black_scorers
