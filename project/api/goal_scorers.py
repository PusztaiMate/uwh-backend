from typing import Tuple

from flask import Blueprint, jsonify, request
from sqlalchemy.exc import IntegrityError

from project.database import db
from project.api.db_utils import (
    find_goal_scorers_by_training_id,
    query_all_scorers,
    query_single_training,
    rollback_db_session,
    update_goal_scorers_for_trainig,
)
from project.services.training_service import TrainingRepository, TrainingService


goal_scorers_api = Blueprint(
    "goal_scorers_api", __name__, url_prefix="/api/v1/goal-scorers"
)


training_repository = TrainingRepository(db)
training_service = TrainingService(training_repository)


@goal_scorers_api.route("/", methods=["GET"])
def all_scorers():
    return jsonify(query_all_scorers()), 200


@goal_scorers_api.route("/", methods=["PUT"])
def update_scorers():
    t_id, scorers = extract_scorer_data_from_request_json(request.json) # type: ignore

    if not validate_scorer_data(scorers):
        return {"message": "invalid input data"}, 400

    try:
        update_goal_scorers_for_trainig(t_id, scorers)
    except IntegrityError:
        rollback_db_session()
        return {"message": "training or player not found in db"}, 404

    return {"message": "successfully updated resource"}, 200


@goal_scorers_api.route("/training/<int:training_id>/")
def get_scorers_for_training(training_id):
    training = query_single_training(training_id)

    if not training:
        return {"message": "training not found"}, 404

    scorers = find_goal_scorers_by_training_id(training_id)
    if not scorers:
        return  {
            "message": "could not find scorers",
            "status": "failed"
        }, 400


    resp = {"black": {}, "white": {}}

    if not training.white_team or not training.black_team:
        return resp

    for player_id in training.white_team:
        if player_id in scorers:
            resp["white"][player_id] = scorers[player_id]
        else:
            resp["white"][player_id] = 0

    for player_id in training.black_team:
        if player_id in scorers:
            resp["black"][player_id] = scorers[player_id]
        else:
            resp["black"][player_id] = 0

    return resp, 200


def extract_scorer_data_from_request_json(json_data: dict) -> Tuple[int, dict]:
    training_id = json_data.get("training_id", None)
    scorers = json_data.get("scorers", {})
    return training_id, scorers


def validate_scorer_data(scorers: dict):
    for _, scored_goals in scorers.items():
        try:
            int(scored_goals)
        except ValueError:
            return False
    return True
