from flask import Flask
from .api.exceptions import UnauthorizedError


def handle_unauthorized_error(error):
    return {"message": "Unauthorized"}, 401


def register_error_handlers(app: Flask):
    app.register_error_handler(UnauthorizedError, handle_unauthorized_error)
