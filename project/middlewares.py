import os
import jwt
from werkzeug import Request, Response


class username_exporter:
    UNKNOWN_USER = None

    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        request = Request(environ)
        try:
            secret_key = os.environ.get("SECRET_KEY")
        except KeyError:
            raise RuntimeError("SECRET_KEY not set")
        if secret_key is None or secret_key == "":
            raise RuntimeError("SECRET_KEY not set")
        authorization_headers = request.headers.get("Authorization", "").split()
        if not authorization_headers or authorization_headers[1] == "null":
            environ["x-user"] = self.UNKNOWN_USER
            return self.app(environ, start_response)

        if len(authorization_headers) != 2:
            response = Response("Invalid token", 401)
            return response(environ, start_response)

        try:
            token = authorization_headers[1]
            data = jwt.decode(token, secret_key, algorithms=["HS256"])
            environ["x-user"] = data["sub"]
            return self.app(environ, start_response)
        except jwt.ExpiredSignatureError:
            response = Response("Expired token", 401)
            return response(environ, start_response)
        except (jwt.InvalidTokenError, RuntimeError):
            response = Response("Invalid token", 401)
            return response(environ, start_response)
