import os


class BasicConfig:
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = os.environ.get("SECRET_KEY")


class DevelopmentConfig(BasicConfig):
    uri = os.getenv(
        "DATABASE_URL", "postgresql://postgres:postgres@localhost:5432/players_dev"
    )
    if uri.startswith("postgres://"):
        uri = uri.replace("postgres://", "postgresql://", 1)
    SQLALCHEMY_DATABASE_URI = uri


class TestingConfig(BasicConfig):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.getenv(
        "DATABASE_TEST_URL",
        "postgresql://postgres:postgres@localhost:5432/players_test",
    )
    SECRET_KEY = os.environ.get("SECRET_KEY", "just for testing")
    DATABASE_URL = os.getenv(
        "DATABASE_TEST_URL",
        "postgresql://postgres:postgres@localhost:5432/players_test",
    )


class ProductionConfig(BasicConfig):
    # I could not modify the "DATABASE_URL" variable in the heroku settings...
    # So here is the wonderful new DATABASE_URL_NEW variable!
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL_NEW")
