terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = "eu-central-1"
}

resource "aws_key_pair" "pmate-msi" {
  key_name   = "pmate-msi-laptop"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCvWYfHqbxkKL2nrBhJlZQ3Af5nElDtQAQplahlJ5nk8DepAkcSmxB8Tg4MTnTquXZseqMa3aK5nKv8YSRloF3e35Jr7jJhLxBJdxQW7//juQ8VdneW8Ao2boh9gphhnhWnCc+DnII8C1kNOnefOhrbr4Cs+O6sXGU8iiASlX98YMeS/n33i8dcFt+WwtqVhWZOBjp6PnrP/1J86de6USw43AyS+HboUxAT+pn7gNi8AJg7inJCFW6+YPrngFran8eKuvSOJjVkQkCffDfq5aaTuExI94Kjm4uDKwYbDu58mergiEtVrNGe4tvD0RFrdquzUruQObwK+aLOhVh0ZCg5qNxgDIKxczDucgR49g5grKC1zdTgkklkoqN89EmfYKpcPVDfMebpureszB8sbZ34KOP45tyqyi5YgzSpTI4PvCwpv7+LEdvCzQsBKNlTuT+OKo8Zrv3+4UgfsoUYfKUGIQNFJvWu/Mi2jKoc5oekZ41TaxYrl/H7+mpVYtRzOt0= pusztai@pop-os"
}

resource "aws_security_group" "ssh" {
  name_prefix = "ssh-"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "nomad" {
  name_prefix = "nomad-"

  ingress {
    from_port = 4646
    to_port   = 4646
    protocol  = "tcp"
    # cidr_blocks = ["89.133.16.0/24"]
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "outbound" {
  name_prefix = "outbound-"
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}


resource "aws_instance" "app_server" {
  ami           = "ami-0c0d3776ef525d5dd"
  instance_type = "t2.micro"
  count         = 1

  key_name = aws_key_pair.pmate-msi.key_name

  security_groups = [aws_security_group.ssh.name, aws_security_group.nomad.name, aws_security_group.outbound.name]

  tags = {
    Name = "ExampleAppServer"
  }

  user_data = <<EOF
#!/bin/bash

echo "Updating system"
yum update -y
yum install -y yum-utils
yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
amazon-linux-extras install docker
yum -y install nomad

echo "Download CNI plugin for Nomad"
curl -L -o cni-plugins.tgz "https://github.com/containernetworking/plugins/releases/download/v1.0.0/cni-plugins-linux-$( [ $(uname -m) = aarch64 ] && echo arm64 || echo amd64)"-v1.0.0.tgz && \
mkdir -p /opt/cni/bin && \
tar -C /opt/cni/bin -xzf cni-plugins.tgz

echo 1 |  tee /proc/sys/net/bridge/bridge-nf-call-arptables && \
echo 1 |  tee /proc/sys/net/bridge/bridge-nf-call-ip6tables && \
echo 1 |  tee /proc/sys/net/bridge/bridge-nf-call-iptables

net.bridge.bridge-nf-call-arptables = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1


echo "Start docker service"
service docker start
systemctl enable docker
usermod -a -G docker ec2-user

echo "Create nomad dirs and service"
mkdir -p /opt/nomad
touch /etc/systemd/system/nomad.service

cat <<EOT >> /etc/systemd/system/nomad.service
[Unit]
Description=Nomad
Documentation=https://www.nomadproject.io/docs/
Wants=network-online.target
After=network-online.target

[Service]
User=root
Group=root

ExecReload=/bin/kill -HUP $MAINPID
ExecStart=/bin/nomad agent -config /etc/nomad.d
KillMode=process
KillSignal=SIGINT
LimitNOFILE=65536
LimitNPROC=infinity
Restart=on-failure
RestartSec=2

TasksMax=infinity
OOMScoreAdjust=-1000

[Install]
WantedBy=multi-user.target
EOT

mkdir -p /etc/nomad.d
chmod 700 /etc/nomad.d
touch /etc/nomad.d/nomad.hcl

cat <<EOT > /etc/nomad.d/nomad.hcl
data_dir  = "/opt/nomad"

bind_addr = "0.0.0.0" # the default

server {
  enabled          = true
  bootstrap_expect = 1
}

client {
  enabled       = true
}
EOT


echo "Start nomad service"
systemctl daemon-reload
systemctl enable nomad
systemctl start nomad

EOF
}
