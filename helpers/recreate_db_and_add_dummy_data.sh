#!/usr/bin/env bash

docker-compose exec players python manage.py recreate_db && docker-compose exec players python manage.py seed_db
