#!/usr/bin/env sh

black .
flake8 --extend-ignore=E203,E402 --max-line-length=88 || echo "flake8 failed"
