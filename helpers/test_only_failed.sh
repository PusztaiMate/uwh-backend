#!/usr/bin/env bash

docker-compose exec players pytest --lf --disable-warnings -v
