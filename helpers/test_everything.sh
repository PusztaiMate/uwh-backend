#!/usr/bin/env bash

docker compose exec players pytest project/tests -v --disable-warnings
