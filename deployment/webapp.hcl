job "uwh-webapp" {
  datacenters = ["dc1"]
  type = "service"

  group "uwh-webapp" {
    count = 1

    network {
      port "http" {
        static = "8080"
      }
    }

    task "webapp" {
      driver = "docker"

      config {
        image = "pmate6/uwh_backend:0.0.0"
        network_mode = "host"
        ports = ["http"]
      }

      env {
        PORT = "${NOMAD_PORT_http}"
      }

      resources {
        cpu    = 500
        memory = 256
      }
    }
  }
}
