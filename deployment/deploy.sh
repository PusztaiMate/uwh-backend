#!/usr/bin/env bash

NOMAD_SERVER_HOST="3.70.240.233"
NOMAD_SERVER_PORT="4646"
NOMAD_SERVER_URL="http://${NOMAD_SERVER_HOST}:${NOMAD_SERVER_PORT}"


SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

nomad run -address=${NOMAD_SERVER_URL} "${SCRIPTS_DIR}/webapp.hcl"