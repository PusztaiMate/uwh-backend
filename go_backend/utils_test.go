package main

import (
	"testing"
	"time"
)

func Test_WeekNumber(t *testing.T) {
	monday, nextMonday := getNthWeekOfTheYear(2)

	if monday.Weekday() != time.Monday {
		t.Errorf("wrong day, wanted %s but got %s", time.Monday, monday.Weekday())
	}
	if nextMonday.Weekday() != time.Monday {
		t.Errorf("wrong day, wanted %s but got %s", time.Monday, nextMonday.Weekday())
	}
	if _, week := monday.ISOWeek(); week != 2 {
		t.Errorf("wrong week, wanted %d but got %d", 2, week)
	}
	if _, week := nextMonday.ISOWeek(); week != 3 {
		t.Errorf("wrong week, wanted %d but got %d", 3, week)
	}
}
