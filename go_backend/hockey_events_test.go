package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http/httptest"
	"os"
	"strconv"
	"sync"
	"testing"
	"time"

	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/PusztaiMate/uwh-backend/db"
)

var (
	conn    *pgxpool.Pool
	queries *db.Queries
)

func init() {
	once := sync.Once{}
	once.Do(func() {
		var err error
		conn, err = newDatabaseConnection(context.Background(), "postgres://postgres:postgres@localhost:5432/players_test")
		if err != nil {
			panic(err)
		}
		queries = db.New(conn)
	})
}

func createHockeyEventRequestBuffer() *bytes.Buffer {
	return bytes.NewBufferString(`{"title":"Test","type":"Type","description":"Description","date":"2024-06-04T19:30:00Z"}`)
}

func parseHockeyEventResponseBuffer(b *bytes.Buffer) HockeyEvent {
	type Response struct {
		HockeyEvent HockeyEvent `json:"data"`
	}
	var responseData Response
	json.NewDecoder(b).Decode(&responseData)
	return responseData.HockeyEvent
}

func createServer(queries *db.Queries) *Server {
	os.Setenv("DATABASE_URL", "postgres://postgres:postgres@localhost:5432/players_test?sslmode=disable")
	activityService := NewStravaActivityService(queries)
	authService := NewStravaAuthService(queries)
	pointsCalculator := NewStravaTrainingPointsCalculator()
	activityCache := newActivityCache(nil, 10*time.Second)
	return NewServer(
		ServerConfig{},
		NewHockeyEventsService(queries),
		authService,
		NewUserService(queries),
		pointsCalculator,
		NewChallengeService(queries),
		NewWeeklyTrainingStatsService(queries),
		activityService,
		NewStravaActivityFetcherService(activityService, authService, nil, activityCache),
		NewPlayerService(queries),
		NewClubService(queries),
		NewTrainingService(queries),
		nil,
	)
}

func Test_ListHockeyEvents(t *testing.T) {
	server := createServer(queries)

	listRequest := httptest.NewRequest("GET", "/hockey-events", nil)
	listResponse := httptest.NewRecorder()
	listContext := server.e.NewContext(listRequest, listResponse)

	err := server.handleListHockeyEvents(listContext)
	require.Nil(t, err)
	require.Equal(t, 200, listResponse.Code)
}

func Test_CreateAndRetrieveHockeyEvent(t *testing.T) {
	server := createServer(queries)

	createRequest := httptest.NewRequest("POST", "/hockey-events", createHockeyEventRequestBuffer())
	createRequest.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	createResponse := httptest.NewRecorder()
	createContext := server.e.NewContext(createRequest, createResponse)

	err := server.handleCreateHockeyEvent(createContext)
	require.Nil(t, err)
	require.Equal(t, 201, createResponse.Code)
	createdEvent := parseHockeyEventResponseBuffer(createResponse.Body)

	getRequest := httptest.NewRequest("GET", "/", nil)
	getResponse := httptest.NewRecorder()
	getContext := server.e.NewContext(getRequest, getResponse)
	getContext.SetPath(fmt.Sprintf("/hockey-events/%d", createdEvent.ID))
	getContext.SetParamNames("id")
	getContext.SetParamValues(strconv.Itoa(createdEvent.ID))

	err = server.handleGetHockeyEvent(getContext)
	require.Nil(t, err)
	require.Equal(t, 200, getResponse.Code)

	retrievedEvent := parseHockeyEventResponseBuffer(getResponse.Body)
	assert.Equal(t, createdEvent, retrievedEvent)
}
