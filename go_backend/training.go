package main

import (
	"context"
	"encoding/json"
	"log/slog"
	"slices"
	"sort"
	"strconv"
	"strings"
	"time"

	"gitlab.com/PusztaiMate/uwh-backend/db"
	"golang.org/x/exp/maps"
)

type Training struct {
	Id      int            `json:"id"`
	Date    time.Time      `json:"date"`
	ClubId  int            `json:"club_id"`
	Scorers map[string]int `json:"scorers"`
}

type ExtendedTrainingData struct {
	Training
	BlackTeam       []int          `json:"black_team"`
	WhiteTeam       []int          `json:"white_team"`
	BlackScorers    map[string]int `json:"black_scorers"`
	WhiteScorers    map[string]int `json:"white_scorers"`
	Players         []int          `json:"players"`
	SwimmingPlayers []int          `json:"swimming_players"`
	BlackScore      int            `json:"black_score"`
	WhiteScore      int            `json:"white_score"`
}

type TrainingRepository interface {
	GetTrainings(ctx context.Context, arg db.GetTrainingsParams) ([]db.Training, error)
	GetTeamsForTrainings(ctx context.Context, trainingIds []int32) ([]db.GetTeamsForTrainingsRow, error)
}

type TrainingService struct {
	db TrainingRepository
}

func NewTrainingService(db TrainingRepository) *TrainingService {
	return &TrainingService{
		db: db,
	}
}

func parseScorers(data []byte) (map[string]int, error) {
	m := make(map[string]int)
	err := json.Unmarshal(data, &m)
	if err != nil {
		return nil, err
	}
	return m, nil
}

func parsePlayers(players string) ([]int, error) {
	ps := strings.Split(players, ";")
	result := make([]int, len(ps))
	for i, p := range ps {
		if p == "{}" {
			continue
		}
		parsed, err := strconv.Atoi(p)
		if err != nil {
			return nil, err
		}
		result[i] = parsed
	}

	return result, nil
}

func (s *TrainingService) GetTrainings(ctx context.Context, pageNumber, pageSize int) ([]Training, error) {
	trainings, err := s.db.GetTrainings(ctx, db.GetTrainingsParams{
		PageNumber: pageNumber,
		PageSize:   int32(pageSize),
	})
	if err != nil {
		return nil, err
	}

	var ts []Training
	for _, t := range trainings {
		scorers, err := parseScorers(t.Scorers)
		if err != nil {
			slog.Error("couldn't unmarshal scorer data", "err", err, "training", t)
			return nil, err
		}
		ts = append(ts, Training{
			Id:      int(t.ID),
			Date:    t.Date.Time,
			ClubId:  int(t.ClubID.Int32),
			Scorers: scorers,
		})
	}

	return ts, nil
}

func getScorersMap(team []int, allScorers map[string]int) map[string]int {
	teamIds := make([]string, len(team))
	for i, t := range team {
		teamIds[i] = strconv.Itoa(t)
	}

	result := make(map[string]int)
	for _, id := range teamIds {
		result[id] = allScorers[id]
	}

	return result
}

func (s *TrainingService) GetExtendedTrainingData(ctx context.Context, trainingIds []int) ([]*ExtendedTrainingData, error) {
	tIds := make([]int32, len(trainingIds))
	for i, tid := range trainingIds {
		tIds[i] = int32(tid)
	}

	rows, err := s.db.GetTeamsForTrainings(ctx, tIds)
	if err != nil {
		return nil, err
	}

	result := make(map[int32]*ExtendedTrainingData, len(trainingIds))

	for _, t := range rows {
		if _, ok := result[t.ID]; !ok {
			scorers, err := parseScorers(t.Scorers)
			if err != nil {
				slog.Error("couldn't unmarshal scorer data", "err", err, "training", t)
				return nil, err
			}
			result[t.ID] = &ExtendedTrainingData{
				Training: Training{
					Id:      int(t.ID),
					Date:    t.Date.Time,
					ClubId:  int(t.ClubID.Int32),
					Scorers: scorers,
				},
			}
		}

		etd := result[t.ID]
		players, err := parsePlayers(t.PlayerIds)
		if err != nil {
			return nil, err
		}

		switch t.Color {
		case "none":
			etd.SwimmingPlayers = players
		case "black":
			etd.BlackTeam = players
			etd.BlackScorers = getScorersMap(players, etd.Scorers)
		case "white":
			etd.WhiteTeam = players
			etd.WhiteScorers = getScorersMap(players, etd.Scorers)
		default:
			continue
		}

		etd.Players = slices.Concat(etd.BlackTeam, etd.WhiteTeam, etd.SwimmingPlayers)

		var sumBlackScore, sumWhiteScore int
		for _, goals := range etd.BlackScorers {
			sumBlackScore += goals
		}
		for _, goals := range etd.WhiteScorers {
			sumWhiteScore += goals
		}
		etd.BlackScore = sumBlackScore
		etd.WhiteScore = sumWhiteScore
	}

	return maps.Values(result), nil
}

func (s *TrainingService) getAllTrainingIds(ctx context.Context) ([]int, error) {
	trainings, err := s.db.GetTrainings(ctx, db.GetTrainingsParams{
		PageNumber: 1,
		PageSize:   10000,
	})
	if err != nil {
		return nil, err
	}

	var ids []int
	for _, t := range trainings {
		ids = append(ids, int(t.ID))
	}

	return ids, nil
}

type StreakData struct {
	PlayerId                          int       `json:"player_id"`
	LongestStreak                     int       `json:"longest_streak"`
	CurrentStreak                     int       `json:"current_streak"`
	LongestStreakStartDate            time.Time `json:"longest_streak_start_date"`
	LongestStreakEndDate              time.Time `json:"longest_streak_end_date"`
	CurrentStreakStartDate            time.Time `json:"current_streak_start_date"`
	CurrentYearLongestStreak          int       `json:"current_year_longest_streak"`
	CurrentYearLongestStreakStartDate time.Time `json:"current_year_longest_streak_start"`
	CurrentYearLongestStreakEndDate   time.Time `json:"current_year_longest_streak_end"`
}

func (s *TrainingService) CalculateTrainingStreakForPlayers(ctx context.Context, players []int) (map[int]*StreakData, error) {
	trainingIds, err := s.getAllTrainingIds(ctx)
	if err != nil {
		return nil, err
	}

	trainings, err := s.GetExtendedTrainingData(ctx, trainingIds)
	if err != nil {
		return nil, err
	}

	trainings = sortExtendedTrainingDataByDate(trainings, true)

	return calculateTrainingStreakForPlayers(trainings, players)
}

func sortExtendedTrainingDataByDate(trainings []*ExtendedTrainingData, ascending bool) []*ExtendedTrainingData {
	sort.Slice(trainings, func(i, j int) bool {
		if ascending {
			return trainings[i].Date.Before(trainings[j].Date)
		}
		return trainings[i].Date.After(trainings[j].Date)
	})

	return trainings
}
