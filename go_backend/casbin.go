package main

import (
	"crypto/tls"
	"log/slog"
	"net/url"
	"slices"
	"strconv"
	"strings"

	"github.com/casbin/casbin/v2"

	pgadapter "github.com/PusztaiMate/casbin-pg-adapter"
	"github.com/go-pg/pg/v10"
)

type CasbinClient struct {
	e *casbin.Enforcer
}

func parsePostgresURL(postgresURL string) (*pg.Options, error) {
	parsedURL, err := url.Parse(postgresURL)
	if err != nil {
		return nil, err
	}

	options := &pg.Options{
		Addr: parsedURL.Host,
	}

	if parsedURL.User != nil {
		options.User = parsedURL.User.Username()
		if password, ok := parsedURL.User.Password(); ok {
			options.Password = password
		}
	}

	if strings.Contains(parsedURL.Path, "/") {
		options.Database = strings.TrimPrefix(parsedURL.Path, "/")
	}

	queryParams := parsedURL.Query()
	sslmode := queryParams.Get("sslmode")

	switch sslmode {
	case "disable":
	case "require", "verify-ca", "verify-full":
		options.TLSConfig = &tls.Config{
			ServerName:         parsedURL.Hostname(),
			MinVersion:         tls.VersionTLS12,
			InsecureSkipVerify: sslmode == "require",
		}
	default:
		options.TLSConfig = &tls.Config{
			ServerName:         parsedURL.Hostname(),
			MinVersion:         tls.VersionTLS12,
			InsecureSkipVerify: true,
		}
	}

	return options, nil
}

func NewCasbinClient(dbUrl string, configPath string) (*CasbinClient, error) {
	options, err := parsePostgresURL(dbUrl)
	if err != nil {
		panic(err)
	}
	slog.Info("options parsed from dbUrl", "dbUrl", dbUrl, "options", options)
	a, err := pgadapter.NewAdapter(options, options.Database)
	if err != nil {
		return nil, err
	}
	e, err := casbin.NewEnforcer(configPath, a)
	if err != nil {
		return nil, err
	}
	err = e.LoadPolicy()
	if err != nil {
		panic(err)
	}

	policies := [][]string{
		// admin policies
		{"admin", "users", "create"},
		{"admin", "users", "update"},
		{"admin", "users", "read"},
		{"admin", "players", "create"},
		{"admin", "players", "read"},
		{"admin", "trainings", "create"},
		{"admin", "trainings", "read"},
		{"admin", "payments", "create"},
		{"admin", "hockey_events", "create"},
		{"admin", "hockey_events", "read"},

		// club manager policies
		{"club_manager", "players", "create"},
		{"club_manager", "players", "read"},
		{"club_manager", "trainings", "create"},
		{"club_manager", "trainings", "read"},
	}

	for _, policy := range policies {
		_, err := e.AddPolicy(policy[0], policy[1], policy[2])
		if err != nil {
			panic(err)
		}
	}

	// "1" is the admins Id
	_, err = e.AddGroupingPolicy("1", "admin")
	if err != nil {
		panic(err)
	}

	return &CasbinClient{
		e: e,
	}, nil
}

func (c *CasbinClient) Enforce(sub int, obj, act string) bool {
	ssub := strconv.Itoa(sub)
	b, err := c.e.Enforce(ssub, obj, act)
	if err != nil {
		slog.Error("error when checking enforce rules", "err", err)
		return false
	}
	return b
}

func (c *CasbinClient) AddPolicy(sub, obj, act string) error {
	_, err := c.e.AddPolicy(sub, obj, act)
	return err
}

func (c *CasbinClient) AddRoleToUser(userId int, role string) error {
	sub := strconv.Itoa(userId)
	_, err := c.e.AddGroupingPolicy(sub, role)
	return err
}

func (c *CasbinClient) GetGroupingPolicies() ([][]string, error) {
	return c.e.GetGroupingPolicy()
}

func (c *CasbinClient) GetGroupingPoliciesForUser(userId int) ([]string, error) {
	uid := strconv.Itoa(userId)
	policies, err := c.e.GetGroupingPolicy()
	if err != nil {
		return nil, err
	}

	var userPolicies []string
	for _, p := range policies {
		if p[0] == uid {
			userPolicies = append(userPolicies, p[1])
		}
	}

	return userPolicies, nil
}

func (c *CasbinClient) GetRoles() ([]string, error) {
	pp, err := c.e.GetPolicy()
	if err != nil {
		return nil, err
	}
	roles := make([]string, 0)
	for _, p := range pp {
		// pp -> [obj, subj, act]
		if !slices.Contains(roles, p[0]) {
			roles = append(roles, p[0])
		}
	}
	return roles, nil
}

func (c *CasbinClient) ValidateRole(role string) bool {
	roles, err := c.GetRoles()
	if err != nil {
		slog.Error("could not fetch roles", "err", err)
		return false
	}

	return slices.Contains(roles, role)
}
