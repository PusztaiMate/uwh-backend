-- name: ListHockeyEvents :many
SELECT *
FROM hockey_events
ORDER BY date ASC;
-- name: CreateHockeyEvent :one
INSERT INTO hockey_events (date, title, type, description)
VALUES ($1, $2, $3, $4)
RETURNING *;
-- name: GetHockeyEvent :one
SELECT *
FROM hockey_events
WHERE id = $1;
-- name: DeleteHockeyEvent :exec
DELETE FROM hockey_events
WHERE id = $1;
-- name: UpdateHockeyEvent :exec
UPDATE hockey_events
SET date = $2,
    title = $3,
    type = $4,
    description = $5
WHERE id = $1
RETURNING *;
-- name: ListStravaAuths :many
SELECT *
FROM strava_oauth_data
ORDER BY id ASC;
-- name: CreateStravaOauthData :one
INSERT INTO strava_oauth_data (
        strava_id,
        firstname,
        lastname,
        access_token,
        refresh_token,
        expires_at
    )
VALUES ($1, $2, $3, $4, $5, $6)
RETURNING *;
-- name: GetStravaOauthData :one
SELECT *
FROM strava_oauth_data
WHERE id = $1;
-- name: FindByStravaId :one
SELECT *
FROM strava_oauth_data
WHERE strava_id = $1;
-- name: UpdateStravaOauthData :one
UPDATE strava_oauth_data
SET access_token = $2,
    refresh_token = $3,
    expires_at = $4
WHERE id = $1
RETURNING *;
-- name: UpsertStravaOauthData :one
INSERT INTO strava_oauth_data (
        strava_id,
        firstname,
        lastname,
        access_token,
        refresh_token,
        expires_at
    )
VALUES ($1, $2, $3, $4, $5, $6) ON CONFLICT (strava_id) DO
UPDATE
SET access_token = $4,
    refresh_token = $5,
    expires_at = $6
RETURNING *;
-- name: FindUserByStravaId :one
SELECT *
FROM users
WHERE strava_oauth_id = $1;
-- name: FindById :one
SELECT *
FROM users
WHERE id = $1;
-- name: CreateUser :one
INSERT INTO users (username, strava_oauth_id)
VALUES ($1, $2)
RETURNING *;
-- name: FindUserByUsername :one
SELECT *
FROM users
WHERE username = $1;
-- name: GetPeriodById :one
SELECT *
FROM challenge_periods
WHERE id = $1;
-- name: ListChallengePeriods :many
SELECT *
FROM challenge_periods
ORDER BY starting_year DESC, first_week DESC;
-- name: CreateChallengePeriod :one
INSERT INTO challenge_periods (starting_year, ending_year, first_week, last_week)
VALUES ($1, $2, $3, $4)
RETURNING *;
-- name: FindPeriodsByDate :many
SELECT *
FROM challenge_periods
WHERE first_week <= $1
    AND last_week >= $1
    AND starting_year = $2;
-- name: ListWeeklyTrainingForWeek :many
SELECT *
FROM weekly_training_stats
WHERE year = $1
    AND week_number = $2;
-- name: CreateWeeklyTrainingStats :one
INSERT INTO weekly_training_stats (
        strava_id,
        points,
        week_number,
        year,
        distance_swam
    )
VALUES ($1, $2, $3, $4, $5)
RETURNING *;
-- name: UpsertWeeklyTrainingStats :one
INSERT INTO weekly_training_stats (
        strava_id,
        points,
        week_number,
        year,
        distance_swam
    )
VALUES ($1, $2, $3, $4, $5) ON CONFLICT (strava_id, week_number, year) DO
UPDATE
SET points = $2,
    distance_swam = $5
RETURNING *;
-- name: ListWeeklyTrainingStatsForWeeks :many
SELECT wts.id,
    wts.strava_id,
    wts.points,
    wts.week_number,
    wts.year,
    wts.distance_swam,
    sod.firstname,
    sod.lastname
FROM weekly_training_stats wts
    INNER JOIN strava_oauth_data sod ON wts.strava_id = sod.strava_id
WHERE year = $1
    AND week_number >= $2
    AND week_number <= $3
ORDER BY week_number ASC;
-- name: GetStravaActivityByStravaId :one
SELECT sa.id,
    sa.strava_id,
    sa.sport_type,
    sa.moving_time,
    sa.strava_upload_id,
    sa.start_date,
    sa."name",
    sa.description,
    sa.strava_athlete_id,
    sa.last_fetched_at,
    sa.excluded_from_challenges,
    sa.distance
FROM strava_activities sa
WHERE sa.strava_id = $1;
-- name: CreateStravaActivity :one
INSERT INTO strava_activities (
        strava_athlete_id,
        sport_type,
        "name",
        description,
        moving_time,
        strava_upload_id,
        start_date,
        strava_id,
        last_fetched_at,
        excluded_from_challenges,
        distance
    )
VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
RETURNING *;
-- name: UpsertStravaActivity :one
INSERT INTO strava_activities (
        strava_athlete_id,
        sport_type,
        "name",
        description,
        moving_time,
        strava_upload_id,
        start_date,
        strava_id,
        last_fetched_at,
        excluded_from_challenges,
        distance
    )
VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) ON CONFLICT (strava_id) DO
UPDATE
SET sport_type = $2,
    "name" = $3,
    description = $4,
    moving_time = $5,
    strava_upload_id = $6,
    start_date = $7,
    last_fetched_at = $9,
    excluded_from_challenges = $10,
    distance = $11
RETURNING *;
-- name: GetStravaActivitiesByStravaIds :many
SELECT *
FROM strava_activities
WHERE id = ANY($1::bigint []);
-- name: GetStravaActivitiesBetweenDates :many
SELECT *
FROM strava_activities
WHERE start_date > @period_start
    and start_date < @period_end;
-- name: GetStravaActivitiesForPeriodByStravaId :many
SELECT *
FROM strava_activities
WHERE start_date > @period_start
    and start_date < @period_end
    and strava_athlete_id = @strava_athlete_id;
-- name: UpdateStravaActivityExcludedList :one
UPDATE strava_activities
SET excluded_from_challenges = $2
WHERE strava_id = $1
RETURNING *;
-- name: DeleteStravaActivity :exec
DELETE FROM strava_activities
WHERE strava_id = $1;
-- name: GetExtendedUserDataById :one
SELECT u.id,
    u.username,
    p.id AS "player_id",
    p.fname as "player_first_name",
    p.lname as "players_last_name",
    sod.strava_id AS "strava_id",
    sod.firstname AS "firstname",
    sod.lastname AS "lastname",
    p.club_id AS "club_id"
FROM users u
    FULL OUTER JOIN strava_oauth_data sod ON sod.id = u.strava_oauth_id
    FULL OUTER JOIN players p ON p.id = u.player_id
WHERE u.id = $1;
-- name: GetAllUsers :many
SELECT u.id,
    u.username,
    p.id AS "player_id",
    p.fname as "player_first_name",
    p.lname as "players_last_name",
    sod.strava_id AS "strava_id",
    sod.firstname AS "firstname",
    sod.lastname AS "lastname",
    p.club_id AS "club_id"
FROM users u
    FULL OUTER JOIN strava_oauth_data sod ON sod.id = u.strava_oauth_id
    LEFT JOIN players p ON p.id = u.player_id
ORDER BY u.id ASC;
-- name: UpdateUser :exec
UPDATE users
SET player_id = $2
WHERE id = $1;
-- name: UpdatePlayer :exec
UPDATE players
SET club_id = $2
WHERE id = $1;
-- name: GetAllClubs :many
SELECT *
FROM clubs
ORDER BY clubs.id ASC;
-- name: GetTrainings :many
SELECT *
FROM trainings t
ORDER BY t.date DESC
LIMIT @page_size OFFSET (@page_number - 1) * @page_size;
-- name: GetTeamsForTrainings :many
SELECT t.id,
    t.date,
    t.scorers,
    t.club_id,
    tt.color,
    tt._player_ids
FROM trainings t
    INNER JOIN training_teams tt ON tt.training_id = t.id
WHERE t.id = ANY($1::int []);
-- name: GetActivePlayers :many
SELECT * from players p
WHERE p.status in ('Aktív', 'active');
