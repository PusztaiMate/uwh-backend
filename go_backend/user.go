package main

import (
	"context"
	"log/slog"
	"math/rand/v2"
	"strconv"

	"github.com/jackc/pgx/v5/pgtype"
	"gitlab.com/PusztaiMate/uwh-backend/db"
)

type User struct {
	Id           int    `json:"id"`
	Username     string `json:"username"`
	PasswordHash string
	PlayerId     int `json:"player_id"`
	StravaAuthId int `json:"strava_auth_id"`
}

type ExtendedUserData struct {
	Id              int    `json:"id"`
	Username        string `json:"username"`
	PlayerId        int    `json:"player_id"`
	ClubId          int    `json:"club_id"`
	StravaId        int    `json:"strava_id"`
	StravaFirstName string `json:"strava_first_name"`
	StravaLastName  string `json:"strava_last_name"`
	PlayerFirstName string `json:"player_first_name"`
	PlayerLastName  string `json:"player_last_name"`
}

type UserRepository interface {
	FindUserByStravaId(ctx context.Context, stravaId pgtype.Int4) (db.User, error)
	FindById(ctx context.Context, id int32) (db.User, error)
	CreateUser(ctx context.Context, arg db.CreateUserParams) (db.User, error)
	FindUserByUsername(ctx context.Context, username string) (db.User, error)
	GetExtendedUserDataById(ctx context.Context, id int32) (db.GetExtendedUserDataByIdRow, error)
	GetAllUsers(ctx context.Context) ([]db.GetAllUsersRow, error)
	UpdateUser(ctx context.Context, arg db.UpdateUserParams) error
}

type UserService struct {
	db UserRepository
}

func NewUserService(db UserRepository) *UserService {
	return &UserService{db: db}
}

func (s *UserService) FindUserByStravaId(ctx context.Context, stravaId int) (*User, error) {
	user, err := s.db.FindUserByStravaId(ctx, pgtype.Int4{Int32: int32(stravaId), Valid: true})
	if err != nil {
		return nil, err
	}

	return &User{
		Id:           int(user.ID),
		Username:     user.Username,
		PasswordHash: user.PasswordHash.String,
		PlayerId:     int(user.PlayerID.Int32),
		StravaAuthId: int(user.StravaOauthID.Int32),
	}, nil
}

func (s *UserService) CreateUser(ctx context.Context, user User) (*User, error) {
	newUser, err := s.db.CreateUser(ctx, db.CreateUserParams{
		Username:      user.Username,
		StravaOauthID: pgtype.Int4{Int32: int32(user.StravaAuthId), Valid: true},
	})
	if err != nil {
		return nil, err
	}

	return &User{
		Id:           int(newUser.ID),
		Username:     newUser.Username,
		PasswordHash: newUser.PasswordHash.String,
		PlayerId:     int(newUser.PlayerID.Int32),
		StravaAuthId: int(newUser.StravaOauthID.Int32),
	}, nil
}
func (s *UserService) FindUserByUsername(ctx context.Context, username string) (*User, error) {
	user, err := s.db.FindUserByUsername(ctx, username)
	if err != nil {
		slog.Error("Error finding user by username", "err", err)
		return nil, err
	}

	return &User{
		Id:           int(user.ID),
		Username:     user.Username,
		PasswordHash: user.PasswordHash.String,
		PlayerId:     int(user.PlayerID.Int32),
		StravaAuthId: int(user.StravaOauthID.Int32),
	}, nil
}

func (s *UserService) FindById(ctx context.Context, id int) (*User, error) {
	user, err := s.db.FindById(ctx, int32(id))
	if err != nil {
		slog.Error("Error finding user by username", "err", err)
		return nil, err
	}

	return &User{
		Id:           int(user.ID),
		Username:     user.Username,
		PasswordHash: user.PasswordHash.String,
		PlayerId:     int(user.PlayerID.Int32),
		StravaAuthId: int(user.StravaOauthID.Int32),
	}, nil
}

func (s *UserService) GetExtendedUserDataById(ctx context.Context, id int) (ExtendedUserData, error) {
	userData, err := s.db.GetExtendedUserDataById(ctx, int32(id))
	if err != nil {
		return ExtendedUserData{}, err
	}

	return ExtendedUserData{
		Id:              int(userData.ID.Int32),
		Username:        userData.Username.String,
		ClubId:          int(userData.ClubID.Int32),
		PlayerId:        int(userData.PlayerID.Int32),
		StravaId:        int(userData.StravaID.Int32),
		StravaFirstName: userData.Firstname.String,
		StravaLastName:  userData.Lastname.String,
		PlayerFirstName: userData.PlayerFirstName.String,
		PlayerLastName:  userData.PlayersLastName.String,
	}, nil
}

func (s *UserService) GetAllUsers(ctx context.Context) ([]ExtendedUserData, error) {
	users, err := s.db.GetAllUsers(ctx)
	if err != nil {
		return nil, err
	}

	var us []ExtendedUserData
	for _, u := range users {
		us = append(us, ExtendedUserData{
			Id:              int(u.ID.Int32),
			Username:        u.Username.String,
			PlayerId:        int(u.PlayerID.Int32),
			ClubId:          int(u.ClubID.Int32),
			StravaId:        int(u.StravaID.Int32),
			StravaFirstName: u.Firstname.String,
			StravaLastName:  u.Lastname.String,
			PlayerFirstName: u.PlayerFirstName.String,
			PlayerLastName:  u.PlayersLastName.String,
		})
	}

	return us, nil
}

func (s *UserService) UpdateUser(ctx context.Context, args db.UpdateUserParams) error {
	return s.db.UpdateUser(ctx, args)
}

func getUsername(a Athlete) string {
	rn := rand.IntN(89999) + 10000
	if a.Username == "" {
		return "user_" + strconv.Itoa(rn)
	}
	return a.Username
}
