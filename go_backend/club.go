package main

import (
	"context"

	"gitlab.com/PusztaiMate/uwh-backend/db"
)

type Club struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

type ClubRepository interface {
	GetAllClubs(ctx context.Context) ([]db.Club, error)
}

type ClubService struct {
	db ClubRepository
}

func NewClubService(db ClubRepository) *ClubService {
	return &ClubService{
		db: db,
	}
}

func (s *ClubService) GetAll(ctx context.Context) ([]Club, error) {
	cc, err := s.db.GetAllClubs(ctx)
	if err != nil {
		return nil, err
	}

	var clubs []Club
	for _, c := range cc {
		clubs = append(clubs, Club{
			Id:   int(c.ID),
			Name: c.Name,
		})
	}

	return clubs, nil
}
