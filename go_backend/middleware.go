package main

import (
	"context"
	"fmt"
	"log/slog"
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
)

type contextKeyUser string

var (
	userContextKey = contextKeyUser("user")
)

func setUserContext(c echo.Context, user *User) {
	enrichedCtx := context.WithValue(
		c.Request().Context(),
		userContextKey,
		user,
	)
	r := c.Request().WithContext(enrichedCtx)
	c.SetRequest(r)
}

func getUserContext(c echo.Context) *User {
	value := c.Request().Context().Value(userContextKey)
	user, ok := value.(*User)
	if !ok {
		return nil
	}
	return user
}

type UserProfileMiddleware struct {
	secret      string
	userService *UserService
}

func NewUserProfileMiddleware(secret string, userService *UserService) *UserProfileMiddleware {
	return &UserProfileMiddleware{
		secret:      secret,
		userService: userService,
	}
}

func (a *UserProfileMiddleware) Process(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		token, err := getTokenFromRequestHeader(c.Request())
		if err != nil {
			return fmt.Errorf("invalid auth header: %w", err)
		}
		if token == "" {
			setUserContext(c, nil)
			next(c)
			return nil
		}

		jwtData, err := decodeJWT(token)
		if err != nil {
			slog.Error("could not decode jwt", "err", err)
			return fmt.Errorf("invalid token")
		}
		user, err := a.userService.FindUserByUsername(c.Request().Context(), jwtData.Sub)
		if err != nil {
			slog.Error("could not query user", "err", err)
			return fmt.Errorf("could not query user")
		}

		setUserContext(c, user)
		next(c)
		return nil
	}
}

type AuthorizerMiddleware struct {
	casbin       *CasbinClient
	routeAuthMap map[string]func(u *User, method string) bool
}

func routeAuthMap(casbin *CasbinClient) map[string]func(*User, string) bool {

	routeAuthMap := map[string]func(*User, string) bool{
		"/trainings": func(u *User, method string) bool {
			return true
		},
		"/hockey-events": func(u *User, method string) bool {
			switch method {
			case http.MethodGet:
				return true
			case http.MethodPost:
				return casbin.Enforce(u.Id, "hockey_events", "create")
			case http.MethodDelete:
				fmt.Println("heeeeeeere")
				return casbin.Enforce(u.Id, "hockey_events", "create")
			default:
				return true
			}
		},
		"/hockey-events/:id": func(u *User, method string) bool {
			return true
		},
		"/strava-auth-callback": func(u *User, method string) bool {
			return true
		},
		"/users/whoami": func(u *User, method string) bool {
			return true
		},
		"/challenge/weekly-report": func(u *User, method string) bool {
			return true
		},
		"/challenge/list-activities": func(u *User, method string) bool {
			return true
		},
		"/challenge-points": func(u *User, method string) bool {
			return true
		},
		"/challenge-periods": func(u *User, method string) bool {
			return true
		},
		"/challenge/mark-activity-as-excluded": func(u *User, method string) bool {
			return u != nil
		},
		"/challenge/mark-activity-as-included": func(u *User, method string) bool {
			return u != nil
		},
		"/activities/force-refetch": func(u *User, method string) bool {
			return true
		},
		"/users/:id": func(u *User, method string) bool {
			switch method {
			case http.MethodGet:
				return u != nil
			case http.MethodPut:
				return casbin.Enforce(u.Id, "users", "update")
			default:
				return true
			}
		},
		"/users/add-role": func(u *User, s string) bool {
			return casbin.Enforce(u.Id, "users", "update")
		},
		"/users/roles": func(u *User, s string) bool {
			return casbin.Enforce(u.Id, "users", "update")
		},
		"/users": func(u *User, method string) bool {
			return u != nil
		},
		"/clubs": func(u *User, method string) bool {
			return true
		},
	}

	return routeAuthMap
}

func NewAuthorizerMiddleware(casbin *CasbinClient) *AuthorizerMiddleware {

	return &AuthorizerMiddleware{
		casbin:       casbin,
		routeAuthMap: routeAuthMap(casbin),
	}
}

func (s *AuthorizerMiddleware) Process(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		path := strings.Split(c.Request().URL.Path, "api/v2")[1]
		user := getUserContext(c)
		allowed := true
		if f, ok := s.routeAuthMap[path]; ok {
			allowed = f(user, c.Request().Method)
		}
		if !allowed {
			slog.Warn("unauthorized access", "user", user, "path", path)
			return c.JSON(http.StatusUnauthorized, GenericMessage{Message: "unauthorized access"})
		}
		next(c)
		return nil
	}
}
