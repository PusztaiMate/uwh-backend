package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"os"
	"slices"
	"strconv"
	"strings"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/jackc/pgx/v5/pgtype"
	"github.com/labstack/echo/v4"
	"gitlab.com/PusztaiMate/uwh-backend/db"
	"gitlab.com/PusztaiMate/uwh-backend/strava-client/strava_client/activities"
)

var ErrUnauthorized = errors.New("unauthorized access")

func (s *Server) handleListHockeyEvents(c echo.Context) error {
	events, err := s.hockeyEventsService.ListHockeyEvents(c.Request().Context())
	if err != nil {
		return c.JSON(500, GenericMessage{Message: "Error fetching hockey events"})
	}

	return c.JSON(200, map[string][]HockeyEvent{"hockey_events": events})
}

func (s *Server) handleCreateHockeyEvent(c echo.Context) error {
	newHockeyEvent := HockeyEvent{}
	if err := c.Bind(&newHockeyEvent); err != nil {
		return err
	}

	created, err := s.hockeyEventsService.CreateHockeyEvent(c.Request().Context(), newHockeyEvent)
	if err != nil {
		return c.JSON(500, GenericMessage{Message: "Error creating hockey event"})
	}

	return c.JSON(201, map[string]any{"data": created})
}

func (s *Server) handleDeleteHockeyEvent(c echo.Context) error {
	eventId := c.Param("id")
	eventIdConverted, err := strconv.Atoi(eventId)
	if err != nil {
		return c.JSON(http.StatusBadRequest, GenericMessage{Message: "Invalid hockey event ID"})
	}
	err = s.hockeyEventsService.Delete(c.Request().Context(), eventIdConverted)
	if err != nil {
		slog.Error("couldn't delete event", "eventId", eventIdConverted, "err", err)
		return c.JSON(http.StatusInternalServerError, GenericMessage{Message: "could not delete event"})
	}

	return c.JSON(http.StatusOK, GenericMessage{Message: "successfully deleted the event"})
}

func (s *Server) handleUpdateHockeyEvent(c echo.Context) error {
	type Request struct {
		Date        string `json:"date"`
		Title       string `json:"title"`
		Type        string `json:"type"`
		Description string `json:"description"`
	}
	var request Request
	if err := c.Bind(&request); err != nil {
		slog.Error("could not bind request", "err", err)
		return c.JSON(http.StatusBadRequest, GenericMessage{Message: "invalid request"})
	}

	eventId := c.Param("id")
	eventIdConverted, err := strconv.Atoi(eventId)
	if err != nil {
		return c.JSON(http.StatusBadRequest, GenericMessage{Message: "Invalid hockey event ID"})
	}

	event, err := s.hockeyEventsService.GetHockeyEvent(c.Request().Context(), eventIdConverted)
	if err != nil || event == nil {
		slog.Error("could not get event", "eventId", eventIdConverted, "err", err)
		return c.JSON(http.StatusInternalServerError, GenericMessage{Message: "could not get event"})
	}

	//2024-12-11T11:00:00.000Z
	newDate, err := time.Parse("2006-01-02T15:04:05.000Z", request.Date)
	if err != nil {
		slog.Error("could not parse date", "err", err)
		return c.JSON(http.StatusBadRequest, GenericMessage{Message: "could not parse date"})
	}
	err = s.hockeyEventsService.Update(c.Request().Context(), eventIdConverted, HockeyEvent{
		ID:          eventIdConverted,
		Date:        newDate,
		Title:       request.Title,
		Type:        request.Type,
		Description: request.Description,
	})
	if err != nil {
		slog.Error("could not update event", "eventId", eventIdConverted, "err", err)
		return c.JSON(http.StatusInternalServerError, GenericMessage{Message: "could not update event"})
	}

	return c.JSON(http.StatusOK, GenericMessage{Message: "successfully updated the event"})
}

func (s *Server) handleGetHockeyEvent(c echo.Context) error {
	eventId := c.Param("id")
	eventIdConverted, err := strconv.Atoi(eventId)
	if err != nil {
		return c.JSON(http.StatusBadRequest, GenericMessage{Message: "Invalid hockey event ID"})
	}

	event, err := s.hockeyEventsService.GetHockeyEvent(c.Request().Context(), eventIdConverted)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, GenericMessage{Message: "Error fetching hockey event"})
	}

	type Response struct {
		Data *HockeyEvent `json:"data"`
	}

	return c.JSON(http.StatusOK, Response{Data: event})
}

func (s *Server) handleStravaAuthCallback(c echo.Context) error {
	type AuthorizationResponse struct {
		Code  string `query:"code"`
		Error string `query:"error"`
		Scope string `query:"scope"`
		State string `query:"state"`
	}

	var params AuthorizationResponse
	if err := c.Bind(&params); err != nil {
		slog.Error("could not bind query params", "err", err)
		return c.JSON(500, GenericMessage{Message: "Could not bind query params"})
	}

	clientSecret := os.Getenv("STRAVA_APP_CLIENT_SECRET")
	if clientSecret == "" {
		slog.Error("could not find client secret (missing env var?)")
		return c.JSON(500, GenericMessage{Message: "Missing client secret"})
	}

	resp, err := sendStravaAuthRequest(clientId(), clientSecret, params.Code, "authorization_code")
	if err != nil {
		slog.Error("could not post to strava oauth endpoint", "err", err)
		return c.JSON(500, "Could not post to Strava oauth endpoint")
	}
	defer resp.Body.Close()

	var oathResponse OAuthResponse
	err = json.NewDecoder(resp.Body).Decode(&oathResponse)
	if err != nil {
		slog.Error("could not decode strava oauth response", "err", err)
		return c.JSON(500, "Could not decode Strava auth response")
	}

	updatedStravaAuthData, err := s.stravaAuthService.UpsertStravaOauthData(c.Request().Context(), StravaAuthData{
		StravaID:     int(oathResponse.Athlete.ID),
		Firstname:    oathResponse.Athlete.Firstname,
		Lastname:     oathResponse.Athlete.Lastname,
		AccessToken:  oathResponse.AccessToken,
		RefreshToken: oathResponse.RefreshToken,
		ExpiresAt:    int(oathResponse.ExpiresAt),
	})
	if err != nil {
		slog.Error("could not upsert strava oauth data", "err", err)
		return c.JSON(500, "Could not upsert Strava oauth data")
	}

	foundUser, err := s.userService.FindUserByStravaId(c.Request().Context(), int(updatedStravaAuthData.ID))
	if err != nil {
		if !isNotFoundDbError(err) {
			slog.Error("could not query user by strava id", "err", err)
			return c.JSON(500, "Could not query user by strava id")
		}
		foundUser, err = s.userService.CreateUser(c.Request().Context(), User{
			Username:     getUsername(oathResponse.Athlete),
			StravaAuthId: updatedStravaAuthData.ID,
		})
		if err != nil {
			slog.Error("could not create user", "err", err)
			return c.JSON(500, "Could not create user")
		}
	}

	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub": foundUser.Username,
	}).SignedString([]byte(getSecretKey()))
	if err != nil {
		slog.Error("could not sign jwt", "err", err)
		return c.Redirect(http.StatusFound, fmt.Sprintf("%s/", vizalattihokiDomain()))
	}

	return c.Redirect(http.StatusFound, fmt.Sprintf("%s/login?token=%s", vizalattihokiDomain(), token))
}

type JWTData struct {
	Sub string `json:"sub"`
}

func decodeJWT(tokenString string) (*JWTData, error) {
	token, err := jwt.Parse(tokenString, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", t.Header["alg"])
		}
		return []byte(getSecretKey()), nil
	})
	if err != nil {
		return nil, err

	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		return nil, fmt.Errorf("invalid token")
	}

	sub, ok := claims["sub"].(string)
	if !ok {
		return nil, fmt.Errorf("invalid token")
	}

	return &JWTData{
		Sub: sub,
	}, nil
}

func getTokenFromRequestHeader(r *http.Request) (string, error) {
	authHeaders := r.Header.Get("Authorization")
	if authHeaders == "" {
		return "", nil
	}

	authHeaderParts := strings.Split(authHeaders, " ")
	if len(authHeaderParts) != 2 {
		slog.Error("auth header does not have two parts", "header", authHeaders)
		return "", fmt.Errorf("invalid header format")
	}

	return authHeaderParts[1], nil
}

func getTokenFromRequestHeaderStrict(r *http.Request) (string, error) {
	authHeaders := r.Header.Get("Authorization")
	if authHeaders == "" {
		slog.Error("no auth header")
		return "", ErrUnauthorized
	}

	authHeaderParts := strings.Split(authHeaders, " ")
	if len(authHeaderParts) != 2 {
		slog.Error("auth header does not have two parts", "header", authHeaders)
		return "", ErrUnauthorized
	}

	return authHeaderParts[1], nil
}

func (s *Server) getUserAndStravaDataFromToken(ctx context.Context, token string) (*User, *StravaAuthData, error) {
	jwtData, err := decodeJWT(token)
	if err != nil {
		slog.Error("could not decode jwt", "err", err)
		return nil, nil, err
	}

	user, err := s.userService.FindUserByUsername(ctx, jwtData.Sub)
	if err != nil {
		slog.Error("could not query user", "err", err)
		return nil, nil, err
	}

	stravaAuthData, err := s.stravaAuthService.GetStravaAuth(ctx, user.StravaAuthId)
	if err != nil {
		// no strava connected to the user
		slog.Info("no strava auth data found", "err", err)
		stravaAuthData = &StravaAuthData{}
	}

	return user, stravaAuthData, nil
}

func (s *Server) handleWhoAmI(c echo.Context) error {
	type Response struct {
		Id        int      `json:"id"`
		Username  string   `json:"username"`
		FirstName string   `json:"firstname"`
		LastName  string   `json:"lastname"`
		StravaId  int      `json:"stravaId"`
		Roles     []string `json:"roles"`
	}

	token, err := getTokenFromRequestHeaderStrict(c.Request())
	if err != nil {
		return c.JSON(200, Response{})
	}

	user, stravaAuthData, err := s.getUserAndStravaDataFromToken(c.Request().Context(), token)
	if err != nil {
		slog.Error("could not query user", "err", err)
		return c.JSON(500, GenericMessage{Message: "Error querying user / strava data"})
	}

	roles, err := s.casbin.GetGroupingPoliciesForUser(user.Id)
	if err != nil {
		slog.Error("could not query roles for user", "userId", user.Id, "err", err)
		return c.JSON(500, GenericMessage{Message: "Error querying user roles"})
	}

	return c.JSON(200, Response{
		Id:        user.Id,
		Username:  user.Username,
		FirstName: stravaAuthData.Firstname,
		LastName:  stravaAuthData.Lastname,
		StravaId:  stravaAuthData.StravaID,
		Roles:     roles,
	})
}

type WeeklyActivityReport struct {
	ModifiedTime int     `json:"modified_time"`
	UWHTrainings int     `json:"uwh_trainings"`
	DistanceSwam float64 `json:"distance_swam"`
}

type WeeklyActivityReports struct {
	Report           WeeklyActivityReport `json:"report"`
	AthleteName      string               `json:"athlete_name"`
	AtheleteStravaId int                  `json:"athlete_strava_id"`
	Points           int                  `json:"points"`
}

type WeeklyActivityReportsResponse struct {
	Reports                []WeeklyActivityReports `json:"reports"`
	CalculationDescription string                  `json:"calculation_description"`
}

func getWeekNumberFromQueryParam(c echo.Context) int {
	week := c.QueryParam("week")
	weekN, err := strconv.Atoi(week)
	if err != nil {
		weekN = 1
	}

	return weekN
}

func getAtheleteStravaId(c echo.Context) int64 {
	athleteStravaId := c.QueryParam("stravaAthleteId")
	asi, err := strconv.ParseInt(athleteStravaId, 10, 64)
	if err != nil {
		asi = 1
	}

	return asi
}

func createActivityQueryParams(startingMonday, nextMonday time.Time) activities.GetLoggedInAthleteActivitiesParams {
	after := startingMonday.Unix()
	before := nextMonday.Unix()
	params := activities.GetLoggedInAthleteActivitiesParams{}
	params.After = &after
	params.Before = &before

	return params
}

func (s *Server) handleGetListActivities(c echo.Context) error {
	weekN := getWeekNumberFromQueryParam(c)
	athleteStravaId := getAtheleteStravaId(c)
	mondayOfWeek, nextMonday := getNthWeekOfTheYear(weekN)

	activities, err := s.stravaActivityService.GetStravaActivitiesForPeriodByStravaId(
		c.Request().Context(),
		athleteStravaId,
		mondayOfWeek,
		nextMonday,
	)
	if err != nil {
		slog.Error(
			"error fetching athletes's strava activities from db",
			"athleteStravaId", athleteStravaId,
			"weekN", weekN,
		)
		return c.JSON(500, GenericMessage{Message: "error fetching data from db"})
	}

	type Response struct {
		Data    []StravaActivity `json:"data"`
		Message string           `json:"message"`
	}

	return c.JSON(200, Response{Data: activities, Message: "Activity data successfully fetched from db"})
}

func (s *Server) handleGetWeeklyReport(c echo.Context) error {
	weekN := getWeekNumberFromQueryParam(c)
	mondayOfWeek, nextMonday := getNthWeekOfTheYear(weekN)

	acts, err := s.stravaActivityService.GetStravaActivitiesBetweenDates(
		c.Request().Context(),
		db.GetStravaActivitiesBetweenDatesParams{
			PeriodStart: pgtype.Timestamp{Time: mondayOfWeek, Valid: true},
			PeriodEnd:   pgtype.Timestamp{Time: nextMonday, Valid: true},
		},
	)
	if err != nil {
		slog.Error("could not query db for activity data", "err", err)
		return c.JSON(500, GenericMessage{Message: "error querying db"})
	}

	athleteIdToActivities := make(map[int][]StravaActivity)
	for _, a := range acts {
		if _, ok := athleteIdToActivities[a.StravaAthleteId]; !ok {
			athleteIdToActivities[a.StravaAthleteId] = make([]StravaActivity, 0)
		}
		athleteIdToActivities[a.StravaAthleteId] = append(athleteIdToActivities[a.StravaAthleteId], StravaActivity{
			StravaId:               a.StravaId,
			SportType:              a.SportType,
			MovingTime:             a.MovingTime,
			StravaUploadId:         a.StravaUploadId,
			StartDate:              a.StartDate,
			Name:                   a.Name,
			Description:            a.Description,
			StravaAthleteId:        a.StravaAthleteId,
			LastFetchedAt:          a.LastFetchedAt,
			ExcludedFromChallenges: a.ExcludedFromChallenges,
			Distance:               a.Distance,
		})
	}

	auths, err := s.stravaAuthService.ListStravaAuthData(c.Request().Context())
	if err != nil {
		slog.Error("could not query auth data", "err", err)
		return c.JSON(500, GenericMessage{Message: "error querying db"})
	}

	reports := calculateWeeklyActivityReports(athleteIdToActivities, auths)

	reports = extendReportsWithPoints(reports)
	s.refreshReportsInDb(c.Request().Context(), weekN, reports)

	return c.JSON(200, WeeklyActivityReportsResponse{Reports: reports})
}

func convertToIntAndDeduplicateExcludedList(input []string) []int {
	newExcludedIdList := make([]int, 0)
	for _, excl := range input {
		// don't really know if this could happen
		if excl == "" {
			continue
		}
		ei, err := strconv.Atoi(excl)
		if err != nil {
			continue
		}

		newExcludedIdList = append(newExcludedIdList, ei)
	}

	slices.Sort(newExcludedIdList)
	return slices.Compact(newExcludedIdList)
}

func (s *Server) handleForceRefetchActivity(c echo.Context) error {
	type Request struct {
		StravaActivityId int64 `json:"strava_activity_id"`
	}
	var request Request
	if err := c.Bind(&request); err != nil {
		slog.Error("could not bind request")
		return c.JSON(http.StatusBadRequest, GenericMessage{Message: "invalid request"})
	}

	activity, err := s.stravaActivityService.GetStravaActivityByStravaId(c.Request().Context(), request.StravaActivityId)
	if err != nil {
		slog.Error("couldn't retrive activity from db", "request.StravaActivityId", request.StravaActivityId, "err", err)
		return c.JSON(http.StatusInternalServerError, GenericMessage{Message: "could not query activity from db"})
	}

	authData, err := s.stravaAuthService.FindByStravaId(c.Request().Context(), activity.StravaAthleteId)
	if err != nil || authData == nil {
		slog.Error("could not find strava auth data based on athlete id", "athleteId", activity.StravaAthleteId, "err", err)
		return c.JSON(http.StatusInternalServerError, GenericMessage{"could not find auth data to fetch activity from strava"})
	}

	authData, err = s.stravaAuthService.RefreshTokenAndSaveIntoDb(c.Request().Context(), *authData)
	if err != nil {
		slog.Error("could not refresh strava token in db", "athleteId", activity.StravaAthleteId, "err", err)
		return c.JSON(http.StatusInternalServerError, GenericMessage{"could not refresh strava access token"})
	}

	act, err := s.activityFetcher.fetchActivity(c.Request().Context(), request.StravaActivityId, authData.AccessToken)
	if err != nil {
		slog.Error("could not fetch activity from strava", "err", err)
		return c.JSON(http.StatusInternalServerError, GenericMessage{"could not fetch activity from strava"})
	}

	err = s.stravaActivityService.DeleteStravaActivity(c.Request().Context(), request.StravaActivityId)
	if err != nil {
		slog.Error("could not delete activity from db", "err", err)
		return c.JSON(http.StatusInternalServerError, GenericMessage{"could not delete activity from db"})
	}

	_, err = s.stravaActivityService.SaveFetchedActivityToDb(c.Request().Context(), act)
	if err != nil {
		slog.Error("could not save activity to db", "err", err)
		return c.JSON(http.StatusInternalServerError, GenericMessage{"could not save activity to db"})
	}

	return c.JSON(http.StatusOK, GenericMessage{Message: "successfully updated the activity"})
}

func (s *Server) handleMarkStravaActivityAsIncludedInChallenge(c echo.Context) error {
	type Request struct {
		StravaActivityId int64 `json:"strava_activity_id"`
		ChallengeId      int   `json:"challenge_id"`
	}

	var request Request
	if err := c.Bind(&request); err != nil {
		slog.Error("could not bind request")
		return c.JSON(http.StatusBadRequest, GenericMessage{Message: "invalid request"})
	}

	_, err := s.stravaActivityService.UpdateStravaActivityExcludedList(c.Request().Context(), db.UpdateStravaActivityExcludedListParams{
		StravaID:               request.StravaActivityId,
		ExcludedFromChallenges: pgtype.Text{String: "", Valid: true},
	})
	if err != nil {
		slog.Error("could not update activity excluded list in db", "err", err)
		return c.JSON(http.StatusInternalServerError, GenericMessage{Message: "could not update activity"})
	}

	return c.JSON(http.StatusOK, GenericMessage{Message: "successfully updated"})
}

func (s *Server) handleMarkStravaActivityAsExcludedFromChallenge(c echo.Context) error {
	type Request struct {
		StravaActivityId int64 `json:"strava_activity_id"`
		ChallengeId      int   `json:"challenge_id"`
	}

	var request Request
	if err := c.Bind(&request); err != nil {
		slog.Error("could not bind request")
		return c.JSON(http.StatusBadRequest, GenericMessage{Message: "invalid request"})
	}

	activity, err := s.stravaActivityService.GetStravaActivityByStravaId(c.Request().Context(), request.StravaActivityId)
	if err != nil {
		slog.Error("could not load strava activity from db", "request.StravaActivityId", request.StravaActivityId)
		return c.JSON(http.StatusInternalServerError, GenericMessage{Message: "could not load strava activity from db"})
	}

	currentlyExcluded := strings.Split(activity.ExcludedFromChallenges, ",")
	converted := convertToIntAndDeduplicateExcludedList(currentlyExcluded)

	for _, v := range converted {
		if request.ChallengeId == v {
			return c.JSON(http.StatusOK, GenericMessage{Message: "already excluded, nothing to do"})
		}
	}

	converted = append(converted, request.ChallengeId)
	newExcludedList := make([]string, len(converted))
	for i, v := range converted {
		newExcludedList[i] = strconv.Itoa(v)
	}

	_, err = s.stravaActivityService.UpdateStravaActivityExcludedList(c.Request().Context(), db.UpdateStravaActivityExcludedListParams{
		StravaID:               request.StravaActivityId,
		ExcludedFromChallenges: pgtype.Text{String: strings.Join(newExcludedList, ","), Valid: true},
	})
	if err != nil {
		slog.Error("could not update activity excluded list in db", "err", err)
		return c.JSON(http.StatusInternalServerError, GenericMessage{Message: "could not update activity"})
	}

	return c.JSON(http.StatusOK, GenericMessage{Message: "successfully updated"})
}

func (s *Server) handleGetChallengePoints(c echo.Context) error {
	// TODO: handle missing period id -> all periods?
	periodId := c.QueryParam("period_id")
	periodIdInt, err := strconv.Atoi(periodId)
	if err != nil {
		slog.Error("invalid period id", "period_id", periodId, "err", err)
		return c.JSON(400, GenericMessage{Message: "Invalid period id"})
	}

	challengePeriod, err := s.challengeService.GetPeriodById(c.Request().Context(), periodIdInt)
	if err != nil {
		slog.Error("could not get challenge period", "err", err)
		return c.JSON(500, GenericMessage{Message: "Could not get challenge period"})
	}

	stats, err := s.weeklyTrainingStatsService.ListWeeklyTrainingStatsForWeeks(
		c.Request().Context(),
		challengePeriod.StartingYear,
		challengePeriod.FirstWeek,
		challengePeriod.LastWeek,
	)
	if err != nil {
		slog.Error("could not get weekly training stats", "err", err)
		return c.JSON(500, GenericMessage{Message: "Could not get weekly training stats"})
	}

	type Response struct {
		Data []struct {
			AthleteName     string  `json:"athlete_name"`
			AthleteStravaId int     `json:"athlete_strava_id"`
			Points          int     `json:"points"`
			DistanceSwam    float64 `json:"distance_swam"`
		} `json:"data"`
	}

	var response Response
	for _, stat := range stats {
		response.Data = append(response.Data, struct {
			AthleteName     string  `json:"athlete_name"`
			AthleteStravaId int     `json:"athlete_strava_id"`
			Points          int     `json:"points"`
			DistanceSwam    float64 `json:"distance_swam"`
		}{
			AthleteName:     stat.AthleteName,
			AthleteStravaId: stat.StravaId,
			Points:          stat.Points,
			DistanceSwam:    stat.DistanceSwam,
		})
	}

	return c.JSON(200, response)
}

func (s *Server) handleCreateChallengePeriod(c echo.Context) error {
	type Request struct {
		StartingYear int `json:"starting_year"`
		EndingYear   int `json:"ending_year"`
		FirstWeek    int `json:"first_week"`
		LastWeek     int `json:"last_week"`
	}

	var req Request
	if err := c.Bind(&req); err != nil {
		slog.Error("could not bind request", "err", err)
		return c.JSON(400, GenericMessage{Message: "Invalid request"})
	}

	period, err := s.challengeService.CreateChallengePeriod(
		c.Request().Context(),
		req.StartingYear,
		req.FirstWeek,
		req.EndingYear,
		req.LastWeek,
	)
	if err != nil {
		slog.Error("could not create challenge period", "err", err)
		return c.JSON(500, GenericMessage{Message: "Could not create challenge period"})
	}

	return c.JSON(201, map[string]any{"data": period, "message": "Challenge period created"})
}

func (s *Server) handleListChallengePeriods(c echo.Context) error {
	periods, err := s.challengeService.ListChallengePeriods(c.Request().Context())
	if err != nil {
		slog.Error("could not list challenge periods", "err", err)
		return c.JSON(500, GenericMessage{Message: "Could not list challenge periods"})
	}

	return c.JSON(200, map[string][]ChallengePeriod{"challenge_periods": periods})
}

func (s *Server) handleGetExtendedUserData(c echo.Context) error {
	uid := c.Param("id")
	userId, err := strconv.Atoi(uid)
	if err != nil {
		return c.JSON(http.StatusBadRequest, GenericMessage{Message: "Invalid hockey event ID"})
	}

	user, err := s.userService.GetExtendedUserDataById(c.Request().Context(), userId)
	if err != nil {
		slog.Error("could not retrieve user from db", "userId", userId, "err", err)
		return c.JSON(http.StatusInternalServerError, GenericMessage{Message: "Could not fetch user from db"})
	}

	type Response struct {
		Data    ExtendedUserData `json:"data"`
		Message string           `json:"message"`
	}

	return c.JSON(http.StatusOK, Response{
		Message: "successfully fetched user",
		Data:    user,
	})
}

func (s *Server) handleGetAllUsers(c echo.Context) error {
	users, err := s.userService.GetAllUsers(c.Request().Context())
	if err != nil {
		slog.Error("could not retrieve users from db", "err", err)
		return c.JSON(http.StatusInternalServerError, GenericMessage{Message: "Could not fetch users from db"})
	}

	type Response struct {
		Data    []ExtendedUserData `json:"data"`
		Message string             `json:"message"`
	}

	return c.JSON(http.StatusOK, Response{
		Message: "successfully fetched user",
		Data:    users,
	})
}

func (s *Server) handleUpdateUser(c echo.Context) error {
	uid := c.Param("id")
	userId, err := strconv.Atoi(uid)
	if err != nil {
		return c.JSON(http.StatusBadRequest, GenericMessage{Message: "Invalid hockey event ID"})
	}
	type Request struct {
		ClubId   int `json:"club_id"`
		PlayerId int `json:"player_id"`
	}

	var request Request
	if err := c.Bind(&request); err != nil {
		slog.Error("could not bind update user request to go object", "err", err)
		return c.JSON(http.StatusBadRequest, "could not bind request")
	}

	err = s.userService.UpdateUser(c.Request().Context(), db.UpdateUserParams{
		ID: int32(userId),
		PlayerID: pgtype.Int4{
			Int32: int32(request.PlayerId),
			Valid: request.PlayerId != 0},
	})
	if err != nil {
		slog.Error("could not update user's player id", "err", err, "playerId", request.PlayerId)
		return c.JSON(http.StatusInternalServerError, "could not update user's player id")
	}

	err = s.playerService.UpdatePlayer(c.Request().Context(), db.UpdatePlayerParams{
		ID: int32(request.PlayerId),
		ClubID: pgtype.Int4{
			Int32: int32(request.ClubId),
			Valid: request.ClubId != 0,
		},
	})
	if err != nil {
		slog.Error("could not update player's club id", "err", err, "playerId", request.PlayerId, "clubId", request.ClubId)
		return c.JSON(http.StatusInternalServerError, "could not update player's club id")
	}

	return c.JSON(http.StatusOK, GenericMessage{Message: "successfully updated"})
}

func (s *Server) handleGetClubs(c echo.Context) error {
	clubs, err := s.clubService.GetAll(c.Request().Context())
	if err != nil {
		slog.Error("could not fetch clubs from db", "err", err)
		return c.JSON(http.StatusInternalServerError, GenericMessage{Message: "could not fetch clubs from db"})
	}

	type Response struct {
		Data    []Club `json:"data"`
		Message string `json:"message"`
	}
	return c.JSON(http.StatusOK, Response{Data: clubs, Message: "successfully fetched clubs"})
}

func (s *Server) handleGetTrainings(c echo.Context) error {
	pageSize := c.QueryParam("pageSize")
	ps, err := strconv.Atoi(pageSize)
	if err != nil {
		return c.JSON(http.StatusBadRequest, GenericMessage{Message: "invalid page size"})
	}
	if ps == 0 {
		ps = 10
	}
	pageNumber := c.QueryParam("pageNumber")
	pn, err := strconv.Atoi(pageNumber)
	if err != nil {
		return c.JSON(http.StatusBadRequest, GenericMessage{Message: "invalid page number"})
	}
	if pn == 0 {
		pn = 10
	}

	trainings, err := s.traininService.GetTrainings(c.Request().Context(), pn, ps)
	if err != nil {
		slog.Error("could not query training data from db", "err", err)
		return c.JSON(http.StatusInternalServerError, GenericMessage{Message: "could not load training data from db"})
	}

	trainingIds := make([]int, len(trainings))
	for i, t := range trainings {
		trainingIds[i] = t.Id
	}
	extended, err := s.traininService.GetExtendedTrainingData(c.Request().Context(), trainingIds)
	if err != nil {
		slog.Error("could not query training data from db", "err", err)
		return c.JSON(http.StatusInternalServerError, GenericMessage{Message: "could not load training data from db"})
	}

	return c.JSON(http.StatusOK, extended)
}

func (s *Server) handleGetRoles(c echo.Context) error {
	roles, err := s.casbin.GetRoles()
	if err != nil {
		slog.Error("could not fetch roles", "err", err)
		return c.JSON(http.StatusInternalServerError, GenericMessage{Message: "could not load roles"})
	}

	type Response struct {
		Message string   `json:"message"`
		Data    []string `json:"data"`
	}

	return c.JSON(http.StatusOK, Response{Message: "successfully retrieved roles", Data: roles})
}

func (s *Server) handleAddRoleToUser(c echo.Context) error {
	type Request struct {
		UserId int    `json:"user_id"`
		Role   string `json:"role"`
	}
	var request Request
	if err := c.Bind(&request); err != nil {
		slog.Error("could not bind request", "err", err)
		return c.JSON(http.StatusBadRequest, GenericMessage{Message: "invalid payload"})
	}

	user, err := s.userService.FindById(c.Request().Context(), request.UserId)
	if err != nil || user == nil {
		slog.Error("could not load user", "err", err, "userId", request.UserId)
		return c.JSON(http.StatusInternalServerError, GenericMessage{Message: "could not fetch user"})
	}

	if valid := s.casbin.ValidateRole(request.Role); !valid {
		return c.JSON(http.StatusBadRequest, GenericMessage{Message: "role is not valid"})
	}

	err = s.casbin.AddRoleToUser(request.UserId, request.Role)
	if err != nil {
		slog.Error("could not add role to user", "userId", request.UserId, "role", request.Role, "err", err)
		return c.JSON(http.StatusInternalServerError, "could not add role to user")
	}

	return c.JSON(http.StatusOK, GenericMessage{Message: "role successfully added to user"})
}

func parseStringToIntSlice(inp string) ([]int, error) {
	if inp == "" {
		return []int{}, nil
	}

	sep := ","
	ss := strings.Split(inp, sep)

	results := make([]int, 0, len(ss))
	for _, s := range ss {
		parsed, err := strconv.Atoi(s)
		if err != nil {
			return nil, err
		}
		results = append(results, parsed)
	}

	return results, nil
}

func (s *Server) GetPlayerStreakData(c echo.Context) error {
	type Request struct {
		PlayerIds string `query:"player_ids"`
	}
	var queries Request
	if err := c.Bind(&queries); err != nil {
		slog.Error("could not bind request", "err", err)
		return c.JSON(400, GenericMessage{Message: "Invalid request"})
	}

	playerIds, err := parseStringToIntSlice(queries.PlayerIds)
	if err != nil {
		slog.Error("could not parse player id array into int array", "err", err)
		return c.JSON(400, GenericMessage{Message: "Invalid request"})
	}

	// no specific playerIds fetched, let's use the currently active players
	if len(playerIds) == 0 {
		players, err := s.playerService.GetActivePlayers(c.Request().Context())
		if err != nil {
			slog.Error("could not fetch active players from db", "err", err)
			return c.JSON(
				http.StatusInternalServerError,
				GenericMessage{Message: "could not fetch players from db"},
			)
		}
		for _, p := range players {
			playerIds = append(playerIds, p.Id)
		}
		slog.Info("playerIds are", "queries.PlayerIds", queries.PlayerIds)
	}

	result, err := s.traininService.CalculateTrainingStreakForPlayers(c.Request().Context(), playerIds)
	if err != nil {
		slog.Error("could not calculate streak data", "err", err)
		return c.JSON(http.StatusInternalServerError, GenericMessage{Message: "could not calculate streak data"})
	}

	type Response struct {
		Data    map[int]*StreakData `json:"data"`
		Message string              `json:"message"`
	}

	return c.JSON(http.StatusOK, Response{Data: result, Message: "successfully calculated streak data"})
}
