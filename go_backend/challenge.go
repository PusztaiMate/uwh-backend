package main

import (
	"context"
	"fmt"

	"gitlab.com/PusztaiMate/uwh-backend/db"
)

type WeeklyTrainingStat struct {
	WeekNumber   int     `json:"week_number"`
	Year         int     `json:"year"`
	Points       int     `json:"points"`
	StravaId     int     `json:"strava_id"`
	AthleteName  string  `json:"athlete_name"`
	DistanceSwam float64 `json:"distance_swam"`
}

type ChallengePeriod struct {
	StartingYear int `json:"starting_year"`
	EndingYear   int `json:"ending_year"`
	FirstWeek    int `json:"first_week"`
	LastWeek     int `json:"last_week"`
	Id           int `json:"id"`
}

type ChallengeRepository interface {
	CreateChallengePeriod(ctx context.Context, arg db.CreateChallengePeriodParams) (db.ChallengePeriod, error)
	FindPeriodsByDate(ctx context.Context, arg db.FindPeriodsByDateParams) ([]db.ChallengePeriod, error)
	ListChallengePeriods(ctx context.Context) ([]db.ChallengePeriod, error)
	GetPeriodById(ctx context.Context, id int32) (db.ChallengePeriod, error)
}

type WeeklyTrainingStatsRepository interface {
	CreateWeeklyTrainingStats(ctx context.Context, arg db.CreateWeeklyTrainingStatsParams) (db.WeeklyTrainingStat, error)
	ListWeeklyTrainingForWeek(ctx context.Context, arg db.ListWeeklyTrainingForWeekParams) ([]db.WeeklyTrainingStat, error)
	UpsertWeeklyTrainingStats(ctx context.Context, arg db.UpsertWeeklyTrainingStatsParams) (db.WeeklyTrainingStat, error)
	ListWeeklyTrainingStatsForWeeks(ctx context.Context, arg db.ListWeeklyTrainingStatsForWeeksParams) ([]db.ListWeeklyTrainingStatsForWeeksRow, error)
}

type ChallengeService struct {
	challengeRepo ChallengeRepository
}

func NewChallengeService(challengeRepo ChallengeRepository) *ChallengeService {
	return &ChallengeService{challengeRepo: challengeRepo}
}

func convertChallengePeriodDbObjIntoDomainObj(challengePeriod db.ChallengePeriod) ChallengePeriod {
	return ChallengePeriod{
		StartingYear: int(challengePeriod.StartingYear),
		EndingYear:   int(challengePeriod.EndingYear),
		FirstWeek:    int(challengePeriod.FirstWeek),
		LastWeek:     int(challengePeriod.LastWeek),
		Id:           int(challengePeriod.ID),
	}
}

func (s *ChallengeService) GetPeriodById(ctx context.Context, id int) (ChallengePeriod, error) {
	challengePeriod, err := s.challengeRepo.GetPeriodById(ctx, int32(id))
	if err != nil {
		return ChallengePeriod{}, err
	}

	return convertChallengePeriodDbObjIntoDomainObj(challengePeriod), nil
}

func (s *ChallengeService) CreateChallengePeriod(ctx context.Context, firstYear, firstWeek, lastYear, lastWeek int) (ChallengePeriod, error) {
	challengePeriod, err := s.challengeRepo.CreateChallengePeriod(ctx, db.CreateChallengePeriodParams{
		FirstWeek:    int32(firstWeek),
		LastWeek:     int32(lastWeek),
		StartingYear: int32(firstYear),
		EndingYear:   int32(lastYear),
	})
	if err != nil {
		return ChallengePeriod{}, err
	}

	return convertChallengePeriodDbObjIntoDomainObj(challengePeriod), nil
}

func (s *ChallengeService) ListChallengePeriods(ctx context.Context) ([]ChallengePeriod, error) {
	challengePeriods, err := s.challengeRepo.ListChallengePeriods(ctx)
	if err != nil {
		return nil, err
	}
	for i, period := range challengePeriods {
		fmt.Printf("%d -> %#v\n", i, period)
	}

	var domainChallengePeriods []ChallengePeriod
	for _, period := range challengePeriods {
		domainChallengePeriods = append(domainChallengePeriods, convertChallengePeriodDbObjIntoDomainObj(period))
	}

	return domainChallengePeriods, nil
}

func (s *ChallengeService) FindPeriodsByWeek(ctx context.Context, weekNumber int, year int) ([]ChallengePeriod, error) {
	challengePeriods, err := s.challengeRepo.FindPeriodsByDate(ctx, db.FindPeriodsByDateParams{
		FirstWeek:    int32(weekNumber),
		StartingYear: int32(year),
	})
	if err != nil {
		return nil, err
	}

	var domainChallengePeriods []ChallengePeriod
	for _, period := range challengePeriods {
		domainChallengePeriods = append(domainChallengePeriods, convertChallengePeriodDbObjIntoDomainObj(period))
	}

	return domainChallengePeriods, nil
}

type WeeklyTrainingStatsService struct {
	repo WeeklyTrainingStatsRepository
}

func NewWeeklyTrainingStatsService(repo WeeklyTrainingStatsRepository) *WeeklyTrainingStatsService {
	return &WeeklyTrainingStatsService{repo: repo}
}

func convertWeeklyTrainingStatsDbObjIntoDomainObj(stats db.WeeklyTrainingStat) WeeklyTrainingStat {
	return WeeklyTrainingStat{
		WeekNumber:   int(stats.WeekNumber),
		Year:         int(stats.Year),
		Points:       int(stats.Points),
		StravaId:     int(stats.StravaID),
		DistanceSwam: float64(stats.DistanceSwam),
	}
}

func (s *WeeklyTrainingStatsService) CreateWeeklyTrainingStats(ctx context.Context, year, weekNumber, points, stravaId int, distanceSwam float64) (WeeklyTrainingStat, error) {
	stats, err := s.repo.CreateWeeklyTrainingStats(ctx, db.CreateWeeklyTrainingStatsParams{
		Year:         int32(year),
		WeekNumber:   int32(weekNumber),
		Points:       int32(points),
		StravaID:     int32(stravaId),
		DistanceSwam: distanceSwam,
	})
	if err != nil {
		return WeeklyTrainingStat{}, err
	}

	return convertWeeklyTrainingStatsDbObjIntoDomainObj(stats), nil
}

func (s *WeeklyTrainingStatsService) ListWeeklyTrainingForWeek(ctx context.Context, year, weekNumber int) ([]WeeklyTrainingStat, error) {
	stats, err := s.repo.ListWeeklyTrainingForWeek(ctx, db.ListWeeklyTrainingForWeekParams{
		Year:       int32(year),
		WeekNumber: int32(weekNumber),
	})
	if err != nil {
		return nil, err
	}

	var domainStats []WeeklyTrainingStat
	for _, stat := range stats {
		domainStats = append(domainStats, convertWeeklyTrainingStatsDbObjIntoDomainObj(stat))
	}

	return domainStats, nil
}

func (s *WeeklyTrainingStatsService) ListWeeklyTrainingStatsForWeeks(ctx context.Context, year, weekNumber, lastWeekNumber int) ([]WeeklyTrainingStat, error) {
	stats, err := s.repo.ListWeeklyTrainingStatsForWeeks(ctx, db.ListWeeklyTrainingStatsForWeeksParams{
		Year:         int32(year),
		WeekNumber:   int32(weekNumber),
		WeekNumber_2: int32(lastWeekNumber),
	})
	if err != nil {
		return nil, err
	}

	var domainStats []WeeklyTrainingStat
	for _, stat := range stats {
		domainStats = append(domainStats, WeeklyTrainingStat{
			WeekNumber:   int(stat.WeekNumber),
			Year:         int(stat.Year),
			Points:       int(stat.Points),
			StravaId:     int(stat.StravaID),
			AthleteName:  stat.Lastname + " " + stat.Firstname,
			DistanceSwam: stat.DistanceSwam,
		})
	}

	return domainStats, nil
}

type UpdateWeeklyTrainingsStatsWhere struct {
	Year       int
	WeekNumber int
	StravaID   int
}

func (s *WeeklyTrainingStatsService) UpsertWeeklyTrainingStats(ctx context.Context, where UpdateWeeklyTrainingsStatsWhere, newPoints int, distanceSwam float64) (WeeklyTrainingStat, error) {
	stats, err := s.repo.UpsertWeeklyTrainingStats(ctx, db.UpsertWeeklyTrainingStatsParams{
		Year:         int32(where.Year),
		WeekNumber:   int32(where.WeekNumber),
		Points:       int32(newPoints),
		StravaID:     int32(where.StravaID),
		DistanceSwam: distanceSwam,
	})
	if err != nil {
		return WeeklyTrainingStat{}, err
	}

	return convertWeeklyTrainingStatsDbObjIntoDomainObj(stats), nil
}
