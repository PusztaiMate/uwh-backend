package main

import (
	"testing"
	"time"
)

func Test_calculateTrainingStreakForPlayers(t *testing.T) {
	player1Id := 1
	player2Id := 2
	/*
		january 1st - player1 & player2
		january 2nd - only player2
		january 3rd - player1 & player2
	*/
	trainings := []*ExtendedTrainingData{
		{
			BlackTeam: []int{player1Id},
			WhiteTeam: []int{player2Id},
			Training: Training{
				Date: time.Date(2020, time.January, 2, 0, 0, 0, 0, time.UTC),
			},
		},
		{
			BlackTeam: []int{},
			WhiteTeam: []int{player2Id},
			Training: Training{
				Date: time.Date(2020, time.January, 1, 0, 0, 0, 0, time.UTC),
			},
		},
		{
			BlackTeam: []int{},
			WhiteTeam: []int{player2Id},
			Training: Training{
				Date: time.Date(2020, time.January, 3, 0, 0, 0, 0, time.UTC),
			},
		},
		{
			BlackTeam: []int{player1Id},
			WhiteTeam: []int{},
			Training: Training{
				Date: time.Date(2020, time.January, 3, 0, 0, 0, 0, time.UTC),
			},
		},
	}

	streak, err := calculateTrainingStreakForPlayers(trainings, []int{player1Id, player2Id})
	if err != nil {
		t.Error("could not calculate streak for some reason", err)
	}

	lsp1 := 1 // longest streak player 1
	csp1 := 1
	lsp2 := 3
	csp2 := 0

	if streak[player1Id].LongestStreak != lsp1 {
		t.Errorf("expected streak for player1 to be %d, got %d", lsp1, streak[player1Id].LongestStreak)
	}
	if streak[player1Id].CurrentStreak != csp1 {
		t.Errorf("expected streak for player1 to be %d, got %d", csp2, streak[player1Id].CurrentStreak)
	}
	if streak[player2Id].LongestStreak != lsp2 {
		t.Errorf("expected streak for player1 to be %d, got %d", lsp2, streak[player1Id].LongestStreak)
	}
	if streak[player2Id].CurrentStreak != csp2 {
		t.Errorf("expected streak for player1 to be %d, got %d", csp2, streak[player1Id].CurrentStreak)
	}
}
