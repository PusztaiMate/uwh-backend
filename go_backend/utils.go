package main

import (
	"log/slog"
	"math"
	"os"
	"slices"
	"strings"
	"time"
)

func isUHWTraining(activityName string) bool {
	name := strings.ToLower(activityName)
	return (strings.Contains(name, "uwh") ||
		strings.Contains(name, "underwater hockey") ||
		strings.Contains(name, "vízihoki") ||
		strings.Contains(name, "vizihoki") ||
		strings.Contains(name, "uwhockey") ||
		strings.Contains(name, "víz alatti hoki") ||
		strings.Contains(name, "vízalatti hoki"))
}

func isSameDay(t1, t2 time.Time) bool {
	return t1.Year() == t2.Year() && t1.YearDay() == t2.YearDay()
}

func isOnTheSameDay(activities []StravaActivity, a StravaActivity) bool {
	for _, act := range activities {
		if isSameDay(act.StartDate, a.StartDate) {
			return true
		}
	}
	return false
}

func countUHWTrainings(activities []StravaActivity) int {
	notOnSameDay := make([]StravaActivity, 0)
	for _, a1 := range activities {
		if !isUHWTraining(a1.Name) {
			continue
		}
		if !isOnTheSameDay(notOnSameDay, a1) {
			notOnSameDay = append(notOnSameDay, a1)
		}
	}

	return len(notOnSameDay)
}

func getNthWeekOfTheYear(weekNumber int) (time.Time, time.Time) {
	thisYear := time.Now().Year()
	firstMondayOfTheYear := time.Time{}

	// first day of week 1 can be anything from 1st of jan to 4th of jan
	for d := range []int{1, 2, 3, 4} {
		firstDayOfFirstWeek := time.Date(thisYear, time.January, d, 0, 0, 0, 0, time.UTC)
		if _, week := firstDayOfFirstWeek.ISOWeek(); week != 1 {
			continue
		}
		firstMondayOfTheYear = firstDayOfFirstWeek
		break
	}

	mondayOfWeekN := firstMondayOfTheYear.AddDate(0, 0, (weekNumber-1)*7)
	nextMonday := mondayOfWeekN.AddDate(0, 0, 7)

	return mondayOfWeekN, nextMonday
}

func getSecretKey() string {
	return os.Getenv("SECRET_KEY")
}

func vizalattihokiDomain() string {
	domainUrl := os.Getenv("DOMAIN_URL")
	if domainUrl == "" {
		slog.Warn("missing domain url env variable (DOMAIN_URL)")
	}
	return domainUrl
}

func clientId() string {
	return "60914"
}

func extendReportsWithPoints(weeklyReports []WeeklyActivityReports) []WeeklyActivityReports {
	slices.SortFunc(weeklyReports, func(a, b WeeklyActivityReports) int {
		return b.Report.ModifiedTime - a.Report.ModifiedTime
	})

	for i := 0; i < len(weeklyReports); i++ {
		report := &weeklyReports[i]
		if report.Report.ModifiedTime == 0 {
			continue
		}

		if i < 3 { // first 3 players get 5 - 4 - 3 points
			report.Points += 5 - i
		} else if report.Report.ModifiedTime > 8*60*60 { // any player who has more than 8 hours of training gets 2 points
			report.Points += 2
		}

		for i := range report.Report.UWHTrainings {
			if i < 2 {
				report.Points += 2
			} else {
				report.Points += 1
			}
		}
	}

	return weeklyReports
}

func almostEqual(a, b float64, epsilon float64) bool {
	abs := math.Abs(a - b)
	return abs < epsilon
}
