package main

import (
	"context"

	"gitlab.com/PusztaiMate/uwh-backend/db"
)

type Player struct {
	Id                int     `json:"id"`
	Fname             string  `json:"fname"`
	Lname             string  `json:"lname"`
	ClubId            int     `json:"club_id"`
	UserId            int     `json:"user_id"`
	TrainingStrength  float64 `json:"training_strength"`
	PrimaryPosition   int     `json:"primary_position"`
	SecondaryPosition int     `json:"secondary_position"`
	Status            string  `json:"status"`
}

type PlayerRepository interface {
	UpdatePlayer(ctx context.Context, arg db.UpdatePlayerParams) error
	GetActivePlayers(ctx context.Context) ([]db.Player, error)
}

type PlayerService struct {
	db PlayerRepository
}

func NewPlayerService(db PlayerRepository) *PlayerService {
	return &PlayerService{
		db: db,
	}
}

func (s *PlayerService) UpdatePlayer(ctx context.Context, args db.UpdatePlayerParams) error {
	return s.db.UpdatePlayer(ctx, args)
}

func convertDbPlayerToPlayer(p db.Player) Player {
	return Player{
		Id:                int(p.ID),
		Fname:             p.Fname,
		Lname:             p.Lname,
		ClubId:            int(p.ClubID.Int32),
		UserId:            int(p.UserID.Int32),
		TrainingStrength:  p.TrainingStrength.Float64,
		PrimaryPosition:   int(p.PrimaryPosition.Int32),
		SecondaryPosition: int(p.SecondaryPosition.Int32),
		Status:            p.Status,
	}
}

func (s *PlayerService) GetActivePlayers(ctx context.Context) ([]Player, error) {
	players, err := s.db.GetActivePlayers(ctx)
	if err != nil {
		return nil, err
	}

	result := make([]Player, len(players))
	for i := 0; i < len(players); i++ {
		result[i] = convertDbPlayerToPlayer(players[i])
	}

	return result, nil
}
