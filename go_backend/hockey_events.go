package main

import (
	"context"
	"time"

	"github.com/jackc/pgx/v5/pgtype"
	"gitlab.com/PusztaiMate/uwh-backend/db"
)

type HockeyEvent struct {
	ID          int       `json:"id"`
	Date        time.Time `json:"date"`
	Title       string    `json:"title"`
	Type        string    `json:"type"`
	Description string    `json:"description"`
}

type HockeyEventsRepository interface {
	ListHockeyEvents(ctx context.Context) ([]db.HockeyEvent, error)
	CreateHockeyEvent(ctx context.Context, arg db.CreateHockeyEventParams) (db.HockeyEvent, error)
	GetHockeyEvent(ctx context.Context, id int32) (db.HockeyEvent, error)
	DeleteHockeyEvent(ctx context.Context, id int32) error
	UpdateHockeyEvent(ctx context.Context, arg db.UpdateHockeyEventParams) error
}

type HockeyEventsService struct {
	db HockeyEventsRepository
}

func NewHockeyEventsService(db HockeyEventsRepository) *HockeyEventsService {
	return &HockeyEventsService{db: db}
}

func convertDbEntityIntoDomainEntity(dbEvent db.HockeyEvent) HockeyEvent {
	return HockeyEvent{
		ID:          int(dbEvent.ID),
		Date:        dbEvent.Date.Time,
		Title:       dbEvent.Title,
		Type:        dbEvent.Type,
		Description: dbEvent.Description.String,
	}
}

func (s *HockeyEventsService) ListHockeyEvents(ctx context.Context) ([]HockeyEvent, error) {
	events, err := s.db.ListHockeyEvents(ctx)
	if err != nil {
		return nil, err
	}

	var domainEvents []HockeyEvent
	for _, event := range events {
		domainEvents = append(domainEvents, convertDbEntityIntoDomainEntity(event))
	}

	return domainEvents, nil
}

func (s *HockeyEventsService) CreateHockeyEvent(ctx context.Context, event HockeyEvent) (*HockeyEvent, error) {
	newEvent, err := s.db.CreateHockeyEvent(ctx, db.CreateHockeyEventParams{
		Date:        pgtype.Timestamp{Time: event.Date, Valid: true},
		Title:       event.Title,
		Type:        event.Type,
		Description: pgtype.Text{String: event.Description, Valid: true},
	})
	if err != nil {
		return nil, err
	}

	converted := convertDbEntityIntoDomainEntity(newEvent)

	return &converted, nil
}

func (s *HockeyEventsService) GetHockeyEvent(ctx context.Context, eventId int) (*HockeyEvent, error) {
	event, err := s.db.GetHockeyEvent(ctx, int32(eventId))
	if err != nil {
		return nil, err
	}

	converted := convertDbEntityIntoDomainEntity(event)
	return &converted, nil
}

func (s *HockeyEventsService) Delete(ctx context.Context, eventId int) error {
	return s.db.DeleteHockeyEvent(ctx, int32(eventId))
}

func (s *HockeyEventsService) Update(ctx context.Context, eventId int, event HockeyEvent) error {
	return s.db.UpdateHockeyEvent(ctx, db.UpdateHockeyEventParams{
		ID:          int32(eventId),
		Date:        pgtype.Timestamp{Time: event.Date, Valid: true},
		Title:       event.Title,
		Type:        event.Type,
		Description: pgtype.Text{String: event.Description, Valid: true},
	})
}
