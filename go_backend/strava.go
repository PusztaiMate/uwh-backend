package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log/slog"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"
	"gitlab.com/PusztaiMate/uwh-backend/db"
	"gitlab.com/PusztaiMate/uwh-backend/strava-client/models"
	"gitlab.com/PusztaiMate/uwh-backend/strava-client/strava_client"
	"gitlab.com/PusztaiMate/uwh-backend/strava-client/strava_client/activities"
)

const (
	cacheTime = 15 * time.Minute
)

func isNotFoundDbError(err error) bool {
	return strings.Contains(sql.ErrNoRows.Error(), err.Error())
}

type Athlete struct {
	Firstname string  `json:"firstname"`
	ID        float64 `json:"id"`
	Lastname  string  `json:"lastname"`
	Profile   string  `json:"profile"`
	Username  string  `json:"username"`
}

type OAuthResponse struct {
	AccessToken  string  `json:"access_token"`
	Athlete      Athlete `json:"athlete"`
	ExpiresAt    int64   `json:"expires_at"`
	ExpiresIn    int     `json:"expires_in"`
	RefreshToken string  `json:"refresh_token"`
	TokenType    string  `json:"token_type"`
}

type StravaAuthData struct {
	ID           int    `json:"id"`
	StravaID     int    `json:"strava_id"`
	Firstname    string `json:"firstname"`
	Lastname     string `json:"lastname"`
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	ExpiresAt    int    `json:"expires_at"`
}

type StravaAuthRepository interface {
	ListStravaAuths(ctx context.Context) ([]db.StravaOauthDatum, error)
	CreateStravaOauthData(ctx context.Context, arg db.CreateStravaOauthDataParams) (db.StravaOauthDatum, error)
	GetStravaOauthData(ctx context.Context, id int32) (db.StravaOauthDatum, error)
	FindByStravaId(ctx context.Context, stravaId int32) (db.StravaOauthDatum, error)
	UpdateStravaOauthData(ctx context.Context, arg db.UpdateStravaOauthDataParams) (db.StravaOauthDatum, error)
	UpsertStravaOauthData(ctx context.Context, arg db.UpsertStravaOauthDataParams) (db.StravaOauthDatum, error)
}

type StravaAuthService struct {
	db StravaAuthRepository
}

func NewStravaAuthService(db StravaAuthRepository) *StravaAuthService {
	return &StravaAuthService{db: db}
}

func stravaAuthDbToDomainEntity(dbAuth db.StravaOauthDatum) StravaAuthData {
	return StravaAuthData{
		ID:           int(dbAuth.ID),
		StravaID:     int(dbAuth.StravaID),
		Firstname:    dbAuth.Firstname,
		Lastname:     dbAuth.Lastname,
		AccessToken:  dbAuth.AccessToken,
		RefreshToken: dbAuth.RefreshToken,
		ExpiresAt:    int(dbAuth.ExpiresAt),
	}
}

func (s *StravaAuthService) ListStravaAuthData(ctx context.Context) ([]StravaAuthData, error) {
	authData, err := s.db.ListStravaAuths(ctx)
	if err != nil {
		return nil, err
	}

	var domainAuthData []StravaAuthData
	for _, data := range authData {
		domainAuthData = append(domainAuthData, stravaAuthDbToDomainEntity(data))
	}

	return domainAuthData, nil
}

func (s *StravaAuthService) CreateStravaAuth(ctx context.Context, authData StravaAuthData) (*StravaAuthData, error) {
	newAuthData, err := s.db.CreateStravaOauthData(ctx, db.CreateStravaOauthDataParams{
		StravaID:     int32(authData.StravaID),
		Firstname:    authData.Firstname,
		Lastname:     authData.Lastname,
		AccessToken:  authData.AccessToken,
		RefreshToken: authData.RefreshToken,
		ExpiresAt:    int32(authData.ExpiresAt),
	})
	if err != nil {
		return nil, err
	}

	converted := stravaAuthDbToDomainEntity(newAuthData)

	return &converted, nil
}

func (s *StravaAuthService) GetStravaAuth(ctx context.Context, authId int) (*StravaAuthData, error) {
	authData, err := s.db.GetStravaOauthData(ctx, int32(authId))
	if err != nil {
		return nil, err
	}

	converted := stravaAuthDbToDomainEntity(authData)

	return &converted, nil
}

func (s *StravaAuthService) FindByStravaId(ctx context.Context, id int) (*StravaAuthData, error) {
	authData, err := s.db.FindByStravaId(ctx, int32(id))
	if err != nil {
		return nil, err
	}

	converted := stravaAuthDbToDomainEntity(authData)

	return &converted, nil
}

func (s *StravaAuthService) UpdateStravaOauthData(ctx context.Context, authData StravaAuthData) (*StravaAuthData, error) {
	updatedAuthData, err := s.db.UpdateStravaOauthData(ctx, db.UpdateStravaOauthDataParams{
		ID:           int32(authData.ID),
		AccessToken:  authData.AccessToken,
		RefreshToken: authData.RefreshToken,
		ExpiresAt:    int32(authData.ExpiresAt),
	})
	if err != nil {
		return nil, err
	}

	converted := stravaAuthDbToDomainEntity(updatedAuthData)

	return &converted, nil
}

func (s *StravaAuthService) UpsertStravaOauthData(ctx context.Context, authData StravaAuthData) (*StravaAuthData, error) {
	upsertedAuthData, err := s.db.UpsertStravaOauthData(ctx, db.UpsertStravaOauthDataParams{
		StravaID:     int32(authData.StravaID),
		Firstname:    authData.Firstname,
		Lastname:     authData.Lastname,
		AccessToken:  authData.AccessToken,
		RefreshToken: authData.RefreshToken,
		ExpiresAt:    int32(authData.ExpiresAt),
	})
	if err != nil {
		return nil, err
	}

	converted := stravaAuthDbToDomainEntity(upsertedAuthData)
	return &converted, nil
}

func sendStravaAuthRequest(clientId string, clientSecret string, code string, grantType string) (*http.Response, error) {
	stravaAuthUrl := "https://www.strava.com/oauth/token"
	data := url.Values{}
	data.Set("client_id", clientId)
	data.Set("client_secret", clientSecret)
	if grantType == "refresh_token" {
		data.Set("refresh_token", code)
	} else if grantType == "authorization_code" {
		data.Set("code", code)
	}
	data.Set("grant_type", grantType)
	req, err := http.NewRequest("POST", stravaAuthUrl, strings.NewReader(data.Encode()))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	if err != nil {
		slog.Error("could not create http request", "err", err)
		return nil, fmt.Errorf("could not create http request")
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		slog.Error("could not post to strava oauth endpoint", "err", err)
		return nil, fmt.Errorf("could not post to Strava oauth endpoint")
	}
	return resp, nil
}

func (s *StravaAuthService) refreshToken(_ context.Context, authData StravaAuthData) (*OAuthResponse, error) {
	clientSecret := os.Getenv("STRAVA_APP_CLIENT_SECRET")
	if clientSecret == "" {
		slog.Error("could not find client secret (missing env var?)")
		return nil, fmt.Errorf("could not find client secret")
	}
	resp, err := sendStravaAuthRequest(clientId(), clientSecret, authData.RefreshToken, "refresh_token")
	if err != nil {
		slog.Error("could not post to strava oauth endpoint", "err", err)
		return nil, fmt.Errorf("could not post to Strava oauth endpoint")
	}
	defer resp.Body.Close()

	var oathResponse OAuthResponse
	err = json.NewDecoder(resp.Body).Decode(&oathResponse)
	if err != nil {
		slog.Error("could not decode strava oauth response", "err", err)
		return nil, fmt.Errorf("could not decode Strava auth response")
	}

	return &oathResponse, nil
}

func (s *StravaAuthService) RefreshTokenAndSaveIntoDb(ctx context.Context, authData StravaAuthData) (*StravaAuthData, error) {
	oathResponse, err := s.refreshToken(ctx, authData)
	if err != nil {
		return nil, err
	}

	updatedStravaAuthData, err := s.UpdateStravaOauthData(ctx, StravaAuthData{
		ID:           authData.ID,
		StravaID:     authData.StravaID,
		Firstname:    authData.Firstname,
		Lastname:     authData.Lastname,
		AccessToken:  oathResponse.AccessToken,
		RefreshToken: oathResponse.RefreshToken,
		ExpiresAt:    int(oathResponse.ExpiresAt),
	})
	if err != nil {
		slog.Error("could not upsert strava oauth data", "err", err)
		return nil, fmt.Errorf("could not upsert Strava oauth data")
	}

	return updatedStravaAuthData, nil
}

func (s *StravaAuthService) refreshTokenIfExpiredAndSaveIntoDb(ctx context.Context, auth StravaAuthData) (*StravaAuthData, error) {
	validAuth := &auth
	unixNow := time.Now().Unix()
	// if the access token is expired, refresh it
	if unixNow > int64(auth.ExpiresAt) {
		refreshedAuthData, err := s.RefreshTokenAndSaveIntoDb(ctx, auth)
		if err != nil {
			return nil, err
		}
		validAuth = refreshedAuthData
	}
	return validAuth, nil
}

type athleteTimeParams struct {
	before int64
	after  int64
}

type activityCacheKey struct {
	stravaId    int
	queryParams athleteTimeParams
}

type activityCacheValue struct {
	lastRefresh time.Time
	data        *activities.GetLoggedInAthleteActivitiesOK
}

type activityCache struct {
	client      *strava_client.StravaAPIV3
	data        map[activityCacheKey]activityCacheValue
	cachingTime time.Duration
}

func newActivityCache(client *strava_client.StravaAPIV3, cachingTime time.Duration) *activityCache {
	data := make(map[activityCacheKey]activityCacheValue)
	return &activityCache{
		client:      client,
		data:        data,
		cachingTime: cachingTime,
	}
}

func makeActivityCachekey(stravaId int, queryParams activities.GetLoggedInAthleteActivitiesParams) (activityCacheKey, error) {
	if queryParams.After == nil || queryParams.Before == nil {
		return activityCacheKey{}, fmt.Errorf("'params.After or params.Before not set, can't create query key")
	}

	return activityCacheKey{
		stravaId: stravaId,
		queryParams: athleteTimeParams{
			before: *queryParams.Before,
			after:  *queryParams.After,
		},
	}, nil
}

func (ac *activityCache) getActivitiesForAthlete(
	_ context.Context,
	accessToken string,
	stravaId int,
	params activities.GetLoggedInAthleteActivitiesParams,
) ([]*models.SummaryActivity, error) {
	k, errMakingCacheKey := makeActivityCachekey(stravaId, params)
	if errMakingCacheKey != nil {
		slog.Warn("could not create activity cache key", "err", errMakingCacheKey)
	}

	now := time.Now()
	eightHoursAgo := now.Add(-ac.cachingTime)
	if value, ok := ac.data[k]; ok && errMakingCacheKey == nil {
		if value.lastRefresh.After(eightHoursAgo) {
			return value.data.Payload, nil
		}
	}

	activities, err := ac.client.Activities.GetLoggedInAthleteActivities(
		&params,
		runtime.ClientAuthInfoWriterFunc(func(cr runtime.ClientRequest, r strfmt.Registry) error {
			cr.SetHeaderParam("Authorization", "Bearer "+accessToken)
			return nil
		}),
		func(co *runtime.ClientOperation) {
		},
	)
	if err != nil {
		return nil, err
	}

	ac.data[k] = activityCacheValue{
		lastRefresh: now,
		data:        activities,
	}

	return activities.Payload, nil
}

type StravaTrainingPointsCalculator struct {
	client        *strava_client.StravaAPIV3
	activityCache *activityCache
}

func NewStravaTrainingPointsCalculator() *StravaTrainingPointsCalculator {
	client := strava_client.NewHTTPClient(strfmt.Default)
	calculator := &StravaTrainingPointsCalculator{
		client:        client,
		activityCache: newActivityCache(client, cacheTime),
	}
	return calculator
}

// TODO: move it to StravaActivityFetcher
func (c *StravaTrainingPointsCalculator) GetActivitiesForAthlete(
	ctx context.Context,
	oathData StravaAuthData,
	params activities.GetLoggedInAthleteActivitiesParams,
) ([]*models.SummaryActivity, error) {
	return c.activityCache.getActivitiesForAthlete(ctx, oathData.AccessToken, oathData.StravaID, params)
}

func sportRates() map[string]float64 {
	return map[string]float64{
		"run":  1.0,
		"ride": 1.0,
		"walk": 0.8,
		"swim": 1.0,
		"hike": 0.9,
		"yoga": 0.8,
	}
}

func createOathDataLookupTable(auths []StravaAuthData) map[int]StravaAuthData {
	m := make(map[int]StravaAuthData)
	for _, d := range auths {
		m[d.StravaID] = d
	}

	return m
}

func doesActivityCountsTowardsChallenge(a StravaActivity) bool {
	return a.ExcludedFromChallenges == ""
}

func createReportFromActivities(activities []StravaActivity) WeeklyActivityReport {
	modifiers := sportRates()
	activityReport := WeeklyActivityReport{}
	for _, a := range activities {
		// filter out activities, that are not part of the challenge
		if !doesActivityCountsTowardsChallenge(a) {
			continue
		}

		movingTime := float64(a.MovingTime)
		switch a.SportType {
		case models.ActivityTypeRun:
			activityReport.ModifiedTime += int(movingTime * modifiers["run"])
		case models.ActivityTypeRide:
			activityReport.ModifiedTime += int(movingTime * modifiers["ride"])
		case models.ActivityTypeWalk:
			activityReport.ModifiedTime += int(movingTime * modifiers["walk"])
		case models.ActivityTypeSwim:
			activityReport.ModifiedTime += int(movingTime * modifiers["swim"])
			activityReport.DistanceSwam += a.Distance
		case models.ActivityTypeHike:
			activityReport.ModifiedTime += int(movingTime * modifiers["hike"])
		case models.ActivityTypeYoga:
			activityReport.ModifiedTime += int(movingTime * modifiers["yoga"])
		default:
			activityReport.ModifiedTime += int(movingTime)
		}
		activityReport.UWHTrainings = countUHWTrainings(activities)
	}
	return activityReport
}

func calculateWeeklyActivityReports(actsMap map[int][]StravaActivity, auths []StravaAuthData) []WeeklyActivityReports {
	authLookupTable := createOathDataLookupTable(auths)
	var activityReports []WeeklyActivityReports
	for athleteId, activities := range actsMap {
		activityReport := createReportFromActivities(activities)
		athleteAuthData := authLookupTable[athleteId]
		weeklyReport := WeeklyActivityReports{
			AthleteName:      fmt.Sprintf("%s %s", athleteAuthData.Lastname, athleteAuthData.Firstname),
			AtheleteStravaId: athleteId,
			Report:           activityReport,
		}
		activityReports = append(activityReports, weeklyReport)
	}
	return activityReports
}
