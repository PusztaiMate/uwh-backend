package main

import (
	"context"
	"fmt"
	"log/slog"
	"strings"
	"time"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"
	"github.com/jackc/pgx/v5/pgtype"
	"gitlab.com/PusztaiMate/uwh-backend/db"
	"gitlab.com/PusztaiMate/uwh-backend/strava-client/models"
	"gitlab.com/PusztaiMate/uwh-backend/strava-client/strava_client"
	"gitlab.com/PusztaiMate/uwh-backend/strava-client/strava_client/activities"
)

type StravaActivityRepository interface {
	GetStravaActivityByStravaId(ctx context.Context, stravaID int64) (db.StravaActivity, error)
	GetStravaActivitiesByStravaIds(ctx context.Context, ids []int64) ([]db.StravaActivity, error)
	CreateStravaActivity(ctx context.Context, arg db.CreateStravaActivityParams) (db.StravaActivity, error)
	GetStravaActivitiesBetweenDates(ctx context.Context, arg db.GetStravaActivitiesBetweenDatesParams) ([]db.StravaActivity, error)
	GetStravaActivitiesForPeriodByStravaId(ctx context.Context, arg db.GetStravaActivitiesForPeriodByStravaIdParams) ([]db.StravaActivity, error)
	UpdateStravaActivityExcludedList(ctx context.Context, arg db.UpdateStravaActivityExcludedListParams) (db.StravaActivity, error)
	DeleteStravaActivity(ctx context.Context, stravaID int64) error
	UpsertStravaActivity(ctx context.Context, arg db.UpsertStravaActivityParams) (db.StravaActivity, error)
}

type StravaActivityFetcher struct {
	activityService *StravaActivityService
	authService     *StravaAuthService
	stravaClient    *strava_client.StravaAPIV3
	activityCache   *activityCache
}

func NewStravaActivityFetcherService(
	activityService *StravaActivityService,
	authService *StravaAuthService,
	stravaClient *strava_client.StravaAPIV3,
	activityCache *activityCache,
) *StravaActivityFetcher {
	return &StravaActivityFetcher{
		activityService: activityService,
		authService:     authService,
		stravaClient:    stravaClient,
		activityCache:   activityCache,
	}
}

// fetchActivity assumes a valid access token
func (s *StravaActivityFetcher) fetchActivity(ctx context.Context, activityId int64, accessToken string) (*models.DetailedActivity, error) {
	includeAllEfforts := false
	params := &activities.GetActivityByIDParams{
		ID:                int64(activityId),
		Context:           ctx,
		IncludeAllEfforts: &includeAllEfforts,
	}
	data, err := s.stravaClient.Activities.GetActivityByID(params,
		runtime.ClientAuthInfoWriterFunc(func(cr runtime.ClientRequest, r strfmt.Registry) error {
			cr.SetHeaderParam("Authorization", "Bearer "+accessToken)
			return nil
		}),
		func(co *runtime.ClientOperation) {
		},
	)
	if err != nil {
		return nil, err
	}

	return data.Payload, nil
}

func shouldExcludeActivityFromChallenge(act *models.DetailedActivity) bool {
	return strings.Contains(strings.ToLower(act.Description), "vk-")
}

func (s *StravaActivityFetcher) upsertActivityIntoDb(ctx context.Context, act *models.DetailedActivity) error {
	var excl string
	if shouldExcludeActivityFromChallenge(act) {
		excl = "1"
	}
	_, err := s.activityService.UpsertStravaActivity(ctx, db.UpsertStravaActivityParams{
		StravaAthleteID:        int32(act.Athlete.ID),
		SportType:              string(act.SportType),
		Name:                   act.Name,
		Description:            pgtype.Text{Valid: true, String: act.Description},
		MovingTime:             int32(act.MovingTime),
		StravaUploadID:         act.ID,
		StartDate:              pgtype.Timestamp{Time: time.Time(act.StartDate), Valid: true},
		StravaID:               act.MetaActivity.ID,
		LastFetchedAt:          pgtype.Timestamp{Time: time.Now(), Valid: true},
		ExcludedFromChallenges: pgtype.Text{String: excl, Valid: true},
		Distance:               float64(act.Distance),
	})
	return err
}

// fetchActivityAndUpserIntoDb assumes that the access token is valid
func (s *StravaActivityFetcher) fetchActivityAndUpsertIntoDb(ctx context.Context, activityId int64, accessToken string) error {
	a, err := s.fetchActivity(ctx, activityId, accessToken)
	if err != nil {
		return err
	}

	return s.upsertActivityIntoDb(ctx, a)
}

func (s *StravaActivityFetcher) getActivitiesForAthlete(
	ctx context.Context,
	oathData StravaAuthData,
	params activities.GetLoggedInAthleteActivitiesParams,
) ([]*models.SummaryActivity, error) {
	return s.activityCache.getActivitiesForAthlete(ctx, oathData.AccessToken, oathData.StravaID, params)
}

func (s *StravaActivityFetcher) fetchActivitiesForTimePeriodAndSaveIntoDbIfNotPresent(ctx context.Context, periodStart, periodEnd time.Time) error {
	// query db for auth info
	authDatas, err := s.authService.ListStravaAuthData(ctx)
	if err != nil {
		return err
	}

	// for every athlete
	for _, auth := range authDatas {
		// - query the activities in time period
		auth, err := s.authService.refreshTokenIfExpiredAndSaveIntoDb(ctx, auth)
		if err != nil {
			return err
		}
		if auth == nil {
			return fmt.Errorf("auth data is not found, should not be possible")
		}

		params := createActivityQueryParams(periodStart, periodEnd)
		activitySummaries, err := s.getActivitiesForAthlete(ctx, *auth, params)
		if err != nil {
			slog.Error("error getting activities for athelete", "err", err, "auth", auth, "params", params)
			continue
		}

		slog.Info("iteration through activity summaries", "len", len(activitySummaries))
		for _, summary := range activitySummaries {
			activityId := summary.MetaActivity.ID
			activity, err := s.activityService.GetStravaActivityByStravaId(ctx, activityId)
			if err != nil && !isNotFoundDbError(err) {
				return err
			}

			// if the name and distance is the same as before we assume that
			if activity.Id != 0 && activity.Name == summary.Name && almostEqual(activity.Distance, float64(summary.Distance), 0.01) {
				slog.Info("activity in db and it has not changed", "id", activityId)
				continue
			}

			// either not in db (insert) or there but name changed (update)
			err = s.fetchActivityAndUpsertIntoDb(ctx, activityId, auth.AccessToken)
			if err != nil {
				return fmt.Errorf("could not fetch activity or save it into the db: %w", err)
			}
		}
	}

	return nil
}

type StravaActivity struct {
	Id                     int                 `json:"id"`
	StravaId               int                 `json:"strava_id"`
	SportType              models.ActivityType `json:"sport_type"`
	MovingTime             int                 `json:"moving_time"`
	StravaUploadId         int                 `json:"strava_upload_id"`
	StartDate              time.Time           `json:"start_date"`
	Name                   string              `json:"name"`
	Description            string              `json:"description"`
	StravaAthleteId        int                 `json:"strava_athlete_id"`
	LastFetchedAt          time.Time           `json:"last_fetched_at"`
	ExcludedFromChallenges string              `json:"excluded_from_challenges"`
	Distance               float64             `json:"distance"`
}

type StravaActivityService struct {
	db StravaActivityRepository
}

func NewStravaActivityService(db StravaActivityRepository) *StravaActivityService {
	return &StravaActivityService{
		db: db,
	}
}

func convertDbStravaActivityToDomainObj(dsa db.StravaActivity) StravaActivity {
	return StravaActivity{
		Id:                     int(dsa.ID),
		StravaId:               int(dsa.StravaID),
		SportType:              models.ActivityType(dsa.SportType),
		MovingTime:             int(dsa.MovingTime),
		StravaUploadId:         int(dsa.StravaUploadID),
		StartDate:              dsa.StartDate.Time,
		Name:                   dsa.Name,
		Description:            dsa.Description.String,
		StravaAthleteId:        int(dsa.StravaAthleteID),
		LastFetchedAt:          dsa.LastFetchedAt.Time,
		ExcludedFromChallenges: dsa.ExcludedFromChallenges.String,
		Distance:               dsa.Distance,
	}
}

func (s *StravaActivityService) GetStravaActivitiesByStravaIds(ctx context.Context, ids []int64) ([]StravaActivity, error) {
	activities, err := s.db.GetStravaActivitiesByStravaIds(ctx, ids)
	if err != nil {
		return nil, err
	}

	result := make([]StravaActivity, len(activities))
	for _, a := range activities {
		result = append(result, convertDbStravaActivityToDomainObj(a))
	}

	return result, nil
}

func (s *StravaActivityService) UpsertStravaActivity(ctx context.Context, arg db.UpsertStravaActivityParams) (StravaActivity, error) {
	var result StravaActivity
	a, err := s.db.UpsertStravaActivity(ctx, arg)
	if err != nil {
		return result, err
	}
	return convertDbStravaActivityToDomainObj(a), nil
}

func (s *StravaActivityService) GetStravaActivitiesForPeriodByStravaId(ctx context.Context, stravaId int64, periodStart, periodEnd time.Time) ([]StravaActivity, error) {
	activities, err := s.db.GetStravaActivitiesForPeriodByStravaId(ctx, db.GetStravaActivitiesForPeriodByStravaIdParams{
		PeriodStart:     pgtype.Timestamp{Time: periodStart, Valid: true},
		PeriodEnd:       pgtype.Timestamp{Time: periodEnd, Valid: true},
		StravaAthleteID: int32(stravaId),
	})
	if err != nil {
		return nil, err
	}

	result := make([]StravaActivity, len(activities))
	for i, a := range activities {
		result[i] = convertDbStravaActivityToDomainObj(a)
	}

	return result, nil
}

func (s *StravaActivityService) GetStravaActivityByStravaId(ctx context.Context, stravaID int64) (StravaActivity, error) {
	var result StravaActivity
	a, err := s.db.GetStravaActivityByStravaId(ctx, stravaID)
	if err != nil {
		return result, err
	}
	return convertDbStravaActivityToDomainObj(a), nil
}

func (s *StravaActivityService) SaveFetchedActivityToDb(ctx context.Context, act *models.DetailedActivity) (*StravaActivity, error) {
	// not found in db, let's create it
	var excl string
	if shouldExcludeActivityFromChallenge(act) {
		excl = "1"
	}
	recreated, err := s.CreateStravaActivity(ctx, db.CreateStravaActivityParams{
		StravaAthleteID:        int32(act.Athlete.ID),
		SportType:              string(act.SportType),
		Name:                   act.Name,
		Description:            pgtype.Text{Valid: true, String: act.Description},
		MovingTime:             int32(act.MovingTime),
		StravaUploadID:         act.ID,
		StartDate:              pgtype.Timestamp{Time: time.Time(act.StartDate), Valid: true},
		StravaID:               act.MetaActivity.ID,
		LastFetchedAt:          pgtype.Timestamp{Time: time.Now(), Valid: true},
		ExcludedFromChallenges: pgtype.Text{String: excl, Valid: true},
		Distance:               float64(act.Distance),
	})
	if err != nil {
		return nil, err
	}
	return &recreated, nil

}

func (s *StravaActivityService) CreateStravaActivity(ctx context.Context, args db.CreateStravaActivityParams) (StravaActivity, error) {
	var result StravaActivity
	a, err := s.db.CreateStravaActivity(ctx, args)
	if err != nil {
		return result, err
	}
	return convertDbStravaActivityToDomainObj(a), nil
}

func (s *StravaActivityService) GetStravaActivitiesBetweenDates(ctx context.Context, arg db.GetStravaActivitiesBetweenDatesParams) ([]StravaActivity, error) {
	activities, err := s.db.GetStravaActivitiesBetweenDates(ctx, arg)
	if err != nil {
		return nil, err
	}

	result := make([]StravaActivity, len(activities))
	for i, a := range activities {
		result[i] = convertDbStravaActivityToDomainObj(a)
	}

	return result, nil
}

func (s *StravaActivityService) UpdateStravaActivityExcludedList(ctx context.Context, arg db.UpdateStravaActivityExcludedListParams) (StravaActivity, error) {
	var result StravaActivity
	a, err := s.db.UpdateStravaActivityExcludedList(ctx, arg)
	if err != nil {
		return result, err
	}

	return convertDbStravaActivityToDomainObj(a), nil
}

func (s *StravaActivityService) DeleteStravaActivity(ctx context.Context, stravaID int64) error {
	return s.db.DeleteStravaActivity(ctx, stravaID)
}
