--
-- PostgreSQL database dump
--

-- Dumped from database version 14.7
-- Dumped by pg_dump version 14.11 (Ubuntu 14.11-0ubuntu0.22.04.1)
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;
SET default_tablespace = '';
SET default_table_access_method = heap;
--
-- Name: alembic_version; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.alembic_version (version_num character varying(32) NOT NULL);
ALTER TABLE public.alembic_version OWNER TO postgres;
--
-- Name: casbin_rule; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.casbin_rule (
    id integer NOT NULL,
    ptype character varying(255),
    v0 character varying(255),
    v1 character varying(255),
    v2 character varying(255),
    v3 character varying(255),
    v4 character varying(255),
    v5 character varying(255)
);
ALTER TABLE public.casbin_rule OWNER TO postgres;
--
-- Name: casbin_rule_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.casbin_rule_id_seq AS integer START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
ALTER TABLE public.casbin_rule_id_seq OWNER TO postgres;
--
-- Name: casbin_rule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.casbin_rule_id_seq OWNED BY public.casbin_rule.id;
--
-- Name: clip_tags; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clip_tags (
    id integer NOT NULL,
    name character varying(128) NOT NULL
);
ALTER TABLE public.clip_tags OWNER TO postgres;
--
-- Name: clip_tags_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clip_tags_id_seq AS integer START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
ALTER TABLE public.clip_tags_id_seq OWNER TO postgres;
--
-- Name: clip_tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clip_tags_id_seq OWNED BY public.clip_tags.id;
--
-- Name: clips; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clips (
    id integer NOT NULL,
    video_url character varying(256) NOT NULL,
    start_ts character varying(32) NOT NULL,
    end_ts character varying(32) NOT NULL
);
ALTER TABLE public.clips OWNER TO postgres;
--
-- Name: clips_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clips_id_seq AS integer START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
ALTER TABLE public.clips_id_seq OWNER TO postgres;
--
-- Name: clips_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clips_id_seq OWNED BY public.clips.id;
--
-- Name: clips_to_tags; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clips_to_tags (clip_id integer, tag_id integer);
ALTER TABLE public.clips_to_tags OWNER TO postgres;
--
-- Name: clubs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clubs (
    id integer NOT NULL,
    name character varying(128) NOT NULL
);
ALTER TABLE public.clubs OWNER TO postgres;
--
-- Name: clubs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clubs_id_seq AS integer START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
ALTER TABLE public.clubs_id_seq OWNER TO postgres;
--
-- Name: clubs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clubs_id_seq OWNED BY public.clubs.id;
--
-- Name: hockey_events; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.hockey_events (
    id integer NOT NULL,
    date timestamp without time zone NOT NULL,
    title character varying NOT NULL,
    type character varying NOT NULL,
    description text
);
ALTER TABLE public.hockey_events OWNER TO postgres;
--
-- Name: hockey_events_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.hockey_events_id_seq AS integer START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
ALTER TABLE public.hockey_events_id_seq OWNER TO postgres;
--
-- Name: hockey_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.hockey_events_id_seq OWNED BY public.hockey_events.id;
--
-- Name: images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.images (
    id integer NOT NULL,
    description character varying(256),
    tags character varying(256),
    uuid uuid,
    orig_extension character varying(10)
);
ALTER TABLE public.images OWNER TO postgres;
--
-- Name: images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.images_id_seq AS integer START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
ALTER TABLE public.images_id_seq OWNER TO postgres;
--
-- Name: images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.images_id_seq OWNED BY public.images.id;
--
-- Name: monthly_pay_obligations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.monthly_pay_obligations (
    id integer NOT NULL,
    player_id integer,
    month date,
    is_active boolean
);
ALTER TABLE public.monthly_pay_obligations OWNER TO postgres;
--
-- Name: monthly_pay_obligations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.monthly_pay_obligations_id_seq AS integer START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
ALTER TABLE public.monthly_pay_obligations_id_seq OWNER TO postgres;
--
-- Name: monthly_pay_obligations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.monthly_pay_obligations_id_seq OWNED BY public.monthly_pay_obligations.id;
--
-- Name: notification_subscriptions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notification_subscriptions (
    id integer NOT NULL,
    endpoint character varying(2048) NOT NULL,
    keys json NOT NULL
);
ALTER TABLE public.notification_subscriptions OWNER TO postgres;
--
-- Name: notification_subscriptions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.notification_subscriptions_id_seq AS integer START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
ALTER TABLE public.notification_subscriptions_id_seq OWNER TO postgres;
--
-- Name: notification_subscriptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.notification_subscriptions_id_seq OWNED BY public.notification_subscriptions.id;
--
-- Name: players; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.players (
    id integer NOT NULL,
    fname character varying(64) NOT NULL,
    lname character varying(64) NOT NULL,
    club_id integer,
    description character varying(2048),
    user_id integer,
    training_strength double precision,
    primary_position integer,
    secondary_position integer,
    status character varying(32) NOT NULL
);
ALTER TABLE public.players OWNER TO postgres;
--
-- Name: players_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.players_id_seq AS integer START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
ALTER TABLE public.players_id_seq OWNER TO postgres;
--
-- Name: players_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.players_id_seq OWNED BY public.players.id;
--
-- Name: players_to_trainings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.players_to_trainings (player_id integer, training_id integer);
ALTER TABLE public.players_to_trainings OWNER TO postgres;
--
-- Name: positions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.positions (
    id integer NOT NULL,
    name character varying(24) NOT NULL
);
ALTER TABLE public.positions OWNER TO postgres;
--
-- Name: positions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.positions_id_seq AS integer START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
ALTER TABLE public.positions_id_seq OWNER TO postgres;
--
-- Name: positions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.positions_id_seq OWNED BY public.positions.id;
--
-- Name: strenght_values; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.strenght_values (
    id integer NOT NULL,
    value double precision NOT NULL,
    player_id integer,
    date timestamp without time zone
);
ALTER TABLE public.strenght_values OWNER TO postgres;
--
-- Name: strenght_values_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.strenght_values_id_seq AS integer START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
ALTER TABLE public.strenght_values_id_seq OWNER TO postgres;
--
-- Name: strenght_values_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.strenght_values_id_seq OWNED BY public.strenght_values.id;
--
-- Name: training_teams; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.training_teams (
    id integer NOT NULL,
    color character varying(20) NOT NULL,
    _player_ids character varying(60) NOT NULL,
    training_id integer
);
ALTER TABLE public.training_teams OWNER TO postgres;
--
-- Name: training_teams_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.training_teams_id_seq AS integer START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
ALTER TABLE public.training_teams_id_seq OWNER TO postgres;
--
-- Name: training_teams_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.training_teams_id_seq OWNED BY public.training_teams.id;
--
-- Name: trainings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.trainings (
    id integer NOT NULL,
    date timestamp without time zone,
    club_id integer,
    scorers json
);
ALTER TABLE public.trainings OWNER TO postgres;
--
-- Name: trainings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.trainings_id_seq AS integer START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
ALTER TABLE public.trainings_id_seq OWNER TO postgres;
--
-- Name: trainings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.trainings_id_seq OWNED BY public.trainings.id;
--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer NOT NULL,
    username character varying(64) NOT NULL,
    password_hash character varying(128),
    player_id integer,
    strava_oauth_id integer
);
ALTER TABLE public.users OWNER TO postgres;
ALTER TABLE ONLY public.users
ADD CONSTRAINT users_strava_oauth_id_fkey FOREIGN KEY (strava_oauth_id) REFERENCES public.strava_oauth_data(id);
--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq AS integer START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
ALTER TABLE public.users_id_seq OWNER TO postgres;
--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
--
-- Name: casbin_rule id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.casbin_rule
ALTER COLUMN id
SET DEFAULT nextval('public.casbin_rule_id_seq'::regclass);
--
-- Name: clip_tags id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clip_tags
ALTER COLUMN id
SET DEFAULT nextval('public.clip_tags_id_seq'::regclass);
--
-- Name: clips id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clips
ALTER COLUMN id
SET DEFAULT nextval('public.clips_id_seq'::regclass);
--
-- Name: clubs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clubs
ALTER COLUMN id
SET DEFAULT nextval('public.clubs_id_seq'::regclass);
--
-- Name: hockey_events id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hockey_events
ALTER COLUMN id
SET DEFAULT nextval('public.hockey_events_id_seq'::regclass);
--
-- Name: images id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.images
ALTER COLUMN id
SET DEFAULT nextval('public.images_id_seq'::regclass);
--
-- Name: monthly_pay_obligations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.monthly_pay_obligations
ALTER COLUMN id
SET DEFAULT nextval(
        'public.monthly_pay_obligations_id_seq'::regclass
    );
--
-- Name: notification_subscriptions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification_subscriptions
ALTER COLUMN id
SET DEFAULT nextval(
        'public.notification_subscriptions_id_seq'::regclass
    );
--
-- Name: players id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.players
ALTER COLUMN id
SET DEFAULT nextval('public.players_id_seq'::regclass);
--
-- Name: positions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.positions
ALTER COLUMN id
SET DEFAULT nextval('public.positions_id_seq'::regclass);
--
-- Name: strenght_values id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.strenght_values
ALTER COLUMN id
SET DEFAULT nextval('public.strenght_values_id_seq'::regclass);
--
-- Name: training_teams id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.training_teams
ALTER COLUMN id
SET DEFAULT nextval('public.training_teams_id_seq'::regclass);
--
-- Name: trainings id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trainings
ALTER COLUMN id
SET DEFAULT nextval('public.trainings_id_seq'::regclass);
--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
ALTER COLUMN id
SET DEFAULT nextval('public.users_id_seq'::regclass);
--
-- Name: alembic_version alembic_version_pkc; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.alembic_version
ADD CONSTRAINT alembic_version_pkc PRIMARY KEY (version_num);
--
-- Name: casbin_rule casbin_rule_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.casbin_rule
ADD CONSTRAINT casbin_rule_pkey PRIMARY KEY (id);
--
-- Name: clip_tags clip_tags_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clip_tags
ADD CONSTRAINT clip_tags_name_key UNIQUE (name);
--
-- Name: clip_tags clip_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clip_tags
ADD CONSTRAINT clip_tags_pkey PRIMARY KEY (id);
--
-- Name: clips clips_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clips
ADD CONSTRAINT clips_pkey PRIMARY KEY (id);
--
-- Name: clubs clubs_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clubs
ADD CONSTRAINT clubs_name_key UNIQUE (name);
--
-- Name: clubs clubs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clubs
ADD CONSTRAINT clubs_pkey PRIMARY KEY (id);
--
-- Name: hockey_events hockey_events_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hockey_events
ADD CONSTRAINT hockey_events_pkey PRIMARY KEY (id);
--
-- Name: images images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.images
ADD CONSTRAINT images_pkey PRIMARY KEY (id);
--
-- Name: monthly_pay_obligations monthly_pay_obligations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.monthly_pay_obligations
ADD CONSTRAINT monthly_pay_obligations_pkey PRIMARY KEY (id);
--
-- Name: notification_subscriptions notification_subscriptions_endpoint_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification_subscriptions
ADD CONSTRAINT notification_subscriptions_endpoint_key UNIQUE (endpoint);
--
-- Name: notification_subscriptions notification_subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification_subscriptions
ADD CONSTRAINT notification_subscriptions_pkey PRIMARY KEY (id);
--
-- Name: players players_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.players
ADD CONSTRAINT players_pkey PRIMARY KEY (id);
--
-- Name: positions positions_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.positions
ADD CONSTRAINT positions_name_key UNIQUE (name);
--
-- Name: positions positions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.positions
ADD CONSTRAINT positions_pkey PRIMARY KEY (id);
--
-- Name: strenght_values strenght_values_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.strenght_values
ADD CONSTRAINT strenght_values_pkey PRIMARY KEY (id);
--
-- Name: training_teams training_teams_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.training_teams
ADD CONSTRAINT training_teams_pkey PRIMARY KEY (id);
--
-- Name: trainings trainings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trainings
ADD CONSTRAINT trainings_pkey PRIMARY KEY (id);
--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
ADD CONSTRAINT users_pkey PRIMARY KEY (id);
--
-- Name: users users_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
ADD CONSTRAINT users_username_key UNIQUE (username);
--
-- Name: clips_to_tags clips_to_tags_clip_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clips_to_tags
ADD CONSTRAINT clips_to_tags_clip_id_fkey FOREIGN KEY (clip_id) REFERENCES public.clips(id);
--
-- Name: clips_to_tags clips_to_tags_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clips_to_tags
ADD CONSTRAINT clips_to_tags_tag_id_fkey FOREIGN KEY (tag_id) REFERENCES public.clip_tags(id);
--
-- Name: monthly_pay_obligations monthly_pay_obligations_player_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.monthly_pay_obligations
ADD CONSTRAINT monthly_pay_obligations_player_id_fkey FOREIGN KEY (player_id) REFERENCES public.players(id);
--
-- Name: players players_club_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.players
ADD CONSTRAINT players_club_id_fkey FOREIGN KEY (club_id) REFERENCES public.clubs(id);
--
-- Name: players players_primary_position_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.players
ADD CONSTRAINT players_primary_position_fkey FOREIGN KEY (primary_position) REFERENCES public.positions(id);
--
-- Name: players players_secondary_position_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.players
ADD CONSTRAINT players_secondary_position_fkey FOREIGN KEY (secondary_position) REFERENCES public.positions(id);
--
-- Name: players_to_trainings players_to_trainings_player_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.players_to_trainings
ADD CONSTRAINT players_to_trainings_player_id_fkey FOREIGN KEY (player_id) REFERENCES public.players(id);
--
-- Name: players_to_trainings players_to_trainings_training_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.players_to_trainings
ADD CONSTRAINT players_to_trainings_training_id_fkey FOREIGN KEY (training_id) REFERENCES public.trainings(id);
--
-- Name: players players_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.players
ADD CONSTRAINT players_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);
--
-- Name: strenght_values strenght_values_player_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.strenght_values
ADD CONSTRAINT strenght_values_player_id_fkey FOREIGN KEY (player_id) REFERENCES public.players(id);
--
-- Name: training_teams training_teams_training_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.training_teams
ADD CONSTRAINT training_teams_training_id_fkey FOREIGN KEY (training_id) REFERENCES public.trainings(id);
--
-- Name: trainings trainings_club_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trainings
ADD CONSTRAINT trainings_club_id_fkey FOREIGN KEY (club_id) REFERENCES public.clubs(id);
--
-- Name: users users_player_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
ADD CONSTRAINT users_player_id_fkey FOREIGN KEY (player_id) REFERENCES public.players(id);
CREATE TABLE public.strava_oauth_data (
    id serial4 NOT NULL,
    strava_id int4 NOT NULL,
    firstname varchar(24) NOT NULL,
    lastname varchar(24) NOT NULL,
    access_token varchar(64) NOT NULL,
    refresh_token varchar(64) NOT NULL,
    expires_at int4 NOT NULL,
    CONSTRAINT strava_oauth_data_pkey PRIMARY KEY (id),
    CONSTRAINT strava_oauth_data_strava_id_key UNIQUE (strava_id)
);
CREATE TABLE public.challenge_periods (
    id serial4 NOT NULL,
    first_week int4 NOT NULL,
    last_week int4 NOT NULL,
    starting_year int4 NOT NULL,
    ending_year int4 NOT NULL,
    CONSTRAINT challenge_periods_pkey PRIMARY KEY (id)
);
CREATE TABLE public.weekly_training_stats (
    id serial4 NOT NULL,
    strava_id int4 NOT NULL,
    points int4 NOT NULL,
    week_number int4 NOT NULL,
    distance_swam float8 NOT NULL,
    year int4 NOT NULL,
    CONSTRAINT weekly_training_stats_pkey PRIMARY KEY (id)
);
ALTER TABLE weekly_training_stats
ADD CONSTRAINT unique_weekly_stats UNIQUE (strava_id, week_number, year);
-- public.strava_activities definition
CREATE TABLE public.strava_activities (
    id serial4 NOT NULL,
    strava_id bigint NOT NULL,
    sport_type varchar(128) NOT NULL,
    moving_time int4 NOT NULL,
    strava_upload_id bigint NOT NULL,
    start_date timestamp NOT NULL,
    "name" varchar(128) NOT NULL,
    description text NULL,
    strava_athlete_id int4 NOT NULL,
    last_fetched_at timestamp NOT NULL,
    excluded_from_challenges varchar(128) NULL,
    distance float8 NOT NULL,
    CONSTRAINT strava_activities_pkey PRIMARY KEY (id),
    CONSTRAINT strava_activities_strava_id_key UNIQUE (strava_id),
    CONSTRAINT strava_activities_strava_upload_id_key UNIQUE (strava_upload_id)
);
ALTER TABLE public.strava_activities
ADD CONSTRAINT strava_activities_strava_athlete_id_fkey FOREIGN KEY (strava_athlete_id) REFERENCES public.strava_oauth_data(strava_id);
--
-- PostgreSQL database dump complete
