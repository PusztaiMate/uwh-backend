package main

import (
	"slices"
	"time"
)

// jelenlegi streak
// elmúlt X hónap edzési %-a
// elmúlt X hónap erősség változás

func calculateTrainingStreakForPlayers(trainings []*ExtendedTrainingData, playerIds []int) (map[int]*StreakData, error) {
	result := make(map[int]*StreakData)
	currentYear, _, _ := time.Now().Date()

	for i, t := range trainings {
		// if any of the present players are not yet in the result, add them
		for _, p := range t.Players {
			if !slices.Contains(playerIds, p) {
				continue
			}
			if _, ok := result[p]; !ok {
				result[p] = &StreakData{
					PlayerId: p,
				}
			}
		}

		for playerId, streakData := range result {
			// if player is not in the current training, then their
			// streak should end
			if !slices.Contains(t.Players, playerId) {
				streakData.CurrentStreak = 0
				continue
			}

			// if player is in the current training, then their streak should continue
			// or start if their streak is 0
			if streakData.CurrentStreak == 0 {
				streakData.CurrentStreak = 1
				streakData.CurrentStreakStartDate = t.Date
				continue
			}

			streakData.CurrentStreak++
			if streakData.LongestStreak < streakData.CurrentStreak {
				streakData.LongestStreak = streakData.CurrentStreak
				streakData.LongestStreakStartDate = streakData.CurrentStreakStartDate
				if i > 0 {
					streakData.LongestStreakEndDate = trainings[i-1].Date
				}
			}

			if streakData.CurrentYearLongestStreak < streakData.CurrentStreak && streakData.CurrentStreakStartDate.Year() == currentYear {
				streakData.CurrentYearLongestStreak = streakData.CurrentStreak
				streakData.CurrentYearLongestStreakStartDate = streakData.CurrentStreakStartDate
				if i > 0 {
					streakData.CurrentYearLongestStreakEndDate = trainings[i-1].Date
				}
			}

		}
	}

	return result, nil
}
