package main

import (
	"context"
	"log/slog"
	"net"
	"os"
	"strings"
	"time"

	"github.com/go-openapi/strfmt"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/PusztaiMate/uwh-backend/db"
	"gitlab.com/PusztaiMate/uwh-backend/strava-client/strava_client"
)

var (
	ACTIVITY_SUMMARIES_CACHING_TIME = 15 * time.Minute
	DEFAULT_FETCH_INTERVAL          = 30 * time.Minute
)

func getRBACModelConfigPath() string {
	defaultPath := "../project/rbac_model.conf"
	if strings.ToLower(os.Getenv("ENV")) == "production" {
		return "/app/project/rbac_model.conf"
	}
	return defaultPath
}

// TODO: generalize encode / decode (see https://grafana.com/blog/2024/02/09/how-i-write-http-services-in-go-after-13-years/#handle-decodingencoding-in-one-place)
type GenericMessage struct {
	Message string `json:"message"`
}

type ServerConfig struct {
	host string
	port string
}

type Server struct {
	e                          *echo.Echo
	config                     ServerConfig
	hockeyEventsService        *HockeyEventsService
	stravaAuthService          *StravaAuthService
	userService                *UserService
	stravaPointsCalculator     *StravaTrainingPointsCalculator
	challengeService           *ChallengeService
	weeklyTrainingStatsService *WeeklyTrainingStatsService
	stravaActivityService      *StravaActivityService
	activityFetcher            *StravaActivityFetcher
	playerService              *PlayerService
	clubService                *ClubService
	traininService             *TrainingService
	casbin                     *CasbinClient
}

func NewServer(
	config ServerConfig,
	hockeyEventsService *HockeyEventsService,
	stravaAuthService *StravaAuthService,
	userService *UserService,
	stravaPointsCalculator *StravaTrainingPointsCalculator,
	challengeService *ChallengeService,
	weeklyTrainingStatsService *WeeklyTrainingStatsService,
	stravaActivityService *StravaActivityService,
	activityFetcher *StravaActivityFetcher,
	playerService *PlayerService,
	clubService *ClubService,
	traininService *TrainingService,
	casbinClient *CasbinClient,
) *Server {
	e := echo.New()

	server := &Server{
		e:                          e,
		config:                     config,
		hockeyEventsService:        hockeyEventsService,
		stravaAuthService:          stravaAuthService,
		userService:                userService,
		stravaPointsCalculator:     stravaPointsCalculator,
		challengeService:           challengeService,
		weeklyTrainingStatsService: weeklyTrainingStatsService,
		stravaActivityService:      stravaActivityService,
		activityFetcher:            activityFetcher,
		playerService:              playerService,
		clubService:                clubService,
		traininService:             traininService,
		casbin:                     casbinClient,
	}

	server.routes()
	server.middlewares()

	return server
}

func (s *Server) middlewares() {
	userProfile := NewUserProfileMiddleware(getSecretKey(), s.userService)

	authorizer := NewAuthorizerMiddleware(s.casbin)

	s.e.Use(middleware.Logger())
	s.e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"http://localhost:3000"},
		AllowMethods: []string{echo.GET, echo.POST, echo.OPTIONS, echo.PUT, echo.DELETE},
	}))
	s.e.Use(userProfile.Process)
	s.e.Use(authorizer.Process)
}

func (s *Server) routes() {
	apiV2RouteGroup := s.e.Group("/api/v2")

	apiV2RouteGroup.GET("/hockey-events", s.handleListHockeyEvents)
	apiV2RouteGroup.POST("/hockey-events", s.handleCreateHockeyEvent)
	apiV2RouteGroup.GET("/hockey-events/:id", s.handleGetHockeyEvent)
	apiV2RouteGroup.DELETE("/hockey-events/:id", s.handleDeleteHockeyEvent)
	apiV2RouteGroup.PUT("/hockey-events/:id", s.handleUpdateHockeyEvent)

	apiV2RouteGroup.GET("/strava-auth-callback", s.handleStravaAuthCallback)
	apiV2RouteGroup.GET("/users/whoami", s.handleWhoAmI)

	apiV2RouteGroup.GET("/challenge/weekly-report", s.handleGetWeeklyReport)
	apiV2RouteGroup.GET("/challenge/list-activities", s.handleGetListActivities)
	apiV2RouteGroup.POST("/challenge/mark-activity-as-excluded", s.handleMarkStravaActivityAsExcludedFromChallenge)
	apiV2RouteGroup.POST("/challenge/mark-activity-as-included", s.handleMarkStravaActivityAsIncludedInChallenge)

	apiV2RouteGroup.GET("/challenge-points", s.handleGetChallengePoints)
	apiV2RouteGroup.POST("/challenge-periods", s.handleCreateChallengePeriod)
	apiV2RouteGroup.GET("/challenge-periods", s.handleListChallengePeriods)

	apiV2RouteGroup.POST("/activities/force-refetch", s.handleForceRefetchActivity)

	apiV2RouteGroup.GET("/users", s.handleGetAllUsers)
	apiV2RouteGroup.PUT("/users/:id", s.handleUpdateUser)
	apiV2RouteGroup.GET("/users/:id", s.handleGetExtendedUserData)
	apiV2RouteGroup.POST("/users/add-role", s.handleAddRoleToUser)
	apiV2RouteGroup.GET("/users/roles", s.handleGetRoles)

	apiV2RouteGroup.GET("/clubs", s.handleGetClubs)

	apiV2RouteGroup.GET("/trainings", s.handleGetTrainings)

	apiV2RouteGroup.GET("/players/training-streak", s.GetPlayerStreakData)
}

func (s *Server) Start() error {
	return s.e.Start(net.JoinHostPort(s.config.host, s.config.port))
}

func newDatabaseConnection(ctx context.Context, databaseUrl string) (*pgxpool.Pool, error) {
	conn, err := pgxpool.New(ctx, databaseUrl)
	if err != nil {
		return nil, err
	}

	return conn, nil
}

func (s *Server) refreshReportsInDb(ctx context.Context, weekNumber int, reports []WeeklyActivityReports) error {
	for _, report := range reports {
		_, err := s.weeklyTrainingStatsService.UpsertWeeklyTrainingStats(
			ctx,
			UpdateWeeklyTrainingsStatsWhere{
				Year:       time.Now().Year(),
				WeekNumber: weekNumber,
				StravaID:   report.AtheleteStravaId,
			},
			report.Points,
			report.Report.DistanceSwam,
		)
		if err != nil {
			slog.Error("could not create weekly training stats", "err", err)
			return err
		}
	}

	return nil
}

func run(ctx context.Context, getenv func(string) string) error {
	databaseUrl := getenv("DATABASE_URL")
	conn, err := newDatabaseConnection(ctx, databaseUrl)
	if err != nil {
		return err
	}
	defer conn.Close()

	dbQueries := db.New(conn)

	hockeyEventsService := NewHockeyEventsService(dbQueries)
	stravaAuthService := NewStravaAuthService(dbQueries)
	userService := NewUserService(dbQueries)
	stravaPointsCalculator := NewStravaTrainingPointsCalculator()
	challengerService := NewChallengeService(dbQueries)
	weeklyTrainingStatsService := NewWeeklyTrainingStatsService(dbQueries)
	stravaActivityService := NewStravaActivityService(dbQueries)
	playerService := NewPlayerService(dbQueries)
	clubService := NewClubService(dbQueries)
	trainingService := NewTrainingService(dbQueries)
	dbUrl := os.Getenv("DATABASE_URL")
	slog.Info("getting database url from env:", "dbUrl", dbUrl)
	casbin, err := NewCasbinClient(dbUrl, getRBACModelConfigPath())
	if err != nil {
		panic(err)
	}

	stravaClient := strava_client.NewHTTPClient(strfmt.Default)
	activityCache := newActivityCache(stravaClient, ACTIVITY_SUMMARIES_CACHING_TIME)
	activityFetcher := NewStravaActivityFetcherService(
		stravaActivityService,
		stravaAuthService,
		stravaClient,
		activityCache,
	)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// start activity fetcher in the background
	// use the default if it is production env, otherwise
	// don't really sync as it uses the production strava
	// keys
	var fetchInterval time.Duration
	env := os.Getenv("ENV")
	if strings.ToLower(env) == "production" {
		fetchInterval = DEFAULT_FETCH_INTERVAL
	} else {
		fetchInterval = 999 * time.Hour
	}

	go func() {
		for {
			select {
			case <-time.After(fetchInterval):
				_, currentWeek := time.Now().ISOWeek()
				_, twoWeeksAgo := time.Now().Add(2 * -7 * 24 * time.Hour).ISOWeek()
				_, weekEnd := getNthWeekOfTheYear(currentWeek)
				lastWeekStart, _ := getNthWeekOfTheYear(twoWeeksAgo)
				err = activityFetcher.fetchActivitiesForTimePeriodAndSaveIntoDbIfNotPresent(ctx, lastWeekStart, weekEnd)
				if err != nil {
					slog.Error("error fetching and saving new activities", "err", err)
				}
			case <-ctx.Done():
				slog.Info("context cancelled, stopping activity fetching")
				return
			}
		}
	}()

	serverConfig := ServerConfig{
		host: getenv("HOST"),
		port: getenv("PORT"),
	}
	server := NewServer(
		serverConfig,
		hockeyEventsService,
		stravaAuthService,
		userService,
		stravaPointsCalculator,
		challengerService,
		weeklyTrainingStatsService,
		stravaActivityService,
		activityFetcher,
		playerService,
		clubService,
		trainingService,
		casbin,
	)
	err = server.Start()
	if err != nil {
		return err
	}

	return nil
}

func main() {
	ctx := context.Background()
	err := run(ctx, func(s string) string {
		switch s {
		case "DATABASE_URL":
			return os.Getenv("DATABASE_URL")
		case "HOST":
			return "0.0.0.0"
		case "PORT":
			return "5001"
		}
		return ""
	})
	if err != nil {
		slog.Error("Error runninng server", "error", err)
	}
}
