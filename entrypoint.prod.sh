#!/usr/bin/env sh

CADDY_CONFIG=/app/Caddyfile.prod

echo "Upgrading db..."
python manage.py db upgrade

echo "Extracting port"
PORT=$(printenv | grep PORT | cut -d"=" -f2)
echo "PORT is $PORT"

echo "Starting flask server"
gunicorn --bind 0.0.0.0:5000 manage:app --daemon
echo "Flask server started"
echo "Starting v2 go echo server"
/binaries/server_v2 &
echo "go echo v2 server started"

caddy run --config $CADDY_CONFIG --adapter caddyfile
