from datetime import datetime

from flask.cli import FlaskGroup

from project import create_app, db
from project.api.db_utils import add_multiple_strength_updates
from project.repositories.user_repository import UserDB
from project.tests.api.db_utils import (
    add_club_if_not_present,
    add_goal_scorer_to_db,
    add_new_position,
    add_player_if_not_present,
    add_training,
    add_training_with_only_players_field,
    add_user_if_not_present,
)

app = create_app()
cli = FlaskGroup(create_app=create_app)


@cli.command("recreate_db")
def recreate_db():
    db.drop_all()
    db.create_all()


@cli.command("seed_db")
def seed_db():
    # create admin user
    UserDB(db).create_user("Admin", "admin", "admin@admin.admin")

    goalie = add_new_position("leghátsó")
    center = add_new_position("középső")
    wing = add_new_position("szélső")
    forward = add_new_position("első")

    p1 = add_player_if_not_present("Jakab Gipsz", 6, center, goalie)
    p2 = add_player_if_not_present("Júlia Gipsz", 9, goalie, center)
    p3 = add_player_if_not_present("János Gipsz", 9, center, goalie)
    p4 = add_player_if_not_present("József Gipsz", 7, goalie, wing)
    p5 = add_player_if_not_present("Janka Gipsz", 8, wing, goalie)
    p6 = add_player_if_not_present("Jenő Gipsz", 7, wing, center)
    p7 = add_player_if_not_present("Jakab Kovács", 10, forward, wing)
    p8 = add_player_if_not_present("Júlia Kovács", 4, forward, wing)
    p9 = add_player_if_not_present("János Kovács", 6, forward, wing)
    p10 = add_player_if_not_present("Janka Kovács", 8, wing, forward)
    p11 = add_player_if_not_present("Jenő Kovács", 8, wing, forward)
    p12 = add_player_if_not_present("József Kovács", 7, forward, wing)
    p13 = add_player_if_not_present("Jakab Szabó", 6, goalie, forward)
    p14 = add_player_if_not_present("Júlia Szabó", 4, center, wing)
    p15 = add_player_if_not_present("János Szabó", 6, wing, forward)
    p16 = add_player_if_not_present("Janka Szabó", 8, wing, center)
    p17 = add_player_if_not_present("Jenő Szabó", 3, forward, goalie)
    p18 = add_player_if_not_present("József Szabó", 7, forward, wing)
    p19 = add_player_if_not_present("Jakab Nagy", 6, wing, forward)
    p20 = add_player_if_not_present("József Nagy", 4, forward, wing)

    c1 = add_club_if_not_present(
        "Egyszusz VSE",
        players=[
            p1,
            p2,
            p3,
            p7,
            p8,
            p9,
            p10,
            p11,
            p12,
            p13,
            p14,
            p15,
            p16,
            p17,
            p18,
            p19,
            p20,
        ],
    )
    c2 = add_club_if_not_present("Piranha VSE", players=[p6, p4, p5])

    # number 1 represents the old trainings, where we had no data about color or scores
    _ = add_training_with_only_players_field(
        datetime(2020, 1, 1, 19, 30),
        [p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12],
    )

    # number 2 has no scoring data
    _ = add_training(
        black_team=[p1, p2, p3, p16, p9, p10, p11],
        white_team=[p14, p5, p17, p8, p12],
        club_id=c1.id,
        date_ = datetime(2020, 1, 3, 19, 30),
    )
    t3 = add_training(
        black_team=[p1, p2, p12, p11, p10],
        white_team=[p13, p14, p18, p9, p7],
        club_id=c1.id,
        date_ = datetime(2020, 2, 8, 19, 30),
    )
    add_goal_scorer_to_db(t3.id, {p1.id: 2})
    add_goal_scorer_to_db(t3.id, {p13.id: 4})
    add_goal_scorer_to_db(t3.id, {p12.id: 3})
    add_goal_scorer_to_db(t3.id, {p9.id: 1})
    add_goal_scorer_to_db(t3.id, {p7.id: 1})
    add_goal_scorer_to_db(t3.id, {p14.id: 3})

    t4 = add_training(
        black_team=[p1, p2, p6, p7, p8],
        white_team=[p3, p4, p5, p11, p12, p20],
        club_id=c2.id,
        date_ = datetime(2020, 3, 10, 19, 30),
    )
    add_goal_scorer_to_db(t4.id, {p3.id: 1})
    add_goal_scorer_to_db(t4.id, {p4.id: 1})
    add_goal_scorer_to_db(t4.id, {p5.id: 2})
    add_goal_scorer_to_db(t4.id, {p2.id: 1})
    add_goal_scorer_to_db(t4.id, {p1.id: 1})
    add_goal_scorer_to_db(t4.id, {p8.id: 3})

    t5 = add_training(
        black_team=[p1, p2, p10, p11, p8],
        white_team=[p3, p14, p15, p16, p19],
        club_id=c2.id,
        date_ = datetime(2020, 4, 10, 19, 30),
    )
    add_goal_scorer_to_db(t5.id, {p3.id: 1})
    add_goal_scorer_to_db(t5.id, {p15.id: 3})
    add_goal_scorer_to_db(t5.id, {p19.id: 2})
    add_goal_scorer_to_db(t5.id, {p2.id: 4})
    add_goal_scorer_to_db(t5.id, {p11.id: 1})
    add_goal_scorer_to_db(t5.id, {p8.id: 3})

    t6 = add_training(
        black_team=[p1, p2, p4, p7, p9, p11],
        white_team=[p3, p6, p8, p10, p12],
        club_id=c2.id,
        date_ = datetime(2020, 5, 10, 19, 30),
    )
    add_goal_scorer_to_db(t6.id, {p14.id: 1})
    add_goal_scorer_to_db(t6.id, {p15.id: 3})
    add_goal_scorer_to_db(t6.id, {p16.id: 2})
    add_goal_scorer_to_db(t6.id, {p4.id: 4})
    add_goal_scorer_to_db(t6.id, {p7.id: 1})
    add_goal_scorer_to_db(t6.id, {p9.id: 3})

    t7 = add_training(
        black_team=[p1, p3, p5, p7, p9, p11],
        white_team=[p2, p4, p6, p8, p10, p12],
        club_id=c2.id,
        date_ = datetime(2020, 6, 15, 19, 30),
    )
    add_goal_scorer_to_db(t7.id, {p1.id: 1})
    add_goal_scorer_to_db(t7.id, {p3.id: 3})
    add_goal_scorer_to_db(t7.id, {p5.id: 2})
    add_goal_scorer_to_db(t7.id, {p8.id: 4})
    add_goal_scorer_to_db(t7.id, {p6.id: 1})
    add_goal_scorer_to_db(t7.id, {p10.id: 3})

    t8 = add_training(
        black_team=[p1, p13, p15, p17, p9, p11, p20],
        white_team=[p2, p14, p16, p18, p10, p12, p19],
        club_id=c2.id,
        date_ = datetime(2020, 7, 15, 19, 30),
    )
    add_goal_scorer_to_db(t8.id, {p13.id: 1})
    add_goal_scorer_to_db(t8.id, {p15.id: 1})
    add_goal_scorer_to_db(t8.id, {p17.id: 2})
    add_goal_scorer_to_db(t8.id, {p18.id: 4})
    add_goal_scorer_to_db(t8.id, {p12.id: 1})
    add_goal_scorer_to_db(t8.id, {p19.id: 1})

    t9 = add_training(
        black_team=[p1, p3, p5, p7, p9, p11, p19],
        white_team=[p2, p4, p6, p8, p10, p12, p20],
        club_id=c2.id,
        date_ = datetime(2020, 8, 15, 19, 30),
    )
    add_goal_scorer_to_db(t9.id, {p9.id: 1})
    add_goal_scorer_to_db(t9.id, {p11.id: 1})
    add_goal_scorer_to_db(t9.id, {p7.id: 2})
    add_goal_scorer_to_db(t9.id, {p12.id: 4})
    add_goal_scorer_to_db(t9.id, {p2.id: 1})
    add_goal_scorer_to_db(t9.id, {p10.id: 1})

    add_multiple_strength_updates(t3.date, {p1.id: 4, p2.id: 7, p12.id: 6, p14.id: 5})
    add_multiple_strength_updates(
        t6.date, {p1.id: 5.2, p2.id: 5.9, p3.id: 6.1, p6.id: 5.7}
    )
    add_multiple_strength_updates(t5.date, {p10.id: 5, p11.id: 6, p16.id: 6, p19.id: 5})
    add_multiple_strength_updates(
        t7.date, {p1.id: 5.4, p2.id: 5.7, p3.id: 6.9, p4.id: 3.4}
    )
    add_multiple_strength_updates(t4.date, {p4.id: 3, p5.id: 9, p6.id: 6, p7.id: 5.5})

    add_user_if_not_present("TestUser", "1234")


if __name__ == "__main__":
    cli()
