export const API_URL =
  process.env["NODE_ENV"] === "production"
    ? "https://www.vizalattihoki.hu/api/v1"
    : "http://localhost:5000/api/v1";
export const NEW_API_URL =
  process.env["NODE_ENV"] === "production"
    ? "https://www.vizalattihoki.hu/api/v2"
    : "http://localhost:5001/api/v2";
