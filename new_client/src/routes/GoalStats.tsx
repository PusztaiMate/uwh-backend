import React from "react";
import { FC } from "react";
import { GoalScoringStatisticsTable } from "../components/GoalScoringStatisticsTable";

export const GoalStats: FC<{}> = () => {

	return (
		<GoalScoringStatisticsTable />
	)
}