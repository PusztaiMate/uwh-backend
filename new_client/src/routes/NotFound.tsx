import React from "react";
import { FC } from "react";

export const NotFound: FC<{}> = () => {
	return (
		<h2>Az oldal nem található</h2>
	)
}