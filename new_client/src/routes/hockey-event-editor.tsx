import {
  Button,
  Container,
  Group,
  Loader,
  NativeSelect,
  Stepper,
  Table,
  Textarea,
  TextInput,
} from "@mantine/core";
import { DatePicker } from "@mantine/dates";
import { useForm } from "@mantine/form";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router";
import { useHockeyEvents } from "../hooks/useHockeyEvent";
import { formatDate } from "../util/naming";

export const HockeyEventEditor = () => {
  const { useUpdateHockeyEvent, useHockeyEvent } = useHockeyEvents();
  const { eventId } = useParams<{ eventId: string }>();
  const { isLoading, error, data } = useHockeyEvent(eventId!);
  const { mutateAsync } = useUpdateHockeyEvent(eventId!);

  const [active, setActive] = useState(0);
  const navigate = useNavigate();

  const form = useForm({
    initialValues: {
      type: data?.data.type || "",
      description: data?.data.description || "",
      title: data?.data.title || "",
      date: data?.data.date
        ? new Date(new Date(data.data.date).setHours(12))
        : new Date(new Date().setHours(12)),
    },

    validate: (values) => {
      if (active === 1) {
        return {
          title: !values.title ? "A cím megadása kötelező" : null,
        };
      }

      return {};
    },
  });

  useEffect(() => {
    if (data) {
      form.setValues({
        type: data.data.type || "",
        description: data.data.description || "",
        title: data.data.title || "",
        date: data.data.date
          ? new Date(new Date(data.data.date).setHours(12))
          : new Date(new Date().setHours(12)),
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  const nextStep = () =>
    setActive((current) => {
      if (form.validate().hasErrors) {
        return current;
      }

      if (current === 1) {
        form.setValues({
          date: new Date(new Date(form.values.date).setHours(12)),
        });
      }

      return current < 2 ? current + 1 : current;
    });

  const prevStep = () =>
    setActive((current) => (current > 0 ? current - 1 : current));

  const handleUpdateHockeyEvent = async () => {
    await mutateAsync({
      date: form.values.date,
      description: form.values.description,
      title: form.values.title,
      type: form.values.type,
    });

    navigate(`/events`);
  };

  if (isLoading) {
    return <Loader />;
  }
  if (error) {
    console.log(error);
    return <div>{`Nem sikerült betölteni az oldalt`}</div>;
  }

  return (
    <>
      <Container>
        <Stepper
          active={active}
        //breakpoint="sm"
        >
          <Stepper.Step label="Cím" description="Személyi adatok">
            <TextInput label="Cím" {...form.getInputProps("title")} />
            <Textarea
              label="Leírás"
              placeholder=""
              {...form.getInputProps("description")}
            />
            <NativeSelect
              data={["Szociális", "Verseny", "Edzés"]}
              label="Típus"
              description="Válassz egy típust"
              {...form.getInputProps("type")}
            />
          </Stepper.Step>

          <Stepper.Step label="Időpont" description="Az esemény időpontja">
            <DatePicker
              // label="Dátum"
              // description="Az esemény időpontja"
              locale="hu"
              title="Válassz egy dátumot"
              {...form.getInputProps("date")}
            />
          </Stepper.Step>

          <Stepper.Completed>
            <Container>
              <Table striped withRowBorders withTableBorder withColumnBorders>
                <tbody>
                  <tr>
                    <th>Cím</th>
                    <td>{form.values.title}</td>
                  </tr>
                  <tr>
                    <th>Típus</th>
                    <td> {form.values.type} </td>
                  </tr>
                  <tr>
                    <th>Leírás</th>
                    <td> {form.values.description} </td>
                  </tr>
                  <tr>
                    <th>Dátum</th>
                    <td> {formatDate(form.values.date)} </td>
                  </tr>
                </tbody>
              </Table>
            </Container>
          </Stepper.Completed>
        </Stepper>

        <Group justify="right" mt="xl">
          {active !== 0 && (
            <Button variant="default" onClick={prevStep}>
              Vissza
            </Button>
          )}
          {active !== 2 && <Button onClick={nextStep}>Következő</Button>}
          {active === 2 && (
            <Button onClick={() => handleUpdateHockeyEvent()}>Mentés</Button>
          )}
        </Group>
      </Container>
    </>
  );
};
