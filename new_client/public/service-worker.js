self.addEventListener('push', function(event) {
    var options = {
        body: event.data.text(),
        icon: 'logo.png', // Path to your icon
        badge: 'logo.png' // Path to your badge
    };

    event.waitUntil(
        self.registration.showNotification('Új hokis értesítő!', options)
    );
});
