"""Add strava auth data

Revision ID: 7e1d4692257c
Revises: 95db9723793c
Create Date: 2024-07-17 10:32:30.351116

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "7e1d4692257c"
down_revision = "95db9723793c"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "strava_oauth_data",
        sa.Column("id", sa.Integer(), autoincrement=True, nullable=False),
        sa.Column("strava_id", sa.Integer(), nullable=False),
        sa.Column("firstname", sa.String(length=24), nullable=False),
        sa.Column("lastname", sa.String(length=24), nullable=False),
        sa.Column("access_token", sa.String(length=64), nullable=False),
        sa.Column("refresh_token", sa.String(length=64), nullable=False),
        sa.Column("expires_at", sa.Integer(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("strava_id"),
    )

    op.add_column(
        "users",
        sa.Column("strava_oauth_id", sa.Integer(), sa.ForeignKey("strava_oauth_data.id")),
    )

    op.drop_column("users", "email")


def downgrade():
    op.add_column("users", sa.Column("email", sa.String(length=128), nullable=False, unique=True))
    op.drop_column("users", "strava_oauth_id")
    op.drop_table("strava_oauth_data")
