"""Add distance covered to saved activity data

Revision ID: b14074f926f5
Revises: 3774cf6cbcd4
Create Date: 2024-09-02 11:56:09.717728

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'b14074f926f5'
down_revision = '3774cf6cbcd4'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('strava_activities', sa.Column('distance', sa.Float(), nullable=True))
    op.execute("UPDATE strava_activities SET distance = 0 WHERE distance IS NULL")
    op.alter_column('strava_activities', 'distance',
               existing_type=sa.Float(),
               nullable=False)

    op.add_column('weekly_training_stats', sa.Column('distance_swam', sa.Float(), nullable=True))
    op.execute("UPDATE weekly_training_stats SET distance_swam = 0 WHERE distance_swam IS NULL")
    op.alter_column('weekly_training_stats', 'distance_swam',
               existing_type=sa.Float(),
               nullable=False)


def downgrade():
    with op.batch_alter_table("strava_activities", schema=None) as batch_op:
        batch_op.drop_column("distance")
    with op.batch_alter_table("weekly_training_stats", schema=None) as batch_op:
        batch_op.drop_column("distance_swam")