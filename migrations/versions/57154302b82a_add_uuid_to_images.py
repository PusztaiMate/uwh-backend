"""Add uuid to Images

Revision ID: 57154302b82a
Revises: d406bb343f24
Create Date: 2023-02-06 15:34:57.366178

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID


# revision identifiers, used by Alembic.
revision = "57154302b82a"
down_revision = "d406bb343f24"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("images", sa.Column("uuid", UUID, nullable=True))
    op.add_column(
        "images", sa.Column("orig_extension", sa.String(length=10), nullable=True)
    )
    op.drop_column("images", "source")


def downgrade():
    op.drop_column("images", "uuid")
    op.drop_column("images", "orig_extension")
    op.add_column("images", sa.Column("source", sa.String(length=256), nullable=True))
