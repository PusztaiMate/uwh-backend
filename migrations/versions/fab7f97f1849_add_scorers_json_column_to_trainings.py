"""Add scorers JSON column to trainings

Revision ID: fab7f97f1849
Revises: 57154302b82a
Create Date: 2023-08-29 22:42:59.559694

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "fab7f97f1849"
down_revision = "57154302b82a"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("trainings", schema=None) as batch_op:
        batch_op.add_column(sa.Column("scorers", sa.JSON(), nullable=True))


def downgrade():
    with op.batch_alter_table("trainings", schema=None) as batch_op:
        batch_op.drop_column("scorers")
