"""Add notification subscription table

Revision ID: 6358a589fcc5
Revises: 991f98b2ed13
Create Date: 2023-11-26 13:52:53.180343

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "6358a589fcc5"
down_revision = "991f98b2ed13"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "notification_subscriptions",
        sa.Column("id", sa.Integer(), autoincrement=True, nullable=False),
        sa.Column("endpoint", sa.String(length=2048), nullable=False),
        sa.Column("keys", sa.JSON(), nullable=False),
        sa.PrimaryKeyConstraint("id", name="notification_subscriptions_pkey"),
        sa.UniqueConstraint("endpoint", name="notification_subscriptions_endpoint_key"),
    )


def downgrade():
    op.drop_table("notification_subscriptions")
