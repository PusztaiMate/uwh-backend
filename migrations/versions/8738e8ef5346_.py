"""Add users table, remove email from players.

Revision ID: 8738e8ef5346
Revises: d0ceb6b0f983
Create Date: 2020-02-21 14:22:29.759738

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "8738e8ef5346"
down_revision = "d0ceb6b0f983"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "users",
        sa.Column("id", sa.Integer(), autoincrement=True, nullable=False),
        sa.Column("email", sa.String(length=128), nullable=False),
        sa.Column("username", sa.String(length=64), nullable=False),
        sa.Column("password_hash", sa.String(length=128), nullable=True),
        sa.Column("player_id", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(
            ["player_id"],
            ["players.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("email"),
        sa.UniqueConstraint("username"),
    )
    op.add_column("players", sa.Column("user_id", sa.Integer(), nullable=True))
    op.drop_constraint("players_email_key", "players", type_="unique")
    op.create_foreign_key(None, "players", "users", ["user_id"], ["id"])
    op.drop_column("players", "email")


def downgrade():
    op.add_column(
        "players",
        sa.Column("email", sa.VARCHAR(length=128), autoincrement=False, nullable=False),
    )
    op.drop_constraint(None, "players", type_="foreignkey") # type: ignore
    op.create_unique_constraint("players_email_key", "players", ["email"])
    op.drop_column("players", "user_id")
    op.drop_table("users")
