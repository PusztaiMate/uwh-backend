"""Add hockey events

Revision ID: 95db9723793c
Revises: 6358a589fcc5
Create Date: 2023-12-01 11:59:17.329741

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "95db9723793c"
down_revision = "6358a589fcc5"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "hockey_events",
        sa.Column("id", sa.Integer(), autoincrement=True, nullable=False),
        sa.Column("date", sa.DateTime(), nullable=False),
        sa.Column("title", sa.String(), nullable=False),
        sa.Column("type", sa.String(), nullable=False),
        sa.Column("description", sa.Text()),
        sa.PrimaryKeyConstraint("id", name="hockey_events_pkey"),
    )


def downgrade():
    op.drop_table("hockey_events")
