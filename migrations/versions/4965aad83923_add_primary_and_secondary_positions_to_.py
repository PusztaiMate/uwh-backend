"""Add primary and secondary positions to players

Revision ID: 4965aad83923
Revises: 3acd949b2743
Create Date: 2021-03-03 12:09:24.265909

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "4965aad83923"
down_revision = "3acd949b2743"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column("players", sa.Column("primary_position", sa.Integer(), nullable=True))
    op.add_column(
        "players", sa.Column("secondary_position", sa.Integer(), nullable=True)
    )
    op.create_foreign_key(None, "players", "positions", ["primary_position"], ["id"])
    op.create_foreign_key(None, "players", "positions", ["secondary_position"], ["id"])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, "players", type_="foreignkey") # type: ignore
    op.drop_constraint(None, "players", type_="foreignkey") # type: ignore
    op.drop_column("players", "secondary_position")
    op.drop_column("players", "primary_position")
    # ### end Alembic commands ###
