"""Drop goal scorers as it is integrated into trainings table

Revision ID: 991f98b2ed13
Revises: fab7f97f1849
Create Date: 2023-08-30 16:41:40.534128

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "991f98b2ed13"
down_revision = "fab7f97f1849"
branch_labels = None
depends_on = None


def upgrade():
    op.drop_table("goal_scorers")


def downgrade():
    op.create_table(
        "goal_scorers",
        sa.Column("id", sa.INTEGER(), autoincrement=True, nullable=False),
        sa.Column("player_id", sa.INTEGER(), autoincrement=False, nullable=True),
        sa.Column("training_id", sa.INTEGER(), autoincrement=False, nullable=True),
        sa.Column("scored", sa.INTEGER(), autoincrement=False, nullable=True),
        sa.ForeignKeyConstraint(
            ["player_id"], ["players.id"], name="goal_scorers_player_id_fkey"
        ),
        sa.ForeignKeyConstraint(
            ["training_id"], ["trainings.id"], name="goal_scorers_training_id_fkey"
        ),
        sa.PrimaryKeyConstraint("id", name="goal_scorers_pkey"),
    )
