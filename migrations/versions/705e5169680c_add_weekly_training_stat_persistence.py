"""Create moving challenge related tables: weekly_training_stats and challenge_periods

Revision ID: 705e5169680c
Revises: 7e1d4692257c
Create Date: 2024-07-30 16:47:57.848345

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '705e5169680c'
down_revision = '7e1d4692257c'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "challenge_periods",
        sa.Column("id", sa.Integer(), autoincrement=True, nullable=False),
        sa.Column("first_week", sa.Integer(), nullable=False),
        sa.Column("last_week", sa.Integer(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )

    op.create_table(
        "weekly_training_stats",
        sa.Column("id", sa.Integer(), autoincrement=True, nullable=False),
        sa.Column("strava_id", sa.Integer(), nullable=False),
        sa.Column("points", sa.Integer(), nullable=False),
        sa.Column("week_number", sa.Integer(), nullable=False),
        sa.Column("year", sa.Integer(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_unique_constraint("unique_weekly_stats", "weekly_training_stats", ["strava_id", "week_number", "year"])

def downgrade():
    op.drop_constraint("unique_weekly_stats", "weekly_training_stats", type_="unique")
    op.drop_table("weekly_training_stats")
    op.drop_table("challenge_periods")