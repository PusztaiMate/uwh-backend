FROM node:19 as build-react
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY ./new_client/package*.json ./
RUN npm install
COPY ./new_client .
ENV NODE_OPTIONS=--openssl-legacy-provider
RUN npm run build

FROM golang:1.20-alpine as build-go
WORKDIR /app
ADD go_utils/calculate_teams.go .
RUN go build -o calculate_teams calculate_teams.go

FROM nginx:stable-alpine as production
WORKDIR /app

RUN apk update && apk add --no-cache python3 py3-virtualenv

# set up python3 virtualenv
RUN python3 -m venv /app/venv
ENV PATH="/app/venv/bin:$PATH"

RUN pip install --upgrade pip setuptools
RUN apk add --no-cache postgresql-dev gcc python3-dev py3-wheel py3-gunicorn musl-dev netcat-openbsd g++

# copy already built react and go files
COPY --from=build-react /app/build /usr/share/nginx/html
RUN mkdir /binaries
COPY --from=build-go /app/calculate_teams /binaries/calculate_teams

# nginx config
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf

COPY ./requirements.txt .
RUN pip install -r requirements.txt

COPY ./entrypoint.sh .
RUN chmod +x ./entrypoint.sh

COPY ./manage.py .
COPY ./migrations ./migrations
COPY ./project ./project

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

ENTRYPOINT [ "/app/entrypoint.sh" ]
