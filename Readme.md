# Mindenféle

**Disclaimer**: ERŐSEN OUTDATED

## Projektről

Az Egyszuszos edzésre járási statisztikákat hivatott egyelőre kezelni, de a jövőben remélem, hogy lesz idő/energia bőviteni:

- Bemutató felület, ahova újonnan érkezők is odatévedhetnek (leirással a hokiról, felszerelésről, edzésekről, stb.)
- Tudásbázis a felállásokkal, edzéstervekkel, stb.
- Fórum esetleg, pl. videóelemzések archiválásához, versenyek lebeszéléséhez
- Tagdij állapota (automatikus köremail, akár személyesen is mindenkinek)
- Versenynaptár
- Hirfal, a facebook feedünkhöz hasonlóan az eredményekkel, stb.

## Mi kell ahhoz, hogy __te__ is hozzá tudj rakni a projekthez?

### Ha nem szeretnél programozni / html-t szerkeszteni

- ötlet (mit csináljon, hogyan nézzen ki, stb.). Ha nagyon szorgalmas vagy, akkor kérlek írj [ide](https://gitlab.com/PusztaiMate/uwh-backend/issues)
- bármi grafika

### Egyébként

**Ami kelleni fog biztosan**

- [**git**](https://git-scm.com/download/win)

  _Ezt a programot fogjuk használni a fájlok "letöltésére" és szerkesztés utáni "visszatöltésére". Ha bármit véletletlenül elírsz / nem emlékszel már, hogy mit szerkesztettél, akkor abban is tud segíteni, hogy gyorsan visszaállítsa a fájlokat a legutóbbi működő állapotra._

  1. Menj a https://git-scm.com/download/win oldalra és töltsd le a 64-bit verziót!
  2. Nyisd meg a telepített fájlt és kezd el telepíteni! Mindig jó lesz a "Tovább"/"Next".
  3. Ellenőrzés képpen nyomd le a Windows gombot és kezd el írni, hogy "Git Bash"! Ha látható egy új program, akkor minden sikerült.

- [**python**](https://www.python.org/downloads/).

  _Ezen a programnyelven írodott a weblap logikájának a nagy része. Ha szeretnél hozzáadni valamilyen statisztikát például (a fehér csapat hányszor nyert a páratlan hónapokban pl.) akkor ezen a nyelven kell leírnod a gondolataidat. Szerencsére arra tervezték, hogy egyszerű legyen!_

  1. Nyisd meg a linket és válaszd ki a 3.8.\*-ot a felsorolt lehetőségek közül (a legújabba, ami 3.8.-tal kezdődik). A megnyíló oldal alján találhatóak az telepítőfájlok, innen válaszd ki a "Windows x86-64 executable installer"-t!
  2. Nyisd meg a fájlt és telepítsd fel a programot. **Fontos: az első fülön pipáld be, hogy "Add Python 3.8 to PATH"!**.
  3. Válaszd ki, hogy **Customize Installation**!. Telepítési útvonalnak olyat adj meg, amiben **csak** angol karakterek szerepelnek (pl. a C:/Users/Máté mappája nem jó, mert később az _á_ és az _é_ betű gondot fog okozni)!
  4. Ezután mehet az "Install", majd az utolsó fülön nyomj rá a **"Disable PATH length limit"** gombra, amit felajánl! Utána mehet a "Close".
  5. Ha minden jól ment, akkor most ha lenyomod a Windows gombot és elkezded gépelni, hogy "Python", akkor több új ikonnak is meg kell jelennie. Az elsőt elindítva felugrik egy _shell_ és három kacsacsőr után megjelenik a kurzor (>>>). Ezt bezárhatod, ha megnyitottad, egyelőre (és talán soha) nem fog kelleni.

- [**docker**](https://hub.docker.com/editions/community/docker-ce-desktop-windows)

  _Ez a program egy mini számítógépet hoz létre a saját számítógépeden belül. Azért kell, hogy (1) mindenki ugyanazon a rendszeren tudja kipróbálni a változtatásait és (2), hogy könnyen lehessen ezt a rendszert reprodukálni._

  1. Töltsd le az linkelt fájt!
  2. Nyisd meg és kezdd el a telepítést! Nem kell sehol sem belenyúlni, mindenhol jó lesz az előre beállított.
  3. Meg fogja kérni, hogy indítsd újra a gépet.
  4. Ha minden jól ment, akkor megjelent egy "Docker Desktop" ikon az asztalodon. Indítsd el, majd várj, míg felugrik egy ablak, hogy minden "Running". Tesztként nyiss meg egy Git Bash-t és írd be következőt (1)-et. Ha nem jön vissza hibával, akkor minden sikerült!

  ```bash
  (1) docker run hello-world
  ```

- [**VSCode**](https://code.visualstudio.com/)

  _Ez egy szövegszerkesztő program, ami a python, html és egyébb programnyelveken írt kód szerkesztéséhez lehet használni._

  1. Menj fel a megadott holnapra, töltsd le és telepítsd fel a programot. Minden mehet saját igényeid szerint, nincs jelentősége a beállításoknak.

**Első lépések Windoshoz**

Ellenőrizd, hogy a fenti listán lévő progranok telepítve vannak!

**_git beállítása_**

1. Nyomd le a windows gombot (vagy nyisd meg a start menüt) és gépeld be, hogy "git bash"! A felugró programot inditsd el!
2. Add ki a következő parancsokat behelyettesitve a neved a kacsacsőrök közé (< >), entert nyomva utánuk! Az idézőjelek is fontosak (tehát "Gipsz Jakab" és nem Gipsz Jakab)
   ```bash
   git config --global user.name "<neved>"
   git config --global user.email "<email cimed>"
   ```
3. Nyisd meg a fájlkezelődet (Mappa?) és menj be abba a mappába, ahol a projektet szeretnéd tartani (pl. Dokumentumok/Vizihoki/Weboldal, de bármi jó)! Ide lesznek "letöltve" a fájlok és itt lehet majd szerkeszteni őket.
4. shift+jobb klikket megnyomva válaszd ki az "Open git bash here" vagy "Git bash megnyitásat itt" menüpontot.
5. A felugró terminálban ird be a következő parancsot:

```bash
git clone https://gitlab.com/PusztaiMate/uwh-backend.git
git remote add origin https://gitlab.com/PusztaiMate/uwh-backend.git
```

6. Telepitsd a szükséges python csomagokat a következő parancsokkal:

```bash
pip install pipenv
pipenv install
```

**_Visual Studio Code beállítása, extensionök telepítése_**

1. Nyisd meg a Visual Studio Code programot!
2. Menj rá az "Extensions" (CTRL+SHIFT+X) fülre és telepitsd a következő extensionöket (ird be őket a keresőbe):
   1. python
   2. Docker
   3. Jinja2 Snippet Kit
   4. Visual Studio IntelliCode
3. A VSCode programban nyisd meg a projekt mappáját (CTRL+K, CTRL+O a billentyűparancs)!
4. Zárd be és nyisd meg újra!
5. Nyiss meg a _manage.py_ nevű fájlt (CTRL+P-t lenyomva elkezdheted begépelni a nevét)! A bal alsó sarkában a képernyőnek a "master" felirat mellett meg kellett jelennie a következő két dolog egyikének: (1) _Select Python Interpreter_, vagy (2) _Python 3.8.1 64-bit ('uwh-backend': pipenv)_. Amennyiben (2), már kész is vagy! Ha (1), akkor kattints a feliratra és a felül felugró menüből válaszd ki azt, amelyiknek benne van a nevében, hogy "uwh-backend"! Ha nincs a felsorolásban ilyen elem, akkor szólj nekem!

**Ha minden jól sikerült, akkor ezen a ponton kész is vagy a rendszer előkészítésével!**
Egy gyors próba, hogy minden ok:

1. Nyomd le az **F1** gombot!
2. Kezdd el gépelni, hogy "Tasks: Run Task", és válaszd ki az ilyen nevű element _Enter_-rel.
3. Válaszd kai a "_buil images & start up containers_" taskot. Ennek a futása eltarthat egy darabig (akár 1-2 perc is első alkalommal, később csak másodpercek). Akkor van kész ha látod a következő feliratot:

```bash
Creating uwh-backend_players-db_1 ... done
Creating uwh-backend_players_1    ... done
```

4. Kattints [ide](http://localhost:5002/)! Ha megjelent a honlap, akkor minden a legnagyobb rendben! Ha nem, akkor jelezz nekem!

## **Fejlesztési Workflow-k (folyt. köv.)**

### **Új model hozzáadása, jelenlegi bővítése**

**Bővítés**

A Training model jelenleg nem tudja tárolni, hogy az adott játékos milyen színben játszott.

### **Jelenlegi lap változtatása**

Folyt. köv.

### **Új oldal hozzáadása**

Folyt. köv.

## API

- /api/v1/players/id-name-pairs/
- /api/v1/players/pairings/
- /api/v1/users/login/
- /api/v1/test/testuser/
- /api/v1/goal-scorers/
- /api/v1/trainings/
- /api/v1/players/
- /api/v1/images/
- /api/v1/test/
- /api/v1/goal-scorers/training/<int:training_id>/
- /api/v1/payments/obligations/<int:year>/<int:month>/
- /api/v1/images/tags/<string:tags>/
- /api/v1/trainings/<training_id>/
- /api/v1/players/<player_id>/
- /api/v1/images/<int:img_id>/
- /static/<path:filename>
