#!/bin/bash

# Change to the root of the Git repository
cd "$(git rev-parse --show-toplevel)" || exit

# Use git ls-files to get all tracked files, excluding .gitignored ones
git ls-files new_client | while IFS= read -r file; do
    # Skip PNG and JPG files
    if [[ "${file,,}" != *.png && "${file,,}" != *.lock && "${file,,}" != *.jpg && "${file,,}" != *.jpeg && "${file,,}" != *.json ]]; then
        # Print the filename
        echo "File: $file"
        echo "----------------------------------------"
        
        # Print the file content
        cat "$file"
        
        # Print a separator
        echo -e "\n========================================\n"
    fi
done
