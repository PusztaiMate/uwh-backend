package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"os"
)

func main() {
	args := os.Args

	if len(args) != 3 {
		fmt.Printf("Number of arguments should be two: '%s <name-of-the-input-json> <name-of-the-output-json>'", args[0])
	}

	inputFilename := args[1]
	outputFilename := args[2]

	team := ReadFile(inputFilename)

	centers := team.Centers
	goalies := team.Goalies
	wings := team.Wings
	forwards := team.Forwards

	teams := make(chan Teams, 200)

	go func() {
		for cis := range GenCombinations(len(centers), len(centers)/2) {
			for gis := range GenCombinations(len(goalies), len(goalies)/2) {
				for wis := range GenCombinations(len(wings), len(wings)/2) {
					for fis := range GenCombinations(len(forwards), len(forwards)/2) {
						cA, cB := getPlayersAndComplementer(centers, cis)
						gA, gB := getPlayersAndComplementer(goalies, gis)
						wA, wB := getPlayersAndComplementer(wings, wis)
						fA, fB := getPlayersAndComplementer(forwards, fis)

						t1 := makeTeam(gA, cA, wA, fA)
						t2 := makeTeam(gB, cB, wB, fB)

						ts := Teams{t1, t2}

						teams <- ts
					}
				}
			}
		}
		close(teams)
	}()

	bests := getThreeBestBalanced(teams)

	b, err := json.Marshal(bests)
	if err != nil {
		log.Fatalf("Could not marshal %#v\n", bests)
	}

	checkError(err)

	checkError(ioutil.WriteFile(outputFilename, b, 0644))
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

func absInt(a int) int {
	if a < 0 {
		return -a
	}
	return a
}

func getThreeBestBalanced(tch chan Teams) [3]Teams {
	filled := 0
	var bests [3]Teams
	for team := range tch {
		if filled < 3 {
			bests[filled] = team
			filled++
			continue
		}
		if diff := absInt(team.A.size() - team.B.size()); diff > 1 {
			tmp := team.A.Wings
			team.A.Wings = team.B.Wings
			team.B.Wings = tmp
		}

		if isSameAsOneOfTheTeams(team, bests) {
			continue
		}

		diff := math.Abs(team.A.averageStrenght() - team.B.averageStrenght())
		idx, biggestDiff := getBiggestStrengthDiff(bests)

		if diff < biggestDiff {
			bests[idx] = team
		}
	}
	return bests
}

func getBiggestStrengthDiff(teams [3]Teams) (int, float64) {
	biggest := -1.0
	idxOfBiggest := -1

	for i, team := range teams {
		if diff := math.Abs(team.A.averageStrenght() - team.B.averageStrenght()); diff > biggest {
			biggest = diff
			idxOfBiggest = i
		}
	}

	return idxOfBiggest, biggest
}

//List is a list of elements of any kind/type
type List []interface{}

//GenCombinations generates, from two natural numbers n > r,
//all the possible combinations of r indexes taken from 0 to n-1.
//For example if n=3 and r=2, the result will be:
//[0,1], [0,2] and [1,2]
func GenCombinations(n, r int) <-chan []int {

	if r > n {
		panic("Invalid arguments")
	}

	ch := make(chan []int)

	go func() {
		result := make([]int, r)
		for i := range result {
			result[i] = i
		}

		temp := make([]int, r)
		copy(temp, result) // avoid overwriting of result
		ch <- temp

		for {
			for i := r - 1; i >= 0; i-- {
				if result[i] < i+n-r {
					result[i]++
					for j := 1; j < r-i; j++ {
						result[i+j] = result[i] + j
					}
					temp := make([]int, r)
					copy(temp, result) // avoid overwriting of result
					ch <- temp
					break
				}
			}
			if result[0] >= n-r {
				break
			}
		}
		close(ch)

	}()
	return ch
}

//CombinationsInt generates all the combinations of r elements
//extracted from an slice of integers
func CombinationsInt(iterable []int, r int) chan []int {

	ch := make(chan []int)

	go func() {

		length := len(iterable)

		for comb := range GenCombinations(length, r) {
			result := make([]int, r)
			for i, val := range comb {
				result[i] = iterable[val]
			}
			ch <- result
		}

		close(ch)
	}()
	return ch
}

//CombinationsStr generates all the combinations of r elements
//extracted from an slice of strings
func CombinationsStr(iterable []string, r int) chan []string {

	ch := make(chan []string)

	go func() {

		length := len(iterable)

		for comb := range GenCombinations(length, r) {
			result := make([]string, r)
			for i, val := range comb {
				result[i] = iterable[val]
			}
			ch <- result
		}

		close(ch)
	}()
	return ch
}

//CombinationsList generates all the combinations of r elements
//extracted from a List (an arbitrary list of elements).
//A List can be created for instance, as follows
//myList := List{"a", "b", 13, 3.523}
func CombinationsList(iterable List, r int) chan List {

	ch := make(chan List)

	go func() {

		length := len(iterable)

		for comb := range GenCombinations(length, r) {
			result := make(List, r)
			for i, val := range comb {
				result[i] = iterable[val]
			}
			ch <- result
		}

		close(ch)
	}()
	return ch
}

func ReadFile(filename string) *Team {
	fmt.Printf("opening file %s\n", filename)

	jsonFile, err := os.Open(filename)

	if err != nil {
		fmt.Println(err)
	}

	defer jsonFile.Close()

	byteVale, _ := ioutil.ReadAll(jsonFile)

	var team Team

	fmt.Println("unmarshaling file content")
	json.Unmarshal(byteVale, &team)

	fmt.Println("finished")

	return &team
}

// Player is the minimum representation that is needed to calculate teams
type Player struct {
	ID       int     `json:"id"`
	Strength float64 `json:"strength"`
}

// Team represents a group of Groups
type Team struct {
	Goalies  []Player `json:"goalies"`
	Centers  []Player `json:"centers"`
	Wings    []Player `json:"wings"`
	Forwards []Player `json:"forwards"`
}

// Teams holds 2 Team
type Teams struct {
	A Team `json:"team_a"`
	B Team `json:"team_b"`
}

func (team *Team) size() int {
	return len(team.Centers) + len(team.Goalies) + len(team.Wings) + len(team.Forwards)
}

func (team *Team) averageStrenght() float64 {
	return avgStrength(team.Centers) + avgStrength(team.Goalies) + avgStrength(team.Wings) + avgStrength(team.Forwards)
}

func avgStrength(players []Player) float64 {
	return sumStrength(players) / float64(len(players))
}

func sumStrength(players []Player) float64 {
	var sum float64
	for _, p := range players {
		sum += p.Strength
	}
	return sum
}

func getPlayersAndComplementer(from []Player, selected []int) ([]Player, []Player) {
	groupA := make([]Player, len(selected))
	groupB := make([]Player, len(from)-len(selected))

	// from is too small
	if len(from) == 0 {
		return groupA, groupB
	}

	var idxSelected, idxGroupA, idxGroupB int

	for i, player := range from {
		if idxSelected >= len(selected) || i != selected[idxSelected] {
			groupB[idxGroupB] = player
			idxGroupB++
		} else {
			groupA[idxGroupA] = player
			idxGroupA++

			idxSelected++
		}
	}

	return groupA, groupB
}

func isSameAsOneOfTheTeams(teams Teams, l [3]Teams) bool {
	for _, ts := range l {
		if areSameTeams(ts, teams) {
			return true
		}
	}
	return false
}

func areSameTeams(a, b Teams) bool {
	if (areSameTeam(a.A, b.A) && areSameTeam(a.B, b.B)) || (areSameTeam(a.A, b.B) && areSameTeam(a.B, b.A)) {
		return true
	}
	return false
}

func areSameTeam(a, b Team) bool {
	if areSamePlayers(a.Goalies, b.Goalies) && areSamePlayers(a.Centers, b.Centers) && areSamePlayers(a.Wings, b.Wings) && areSamePlayers(a.Forwards, b.Forwards) {
		return true
	}

	return false
}

func areSamePlayers(a, b []Player) bool {
	if len(a) != len(b) {
		return false
	}

	for _, p := range a {
		if !isPlayerInList(p, b) {
			return false
		}
	}

	return true
}

func isPlayerInList(player Player, l []Player) bool {
	for _, p := range l {
		if p.ID == player.ID {
			return true
		}
	}
	return false
}

func makeTeam(gs, cs, ws, fs []Player) Team {
	return Team{gs, cs, ws, fs}
}
